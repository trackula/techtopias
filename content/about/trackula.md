---
title: "Trackula"
date: 2020-05-17T12:00:00+01:00
draft: false

# image
image: "images/author.png"

# meta description
description: "Trackula es un grupo de personas que luchamos por tu derecho a la privacidad y trabajamos para concienciar acerca del recorrido de tu información y de para qué es utilizada."

# type
type : "about"
---

Queremos que conozcas el mundo digital en el que vivimos, el alcance de tus datos y lo que conlleva para tu libertad personal que seas rastreado. Defendemos tu derecho a la privacidad y trabajamos para concienciar acerca del recorrido de tu información y de para qué es utilizada.

Creemos que esto te interesa seas quien seas.

Nacimos en MediaLab Prado donde desarrollamos **[un plugin para visualizar cómo tus datos migran por la red](https://trackula.org/es/page/landing-plugin/)**  y qué empresas de seguimiento web, o trackers, se conectan a los contenidos que visitas cada día. Hemos participado en otros espacios como Hirikilabs en el centro Tabakalera donde experimentamos cómo explicar el funcionamiento de las cookies pero con objetos físicos y hasta hemos recibido un **[premio de la Agencia Española de Protección de Datos](https://www.aepd.es/es/prensa-y-comunicacion/notas-de-prensa/la-aepd-concede-los-premios-de-comunicacion-proteccion-de)** por nuestro trabajo.

Nuestro último proyecto es **[LaEscondite](http://laescondite.com/)** que busca reunir a toda la gente que trabaja en privacidad, derechos digitales y sobernía tecnológica para debatir y trabajar por mejorar nuestra sociedad desde el punto de vista tecnológico por lo que te animamos a que nos contactes y te unas a la lucha!

### [trackula.org](https://trackula.org/)