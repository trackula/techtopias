---
title: "El COVID-19 y los retos de privacidad "
date: 2020-05-26T12:00:00+01:00
draft: false

# post thumb
image: "images/post/post-1.jpg"

# meta description
description: "La llegada del covid-19 y los retos de privacidad"

# post type
type: "post"
---

Estas últimas semanas están siendo tremendamente movidas en todos los campos pero especialmente en el referente a la privacidad y a la soberanía tecnológica, ya que la llegada de la pandemia en la que estamos envueltos, abre un mar de posibilidades y también un número enorme de riesgos.

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[España lanzará en Canarias durante el mes de junio su app de rastreo de contactos en un programa piloto](https://elpais.com/tecnologia/2020-05-20/espana-lanzara-en-junio-en-canarias-el-piloto-para-la-app-de-rastreo-de-contactos-contagiados.html). Según diversas informaciones parece que seguirá el protocolo DP-3T descentralizado y aprovechará las APIs que Google/Apple han creado y ofrecen en sus teléfonos a raíz de la crisis sanitaria.

[Google abandona repentinamente su proyecto Sidewalk Labs en Toronto](https://www.theguardian.com/technology/2020/may/07/google-sidewalk-labs-toronto-smart-city-abandoned), el proyecto que buscaba construir frente al mar, la smart city del futuro, motivado por las consecuencias impredecibles que tendrá el coronavirus, que puede llevar a que el proyecto no sea viable financieramente.

Facebook compra Giphy por $400M y en Maldita Tecnología explican muy bien [lo que significa para tus datos que una multinacional tecnológica compre una plataforma que usamos todos](https://maldita.es/malditatecnologia/2020/05/20/datos-multinacional-tecnologica-facebook-compra-giphy/).

[El Corte Inglés tomará la temperatura a los clientes cuando reabra sus tiendas](https://www.elindependiente.com/economia/2020/05/04/el-corte-ingles-tomara-la-temperatura-a-los-clientes-cuando-reabra-sus-tiendas/), se une a otras empresas y múltiples paises que realizarán estas medidas en sus aeropuertos a pesar de que [los expertos indican que este método no funciona](https://www.nbcnews.com/tech/security/fever-detection-cameras-fight-coronavirus-experts-say-they-don-t-n1170791).

Otras empresas como [Vegalsa Eroski optan por soluciones para controlar el aforo en los supermercados](https://www.elespanol.com/quincemil/articulos/actualidad/eroski-estrena-en-a-coruna-un-sistema-para-controlar-el-aforo-en-sus-supermercados) con una serie de sensores en la entrada de sus establecimientos.

Bloomberg analiza el futuro del trabajo, que después del coronavirus parece que va a estar [lleno de nuevas y siniestras tecnologías](https://www.bloomberg.com/news/articles/2020-05-20/creepy-technologies-invade-european-post-pandemic-workplaces) que aumentarán el control sobre la sociedad.

[La Universidad Autónoma de Madrid se alía con los marines de EE UU para combatir los bulos sobre la covid-19](https://elpais.com/tecnologia/2020-05-15/la-universidad-autonoma-de-madrid-se-alia-con-los-marines-de-ee-uu-para-combatir-los-bulos-sobre-la-covid-19.html) a través de la *biometría conductual*, reconociendo los patrones de escritura de los usuarios para detectar si son bots, una propuesta llena de dudas con respecto a la privacidad de los usuarios.

[Facebook lanza un comité de expertos](https://blogs.elconfidencial.com/tecnologia/tribuna/2020-05-16/facebook-tribunal-supremo-juzgar-contenidos_2595031/) que decidirá qué contenidos se eliminarán, [¿quiénes son las 20 personas que integran este comité?](https://www.businessinsider.es/20-miembros-junta-supervision-ha-creado-facebook-635949)

[No es ‘Black Mirror’: los perros de Boston Dynamics ya patrullan las calles contra el coronavirus](https://hipertextual.com/2020/05/perros-robot-boston-dynamics-patrullando-parques-coronavirus/)




### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>
El gobierno de Estados Unidos aprueba una [revisión de la Patriot Act](https://www.adslzone.net/noticias/internet/fbi-espiar-historial-patriot-act/) que da (aún) más poder a sus agencias de seguridad para espiar y conocer el historial de navegación de sus ciudadanos sin necesidad de orden judicial.

Y Mozilla junto con otras empresas tecnológicas [han enviado una carta a la Cámara de Representantes de los Estados Unidos](https://blog.mozilla.org/blog/2020/05/22/protecting-search-and-browsing-data-from-warrantless-access/) para pedir una enmienda que limite el acceso a dicho historial de navegación sin orden judicial.

Altos funcionarios de la ONU tienen [prohibido usar WhatsApp desde 2019](https://wwwhatsnew.com/2020/01/25/altos-funcionarios-de-la-onu-tienen-prohibido-usar-whatsapp-desde-2019-por-motivos-de-seguridad/) por motivos de seguridad al mismo tiempo que la Comisión Europea le pide a sus trabajadores que [se pasen a Signal](https://www.adslzone.net/2020/02/22/ue-signal-whatsapp-seguridad/).

[Francia sigue adelante con el impuesto sobre los gigantes de internet](https://www.cityam.com/france-to-push-ahead-with-tax-on-internet-giants-despite-us-anger/) a pesar del enfado de Estados Unidos, y otros paises como España también [planean continuar con su implantación](https://www.abc.es/economia/abci-montero-defiende-tasa-google-porque-grandes-tecnologicas-tienen-mas-beneficios-esta-crisis-202004301531_noticia.html).

[Bruselas pondrá coto a la inteligencia artificial en sectores de “alto riesgo” para el interés público'](https://elpais.com/economia/2020/02/16/actualidad/1581881524_010755.html).

Munich vuelve a la senda del software libre, y [se compromete con la campaña "Public Money? Public Code!"](https://fsfe.org/news/2020/news-20200506-01.en.html), campaña lanzada por la Free Software Foundation Europe para pedir que el código del software pagado con dinero público se publique bajo licencias libres y que [puedes firmar aquí](https://publiccode.eu/) si aún no lo has hecho.

[Parlamentarios europeos de Alemania proponen que para motivar el uso de #apps de contact tracing, se premie a quien instale la app con menores restricciones al viajar o ir al restaurante](https://twitter.com/Jorge_Morell/status/1258693702974832642), algo que se parece bastante al ya conocido crédito social implantando en China.

[India obliga a utilizar apps de contact tracing para todos los trabajadores](https://www.reuters.com/article/us-health-coronavirus-india-app/india-orders-coronavirus-tracing-app-for-all-workers-idUSKBN22E07K) en un pais de 1.300 millones de habitantes de las cuales se calcula que solo un 40% de la población tiene smartphone.

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>
La Universitat de Barcelona lanza una nueva edición de su [Postgrado en Tecnopolítica y Derechos en la Era Digital](https://xnet-x.net/posgrado-tecnopolitica-simona-levi/).

Mozilla lanza su nuevo servicio, [Firefox Private Relay](https://relay.firefox.com/) para generar múltiples alias de correo que redigirá de forma automática todos nuestros correos a nuestro email principal, con el fin de ocultarlo a los servicios donde nos registremos.


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>
Unicef busca identificar retos y apoyar proyectos para la infancia, adolescencia y familias a través de su [nueva convocatoria](https://innovacion.unicef.es/).

Mozilla lanza su [programa de incubación](https://builders.mozilla.community/) buscando ideas y proyectos que quieran arreglar internet y que estará abierto a propuestas hasta el 5 de Junio en sus tres programas.

La iniciativa NGI (Next Generation Internet) de la Comisión Europea lanza una nueva open call [NGIatlantic](https://ngiatlantic.eu/1st-open-call) para proyectos que busquen hacer un internet más humano y cuya convocatoria termina el 29 de Mayo.

[Grant for the web](https://www.grantfortheweb.org/) es un fondo de $100M para impulsar proyectos innovadores, justos e inclusivos enfocados en los estándares y los modelos de monetización en la web. Se buscan modelos de negocio que puedan hacer frente a los modelos típicos relacionados con la publicidad invasiva, el tráfico de datos o la IA, entre otros. El plazo de convocatoria termina el 12 de Junio.

NGI (Next Generation Internet) tiene otra open call abierta [NGI Pointer](https://pointer.ngi.eu/) en la que poder optar hasta 200.000 € de financiación "equity-free" para personas y empresas que desarrollen protocolos y herramientas alineadas con los valores europeos. El plazo de presentación de solicitudes termina el 1 de Junio.


### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/> y entrevistas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

La Fundación Telefónica ha celebrado una mesa de debate con el título ["Apps, salud y protección de datos"](https://www.youtube.com/watch?v=0T_1HkFhmOc&list=PLMa9fq02Eqo-MzXST9_kqNJFTn0T4Hpza&index=8&t=0s) en su ciclo #RepensandoElMañana con tres expertas como Marta Peirano, Paloma Yaneza y Andrea G. Rodriguez.

[Tecnopolítica y pandemia](https://libre.video/videos/embed/f7d9a640-1267-48b8-8274-1b3c7a11b398), aLabs ha juntado a varios expertos para hablar sobre la intersección entre política y tecnología en plena pandemia de covid-19.

La geopolítica y la soberanía digital se ponen encima de la mesa de la Unión Europea, ["La revancha de la historia: ¿es Europa una colonia digital?"](https://blogs.elconfidencial.com/mundo/wiertz-60/2020-02-25/revancha-historia-europa-colonia-digital_2468531/), analizado por Esteban González Pons.

Gemma Galdón ha estado dentro del ciclo #DistopiasCotiás de CoruñaDixital hablando de [tecnología y privacidad en tiempos del Covid-19](https://www.youtube.com/watch?v=EOAari4e3jw).

Y por último, un análisis general sobre la situación actual del [capitalismo de la vigilancia en los tiempos del Covid-19](https://blog.iuvia.io/es/el-capitalismo-de-la-vigilancia-en-tiempos-de-covid-19/)


---


Este es nuestro primer artículo en Techtopias, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
