---
title: "ARM se va de Europa, los tokens de Movistar y los líos de Facebook."
date: 2020-09-29T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-10.jpg"

# meta description
description: "ARM se va de Europa, los tokens de Movistar y los líos de Facebook."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Oracle se hace con TikTok en EEUU tras las presiones de Donald Trump](https://www.elconfidencial.com/tecnologia/2020-09-14/oracle-estados-unidos-tiktok-presiones-donald-trump-app-tecnologia_2746520/), parece que finalmente Oracle será [el socio tecnológico de la aplicación china, convirtiéndose en su proveedor de cloud en Estados Unidos.](https://edition.cnn.com/2020/09/13/tech/microsoft-tiktok-bytedance/index.html)

[¿Por qué Oracle? El 'discípulo' de la CIA (y amigo de Trump) que ahora vigilará a TikTok.](https://www.elconfidencial.com/tecnologia/2020-09-14/oracle-tiktok-cia-nsa-vigilancia-red-social-china-trump_2746988/)

[Nvidia está comprando Arm por $40 billion](https://www.theverge.com/2020/9/13/21435507/nvidia-acquiring-arm-40-billion-chips-ai-deal), un trato que deja a Europa en una situación incómoda al perder a su última empresa con una posición dominante en microprocesadores, mientras [el cofundador de ARM lanza una campaña para evitar la compra.](https://hipertextual.com/2020/09/arm-nvidia-campana-herman-hauser)

[Se filtran por error más de 18.000 resultados de test de Coronavirus en Gales.](https://www.bbc.com/news/uk-wales-54146755)

[Casi 700.000 cuentas falsas inundaron Facebook durante el confinamiento para manipular a los españoles.](https://www.vozpopuli.com/economia-y-finanzas/cuentas-falsas-facebook-confinamiento-organizada_0_1392161065.html)

[Movistar lanza un proyecto piloto que "recompensa" a sus usuarios con descuentos en su plataforma por dejarles vender tus datos personales,](https://www.elladodelmal.com/2020/09/tokens-de-movistar-un-proyecto-piloto.html) parece que el blog "En el lado del mal" hace justicia a su nombre.

Surge una nueva polémica alrededor de Apple y el control que ejerce sobre su AppStore, [el fundador de WordPress pide a Apple que no corte las actualizaciones de su app completamente gratuita por que quiere un 30% de sus beneficios.](https://www.theverge.com/2020/8/21/21396316/apple-wordpress-in-app-purchase-tax-update-store)

[YouTube volverá a usar moderadores humanos porque su algoritmo no es confiable.](https://hipertextual.com/2020/09/youtube-moderadores-humanos-ia)

[Una empresa gallega lanza cámaras de reconocimiento facial para vigilar la temperatura en las aulas en plena pandemia,](https://www.lavozdegalicia.es/noticia/mercados/2020/09/20/vigilar-temperatura-aulas-plena-pandemia/0003_202009SM20P10991.htm) una propuesta de dudosa proporcionalidad que introduciría el reconocimiento facial en ámbitos en los que su necesidad es cuando menos evitable.

[Apple y Singapur recompensarán a los usuarios de Apple Watch por hacer ejercicio,](https://www.cnbc.com/2020/09/15/apple-partners-with-singapore-to-encourage-fitness-with-apple-watch.html) la empresa tecnológica entra en el negocio de los datos [a pesar de sus anuncios publicitarios en los que dice que "compartimos demasiada información".](https://www.youtube.com/watch?reload=9&v=XW6UHWfdfF8&feature=youtu.be)

[Amazon ha estado espiando secretamente a sus trabajadores a través de grupos privados de Facebook para preveer huelgas y protestas.](https://www.vice.com/en_us/article/3azegw/amazon-is-spying-on-its-workers-in-closed-facebook-groups-internal-reports-show)

[La tecnológica lanza Echo Show 10, una pantalla con webcam que puede seguirnos cuando nos movemos para mantenernos enfocados en la imagen](https://www.xataka.com/domotica-1/amazon-echo-show-10-2020-caracteristicas-precio-ficha-tecnica), y no es el único gadget que lanzará en breve. En su sección de seguridad [Ring, propone un dron como cámara de seguridad que vuela por tu casa.](https://www.theverge.com/2020/9/24/21453709/ring-always-home-cam-indoor-drone-security-camera-price-specs-features-amazon)

[SegurCaixa Adeslas activa su plan de contingencia por un ataque de ransomware](https://www.europapress.es/economia/finanzas-00340/noticia-segurcaixa-adeslas-activa-plan-contingencia-ataque-ransomware-20200910191611.html) que mantiene sus sistemas de salud secuestrados desde hace 2 semanas.

[Taxistas denuncian a Uber y Cabify ante Agencia Española Protección de Datos.](https://www.lavanguardia.com/vida/20200923/483638796317/taxistas-denuncian-a-uber-y-cabify-ante-agencia-espanola-proteccion-de-datos.html)

[La firma Clearview AI, conocida por haber creado una de las mayores bases de datos de imágenes sacadas de múltiples redes sociales y por utilizar reconocimiento facial sobre ellas, ha levantado 8.6 millones de dólares de inversores.](https://www.buzzfeednews.com/article/ryanmac/controversial-clearview-ai-raises-8-million)

[Agencias federales intervinieron teléfonos de manifestantes en Portland.](https://www.thenation.com/article/politics/homeland-security-portland/)

[El antiguo director de monetización de Facebook cuenta que la empresa, de forma intencionada, hizo su producto adictivo como el tabaco y ahora teme que pueda causar una guerra civil.](https://www.businessinsider.com/former-facebook-exec-addictive-as-cigarettes-tim-kendall-2020-9)

["Tengo sangre en mis manos": Un denunciante afirma que Facebook ignoró la manipulación política global que se mueve a través de sus redes aún conociéndola.](https://www.buzzfeednews.com/article/craigsilverman/facebook-ignore-political-manipulation-whistleblower-memo)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Facebook amenaza con dejar de dar servicio en la UE si se le prohíbe enviar datos de los usuarios a EEUU](https://www.eldiario.es/economia/facebook-amenaza-con-dejar-de-dar-servicio-en-la-ue-si-se-le-prohibe-enviar-datos-de-los-usuarios-a-eeuu_1_6234870.html), una noticia que surge tras la invalidación del Privacy Shield que permitía la transferencia de datos entre USA y la Unión Europea.

[Educación prohíbe a institutos y colegios madrileños el uso de plataformas educativas gratuitas](https://elpais.com/espana/madrid/2020-09-24/educacion-prohibe-a-institutos-y-colegios-madrilenos-el-uso-de-plataformas-educativas-gratuitas.html) por problemas de protección de datos. El gobierno de Madrid insta a sus centros a volver a EducaMadrid basado en aplicaciones de software libre.

[El gobierno quiere que WhatsApp, Telegram y otras apps de mensajería paguen la tasa de operadores](https://www.xataka.com/legislacion-y-derechos/espana-presenta-anteproyecto-ley-telecomunicaciones-gobierno-quiere-que-whatsapp-pague-tasa-operadores), además, aprovechando el Anteproyecto de la Ley de Telecomunicaciones, [propone intervenir WhatsApp y otras redes de mensajería como Telegram para difundir mensajes en situaciones de emergencia.](https://www.vozpopuli.com/economia-y-finanzas/gobierno-intervenir-whatsapp_0_1394260763.html)

[La Justicia Europea tumba las tarifas que "no consumen gigas" con ciertos servicios: así nos afecta la primera sentencia sobre la neutralidad de la red](https://www.xataka.com/legislacion-y-derechos/justicia-europea-tumba-tarifas-que-no-consumen-gigas-ciertos-servicios-asi-nos-afecta-primera-sentencia-neutralidad-red). En el siguiente [enlace](https://curia.europa.eu/jcms/jcms/Jo2_7052/) se puede consultar la resolución.

[Los Centros de Datos piden al Congreso una rebaja millonaria en la factura de la luz](https://www.vozpopuli.com/economia-y-finanzas/centros-datos-Congreso-rebaja-factura_0_1393061093.html), solicitan deducciones similares a otros sectores electrointensivos.

[La 'ley rider' disparará costes en Glovo y Deliveroo y pondrá en jaque su modelo.](https://www.lainformacion.com/empresas/ley-rider-disparara-costes-glovo-deliveroo-pondra-jaque-modelo/2807324/)

[Microsoft consigue aprobar una legislación en Washington que permite el uso de algoritmos de IA y reconocimiento facial siempre que los sistemas se sometan a "revisión humana significativa".](https://qz.com/1905159/microsoft-is-shaping-facial-recognition-bills-across-the-us/) En la práctica estas tres palabras abren totalmente la mano a utilizar esta tecnología y se están extendiendo a otras leyes similares en California, Maryland o Idaho.

[Facebook promete restringir la actividad de los usuarios si se desata el caos en las elecciones estadounidenses.](https://elpais.com/tecnologia/2020-09-22/facebook-promete-restringir-la-actividad-de-los-usuarios-si-se-desata-el-caos-en-las-elecciones-estadounidenses.html)

[Google y Facebook bajo presión para prohibir los anuncios y la extracción de datos a menores de 18 años](https://www.bbc.com/news/technology-54205229) ya que las leyes de protección de datos europeas prohiben dicha práctica en niños.

[Italia se prepara para introducir de forma masiva, sistemas de reconocimiento facial y tecnologías de vigilancia en todos los estadios de fútbol.](https://algorithmwatch.org/en/story/italy-stadium-face-recognition/)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[La iniciativa "Ban Facial Recognition" arranca en Europa de la mano de "La Quadrature du Net" entre otros.](https://ban-facial-recognition.wesign.it/droitshumains/prohibir-el-reconocimiento-facial-en-europa)

[MicroG, la alternativa libre a los Google Play Services ya soporta el sistema de "Exposure Notifications API" para las apps de contact tracing.](https://github.com/microg/android_packages_apps_GmsCore/releases/tag/v0.2.12.203315)

[Publicado un análisis técnico detallado sobre la app Radar COVID en las versiones 1.0-5 y 1.0.6-6.](https://git.rip/-/snippets/2)

[La startup estonia, Sentinel, cierra $1.35M para trabajar en la detección de deepfakes.](https://techcrunch.com/2020/09/14/sentinel-loads-up-with-1-35m-in-the-deepfake-detection-arms-race/)

[La cuna que ha desatado la polémica en las redes es en realidad una campaña de Multiópticas contra el abuso de pantallas](https://www.20minutos.es/noticia/4383405/0/la-cuna-que-ha-desatado-la-polemica-en-las-redes-es-en-realidad-una-campana-de-multiopticas-contra-el-abuso-de-pantallas/).

[Los principales desarrolladores de aplicaciones forman una coalición para promover la competencia y proteger la innovación en las plataformas digitales.](https://appfairness.org/app-developers-coalition-for-app-fairness-competition-innovation/)

Grandes eventos en el mundo de la soberanía tecnológica como [MozFest](https://foundation.mozilla.org/en/blog/2021-mozfest-new-era/) y en torno al software libre como [FOSDEM](https://fosdem.org/2021/news/2020-09-01-dates-fosdem-2021/) se pasan al mundo virtual.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

Vuelve nuevamente el programa [NGIatlantic.eu, esta vez con su segundo open call](https://ngiatlantic.eu/ngiatlanticeu-2nd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 30 de Septiembre.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

["Capitalismo de plataformas: tres libros y un documental"](https://elpais.com/cultura/2020/09/25/babelia/1601048485_287643.html), un artículo de Marta Peirano.

[El dilema de las redes](https://www.netflix.com/es/title/81254224), el documental de moda sobre cómo las redes sociales nos manipulan y afectan a muchos aspectos de nuestra sociedad.

[El virus que sacudió la tecnología](https://medium.economiadigital.es/numero-5/el-virus-que-sacudio-la-tecnologia/) por Gemma Galdón.

[Podcast: "COVID-19 is helping turn Brazil into a surveillance state".](https://www.technologyreview.com/2020/09/16/1008495/podcast-covid-19-brazil-surveillance-state/)

Maldita Tecnología nos explica la polémica de la última semana, ["Por qué se acusa a Twitter de tener un algoritmo "racista": lo qué sabemos sobre el experimento con fotos de caras de piel blanca y negra"](https://maldita.es/malditatecnologia/2020/09/21/twitter-algoritmo-racista-experimento-fotos-caras-blancas-negras/).

["Surveillance in an Era of Pandemic and Protest"](https://theintercept.com/2020/09/11/coronavirus-black-lives-matter-surveillance/), un debate entre Naomi Klein, Shoshana Zuboff, y Simone Browne sobre los pelígros del capitalismo de la vigilancia.

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
