---
title: "Facebook y las elecciones, los monopolios en USA y el cifrado en la UE."
date: 2020-10-13T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-11.jpg"

# meta description
description: "Facebook y las elecciones, los monopolios en USA y el cifrado en la UE."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Amazon implementa en sus tiendas de Seattle un sistema para pagar con la palma de tu mano](https://elpais.com/tecnologia/2020-09-29/amazon-quiere-ahora-que-pagues-con-la-palma-de-tu-mano-y-a-los-expertos-no-les-hace-mucha-gracia.html)

[Una empresa gallega crea un geolocalizador para los trabajadores con el fin de evitar cuarentenas masivas](https://retina.elpais.com/retina/2020/09/22/innovacion/1600776325_502044.html). Una tecnología que genera enormes dudas de privacidad ante el control que puede ejercer sobre los trabajadores.

[Palantir, la mayor maquinaria de espionaje de Silicon Valley, se estrena en bolsa afrontando su prueba de fuego.](https://www.elconfidencial.com/tecnologia/2020-09-30/palantir-silicon-valley-ipo-inteligencia-artificial-peter-thiel_2767711/)

[Aquí tenemos un análisis mas profundo sobre a qué se dedica la compañia y lo que sabe Palantir y la policía sobre nosotros gracias a sus tecnologías de vigilancia masiva.](https://www.buzzfeednews.com/article/carolinehaskins1/training-documents-palantir-lapd)

[Microsoft anuncia que tendrá derecho exclusivo para acceder a GPT-3](https://www.technologyreview.es/s/12654/el-peligro-de-que-solo-microsoft-pueda-acceder-gpt-3) a pesar de haber fundado OpenAI como una organización sin ánimo de lucro y para utilizar la IA en beneficio de la humanidad.

Los ciberataques cada día son mas habituales y peligrosos, y están empezando a afectar a infraestructuras críticas como el [ataque a un hospital alemán en plena pandemia](https://elpais.com/internacional/2020-10-03/ciberataque-a-un-hospital-aleman-en-tiempos-de-pandemia.html).

Relacionado con temas de salud ya hemos contando en el anterior Techtopias cómo [Adeslas mantenía sus sistemas paralizados por un ataque informático. Y ya van más de tres semanas con sus sistemas secuestrados.](https://www.elconfidencial.com/tecnologia/2020-10-03/adeslas-ransomware-hackers-ciberataque-datos_2773032/)

[Amnistía Internacional acusa a empresas europeas de vender tecnología de reconocimiento facial a China.](https://retina.elpais.com/retina/2020/09/30/tendencias/1601481091_037696.html)

[Google afirma que su inteligencia artificial está mejorando en el reconocimiento de desinformación.](https://venturebeat.com/2020/09/10/google-claims-its-ai-is-becoming-better-at-recognizing-breaking-news-and-misinformation/)

[Oracle contra Google es el juicio por ‘copyright’ más importante del siglo: en juego está el futuro del software.](https://www.xataka.com/legislacion-y-derechos/oracle-google-juicio-copyright-importante-siglo-juego-esta-futuro-software)

[Documentos judiciales muestran que Google está cediendo datos a la policía basados en palabras clave de búsqueda.](https://www.cnet.com/news/google-is-giving-data-to-police-based-on-search-keywords-court-docs-show/)

[Facebook ha publicado un PDF como respuesta al documental ‘The Social Dilemma’.](https://mobile.twitter.com/MattNavarra/status/1312078715610976257)

La red social continua siendo noticia, esta vez por las medidas que tomarán de cara a las próximas elecciones estadounidenses, [ya que estrenará a su ‘tribunal supremo’ justo antes de las elecciones](https://dircomfidencial.com/marketing-digital/facebook-estrenara-su-tribunal-supremo-justo-antes-de-las-elecciones-presidenciales-de-eeuu-20200926-0400/) y según han anunciado, [finalmente prohibirá el contenido de QAnon y lo marcará como un ‘movimiento social militarizado’.](https://www.theverge.com/2020/10/6/21504887/facebook-qanon-ban-all-apps-pages-groups-instagram-accounts)

[El propio Facebook dice que "Es imposible que lo de Cambridge Analytica nos vuelva a pasar"](https://www.eldiario.es/tecnologia/elecciones-vida-muerte-facebook-imposible-cambridge-analytica-vuelva-pasar_1_6253859.html) aunque otros medios explican que [algunas de las medidas tomadas, como la prohibición de la publicidad política, serán inútiles.](https://www.technologyreview.es/s/12616/la-inutil-medida-de-facebook-de-prohibir-la-publicidad-politica)

Mientras tanto, Microsoft asegura que [vuelve la ofensiva rusa para interferir en las elecciones estadounidenses.](https://elpais.com/internacional/2020-09-10/vuelve-la-ofensiva-rusa-para-interferir-en-las-elecciones-estadounidenses-segun-microsoft.html)

[Las grandes firmas de tecnología como Google o Amazon, están invirtiendo cada día mayores cantidades de dinero para hacer lobby en Europa.](https://corporateeurope.org/en/2020/09/big-tech-lobbying)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Estados Unidos comienza a [condenar los monopolios de las Big Tech y se empieza a hablar de forzar su división para mantener la competencia.](https://www.nytimes.com/2020/10/06/technology/congress-big-tech-monopoly-power.html)

[Incluso el candidato demócrata Joe Biden promete una mayor presión sobre Silicon Valley](https://elpais.com/economia/2020-10-03/silicon-valley-en-el-punto-de-mira.html) mientras el congreso [publica un informe antimonopolio tecnológico de gran repercusión.](https://www.theverge.com/2020/10/6/21504814/congress-antitrust-report-house-judiciary-committee-apple-google-amazon-facebook)

[CNMC califica a Amazon como operador postal y asegura el control de sus tarifas](https://www.lainformacion.com/economia-negocios-y-finanzas/competencia-califica-amazon-operador-postal-asegura-control-sus-tarifas/2816501/) mientras la tecnológica [crea una nueva filial española para sus envíos, con el fin de desafiar a Seur o MRW.](https://www.lainformacion.com/empresas/amazon-desafia-correos-mrw-nueva-filial-espana-envios/6504083/)

[El Senado español aprueba definitivamente las tasas Tobin y Google y entrarán en vigor en 2021.](https://www.elconfidencial.com/economia/2020-10-07/senado-aprueba-definitivamente-tasas-tobin-google-vigor_2779791/)

[Se filtran documentos provenientes de las instituciones más altas de la UE, que buscan desmantelar el cifrado End-to-End.](https://www.eff.org/deeplinks/2020/10/orders-top-eus-timetable-dismantling-end-end-encryption)

Mientras, en Estados Unidos ya es un tema candente y se prepara un [nuevo proyecto de ley contra la encriptación que supera a EARN IT.](https://tutanota.com/es/blog/posts/lawful-access-encrypted-data-act-backdoor/)

[Bruselas prepara una normativa para forzar a las Big Tech a compartir sus datos con sus competidores.](https://www.irishtimes.com/business/technology/brussels-drafts-rules-to-force-big-tech-to-share-data-1.4368323)

[La EU apela la sentencia del Tribunal General sobre el caso de las ayudas estatales de Irlanda a Apple.](https://twitter.com/vestager/status/1309428522352627713)

[Singapur estrena un sistema de verificación facial como DNI.](https://retina.elpais.com/retina/2020/10/02/innovacion/1601653052_245522.html)

[El Vaticano apoya legislar la ética en tecnología y la IA para proteger a la población.](https://www.politico.eu/article/vatican-calls-for-ai-ethics-with-backing-of-ibm-microsoft/)

[Ámsterdam y Helsinki crean un registro de algoritmos para mejorar la confianza ciudadana en la inteligencia artificial.](https://elpais.com/tecnologia/2020-10-06/amsterdam-y-helsinki-crean-un-registro-de-algoritmos-para-mejorar-la-confianza-ciudadana-en-la-inteligencia-artificial.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Social cooling:](https://www.socialcooling.com/) Si te sientes vigilado, no reaccionas igual y esto puede ser un problema para la sociedad.

Mozilla lanza la campaña [#UnTrendTwitter](https://foundation.mozilla.org/en/campaigns/untrendtwitter/) con la idea de pedir a la red social que pause los trending topics durante las elecciones de Estados Unidos.

[Blacklight](https://themarkup.org/blacklight/) te permite analizar las webs en busca de trackers, cookies o grabado de sesión y saber con qué terceros interactúan.

El proyecto Necessary&Proportionate apoyado por la EFF [analiza comparativamente las leyes y prácticas de vigilancia en los diversos países de América Latina (2016).](https://necessaryandproportionate.org/es/country-reports/)

[El Enemigo Anónimo, la primera serie documental sobre seguridad informática, ha sacado un nuevo capítulo sobre los ciberataques a infraestructuras críticas.](https://mobile.twitter.com/EnemigoAnonimo_/status/1313398205388906496)

[La justicia de la EU prohibe el recopilado y la retención de metadatos de forma indiscriminada](https://protonmail.com/blog/eu-data-collection-illegal/), vía ProtonMail.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

["The Usable Project" busca mejorar la usabilidad de proyectos open source sobre seguridad y privacidad a través de "UXFund"](https://usable.tools/blog/2020-10-01-uxfund2020/) con entre $5,000 y $50,000 USD. La convocatoria termina el 30 de Octubre.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

IUVIA saca una nueva edición de su podcast [#NadaQueEsconder: "Facebook y Europa, ni contigo ni sin ti"](https://blog.iuvia.io/es/nadaqueesconder-03/).

[Shoshana Zuboff: “El neoliberalismo lo ha destrozado todo. Debemos empezar de cero”](https://elpais.com/ideas/2020-10-08/shoshana-zuboff-el-neoliberalismo-lo-ha-destrozado-todo-debemos-empezar-de-cero.html).

En el pasado Techtopías hablábamos del famoso documental "The social dilema", en este caso ["The Prodigal Techbro"](https://conversationalist.org/2020/03/05/the-prodigal-techbro/) nos cuenta otro punto de vista sobre el tema.

Y para seguir con los documentales, una recomendación de Marta Peirano: ["The Facebook Dilemma"](https://www.pbs.org/wgbh/frontline/film/facebook-dilemma/).

[Cómo proteger tus feeds de redes sociales contra la desinformación en época de elecciones.](https://www.vox.com/recode/21396326/misinformation-social-feed-facebook-twitter-election)

[Capítulo IV: Dónde estamos ¿Qué futuro queremos para el reconocimiento facial?](https://retina.elpais.com/retina/2020/09/25/tendencias/1600997123_607807.html) en El País.

[DWeb Panel: If Big Tech Is Toxic, How Do We Build Something Better?](https://blog.archive.org/2020/09/24/dweb-panel-if-big-tech-is-toxic-how-do-we-build-something-better/)

[Algoritmos: villanos o héroes de nuestro tiempo](https://www.newtral.es/algoritmos-transparencia-sesgos-que-es/20200930/), por Marilín Gonzalo de Newtral.

Estas semanas hemos visto a Carissa Véliz en varios medios como en El País con ["Prohibir la economía de datos"](https://elpais.com/opinion/2020-10-01/prohibir-la-economia-de-datos.html) ya que ha publicado su libro ["Privacy Is Power. Why and How You Should Take Back Control of Your Data"](https://www.theguardian.com/books/2020/sep/28/carissa-veliz-intrusion-privacy-is-power-data).

También se ha publicado la versión en español de libro de Shoshana Zuboff, "La era del capitalismo de la vigilancia" en el que entre otras cosas se habla de que ["El dinero te vigila"](https://elpais.com/cultura/2020/09/25/babelia/1601043525_508297.html).

[Brett Scott: "Los bancos están creando una idea romántica de los pagos electrónicos para conseguir más poder"](https://www.eldiario.es/economia/bancos-creando-romantica-electronicos-conseguir_128_1361521.html) en elDiario.es

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
