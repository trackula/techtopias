---
title: "La demanda antimonopolio, la UE y cómo regular a los 'gatekeepers'."
date: 2020-10-27T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-12.jpg"

# meta description
description: "La demanda antimonopolio, la UE y cómo regular a los 'gatekeepers'."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Una base de datos filtrada, muestra cómo la campaña de Trump trató de influir en votantes afroamericanos para suprimir su voto en 2016.](https://gizmodo.com/leaked-database-shows-trump-campaign-targeted-black-ame-1845207843)

[Facebook lanza un satélite experimental para probar nuevas formas de mejorar la conectividad de banda ancha.](https://www.lightreading.com/iot/facebook-goes-to-space/d/d-id/764542)

[Microsoft elimina una operación de hacking masivo que podría haber afectado a las elecciones estadounidenses.](https://edition.cnn.com/2020/10/12/tech/microsoft-election-ransomware/index.html)

[Los estudiantes se rebelan contra las herramientas de reconocimiento facial para vigilar exámenes.](https://www.vice.com/en/article/n7wxvd/students-are-rebelling-against-eye-tracking-exam-surveillance-tools)

[El ‘dilema’ de Netflix que utiliza las mismas técnicas de persuasión que el documental "The Social Dilema" que ha producido.](https://elpais.com/tecnologia/2020-10-03/el-dilema-particular-de-netflix-como-crea-experiencias-de-adiccion.html)

[H&M ha sido condenada a pagar 35,3 millones de euros por las autoridades alemanas tras someter a sus trabajadores a vigilancia masiva en sus centros de Alemania y Austria.](https://www.eldiario.es/economia/alemania-multa-capitalismo-vigilancia-h-m_1_6282382.html)

[Disney+ lanza una funcionalidad para ver películas de forma sincronizada con amigos y que incluye reacciones con emojis en tiempo real.](https://twitter.com/Psythor/status/1320382061505544194) Esto le permitirá un nivel de detalle enorme sobre cómo los espectadores responden emocionalmente a cada momento de la película.

[Facebook denuncia a los investigadores que lideran un proyecto para saber cómo funciona la publicidad política personalizada.](https://twitter.com/gemmagaldon/status/1319915750774263808?s=19)

[GitHub elimina el código fuente de una de las herramientas de descarga de videos de YouTube más conocidas.](https://www.engadget.com/github-youtube-downloaders-riaa-223558038.html)

[Google Chrome no elimina las cookies y los datos del navegador de sus propias páginas web a pesar de haberlo marcado explícitamente.](https://www.theregister.com/2020/10/19/google_cookie_wipe/)

[Mozilla teme recibir daños colaterales por el caso antimonopolio contra Google.](https://venturebeat.com/2020/10/21/mozilla-rightly-fears-collateral-damage-in-google-antitrust-case/)

[Facebook dice haber rechazado 2.2m de anuncios por infringir las reglas para campañas políticas en las elecciones estadounidenses.](https://www.theguardian.com/technology/2020/oct/18/facebook-says-it-rejected-22m-ads-seeking-to-obstruct-voting-in-us-election)

[Russia planeó ciberataques contra los juegos olímpicos de Tokyo según el gobierno de UK.](https://www.theguardian.com/world/2020/oct/19/russia-planned-cyber-attack-on-tokyo-olympics-says-uk)

[El reconocimiento facial en los estadios deportivos.](https://www.wsj.com/articles/facial-recognitions-next-big-play-the-sports-stadium-11596290400) Los Angeles Football Club quiere autenticar a sus clientes con la cara y los New York Mets están probando el sistema con sus jugadores y personal.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[EEUU presenta una histórica demanda por monopolio contra Google](https://www.elconfidencial.com/tecnologia/2020-10-20/eeuu-presenta-una-historica-demanda-por-monopolio-contra-google_2797939/) aunque parece que a la tecnológica no le ha afectado ya que [sus acciones continúan en alza.](https://twitter.com/b_fung/status/1318603434778570753)

[Los fiscales del Departamento de Justicia y las oficinas del fiscal general del estado están discutiendo formas de frenar el poder de mercado del gigante, mientras se preparan para demandar a la empresa.](https://www.politico.com/news/2020/10/10/feds-may-target-googles-chrome-browser-for-breakup-428468)

[La comisaria de competencia de la UE, Margrethe Vestager dice que la UE exigirá más responsabilidad a las grandes tecnológicas.](https://elpais.com/economia/2020-10-13/vestager-exigiremos-mas-responsabilidad-a-las-grandes-tecnologicas.html)

Al mismo tiempo que [el parlamento europeo lanza una iniciativa legislativa para regular estas plataformas tecnológicas.](https://www.europarl.europa.eu/news/en/press-room/20201016IPR89543/digital-eu-must-set-the-standards-for-regulating-online-platforms-say-meps)

[La Generalitat revisará el uso de Google en las aulas](https://elpais.com/ccaa/2019/09/18/catalunya/1568832257_627354.html) ante las denuncias del colectivo Xnet que lamenta que Google tenga acceso a los perfiles psicológicos y académicos, calendario escolar, notas... de los niños desde los 10 años.

[Amazon, condenado a dar de alta a 3.000 falsos autónomos de su servicio de logística, Amazon Flex](https://www.elsaltodiario.com/falsos-autonomos/amazon-flex-condenado-dar-de-alta-3.000-repartidores) al mismo tiempo que [la inspección de trabajo obliga a Globo a regularizar a 11.000 falsos autónomos y a pagar 16,2 millones.](https://cincodias.elpais.com/cincodias/2020/10/21/economia/1603286842_126440.html)

[Se descubre que la app Radar Covid ha tenido un problema de seguridad desde su lanzamiento](https://elpais.com/tecnologia/2020-10-22/la-app-radar-covid-ha-tenido-una-brecha-de-seguridad-desde-su-lanzamiento.html) y que al únicamente comunicar con el servidor cuando un usuario era positivo, facilitaba que el proveedor cloud donde se desplegaban sus servidores (en este caso Amazon), pudiera extrapolar esta información. El problema ya ha sido corregido introduciendo tráfico simulado de falsos positivos para esconder dicha información.

[La publicidad con 'influencers' ya tiene regulación en España: un código obligará a señalar los anuncios y los regalos se considerarán pagos](https://www.genbeta.com/redes-sociales-y-comunidades/publicidad-influencers-tiene-regulacion-codigo-obligara-a-senalar-anuncios-regalos-se-consideraran-pagos).

[El fallo del tribunal superior de la UE determina que la vigilancia masiva de Reino Unido, Francia y Bélgica debe igualmente respetar la privacidad, incluso en el contexto de la seguridad nacional.](https://privacyinternational.org/press-release/4205/press-release-ruling-eus-highest-court-finds-uk-french-and-belgian-mass)

[Suecia prohíbe el 5G de las chinas Huawei y ZTE](https://www.elperiodico.com/es/economia/20201020/suecia-prohibe-5g-de-chinas-huawei-y-zte-8164659) y retirará los equipos ya instalados antes de 2025.

[Minneapolis considerará prohibir el reconocimiento facial.](https://www.vice.com/en/article/epdgdj/minneapolis-will-consider-facial-recognition-ban)

[Las quejas presentadas por Éticas Foundation sobre el Real-Time Bidding (RTB) en la industria de la publicidad online, comienzan a dar sus frutos.](https://twitter.com/EticasFdn/status/1317130925247967232?s=20)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

El equipo de Matrix publica ["Combatiendo el abuso en Matrix sin puertas traseras"](https://matrix.org/blog/2020/10/19/combating-abuse-in-matrix-without-backdoors) en respuesta a una publicación del gobierno británico que solicitaba poder eliminar el cifrado de extremo a extremo.

Mozilla lanza [RegretsReporter, una extensión para ayudar a los investigadores a entender el sistema de recomendación de Youtube](https://foundation.mozilla.org/en/campaigns/regrets-reporter/) enviándoles información de tus videos y recomendaciones.

[Cloudflare lanza su herramienta de analítica gratuita y "privacy-first".](https://blog.cloudflare.com/free-privacy-first-analytics-for-a-better-web/)

[Mozilla analiza las seis mayores plataformas sociales y las politicas que están tomando de cara a las elecciones estadounidenses.](https://foundation.mozilla.org/en/blog/mozilla-sheds-light-platform-election-policies/)

[Así se descubre a un ‘bot’: algoritmos que cazan mentiras.](https://elpais.com/tecnologia/2020-09-24/asi-se-descubre-a-un-bot-algoritmos-que-cazan-mentiras.html)

[DuckDuckGo tira por tierra el argumento de Google ante el tribunal de anti-competencia](https://multiversial.es/breves/duckduckgo-tira-por-tierra-el-argumento-de-google-ante-el-tribunal-de-anti-competencia-en-el-mercado-de-las-busquedas/) en el mercado de las búsquedas. Se puede [ver el artículo original en su blog.](https://spreadprivacy.com/one-click-away/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[La Shuttleworth Foundation ofrece becas a individuos que busquen implementar ideas innovadoras para conseguir un cambio social.](https://www.shuttleworthfoundation.org/apply/)

["The Usable Project" busca mejorar la usabilidad de proyectos open source sobre seguridad y privacidad a través de "UXFund"](https://usable.tools/blog/2020-10-01-uxfund2020/) con entre $5,000 y $50,000 USD. La convocatoria termina el 30 de Octubre.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

["Taking Back Our Privacy"](https://www.newyorker.com/magazine/2020/10/26/taking-back-our-privacy) por Moxie Marlinspike, fundador de Signal.

[Data Trust Initiative analiza las aproximaciones de India, Canada y la EU en el campo de la gestión de los datos.](https://datatrusts.uk/blogs/international-policy-developments)

["Why Facebook Can’t Fix Itself"](https://www.newyorker.com/magazine/2020/10/19/why-facebook-cant-fix-itself) por Andrew Marantz en The New Yorker.

Muchos estadounidenses se odian más que nunca, pero no discrepan entre ellos más de lo que solían, [¿por qué se le da tan bien a Facebook o Twitter polarizar?](https://www.elconfidencial.com/mercados/the-wall-street-journal/2020-10-25/redes-sociales-bien-facebook-twitter-polarizar_2797311/)

[Los partidos lo quieren saber todo sobre usted](https://retina.elpais.com/retina/2019/04/25/tendencias/1556187061_537373.html) por Manuel G. Pascual en El País Retina.

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
