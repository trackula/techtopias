---
title: "Trump censurado, el gobierno España a controlar la desinformación y la ley rider."
date: 2020-11-10T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-13.jpg"

# meta description
description: "Trump censurado, el gobierno España a controlar la desinformación y la ley rider."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[El ‘Tinder’ de Facebook se estrena en España y Europa](https://elpais.com/tecnologia/2020-10-22/el-tinder-de-facebook-se-estrena-en-espana-y-europa.html), una aplicación que se lanza [con 9 meses de retraso por dudas con respecto a la gestión de la privacidad.](https://techcrunch.com/2020/10/22/facebook-dating-launches-in-europe-after-9-month-delay-over-privacy-concerns/)

[Facebook ha accedido a la petición de Mozilla de detener las recomendaciones de grupos durante las elecciones de USA.](https://foundation.mozilla.org/en/blog/facebook-heeds-mozilla-call-pauses-group-recommendations/)

[También se ha publicado cómo ejecutivos de Facebook detuvieron los esfuerzos iniciados para reducir la división que la plataforma genera en sus usuarios.](https://www.wsj.com/articles/facebook-knows-it-encourages-division-top-executives-nixed-solutions-11590507499), al mismo tiempo que se han filtrado [las reglas internas de la compañía, que permitían la publicación de desinformación y contenido para incitar a no votar.](https://www.vice.com/en/article/88aadk/these-are-facebooks-internal-rules-on-voter-suppression-and-disinformation)

[Los datos personales de 3,6M de alumnos y padres de Madrid han sido expuestos en la red por un grave fallo de seguridad similar al ocurrido en Lexnet.](https://www.elconfidencial.com/tecnologia/2020-10-31/comunidad-madrid-brecha-datos-colegios-alumnos-profesores_2809919/)

[Trump se autoproclama presidente con los votos sin contar, Twitter y Facebook reaccionan para contradecirle.](https://www.genbeta.com/actualidad/trump-se-autoproclama-presidente-votos-contar-twitter-facebook-reaccionan-para-contradecirle)

Ante su insistencia en publicar contenido no verificado, [Twitter podría cerrar la cuenta de Trump una vez abandone el cargo, según indican sus políticas de contenido.](https://www.independent.co.uk/life-style/gadgets-and-tech/trump-twitter-account-suspended-election-rules-b1620154.html)

[Las autoridades de Tailandia han pedido a los proveedores de internet, bloquear la aplicación de mensajería Telegram.](https://www.bbc.com/news/world-asia-54598956)

[Japón planea utilizar reconocimiento facial para prevenir la extensión del coronavirus durante los Juegos Olímpicos de Tokyo.](https://english.kyodonews.net/news/2020/10/d81cab83c447-facial-recognition-planned-to-halt-virus-spread-during-tokyo-games.html)

[Zoom ha eliminado eventos en los que se hablaba sobre la censura que la propia compañía ejerce.](https://www.buzzfeednews.com/article/janelytvynenko/zoom-deleted-events-censorship)

[El INE recurre de nuevo a Telefónica, Vodafone y Orange para monitorizar la mobilidad de los ciudadanos de las zonas confinadas.](https://www.vozpopuli.com/economia-y-finanzas/ine-telefonica-vodafone-orange-confinamiento_0_1406859477.html)

[Una cámara controlada por inteligencia artificial arruina la emisión de un partido al confundir el balón con un árbitro calvo](https://www.genbeta.com/actualidad/camara-controlada-inteligencia-artificial-arruina-emision-partido-al-confundir-balon-arbitro-calvo), la noticia curiosa del día que muestra cómo esta tecnología está todavía un poco verde.

[Los datos de cientos de pacientes de psicoterapia en Finlandia han sido hackeados y muchos de ellos han recibido emails exigiéndoles una recompensa económica a cambio de no publicar su información de salud.](https://www.theguardian.com/world/2020/oct/26/tens-of-thousands-psychotherapy-records-hacked-in-finland)

[La compañía secreta que podría acabar con la privacidad tal y como la conocemos,](https://www.nytimes.com/2020/01/18/technology/clearview-privacy-facial-recognition.html) un análisis sobre la polémica compañía Clearview AI.

En el mundo de los datos, empresas como [Openbank incluyen en sus políticas la posibilidad de rastrear a sus clientes en redes sociales para determinar si conceden o no créditos.](https://twitter.com/jlacort/status/1324375237786554368)

[Apple pretende desarrollar su propio buscador web para no depender de Google](https://elpais.com/economia/2020-10-28/apple-pretende-desarrollar-su-propio-buscador-web-para-no-depender-de-google.html), una dependencia que lleva años mateniendo ante [un gigante como Google, que es dificil de evitar ya que sus tentáculos llegan a todos los sectores.](https://elpais.com/tecnologia/2020-10-24/google-por-todas-partes-anatomia-de-un-gigante-ubicuo.html)

De hecho, [muchas empresas españolas se han viso aplastadas por las 'big tech': "Si no pagas a Google, no apareces"](https://www.elconfidencial.com/tecnologia/2020-11-02/google-facebook-apple-amazon-gafa-monopolios-thinktek_2811920/), lo que habla de la enorme dependencia tecnológica que tenemos del gigante estadounidense.

Apple solicitará a partir de 2021 que las apps pidan permiso expreso al usuario para ser trackeado. [El sector de la publicidad en Francia ha denunciado a Apple por prácticas anticompetitivas ya que cree que la mayoría de usuarios dirá que no, con la correspondiente pérdida de ingresos.](https://www.macrumors.com/2020/10/28/ios-14-anti-tracking-prompt-antitrust-complaint/)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El gobierno español aprueba un plan contra las 'fake news' que monitorizará la información y podrá pedir colaboración a los medios](https://www.europapress.es/nacional/noticia-gobierno-aprueba-plan-contra-fake-news-monitorizara-informacion-podra-pedir-colaboracion-medios-20201105112017.html), una medida que ha generado gran recelo entre los diversos partidos, muchos ciudadanos y otros colectivos por las grandes dudas con respecto a que un gobierno controle el discurso online.

Maldita publica un artículo al respecto: ["Por qué un gobierno no debe decidir qué es verdad y qué no y por qué la lucha contra la desinformación no se puede hacer desde órganos no independientes del gobierno"](https://maldita.es/nosotros/2020/11/06/gobierno-verdad-no-lucha-desinformacion/).

[La AEPD advierte que las grabaciones de conversaciones o imágenes en el trabajo deben justificarse para evitar sanción.](https://confilegal.com/20201102-la-aepd-advierte-que-las-grabaciones-de-conversaciones-o-imagenes-en-el-trabajo-deben-justificarse-para-evitar-sancion/)

Ante la sentencia del Tribunal Supremo que ha resuelto que los riders son falsos autónomos, [las apps de delivery ya se están moviendo para tratar de burlar al Supremo.](https://www.elconfidencial.com/tecnologia/2020-10-29/empresas-delivery-glovo-sentencia-supremo-flotas_2809623/)

[Mientras, el gobierno continúa avanzando con el primer borrador de la llamada 'ley rider' que plantea un plus salarial por usar los equipos propios y excluye a las VTC.](https://www.lainformacion.com/economia-negocios-y-finanzas/ley-rider-plus-salarial-usar-equipos-propios-excluye-vtc/2819401/)

[El Gobierno exige a Netflix y HBO que paguen el 5% de tasa por ingresos reales](https://www.lainformacion.com/empresas/gobierno-exige-netflix-hbo-tasa-ingresos-reales-espana/2820296/) al igual que lo hacen por ejemplo las televisiones privadas, para contribuir a financiar obras cinematográficas europeas.

[Alemania participa en las directivas que prepara la UE para regular a los gigantes digitales.](https://www.euractiv.com/section/digital/news/germany-weighs-in-on-eus-bid-to-regulate-digital-giants/)

[Entre estas directivas destaca la e-Commerce Directive y la Digital Services Act que aspira a ser un proyecto muy ambicioso para regular a los servicios de internet.](https://www.eff.org/deeplinks/2020/10/eu-parliament-paves-way-ambitious-internet-bill)

[También se han filtrado algunos de los planes antimonopolio que se preparan y la contrapropuesta de Holanda y Francia.](https://www.eff.org/deeplinks/2020/10/eu-vs-big-tech-leaked-enforcement-plans-and-dutch-french-counterproposal)

Regulaciones sobre las grandes tecnológicas que también baraja [EEUU, que busca evitar los monopolios de Silicon Valley.](https://www.elconfidencial.com/tecnologia/2020-10-26/amazon-google-facebook-apple-monopolios-eeuu_2796544/)

[Estas últimas semanas, diversos directivos de Twitter, Facebook o Google han sido interrogados por el Capitolio. “Señor Dorsey, ¿quién demonios le ha elegido a usted?”](https://elpais.com/tecnologia/2020-10-28/el-capitolio-contra-silicon-valley-senor-dorsey-quien-demonios-le-ha-elegido-a-usted.html)

[Portland aprueba el referendum que prohibe la vigilancia con reconocimiento facial.](https://techcrunch.com/2020/11/04/portland-maine-passes-referendum-banning-facial-surveillance/)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

Purism está trabajando para desarrollar un móvil que sirva de alternativa a Google y Apple y [ha lanzado la iniciativa "Fund Your App"](https://puri.sm/fund-your-app/) que permite a sus usuarios realizar donaciones para que la empresa migre las aplicaciones que los usuarios decidan.

Un año más [Mozilla lanza el "call for proposals" para el Mozilla festival que tendrá lugar el 26 de octubre de 2021 de forma online.](https://www.mozillafestival.org/en/get-involved/proposals/)

El colectivo Tactical Tech lanza ["The Glass Room" edición información falsa, en versión online.](https://theglassroom.org/es/informacion-falsa/)

Derechos Digitales lanza [una web para estar al día del avance del reconocimiento facial en América Latina.](https://reconocimientofacial.info/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Si tienes un proyecto tecnológico para mejorar la privacidad y la confianza, NGI Zero Privacy & Trust Fund](https://nlnet.nl/PET/) ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Diciembre.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Diciembre.

[La Shuttleworth Foundation ofrece becas a individuos que busquen implementar ideas innovadoras para conseguir un cambio social.](https://www.shuttleworthfoundation.org/apply/)

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Enero.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[#NadaQueEsconder 04: Seguridad, privacidad y política con Paloma LLaneza,](https://blog.iuvia.io/es/nadaqueesconder-04/) un nuevo podcast de IUVIA.

[La verdad en las democracias algorítmicas](https://www.cidob.org/articulos/revista_cidob_d_afers_internacionals/124/la_verdad_en_las_democracias_algoritmicas) por Daniel Innerarity y Carme Colomina en CIDOB.

["La privacidad online no existe: ¿cuándo perdimos esa batalla?"](https://www.youtube.com/watch?reload=9&v=4g0R9TZwU8I&feature=youtu.be) en "El Enemigo Anónimo".

["Data as Property?"](https://phenomenalworld.org/analysis/data-as-property/) por Salomé Viljoen.

["10 years of Instagram: how it has transformed our lives"](https://www.theguardian.com/news/audio/2020/oct/26/10-years-of-instagram-how-it-has-transformed-our-lives-podcast), Sarah Frier en el podcast de The Guardian.

[Internet access, a new human right,](https://www.youtube.com/watch?reload=9&v=xl5qAdfrgZo&feature=emb_title) por Simona Levi de Xnet.

Y hoy acabamos con un hilo de Marcos Martinez [que muestra la cantidad de información que se puede obtener de una persona a partir de un inocente mensaje de WhatsApp.](https://twitter.com/euklidiadas/status/1323559935909781504)

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
