---
title: "La carta de derechos digitales, Apple también espía y el cifrado en Europa."
date: 2020-11-24T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-14.jpg"

# meta description
description: "La carta de derechos digitales, Apple también espía y el cifrado en Europa."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Una empresa española ha expuesto 24GB de datos personales de millones de clientes de Booking, Expedia y otros portales de reservas.](https://www.genbeta.com/actualidad/empresa-espanola-ha-expuesto-24gb-datos-personales-millones-clientes-booking-expedia-otros-portales-reservas)

[Zoom ha estado mintiendo a sus usuarios sobre el cifrado de extremo a extremo de sus comunicaciones durante años.](https://arstechnica.com/tech-policy/2020/11/zoom-lied-to-users-about-end-to-end-encryption-for-years-ftc-says/)

[Según parece, los portátiles de Apple envían información a la compañía cada vez que abres una app, con tu localización, identificador único y todo sin cifrar.](https://twitter.com/nukeador/status/1327576907362095104)

[Los activistas que vencieron a Facebook denuncian a Apple por rastrear sin consentimiento a los usuarios de iPhone.](https://elpais.com/tecnologia/2020-11-16/los-activistas-que-vencieron-a-facebook-denuncian-a-apple-por-rastrear-sin-consentimiento-a-los-usuarios-de-iphone.html)

[Apple pagará 113 millones en EEUU por haber ralentizado los iPhones viejos](https://expansion.com/empresas/tecnologia/2020/11/18/5fb57a9ae5fdea4e318b457b.html), una multa claramente baja frente a los beneficios que estas prácticas le ha podido reportar.

[Los editores demandan a Google en España por el impago de derechos de autor](https://cincodias.elpais.com/cincodias/2020/11/11/companias/1605094551_279191.html).

[Twitter plantea nuevamente añadir el botón de "dislike" o un sistema de voto negativo.](https://mashable.com/article/twitter-explores-dislike-button-downvote-system/?europe=true)

[La última adquisición de Spotify tiene que ver con nuestros datos,](https://gizmodo.com/spotifys-latest-acquisition-is-all-about-your-data-1845632164) la empresa ha comprado Megaphone, una plataforma publicitaria para podcasts.

[Amazon continua su entrada en el mercado de la salud lanzando Amazon Pharmacy para la entrega de medicamentos recetados.](https://www.theverge.com/2020/11/17/21571091/amazon-pharmacy-store-launch-home-delivery-prescription-medication)

[La empresa de inteligencia artificial que ayuda al Pentágono a evaluar las campañas de desinformación.](https://www.wired.com/story/ai-helping-pentagon-assess-disinfo-campaigns/)

[Cómo el ejército de EE. UU. compra datos de ubicación de aplicaciones comunes.](https://www.vice.com/en/article/jgqm5x/us-military-location-data-xmode-locate-x)

[¿Cuatro años complicados para Silicon Valley? Biden promete mano dura](https://www.elconfidencial.com/mercados/the-wall-street-journal/2020-11-10/silicon-valley-se-enfrenta-a-otro-presidente-que-aplicara-mano-dura-a-las-big-tech_2826627/) aunque al mismo tiempo han salido voces que sugieren lo contrario ya que [muchas de las 'Big Tech' ha aportado enconómicamente a su campaña y la tecnología no forma parte de sus cuatro pilares de campaña.](https://www.technologyreview.es/s/12842/las-big-tech-respiran-mas-tranquilas-con-biden-al-mando-de-ee-uu)

[La Secretaría de Estado de Digitalización e Inteligencia Artificial (SEDIA) anuncia que puedes utilizar la app RadarCovid sin que el consumo de datos compute en tu factura,](https://twitter.com/SEDIAgob/status/1326496906030292992) un movimiento que claramente contradice al Tribunal de Justicia de la Unión Europea que [hace apenas dos meses interpetaba que el zero rating ataca el principio de neutralidad de la red.](https://curia.europa.eu/jcms/upload/docs/application/pdf/2020-09/cp200106en.pdf).

[El servicio secreto CNI también vigila las campañas de desinformación en la red.](https://elpais.com/espana/2020-11-08/el-servicio-secreto-cni-tambien-vigila-las-campanas-de-desinformacion-en-la-red.html)

[La AEPD publica una guía que analiza el uso de nuevas tecnologías en las Administraciones Públicas](https://www.aepd.es/es/prensa-y-comunicacion/notas-de-prensa/aepd-publica-guia-nuevas-tecnologias-aapp) y analiza el impacto y los riesgos que estas puedan tener.

[La Unión Europea financia tecnología biométrica en paises africanos, utilizada para devolver a estos inmigrantes de vuelta a su pais](https://www.bloomberg.com/news/articles/2020-11-11/eu-funds-biometric-tech-used-for-returning-african-migrants) mientras que Naciones Unidas [advierte de los riesgos de la recolección de datos biométricos en las fronteras europeas y el impacto que puede tener en la privacidad de las personas.](https://www.theguardian.com/global-development/2020/nov/11/un-warns-of-impact-of-smart-borders-on-refugees-data-collection-isnt-apolitical)

[Bruselas quiere una Europa con cinco únicos operadores de telefonía, Telefónica entre ellos.](https://www.vozpopuli.com/economia-y-finanzas/operadores-telefonica-europa_0_1408959269.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El Gobierno de España presenta la primera versión de la Carta de Derechos Digitales](https://elpais.com/tecnologia/2020-11-17/inteligencia-artificial-y-pseudoanonimato-el-gobierno-presenta-la-primera-version-de-la-carta-de-derechos-digitales.html), una carta que ha sido publicada para su [consulta pública con la idea de que la sociedad pueda contribuir también.](https://portal.mineco.gob.es/es-es/ministerio/participacionpublica/audienciapublica/Paginas/SEDIA_Carta_Derechos_Digitales.aspx)
En el siguiente [enlace se puede ver la presentación oficial de la misma.](https://digitalfuturesociety.com/es/agenda/carta-derechos-digitales-streaming/)

Por supuesto muchas han sido las dudas con respecto a su validez jurídica, [llegando incluso a afirmar que puede ser simplemente propaganda](https://www.businessinsider.es/quiere-espana-carta-derechos-digitales-757397) ya que realmente no pretende legislar, sino simplemente hacer una declaración de intenciones.

Otras personas como el abogado Javier de la Cueva [también han sido muy críticas ya que el Gobierno sigue obstruyendo en los tribunales la transparencia de otras herramientas de la Administración como el caso del bono social eléctrico](https://twitter.com/jdelacueva/status/1328443573524639744) al mismo tiempo que publica buenas intenciones con dicha carta.

En el campo de los derechos laborales, [el Gobierno de España obligará a las plataformas a pagar bicis, coches o móviles de los empleados](https://www.lainformacion.com/economia-negocios-y-finanzas/plataformas-digitales-pagar-bicis-coches-moviles-empleados/2820788/) y está trabajando en [crear un registro obligatorio de plataformas digitales, que tendrán que revelar sus algoritmos o documentación que permita entender su funcionamiento.](https://www.elespanol.com/invertia/economia/empleo/20201112/trabajo-registro-obligatorio-plataformas-digitales-revelar-algoritmos/535447346_0.html)

Al mismo tiempo, [el ministro Escrivá maniobra para desactivar por la vía rápida el modelo de 'riders' en Glovo,](https://www.lainformacion.com/empresas/seguridad-social-acortar-plazos-cobrar-cuota-glovo/2820332/) que pretende dilatar los pagos a los que ha sido condenada para regularizar a sus trabajadores.

[España dice que la seguridad 5G es una prioridad en su propuesta para traer el European Cybersecurity Industrial, Technology and Research Competence Centre (ECCC) a León.](https://www.euractiv.com/section/cybersecurity/news/5g-security-a-priority-in-our-eu-cyber-centre-bid-spain-says/)

[La Armada creará un 'Alexa' para navegar con la voz y uniformes sensorizados](https://www.vozpopuli.com/economia-y-finanzas/armada-navantia-inteligencia-artificial_0_1408959304.html). Como nota personal, no puedo evitar que me recuerde al conocido [anuncio de la cadena de supermercados REMA 1000](https://www.youtube.com/watch?v=o_41AXiRDmI) 😂

[El CEO de Google se disculpa por un documento filtrado en el que se exponían diversas tácticas de presión para tratar de contrarestrar las nuevas medidas que la UE planea en el campo de la regulación de empresas tecnológicas.](https://es.reuters.com/article/idUKKBN27T1C6)

[Europa y el cifrado de extremo a extremo: la UE plantea una resolución para tener acceso a los mensajes](https://www.xatakamovil.com/mercado/europa-cifrado-extremo-a-extremo-ue-plantea-resolucion-para-tener-acceso-a-mensajes) mientras muchos colectivos alzan la voz para protestar, por ejemplo [EDRI que ha publicado un documento en defensa del cifrado.](https://edri.org/wp-content/uploads/2020/11/20201109-EDRi-Draft-Response-to-Council-on-encryption-SEND.pdf)

[La Comisión Europea carga contra Amazon: la acusa de usar los datos de sus vendedores para competir contra ellos](https://www.xataka.com/empresas-y-economia/comision-europea-carga-amazon-acusa-usar-datos-vendedores-terceros-para-competir-ellos). En el siguiente enlace podemos ver [la nota de prensa oficial de la Comisión Europea al respecto.](https://ec.europa.eu/commission/presscorner/detail/en/ip_19_4291)

[La presidencia alemana del Consejo de la UE propone permitir el procesamiento de metadatos generados por las comunicaciones en linea para monitorizar epidemias o desastres naturales o provocados por el hombre.](https://www.euractiv.com/section/digital/news/german-presidency-charts-new-covid19-metadata-rules-in-leaked-eprivacy-text/) Parece que muchos de los cambios en el campo de la vigilancia que llegaron por el COVID, vienen para quedarse.

[Investigadores estatales y federales preparan cambios contra Facebook por comprar rivales como Instagram o WhatsApp y utilizar sus datos para eliminar a la competencia.](https://www.washingtonpost.com/technology/2020/11/19/facebook-antitrust-lawsuit/)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Tim Berners-Lee lanza "Inrupt", una startup enfocada en la privacidad por defecto con la idea de que cada usuario tenga sus datos bajo su control.](https://elpais.com/tecnologia/2020-11-09/inrupt-el-plan-b-para-internet-de-berners-lee-echa-a-andar.html) La empresa está desarrollando ["Solid", una tecnología que permite almacenar nuestros datos personales y compartirlos únicamente con quien se desee.](https://www.cnet.com/news/tim-berners-lee-startup-launches-privacy-focused-service-to-secure-your-data/)

Aprovechando esta noticia recordamos tamibén a [Empathy.co, la empresa española que colabora con Inrupt creando un buscador para dicha tecnología.](https://retina.elpais.com/retina/2019/09/02/innovacion/1567416042_149910.html)

Varias asociaciones y colectivos como EDRI, Privacy International o La Quadrature du Net lanzan la campaña ["Reclaim your face"](https://reclaimyourface.eu/) para luchar contra el reconocimiento facial y la vigilancia masiva en Europa.

[Éticas, la empresa española que audita algoritmos para asegurar la ética de los datos.](https://innovadores.inndux.com/es/auditorias-de-algoritmos-para-asegurar-la-etica-de-los-datos/)

[Este físico del MIT es la peor pesadilla de Google: "Está manipulando internet"](https://www.elconfidencial.com/tecnologia/2020-11-09/duckduckgo-google-buscadores-manipulacion_2822167/), entrevista al fundador de DuckDuckGo.

[Internet society explica sus proyectos e introduce una guía y múltiples recursos para colaborar con comunidades locales y gobiernos en el desarrollo de redes de comunicación comunitarias.](https://www.internetsociety.org/issues/community-networks/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Si tienes un proyecto tecnológico para mejorar la privacidad y la confianza, NGI Zero Privacy & Trust Fund](https://nlnet.nl/PET/) ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Diciembre.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Diciembre.

[La Shuttleworth Foundation ofrece becas a individuos que busquen implementar ideas innovadoras para conseguir un cambio social.](https://www.shuttleworthfoundation.org/apply/)

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Enero.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

["Diálogos sobre tecnología y poder: El impacto político y social de las redes sociales digitales"](https://panoptikuhn.org/dialogos/dialogos-sobre-tecnologia-y-poder-el-impacto-politico-y-social-de-las-redes-sociales-digitales/) vía Panoptikuhn.

["Breaking Encryption Myths, What the European Commission’s leaked report got wrong about online security"](https://www.internetsociety.org/resources/doc/2020/breaking-the-myths-on-encryption/) via InternetSociety.

["The Future of Encryption in the European Union"](https://www.internetsociety.org/events/the-future-of-encryption-in-the-european-union/) via InternetSociety.

[Un calvario digital: Google cancela mi cuenta de correo y todo lo asociado con ella](https://www.cmmedia.es/noticias/espana/un-calvario-digital-google-cancela-mi-cuenta-de-correo-y-todo-lo-asociado-con-ella/), una reflexión de sobre la perdida total de control sobre nuestros archivos, datos y documentos.

["El imperio más peligroso del mundo: ¿qué hacen Google, Amazon, Facebook y Apple con tus datos?"](https://www.elenemigoanonimo.com/privacidad-negocio-datos-google-amazon-facebook-apple/), un nuevo episodio de El Enemigo Anónimo.

["Finland: How to unionize when your boss is an algorithm and you’re self-employed"](https://algorithmwatch.org/en/story/justice4couriers/), Aleksi Knuutila en Algorithm Watch.

["An outlaw technology to create information freedom in science"](https://meta.decidim.org/conferences/decidimfest2020/f/1390/meetings/1448), una conversación con Alexandra Elbakyan, creadora de Sci-Hub.

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
