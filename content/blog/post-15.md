---
title: "El tracking de Microsoft, la Estrategia nacional de IA y las nuevas directivas europeas sobre tecnología."
date: 2020-12-08T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-15.jpg"

# meta description
description: "El tracking de Microsoft, la Estrategia nacional de IA y las nuevas directivas europeas sobre tecnología."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Una caida en Amazon Web Services deja sin funcionamiento a las aspiradoras Roomba y a múltiples aparatos IOT.](https://www.xataka.com/otros-dispositivos/caida-parcial-servidores-amazon-ha-provocado-que-aspiradoras-dejen-funcionar) Una caida que muestra una vez más la dependencia y los problemas de que todo esté conectado a internet.

Microsoft ha lanzado [una nueva funcionalidad de Microsoft 365 para medir la productividad de los empleados de una empresa en todo momento](https://www.genbeta.com/actualidad/nueva-funcionalidad-microsoft-365-mayor-sistema-vigilancia-puesto-trabajo-fundador-basecamp), una funcionalidad bastante polémica que [pretende llevar prácticas de control laboral a todos los sectores.](https://www.theguardian.com/technology/2020/nov/26/microsoft-productivity-score-feature-criticised-workplace-surveillance) La propia empresa, [se ha apresurado a pedir disculpas y anonimizar dicha información.](https://www.theguardian.com/technology/2020/dec/02/microsoft-apologises-productivity-score-critics-derided-workplace-surveillance)

[Google espió a sus trabajadores antes de despedirlos por tratar de sindicarse, según la National Labor Relations Board.](https://www.theverge.com/2020/12/2/22047383/google-spied-workers-before-firing-labor-complaint) Una noticia que se une a la publicada esta semana sobre cómo [la multinacional ha despedido a una de las mayores investigadoras en la ética de la inteligencia artificial.](https://www.reuters.com/article/us-alphabet-google-research-idUSKBN28D3JN) Mas de 1200 empleados han firmado una carta de protesta ante el despido de esta investigadora, [que denunciaba la falta de responsabilidad en los programas de diversidad de Google y ante los sesgos que sus IAs están creando.](https://www.theguardian.com/technology/2020/dec/04/timnit-gebru-google-ai-fired-diversity-ethics)

[Facebook se ha dado cuenta de que eliminando la desinformación y reduciendo el discurso de odio en su plataforma, su crecimiento cae.](https://www.nytimes.com/2020/11/24/technology/facebook-election-misinformation.html)

[Un periodista noruego averiguó cómo sus datos de localización acabaron en una contratista del Gobierno de EE.UU](https://www.genbeta.com/seguridad/mi-telefono-me-espia-periodista-noruego-averiguo-como-sus-datos-localizacion-acabaron-contratista-gobierno-ee-uu), una noticia que nos muestra cómo funcionan los data brokers y que [sirven también de ayuda a los partidos políticos para conseguir nuestro voto.](https://www.bbc.com/news/technology-54915779).

[Las aplicaciones de rastreo de contactos han sido la propuesta de las empresas tecnológicas para luchar contra el COVID-19, pero, ¿por qué no han sido efectivas?](https://time.com/5905772/covid-19-contact-tracing-apps/).

[Galicia y otras comunidades utilizarán una app que con QR permite realizar tests y entrar a locales nocturnos](https://www.elespanol.com/quincemil/articulos/actualidad/galicia-utilizara-una-app-que-con-qr-permite-realizar-test-y-entrar-a-locales-nocturnos), una herramienta no anónima que va en contra de los principios de privacidad bajo los que han sido desarrolladas las apps de traceo de contactos.
[Carmela Troncoso ha escrito un hilo en Twitter dando su opinión sobre estas nuevas apps.](https://twitter.com/carmelatroncoso/status/1334199116138770432)

Los ataques informáticos se han multiplicado durante la pandemia, en este caso [han intentado atacar a los laboratorios de AstraZeneca desde Corea del Norte.](https://www.lavozdegalicia.es/noticia/sociedad/2020/11/27/intentan-hackear-sistema-astrazeneca-corea-norte/00031606488416738716270.htm)

También el ransomware se está volviendo algo habitual, [en este caso ha caído el sistema público de transporte de Montreal.](https://www.bleepingcomputer.com/news/security/montreals-stm-public-transport-system-hit-by-ransomware-attack/)

[Apple presiona contra un proyecto de ley que busca detener el trabajo forzoso en China.](https://www.washingtonpost.com/technology/2020/11/20/apple-uighur/)

En pasadas ediciones hablábamos de Apple y los frentes que se estaban abriendo en su contra en temas de privacidad y competencia. En este caso son diversos [activistas de la privacidad en Alemania y España quienes demandan a la tecnológica.](https://www.businessinsider.es/demanda-apple-espana-abre-nuevo-frente-europa-759757#)

[Un periodista holandés se cuela en una videoconferencia de Defensa secreta de la UE.](https://www.vozpopuli.com/internacional/conferencia-defensa-ue_0_1411960043.html)

[Protección de Datos multa con 75.000 euros a Conseguridad y GlovoApp por no tener Delegado de Protección de Datos.](https://www.vozpopuli.com/economia-y-finanzas/proteccion-datos-delegado-multas_0_1409559396.html)

[400 parlamentarios de todo el mundo escriben a Jeff Bezos bajo el movimiento "Make Amazon Pay", exigiendo que cambie sus políticas laborales.](https://www.eldiario.es/economia/400-parlamentarios-mundo-escriben-jeff-bezos-impunidad-amazon-terminado_1_6478411.html)

[Empresas como Uber o Lyft gastan millones para luchar contra nuevas propuestas de regulación laboral en Illinois y Nueva York.](https://www.vice.com/en/article/m7avyp/gig-companies-spend-millions-on-anti-labor-pacs-in-illinois-and-new-york)

[Just Eat anuncia que tendrá una flota propia de repartidores asalariados en España](https://www.lavanguardia.com/economia/20201118/49530074342/just-eat-repartidores-riders-espana.html) al mismo tiempo que [se publica cómo serán los contratos para 'riders' que defiende el Gobierno: a tiempo parcial y 6,56 euros la hora.](https://www.elespanol.com/invertia/observatorios/digital/20201119/contratos-riders-defiende-gobierno-tiempo-parcial-euros/536947540_0.html)

[Glovo pide a sus riders que se den de baja en Trabajo como asalariados y que vuelvan a ser autónomos.](https://www.economiadigital.es/finanzas-y-macro/glovo-pide-a-sus-riders-que-se-den-de-baja-en-trabajo-como-asalariados_20109229_102.html)

[Netflix empezará a facturar su negocio en España desde el propio país a partir de 2021](https://cincodias.elpais.com/cincodias/2020/11/23/companias/1606164658_164598.html), una medida que es probable que no suponga un gran cambio en el impuesto de sociedades que paga la multinacional, igual que pasó con [Microsoft en 2018 cuando la tecnológica realizó el mismo movimiento.](https://www.lainformacion.com/empresas/caso-microsoft-netflix-impuestos-espana-facturacion/2822209/)

[Oracle y Salesforce llevados a los tribunales en los Países Bajos por infracción de GDPR.](https://edri.org/our-work/oracle-and-salesforce-taken-to-court-in-the-netherlands-over-gdpr-infringement/)

[China asciende como superpotencia mundial de datos a medida que Internet se fractura.](https://asia.nikkei.com/Spotlight/Century-of-Data/China-rises-as-world-s-data-superpower-as-internet-fractures)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Pedro Sánchez presenta la Estrategia Nacional de Inteligencia Artificial con una inversión pública de 600 millones en el periodo 2021-2023.](https://www.lamoncloa.gob.es/presidente/actividades/Paginas/2020/021220-sanchezenia.aspx) Se habla entre otras cosas de potenciar la investigación en IA e integrarla dentro de la administración pública.

[El Gobierno quintuplica el contrato con Indra para usar Radar Covid hasta 2022.](https://www.lainformacion.com/economia-negocios-y-finanzas/radar-covid-contrato-indra-2022/2822076/)

Estos días en Europa se está hablando mucho sobre las diversas directivas que se plantean. [Aquí tenemos un análisis rápido para saber en qué consiste cada una: "Digital Services Act" (DSA), "Digital Markets Act" (DMA) y "Data Governance Act" (DGA).](https://edri.org/our-work/eu-alphabet-soup-of-digital-acts-dsa-dma-and-dga/)

[Un poco mas sobre la DSA: la gran normativa europea que puede cambiar internet tal y como lo conocemos.](https://www.xataka.com/legislacion-y-derechos/ley-servicios-digitales-dsa-que-sabemos-gran-proyecto-europeo-que-puede-cambiar-internet-tal-como-conocemos)

[Austria pide que la DSA haga a las Big Tech responsables del contenido que almacenan.](https://www.euractiv.com/section/digital/news/make-big-tech-accountable-austria-says-in-digital-services-act-recommendations/)

[EDRI también ha publicado un documento con su punto de vista sobre la DSA.](https://edri.org/wp-content/uploads/2020/04/DSA_EDRiPositionPaper.pdf)

En el pasado Techtopias hemos hablado de cómo se está valorando en la UE la posiblidad de introducir puertas traseras que permitan a los jueces acceder a comunicaciones cifradas. [Aquí tenemos una reflexión sobre por qué esto es mala idea.](https://www.linkedin.com/pulse/eu-parliament-pass-derogation-which-result-total-over-alexander-hanff/)

[La Comisión Europea ha propuesto medidas para impulsar el intercambio de datos y apoyar el espacio de datos europeo.](https://ec.europa.eu/commission/presscorner/detail/en/ip_20_2102) Parece que la UE va en la dirección de compartir los datos generados por las plataformas, un ejemplo lo tenemos en [Amadeus que lidera la estrategia europea para crear un 'cloud' ligado a movilidad y turismo.](https://cincodias.elpais.com/cincodias/2020/11/20/companias/1605901474_511768.html)

[Barcelona lanza una tasa a las empresas de motos, patinetes y otros vehículos compartidos por el uso que hacen del espacio público.](https://www.businessinsider.es/empresas-sharing-no-pagan-ninguna-tasa-usar-espacio-publico-753811) Unas empresas que buscan ofrecer servicios en el momento en el que lo necesitas, sin tener que adquirir un vehículo propio. [Puede que en 10 años nadie compre coches o iPhones, podría ser "el fin de la propiedad" según el CEO de Zuora.](https://www.businessinsider.es/probablemente-10-anos-nadie-comprara-coches-iphones-nuevos-761717)

[ELISA, la herramienta de ciberseguridad que investiga la desinformación en España](https://www.elconfidencial.com/espana/2020-11-22/elisa-la-herramienta-de-ciberseguridad-del-cni-que-investiga-la-desinformacion-en-espana_2842835/). Y no es la única, [en esta otra noticia podemos saber un poco más de las herramientas que utiliza el CNI.](https://www.vozpopuli.com/espana/cni-ciberamenazas-nombre-mujer_0_1413759769.html)

[El INE rastrea las webs de Airbnb, Booking y Vrbo para saber cuántas viviendas turísticas hay en España.](https://www.eldiario.es/economia/ine-rasca-masivamente-webs-airbnb-booking-vrbo-viviendas-turisticas-hay-espana_1_6476086.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Internxt, la nube española que apuesta por la privacidad para competir con gigantes como Google, Microsoft o Apple.](https://www.businessinsider.es/internxt-nube-espanola-apuesta-privacidad-google-750951) y que acaba de cerrar [una ronda de inversión de 500k € con una valoración de 3M €.](https://elreferente.es/inversiones/venture-city-ronda-inversion-internxt-100000-euros/)

[Inrupt lanza su versión alpha de los Pod Spaces para developers](https://inrupt.com/hosted-solid-server-alpha).

Mozilla lanza su tradicional ["Privacy not included"](https://foundation.mozilla.org/en/privacynotincluded/). Una guía de compra para saber cómo de "creepy" son nuestros regalos de navidad.

[ProtonMail lanza un nuevo servicio en beta privada, ProtonDrive.](https://protonmail.com/blog/proton-drive-early-access/)

Y tenemos un nuevo podcast sobre privacidad, ["El Rincón del Tío Nuke"](https://www.nukeador.com/podcast/).

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Enero.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

[NGI lanza su incubadora ONTOCHAIN para proyectos del ámbito del blockchain, web semántica y computación descentralizada entre otros.](https://ontochain.ngi.eu/) y que financia entre 118.000 € y 160.000 € a fondo perdido. La convocatoria termina el 15 de Enero de 2021.

Si trabajas en temas relacionados con portabilidad de datos y servicios, [NGI ha publicado una nueva open call de DAPSI, su incubadora para proyectos de este ámbito](https://dapsi.ngi.eu/) y que financia hasta 150.000 € a fondo perdido. La convocatoria termina el 20 de Enero de 2021.

Vuelve nuevamente el programa [NGIatlantic.eu, esta vez con su tercer open call](https://ngiatlantic.eu/ngiatlanticeu-3rd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 26 de Febrero.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.


### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[To track or not to track? Towards privacy-friendly and sustainable online advertising](https://en.panoptykon.org/privacy-friendly-advertising), un estudio de la Panoptykon Foundation.

[Cinco propuestas radicales para las redes](https://elpais.com/ideas/2020-11-14/cinco-propuestas-radicales-para-las-redes.html), por Marta Peirano.

[Gigantes tecnológicos: por dónde empezar a regular](http://agendapublica.elpais.com/gigantes-tecnologicos-por-donde-empezar-a-regular/), por Lucía Velasco.

[Video Analytics User Manuals Are a Guide to Dystopia](https://www.eff.org/deeplinks/2020/11/video-analytics-user-manuals-are-guide-dystopia), un artículo de la Electronic Frontier Foundation.

[Para qué sirve la Carta de Derechos Digitales](https://www.newtral.es/carta-derechos-digitales-pseudonimato-que-es/20201125/) por Marilín Gonzalo.

[Contradicciones y sinsentidos de la Carta de Derechos Digitales del Gobierno](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/espana/20201130/contradicciones-sinsentidos-carta-derechos-digitales-gobierno/539946777_0.html) un análisis de Esther Paniagua.

[Virtual Debate: Interoperability and the EU Digital Markets Act](https://www.openforumeurope.org/event/virtual-debate-interoperability-and-the-digital-services-act/).

[Fitbit. Cómo comercializan nuestros datos de salud y hacen peligrosas predicciones.](https://www.transparentinternet.com/es/tecnologia-y-sociedad/fitbit/) por Manuela Battaglini.


---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
