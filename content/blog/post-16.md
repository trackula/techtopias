---
title: "Las directivas europeas, el monopolio de Facebook y la compra de Fitbit."
date: 2020-12-22T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-16.jpg"

# meta description
description: "Las directivas europeas, el monopolio de Facebook y la compra de Fitbit."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Estados Unidos demanda a Facebook por monopolio](https://elpais.com/economia/2020-12-09/estados-unidos-demanda-a-facebook-por-monopolio.html) e insta a la tecnológica a deshacerse de Instagram y WhatsApp al mismo tiempo que [está siendo investigado por obligar a los usuarios de Oculus a utilizar su cuenta de Facebook.](https://techcrunch.com/2020/12/10/facebook-hit-with-antitrust-probe-for-tying-oculus-use-to-facebook-accounts/)

La propia [Facebook ve WhatsApp como su futuro a pesar de la demanda por monopolio.](https://www.bloomberg.com/news/features/2020-12-09/facebook-fb-plans-to-turn-messaging-app-whatsapp-into-a-moneymaking-business) Mientras tanto, [la tecnológica ha decidido mover a sus usuarios de UK a los términos de uso de California para evitar la regulación de privacidad europea.](https://www.reuters.com/article/britain-eu-facebook-exclusive-idUSKBN28P2HH)

[Diez estados de Estados Unidos acusan a Google de monopolio en el campo de la publicidad digital.](https://www.nytimes.com/2020/12/16/technology/google-monopoly-antitrust.html) Al respecto de esto, en The New York Times se ha publicado un  artículo [sobre las lecciones que Google puede aprender de la experiencia de Microsoft defendiéndose de sus demandas por monopolio.](https://www.nytimes.com/2020/12/17/technology/microsofts-lessons-for-google.html)

[Facebook ataca a Apple con anuncios en periódicos en respuesta a los cambios en materia de privacidad que la compañia está introduciendo en iOS 14.](https://www.theverge.com/2020/12/17/22180102/facebook-new-newspaper-ad-apple-ios-14-privacy-prompt)
Del mismo modo Apple y Google [planean prohibir las aplicaciones que utilicen el X-Mode Tracker que se ha probado que proporcionaban datos a contratistas de defensa de los Estados Unidos.](https://www.macrumors.com/2020/12/09/apple-bans-apps-x-mode-tracker/)

[Si Spotify pierde dinero y los artistas apenas cobran, ¿dónde está la pasta de la música?](https://www.elconfidencial.com/cultura/2020-12-06/spotify-musica-musicos-conciertos-dinero_2861256/)

[Una nueva actualización de Nintendo Switch activa el compartir datos de usuario incluso aunque estuvieran deshabilitados.](https://www.thegamer.com/switch-update-turns-sharing-on/)

[La Agencia de Protección de Datos multa al BBVA con 5 millones de euros por el uso de datos sin consentimiento.](https://www.xataka.com/legislacion-y-derechos/agencia-proteccion-datos-se-pone-seria-multa-record-al-bbva-5-millones-euros-uso-datos-consentimiento)

Telefónica planea vender el cable submarino y [ficha a Société Générale y a Greenhill para vender el cable de su filial Telxius.](https://cincodias.elpais.com/cincodias/2020/11/27/companias/1606511746_793583.html)

[SpaceX obtiene 886 millones de dólares de la Comisión Federal de Comunicaciones para subvencionar a Starlink en 35 estados.](https://arstechnica.com/tech-policy/2020/12/spacex-gets-886-million-from-fcc-to-subsidize-starlink-in-35-states/)

En la sección de ataques informáticos, [Foxconn electronics ha sido atacado con ransomware y solicitado un rescate de 34 millones de dólares.](https://www.bleepingcomputer.com/news/security/foxconn-electronics-giant-hit-by-ransomware-34-million-ransom/)

La semana pasada se ha sabido que múltiples [agencias de los Estados Unidos han sido hackeadas en una campaña de espionaje vinculado a Rusia.](https://www.wsj.com/articles/agencies-hacked-in-foreign-cyber-espionage-campaign-11607897866)

[Huawei ha testeado un software de IA que permite reconocer a minorías Uighur y alertar a la policía.](https://www.washingtonpost.com/technology/2020/12/08/huawei-tested-ai-software-that-could-recognize-uighur-minorities-alert-police-report-says/)

[Uber abandona sus coches autónomos para centrarse en los beneficios.](https://www.elespanol.com/invertia/disruptores-innovadores/innovadores/tecnologicas/20201210/uber-abandona-autonomos-travis-kalanick-problemas-financieros/542196850_0.html)

[Hyundai Motor compra la startup Boston Dynamics por 921 millones de dólares.](https://www.kedglobal.com/newsView/ked202012080011)

Una encuesta revela que [los trabajadores recurren a asistentes digitales y chats como Google Home o Alexa para hacer frente a la salud mental durante la pandemia.](https://www.fastcompany.com/90580177/mental-health-crisis-robots)

[Empresas de marketing digital de UK critican ante la autoridad de competencia la intención de Google de crear una "sandbox de privacidad".](https://techcrunch.com/2020/11/23/digital-marketing-firms-file-uk-competition-complaint-against-googles-privacy-sandbox) La tecnológica que controla en torno al 90% del mercado de los buscadores y navegadores web, [proponía meses atrás cambiar el modelo de cookies por una serie de APIs "pro privacidad y anti-fingerprinting".](https://digiday.com/marketing/wtf-googles-privacy-sandbox//)

En el pasado Techtopías hablábamos del [despido de una de las mayores expertas en ética de la IA por parte de Google que se ha convertido en un escándalo global](https://www.technologyreview.es/s/12953/polemica-en-google-por-despedir-su-codirectora-de-etica-para-la-ia), una polémica que ha llevado al [CEO de la tecnológica a abrir una investigación sobre la misma.](https://www.axios.com/sundar-pichai-memo-timnit-gebru-exit-18b0efb0-5bc3-41e6-ac28-2956732ed78b.html)

[Francia multa a Google con 120 millones de dólares y a Amazon con 42 por injectar cookies de rastreo sin consentimiento.](https://techcrunch.com/2020/12/10/france-fines-google-120m-and-amazon-42m-for-dropping-tracking-cookies-without-consent/)

[Bruselas aprueba la compra de Fitbit por Google con algunos matices](https://www.elconfidencial.com/tecnologia/2020-12-17/google-fitbi-acuerdo-union-europea-compra-datos_2876824/) como separar los datos generados por las pulseras del resto de la empresa y no utilizar dicha información para los anuncios de Google Ads.


### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Semanas atrás el Gobierno de España publicaba una Carta de Derechos Digitales en la cual se hablaba de entre otros sobre [los neuroderechos. La idea es proteger los "procesos cerebrales" de la tecnología abusiva.](https://www.eldiario.es/tecnologia/son-neuroderechos-gobierno-plantea-primera-vez-proteger-procesos-cerebrales-tecnologia-invasiva_1_6478246.html)

Relacionado con esta carta, comienzan a salir las primeras [enmiendas al borrador, como la realizada por Simona Levi.](https://xnet-x.net/enmiendas-borrador-carta-derechos-digitales-espana/)

La secretaria de Estado de Digitalización e Inteligencia Artificial, [Carme Artigas dice que quieren impulsar un código deontológico de los científicos de datos y fomentar la autoregulación en el sector para lograr que los algoritmos no discriminen.](https://elpais.com/tecnologia/2020-12-05/carme-artigas-queremos-impulsar-un-sello-de-calidad-para-la-inteligencia-artificial.html) Una reflexión no exenta de dudas viendo la cantidad de problemas que están generando muchas de estas tecnologías y los pocos incentivos que tienen las empresas para corregirlos.

La noticia de la semana (y del mes), esta vez viene de Europa donde [la Comisión Europea ha propuesto nuevas normas aplicables a las plataformas digitales, la  Digital Services Act (DSA) y la Digital Markets Act (DMA).](https://ec.europa.eu/commission/presscorner/detail/es/ip_20_2347)

La Electronic Frontier Foundation (EFF) se ha lanzado a revisar la [Digital Services Act](https://www.eff.org/deeplinks/2020/12/european-commissions-proposed-regulations-require-platforms-let-users-appeal) y la [Digital Markets Act](https://www.eff.org/deeplinks/2020/12/eus-digital-markets-act-there-lot-room-improvement) y su conclusión en ambas es que pintan bien, pero hay bastante espacio para mejoras.

[Palantir anuncia que se une al proyecto GaiaX para crear un cloud europeo.](https://twitter.com/PalantirTech/status/1339903435672580098?s=09) Un movimiento extraño que ha llevado a muchos expertos a dudar de cómo está gestionando este proyecto la Unión Europea al introducir a una de las mayores empresas de vigilancia y control de datos del mundo, y que ni tan siquiera es europea.

[El Gobierno lanza la primera convocatoria del 'sandbox' en el sector fintech,](https://cincodias.elpais.com/cincodias/2020/12/15/companias/1608041525_122520.html) una noticia que enlaza con la [la Ley de Startups que prepara también el gobierno para el próximo año.](https://www.merca2.es/ley-startup-emprendedores/)

[Google y Microsoft se instalan oficialmente en la educación pública andaluza](https://www.elsaltodiario.com/educacion-publica/google-microsoft-educacion-publica-andalucia), una noticia que se une a otras que hemos visto anteriormente en múltiples comunidades autónomas. Mientras tanto, el colectivo [XNET impulsa un Plan de Digitalización Democrática de la Educación con un primer piloto en Barcelona](https://elpais.com/espana/catalunya/2020-12-10/escuelas-que-buscan-alternativas-a-google.html). En el siguiente enlace [podéis encontrar el comunicado oficial.](https://xnet-x.net/plan-digitalizacion-democratica-educacion-piloto-barcelona/)


[Hacienda se prepara para cobrar la Tasa Google y planea exigir información extra a los gigantes tech.](https://www.lainformacion.com/economia-negocios-y-finanzas/hacienda-exige-striptease-total-gigantes-tech-cobrarles-tasa-google/2822913/)

[Trabajo aprobará la ‘Ley Rider’ antes de fin de año para negociar la contrarreforma laboral en enero](https://www.elespanol.com/invertia/economia/empleo/20201202/trabajo-aprobara-ley-rider-negociar-contrarreforma-laboral/540197341_0.html) al mismo tiempo que se ha sabido que [el Gobierno sucumbe al lobby y elimina el registro de plataformas de dicha ley.](https://www.lainformacion.com/economia-negocios-y-finanzas/gobierno-lobby-registro-plataformas-ley-rider/2822384/)

[El INE compra datos de localización móvil a Orange, Vodafone y Telefónica para saber cuántos turistas vienen a España.](https://www.eldiario.es/economia/ine-compra-datos-localizacion-movil-orange-turistas-vienen-espana_1_6508516.html)

[El gobierno también está preparando una lista de proveedores de 5G "seguros" en lugar de buscar una prohibición directa de Huawei.](https://www.euractiv.com/section/5g/news/spanish-government-to-prepare-a-list-of-safe-5g-mobile-providers/)

Al otro lado del charco, [Kamala Harris se postula como pieza clave entre Silicon Valley y la nueva Casa Blanca.](https://www.elespanol.com/invertia/disruptores-innovadores/america-tech/20201209/kamala-harris-pieza-silicon-valley-casa-blanca/541946376_0.html)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Global Privacy Control](https://globalprivacycontrol.org/), una nueva herramienta que [busca impedir que las webs vendan tus datos personales.](https://www.digitaltrends.com/features/global-privacy-control-internet-data-misuse/)

[You](https://you.com/), un nuevo buscador [que lanza el ex-jefe científico de Salesforce para enfrentarse a Google.](https://techcrunch.com/2020/12/08/former-salesforce-chief-scientist-announces-new-search-engine-to-take-on-google/)

[Signal añade videollamadas cifradas de grupo.](https://signal.org/blog/group-calls/)

[Element analiza la DSA y la interoperabilidad que busca la UE.](https://element.io/blog/interoperability-digital-markets-act/)

[Brave lanza Brave Today, un lector de noticias integrado en el buscador con la privacidad como bandera.](https://brave.com/announcing-brave-today/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Enero.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

[NetGain lanza unas pequeñas becas de entre 25.000 y 100.000 dólares](https://www.netgainpartnership.org/smallgrantsrfp) para proyectos que investigen en el alcance de la desinformación y los daños que provoca, así como intervenciones que funcionen de contrapeso al poder de las plataformas. La convocatoria termina el 11 de Enero de 2021.

[NGI lanza su incubadora ONTOCHAIN para proyectos del ámbito del blockchain, web semántica y computación descentralizada entre otros.](https://ontochain.ngi.eu/) y que financia entre 118.000 € y 160.000 € a fondo perdido. La convocatoria termina el 15 de Enero de 2021.

Si trabajas en temas relacionados con portabilidad de datos y servicios, [NGI ha publicado una nueva open call de DAPSI, su incubadora para proyectos de este ámbito](https://dapsi.ngi.eu/) y que financia hasta 150.000 € a fondo perdido. La convocatoria termina el 20 de Enero de 2021.

Vuelve nuevamente el programa [NGIatlantic.eu, esta vez con su tercer open call](https://ngiatlantic.eu/ngiatlanticeu-3rd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 26 de Febrero.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

IUVIA saca nuevo podcast, [#NadaQueEsconder 05: Menos mal que estamos en Europa, con Xabier Lareo](https://blog.iuvia.io/es/nadaqueesconder-05/).

[La privacidad, en el centro de las tecnologías impulsadas por la pandemia](https://www.newtral.es/privacidad-digital-teconologia-zoom-derechos/20201209/) por Marilín Gonzalo en Newtral.

["Los adolescentes corren un gran riesgo de acabar depredados por las redes sociales"](https://www.elconfidencial.com/tecnologia/2020-12-09/redes-sociales-stories-sociedad-cambios-jovenes_2858391/), entrevista con Iago Moreno en El Confidencial.

[#ForoTelos2020: El dominio mental](https://www.fundaciontelefonica.com/cultura-digital/conferencias/foro-telos2020-dominio-mental/) con Pedro Baños y Marta Peirano.

[Seguridad a pesar del cifrado: el riesgo de las puertas traseras en la UE](https://www.newtral.es/seguridad-a-pesar-cifrado-puertas-traseras-ue/20201216/) por Marilín Gonzalo en Newtral.

[Should We End the Data Economy?](https://open.spotify.com/episode/7BXxzIYLvsiET6n3OaHm87?si=IBaEf-CAQVKDuT-SFN_vcw), un podcast con Carissa Véliz.

[Panel for the Future of Science and Technology](https://multimedia.europarl.europa.eu/en/panel-for-future-of-science-and-technology-annual-lecture-2020_20201209-1500-SPECIAL-STOA_vd), Annual Lecture 2020.

En Trackula estuvimos en [#DistopiasCotias](http://distopiascotias.gal) hablando de derechos digitales. [Conversa aberta sobre a "Carta de Dereitos Dixitais"](https://www.youtube.com/watch?v=EbAt8JnCR2M&feature=youtu.be).

[Europa regula cómo será Internet de las próximas décadas. Sobre la nueva Directiva de Servicios Digitales (DSA)](http://libertadinformacion.cc/organizaciones-y-expertos-valoran-positivamente-que-la-comision-europea-deje-a-salvo-la-libertad-de-expresion-en-internet-en-su-propuesta-de-regulacion-de-las-plataformas/) en la Plataforma en Defensa de la Libertad de Información.


### Un último apunte 📝

Como sabéis, hace unas semanas la Secretaría de Estado de Digitalización e Inteligencia Artificial publicaba la [Carta de Derechos Digitales](https://portal.mineco.gob.es/es-es/ministerio/participacionpublica/audienciapublica/Paginas/SEDIA_Carta_Derechos_Digitales.aspx) para su consulta pública, abierta a propuestas de la sociedad.

Fue entonces cuando en Trackula recibimos la llamada de la asociación [Interferencias](https://interferencias.tech/) que nos contactaban con el fin de elaborar una respuesta conjunta entre ambas asociaciones a dicha carta, y a ello nos hemos puesto.

[En este enlace](https://trackula.org/es/post/2020-12-20-carta-derechos-digitales/) podeis ver el documento al completo, esperamos que sea de vuestro agrado, aunque ya sabemos que se nos ha quedado un pelín largo 😇.

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
