---
title: "La Carta de Derechos Digitales - Especial de Reyes."
date: 2021-01-05T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-17.jpg"

# meta description
description: "La Carta de Derechos Digitales - Especial de Reyes."

# post type
type: "post"
---

Aprovechando las navidades, hoy pensábamos cogernos el día libre para tomarnos el roscón de Reyes 👑 con calma, pero nos ha parecido más interesante hacer una versión un pelín diferente de Techtopias, esta vez enfocada en una temática en concreta.

Como ya hemos comentado, hace un par de semanas publicamos nuestra respuesta junto con la asociación [Interferencias](https://interferencias.tech/), a la consulta pública de la *"Carta de Derechos Digitales"* que ha impulsado la SEDIA (Secretaría de Estado de Digitalización e Inteligencia Artificial).

Es por eso que hoy queremos haceros un resumen un poco mas "human-friendly" que el documento final, que es un pelín denso, pero que por supuesto también os adjuntamos por si queréis profundizar un poco más.

Antes de todo nos gustaría agradecer a la SEDIA y al grupo de expertos que ha participado en su redacción, por la existencia de esta carta, y sobre todo por el proceso participativo de consulta pública posterior. Ojalá sea el comienzo de más acciones similares 😍

## ¿Cómo leer el documento?
Si os animáis a echarle un ojo al documento, os recomendaríamos que escojáis las temáticas que os interesen en el índice, o que comencéis leyendo los resumenes y las propuestas de cada sección para decidir si os interesa profundizar un poco más sobre los motivos que nos han llevado a esas propuestas concretas.

[En este enlace podéis descargar el documento completo.](https://trackula.org/es/post/2020-12-20-carta-derechos-digitales/Respuesta_Carta_Trackula_Interferencias.pdf)

## La forma
En cuanto a la Carta, nos da bastante pena que no profundice más y se quede en una lista de deseos sin apenas concrección, creemos que se debería trabajar en propuestas que traigan soluciones, no solo deseos abstractos.

También echamos de menos derechos ya existentes que no se han mencionado, como el derecho al olvido u otros temas que tampoco se mencionan y que a nosotros nos parecen imprescindibles. A continuación veremos algunos ejemplos:

## Cosas que echamos de menos

### Software libre 💻
En la carta ni se menciona el **software libre**, imprescindible si hablamos de soberanía tecnológica y sobre todo de transparencia. Creemos que la iniciativa [Public Money, Public code](https://publiccode.eu/es/) debería ser la base sobre la que construir nuestros derechos. Fomentando el compartir desarrollos entre administraciones para ahorrar costes y permitir centrarnos en innovar en lugar de reinventar la rueda en cada institución individual.

<img src="/images/post/post-17/softwarelibre.png" width="600px" style="display: block; margin: auto;"/>

### Open Data y transparencia 📊
El **Open Data** también está ausente en la carta, mientras que los apartados sobre **transparencia** se quedan cortos y poco concretos. Grupos como [Civio llevan años litigando para tratar de obtener el código del programa del bono social eléctrico](https://civio.es/novedades/2020/09/03/codigo-fuente-radar-covid-transparencia-bono-social/), esta era una buena ocasión para cambiar de rumbo.

En cuanto al **Open Data** se podría profundizar mucho, pero creemos que hay temas básicos que al menos se deberían abordar:

<img src="/images/post/post-17/opendata.png" width="600px" style="display: block; margin: auto auto 10px auto;"/>

En **transparencia**, creemos que los algoritmos de decisión son parte del procedimiento administrativo por lo que es fundamental que cualquier ciudadano pueda conocer cómo funcionan.

<img src="/images/post/post-17/transparencia.png" width="600px" style="display: block; margin: auto;"/>

### IA y reconocimiento biométrico 🕵️‍♂️
En la carta se habla también de derechos con respecto a la IA, pero quizás esta definición se queda corta ya que debería cubrir a las decisiones automatizadas en general. Además creemos que el **reconocimiento biométrico** y en concreto el facial, puede cambiar tanto nuestra sociedad que debería tener sección propia, aquí alguna de nuestras propuestas.

<img src="/images/post/post-17/biometria.png" width="600px" style="display: block; margin: auto;"/>

### Cifrado end-to-end 🔒
El **cifrado end-to-end** también es un tema a debate a nivel internacional ya que, como ya hemos comentado en Techtopias, [se está debatiendo la posibilidad de limitarlo o introducir puertas traseras para acceder al contenido](https://www.xatakamovil.com/mercado/europa-cifrado-extremo-a-extremo-ue-plantea-resolucion-para-tener-acceso-a-mensajes).
Creemos que nuestro país debe mojarse y apoyarlo sin contemplaciones, en la carta ni se menciona.

Si quereis saber más sobre esto, [EDRI ha publicado un documento muy facil de leer en el que cuenta su punto de vista](https://www.edri.org/files/20160125-edri-crypto-position-paper.pdf). Nosotros proponemos que se garantice el cifrado end-to-end como un derecho mas.

<img src="/images/post/post-17/cifrado.png" width="600px" style="display: block; margin: auto;"/>

### Cultura 📔
La carta se centra en la defensa de la cultura y en la correcta remuneración de sus autores, que es un tema importante, pero también lo es el garantizar acceso abierto a la misma. Se debe fomentar el generar obras de **cultura libre** y promover la publicación científica en **Open Access**, así como contratar material docente con licencias libres.

<img src="/images/post/post-17/cultura.png" width="600px" style="display: block; margin: auto;"/>

### Salud mental y adicción 😰
Los nuevos modelos de negocio de la economía de la atención están afectando profundamente a la **salud mental** de mayores y niños, generando [enfermedades mentales](https://jamanetwork.com/journals/jamapsychiatry/fullarticle/2749480), conductas adictivas, [déficit de atención](https://jamanetwork.com/journals/jama/article-abstract/2687861) o incluso depresión, por lo que creemos que se debería actuar también sobre ellos.

<img src="/images/post/post-17/salud.png" width="600px" style="display: block; margin: auto;"/>

### Neurotecnologías 🧠
Otro punto interesante introducido en la carta, son las **neurotecnologias**, un tema que aún no está apenas en la esfera pública pero en el que desde hace años se está avanzando a gran velocidad.

Aunque celebramos que se introduzcan propuestas de derechos sobre estas tecnologías, creemos que se puede profundizar un poco más, tratando de proponer soluciones que ayuden a reducir las desigualdades en su uso y a abrir un debate público sobre su conveniencia.

<img src="/images/post/post-17/neurotecnologias.png" width="600px" style="display: block; margin: auto auto 10px auto;"/>


En definitiva, tras un mes de trabajo con los compañeros de Interferencias que nos han liado para esta, esperamos que nuestra respuesta pueda servir de ayuda, no solo de cara a posibles cambios en la redacción de la carta, sino también a para posibles futuras legislaciones.

Esperamos que sea de vuestro agrado y no dudéis en enviarnos feedback 😁 por [Twitter](https://twitter.com/trackula_) o correo electrónico 📨 en **info (at) trackula.org**.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
