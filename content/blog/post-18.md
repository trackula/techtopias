---
title: "Trump baneado, la huida de Whatsapp y el primer sindicato en Google."
date: 2021-01-19T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-18.jpg"

# meta description
description: "Trump baneado, la huida de Whatsapp y el primer sindicato en Google."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[No quieren tu trabajo, quieren tus datos. El negocio oculto de muchos pequeños buscadores de empleo.](https://elpais.com/tecnologia/2020-12-18/no-quieren-tu-trabajo-quieren-tus-datos-el-negocio-oculto-de-muchos-pequenos-buscadores-de-empleo.html)

La llegada de las vacunas para el COVID-19 ha generado una oportunidad para las empresas especializadas en análisis de datos que se han lanzado a crear software para gestionar dicha vacunación, como es el caso de [Palantir cuyos algoritmos opacos pueden ayudar también a afianzar las desigualdades en los sistemas de salud.](https://venturebeat.com/2020/12/19/covid-19-vaccine-distribution-algorithms-may-cement-health-care-inequalities/)

De hecho, en múltiples lugares se han utilizado algoritmos para decidir a quién vacunar primero, [un ejemplo es el caso de los sanitarios de Stanford cuyo resultado ha sido desastroso.](https://www.elconfidencial.com/tecnologia/2021-01-03/sanitarios-stanford-vacunacion-algoritmos-injustos_2890104/)

[El FMI propone que los bancos revisen el historial web de los clientes que pidan un crédito](https://www.20minutos.es/noticia/4518216/0/fmi-propone-bancos-revisen-historial-web-clientes-pidan-credito/) con el fin de calcular de forma mas precisa su calificación crediticia.

[Google continua acumulando demandas antimonopolio, esta vez encabezada por el estado de Colorado.](https://www.technologyreview.es/s/12994/google-suma-y-sigue-tercera-demanda-antimonopolio-en-dos-meses)

Semanas atrás hablábamos sobre [la salida de la experta en IA Timnit Gebru’s de Google,](https://www.wired.com/story/timnit-gebru-exit-google-exposes-crisis-in-ai/#intcid=_wired-bottom-recirc_ed9366a2-a216-433d-9503-8e452040ba70_aff-user-entity-topic-similarity-v2) y que ha creado un gran revuelo en el sector, al mismo tiempo que la tecnológica [se ha lanzado a pedir a sus científicos que utilicen 'un tono positivo' en los documentos que publiquen sobre sus investigaciones sobre Inteligencia Artificial.](https://www.reuters.com/article/us-alphabet-google-research-focus-idUSKBN28X1CB)

También relacionado con Google, esta semana se publicaba que los [trabajadores de la empresa se están organizando en el que será el primer sindicato de una gran tecnológica](https://elpais.com/economia/2021-01-05/trabajadores-de-google-se-organizan-en-el-primer-sindicato-creado-en-una-gran-tecnologica.html).
Descifrando la Guerra ha publicado [un hilo en twitter](https://twitter.com/descifraguerra/status/1346878277898674179?s=20) con enlaces muy interesantes al respecto y también parece que [el sindicado se centrará principalmente en influir en cuestiones políticas y sociales.](https://www.wired.com/story/googles-new-union-addressing-political-issues/)

El criptógrafo Bruce Schneier [ha publicado un artículo sobre el masivo ciberataque que ha sufrido Estados Unidos y sobre sus enormes proporciones.](https://www.theguardian.com/commentisfree/2020/dec/23/cyber-attack-us-security-protocols) Una noticia que saltaba semanas atrás cuando [EE.UU. descubría que dicha inflitración en sus redes llegaba a un nivel inimaginado.](https://www.lavanguardia.com/internacional/20201219/6134535/ciberataque-eeuu-hackers-pirateo-redes-operacion.html)

[Un grupo de trabajadores consigue evitar el uso obligatorio de reconocimiento facial para fichar, gracias a la ayuda de "Big Brother Watch".](https://bigbrotherwatch.org.uk/2020/12/workers-fighting-compulsory-facial-recognition-with-big-brother-watch-and-winning/)

[Continúan los arrestos y encarcelamientos debido al mal funcionamiento del reconocimiento facial](https://www.nytimes.com/2020/12/29/technology/facial-recognition-misidentify-jail.html) mientras que diversas empresas como [NEC avanzan en sus algoritmos para que el uso de mascarillas no sea un impedimiento para reconocer facialmente a ciudadanos.](https://www.reuters.com/article/us-health-coronavirus-japan-facial-recog/masks-no-obstacle-for-new-nec-facial-recognition-system-idUSKBN29C0JZ?il=0)

Estas tecnologías se están expandiendo rápidamente por todo tipo de estaciones de tren y aeropuertos en los últimos años, [en este artículo se habla de cómo se puede hacer "Opt Out" de este reconocimiento en Estados Unidos.](https://www.cntraveler.com/story/how-to-opt-out-of-facial-recognition-at-the-airport)

[AT&T planea descuentos en sus smartphones a cambio de aceptar anuncios personalizados](https://arstechnica.com/tech-policy/2020/09/att-wants-to-put-ads-on-your-smartphone-in-exchange-for-5-discount/) y según parece, desde 2015 ha estado cobrando entre $29 y $60 extra por mantener la privacidad de sus clientes.

[Fiscales federales acusan a Zoom de haber trabajado con el gobierno chino para vigilar a usuarios y suprimir videollamadas.](https://www.washingtonpost.com/technology/2020/12/18/zoom-helped-china-surveillance/)

Privacy International descubre que [varias apps para controlar los ciclos de menstruación, almacenan demasiada información.](https://www.theguardian.com/society/2020/dec/21/menstruation-apps-store-excessive-information-privacy-charity-says)

[Hackers cierran un cinturon de castidad conectado a internet y piden un rescate.](https://www.vice.com/en/article/m7apnn/your-cock-is-mine-now-hacker-locks-internet-connected-chastity-cage-demands-ransom) Afortunadamente la persona que sufrió el ataque no estaba utilizando dicho cinturón en ese momento pero añade nuevas dudas sobre la necesidad de conectar todo a la red.

[Twitter, Facebook e Instagram bloquean la cuenta de Trump tras el asalto al Capitolio](https://verne.elpais.com/verne/2021/01/07/mexico/1609980198_796734.html). Así mismo, algunas redes sociales como [Facebook han estado eliminando contenido en el que se mencionaba ‘Stop the Steal’.](https://www.wsj.com/articles/facebook-says-it-is-removing-all-content-mentioning-stop-the-steal-11610401305)

Por todo esto muchos de los seguidos de Trump han comenzado su paso a la red social Parler, conocida por no censurar el discurso de odio. Ante este rápido crecimiento de Paler, [Amazon ha decidido dejar de dar servicio a la red social que podría verse obligada a cerrar en los próximos días.](https://www.businessinsider.es/parler-podria-cerrar-permanentemente-ser-bloqueada-amazon-790757) Otras empresas como [Google o Apple también han eliminado sus aplicaciones de sus app stores.](https://techcrunch.com/2021/01/08/parler-removed-from-google-play-store-as-apple-app-store-suspension-reportedly-looms/)

Ante estas noticias, múltiples grupos han expresado su opinión sobre la censura y el control que las tecnológicas tienen sobre internet, como es el caso de [Access Now](https://mobile.twitter.com/accessnow/status/1349459984225087491). También es interesante reflexionar sobre quién está detrás de [Parler, cuyos principales inversores, la familia Mercer, han sido también fundadores de Cambridge Analytica.](https://apnews.com/article/64280ae1f30c44eb8b82049f6875dddd)

Hugo Sáez ha publicado un [hilo sobre cómo hemos llegado a este punto de radicalización en la sociedad](https://mobile.twitter.com/Hugo_saez/status/1347164593186099216) y que en gran parte ha sido culpa de las grandes empresas tecnológicas que ahora han empezado a tomar alguna medida, claramente tarde.

Debemos abrir el debate sobre [quién debe crear y hacer cumplir las leyes online.](https://www.nytimes.com/2021/01/11/technology/twitter-facebook-parler-rules.html)

WhatsApp ha anunciado nuevas nuevas condiciones de privacidad que han llevado a una gran migración de usuarios a otras apps de mensajería alternativas, por lo que la empresa propiedad de Facebook, [ha decidido retrasar el lanzamiento de sus nuevas condiciones para aclarar la "desinformación" al respecto.](https://www.xataka.com/privacidad/whatsapp-aplaza-entrada-vigor-nuevas-condiciones-privacidad-para-aclarar-desinformacion-al-respecto)

Como menciona [este artículo de Wire, WhatsApp ha estado compartiendo tus datos con Facebook durante años.](https://www.wired.com/story/whatsapp-facebook-data-share-notification/)

Maldita nos aclara todas las novedades de [la nueva Política de Privacidad de la app de mensajería: para usuarios europeos no hay grandes cambios, tampoco para los datos que ya compartía con Facebook.](https://maldita.es/malditatecnologia/20210111/nueva-politica-privacidad-condiciones-whatsapp/)

Alternativas como [Signal se han visto desbordadas temporalmente](https://twitter.com/signalapp/status/1350165610936766464) al mismo tiempo que [Telegram ha anunciado que comenzará a introducir publicidad en sus canales el próximo año.](https://www.engadget.com/telegram-channel-ads-192249765.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[España mantiene la segunda posición de la Unión Europea en datos abiertos](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/europa/20201231/espana-mantiene-segunda-posicion-union-europea-abiertos/546945773_0.html) según el Open Data Maturity Report 2020.

[El gobierno busca desbloquear la 'ley rider' con una propuesta rebajada y basada en sentencias,](https://www.elconfidencial.com/espana/2021-01-07/ley-rider-propuesta-rebajada-basada-en-sentencias_2896040/) descartando que las plataformas digitales tengan que hacer públicos sus algoritmos.

[Facebook se pone al día con Hacienda y paga una cifra récord de 34 millones](https://www.lainformacion.com/empresas/facebook-pone-dia-hacienda-cifra-record-inspeccion/2824431/), una regularización por la que anteriormente han pasado Google, Microsoft o Amazon. Mientras tanto [Google decide transladar 34.000 millones de euros de beneficios al paraíso fiscal de las Bermudas para "tributar al 0%".](https://www.eldiario.es/economia/google-llevo-34-000-millones-euros-paraiso-fiscal-bermudas-tributar-0_1_6628894.html)

[El Tribunal Superior de Viena falla que Facebook puede "pasar por alto" el consentimiento de la GDPR, pero debe dar acceso a los datos](https://noyb.eu/en/vienna-superior-court-facebook-can-bypass-gdpr-consent-must-give-access-data), una decisión controvertida que será llevada a más altas instancias del poder judicial.

Tras el fin del "Privacy Shield", [las tecnológicas españolas arremeten contra las nuevas medidas de protección de datos de la UE](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/europa/20201221/tecnologicas-arremeten-nuevas-medidas-proteccion-datos-ue/545195970_0.html).

[El apoyo secreto de Europa a las tecnologías de reconocimiento biométrico.](https://www.theguardian.com/world/2020/dec/10/sci-fi-surveillance-europes-secretive-push-into-biometric-technology)

["Donde dije digo...": Singapur dará acceso a los datos recogidos por su aplicación de rastreo del COVID-19 a sus cuerpos de seguridad para investigaciones criminales](https://www.xataka.com/privacidad/donde-dije-digo-singapur-dara-acceso-a-datos-covid-19-a-sus-cuerpos-seguridad-para-investigaciones-criminales)

[¿Por qué Joe Biden no podrá llevar su bicicleta para hacer ejercicio a la Casa Blanca?](https://www.independentespanol.com/noticias/joe-biden-bicicleta-ejercicio-casa-blanca-b1788197.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

La startup IUVIA se presenta como ["el invento gallego para no volver a usar Google y proteger todos tus datos en internet"](https://www.elconfidencial.com/tecnologia/2021-01-11/iuva-empresa-gallega-datos-google-amazon_2897755/).

Tim Berners-Lee está trabajando para que [las personas puedan mantener el control de su información personal gracias a la tecnología que está desarrollando Inrupt](https://www.nytimes.com/2021/01/10/technology/tim-berners-lee-privacy-internet.html). La compañía ha levantando $20M de venture capital.

[Proton Calendar beta ya está disponible para teléfonos Android.](https://protonmail.com/blog/calendar-android-beta/)

Mozilla ha publicado un white paper sobre los retos y las oportunidades que existen alrededor de la Inteligencia Artificial titulado ["Creating Trustworthy AI"](https://foundation.mozilla.org/en/insights/trustworthy-ai-whitepaper/).

Amazon, una de las mayores librerías y editoriales, se niega a dejar que las bibliotecas presten cualquier libro electrónico que publiquen o creen, por ello Fight for the Future lanza la campaña ["Stop Amazon's War on Libraries!"](https://actionnetwork.org/petitions/amazon-let-libraries-have-books).

EDRI organiza la [PrivacyCamp](https://privacycamp.eu/) el próximo 26 de Enero cuyo tema central es el reclamar las infraestructuras digitales y reparar el futuro a través de los derechos digitales.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

NGI lanza una nueva open call, [NGI Assure](https://ngi-assure-opencall.fundingbox.com/pages/Guide%20for%20Applicants), con el fin de financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Febrero de 2021.

Si trabajas en temas relacionados con portabilidad de datos y servicios, [NGI mantiene abierta la open call de DAPSI, su incubadora para proyectos de este ámbito](https://dapsi.ngi.eu/) y que financia hasta 150.000 € a fondo perdido. La convocatoria termina el 20 de Enero de 2021.

También continua el programa [NGIatlantic.eu, esta vez con su tercer open call](https://ngiatlantic.eu/ngiatlanticeu-3rd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 26 de Febrero.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) mantiene su convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Nada que Esconder 06: El poder de una potencia media con Andrea G. Rodríguez](https://blog.iuvia.io/es/nadaqueesconder-06/), una nueva edición del podcast de IUVIA.

El Enemigo Anónimo publica un nuevo video, [¿son un peligro los dispositivos de Internet of Things? Lo bueno, lo malo y lo terrible.](https://www.elenemigoanonimo.com/internet-of-things-iot-peligro-ciberseguridad-dispositivos-conectados/)

[The Peril of Persuasion in the Big Tech Age](https://foreignpolicy.com/2020/12/11/big-tech-data-personal-information-persuasion/) por Bruce Schneier y Alicia Wanless.

[La dependencia de proveedores tecnológicos (o por qué la caída de Google nos hizo temblar)](https://maldita.es/malditatecnologia/20201216/dependencia-proveedores-tecnologicos-caida-google/) via Maldita.

[Redes sociales ¿Qué le pasa a tu cerebro si dejas Facebook?](https://retina.elpais.com/retina/2019/02/11/talento/1549885132_534670.html) por Rebeca Gimeno en El País Retina.



---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
