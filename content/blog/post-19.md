---
title: "Apple liderando la privacidad, el Euro digital y el crecimiento de los deepfakes."
date: 2021-02-02T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-19.jpg"
# meta description
description: "Apple liderando la privacidad, el Euro digital y el crecimiento de los deepfakes."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Jack Dorsey defiende el veto a Trump, pero reconoce que Twitter ha fallado en promover una conversación saludable y cree que sienta un “peligroso precedente”.](https://elpais.com/tecnologia/2021-01-14/el-fundador-de-twitter-defiende-el-veto-a-trump-pero-admite-que-sienta-un-peligroso-precedente.html)

[Filtran los datos sobre la vacuna de la Covid-19 robados en un hackeo a la Agencia Europea de Medicamentos en diciembre](https://www.europapress.es/portaltic/ciberseguridad/noticia-filtran-datos-vacuna-covid-19-robados-hackeo-agencia-europea-medicamentos-diciembre-20210114155259.html).

[El Banco de la Reserva de Nueva Zelanda sufre una filtración de datos críticos](https://unaaldia.hispasec.com/2021/01/banco-reserva-nueva-zelanda-sufre-filtracion-datos.html), se desconoce el número de clientes que han sido afectado y se han desconectado los sistemas como medida de mitigación temporal.

[Multa histórica a Grindr de casi 10 millones de euros](https://www.businessinsider.es/grindr-multada-10-millones-brecha-datos-usuarios-798273) por compartir datos de sus usuarios, etiquetados según su orientación sexual, a 5 plataformas publicitarias.

[El Partido Conservador del Reino Unido ha almacenado ilegalmente datos sobre la etnia de mas de 10 millones de votantes.](https://www.theguardian.com/technology/2021/jan/26/conservative-party-illegally-collected-data-on-ethnicity-of-10m-voters-mps-told)

Estas semanas veíamos cómo Lola Flores cobraba vida de nuevo en un anuncio televisivo, una anécdota curiosa que esconde [un peligro para los ciudadanos y para la democracia, de la mano de los vídeos falsos hiperrealistas.](https://elpais.com/tecnologia/2021-01-30/mas-alla-de-lola-flores-el-peligro-para-los-ciudadanos-y-para-la-democracia-de-los-videos-falsos-hiperrealistas.html)

Estas semanas también eran publicados nuevos avances en [DALL·E, una versión del algoritmo GPT-3, entrenada para generar imagenes a partir de texto.](https://openai.com/blog/dall-e/)

[La aplicación de chat Element ha sido suspendido en Google Play Store durante un día por "violar sus términos de uso".](https://element.io/blog/element-on-google-play-store/)

[Amazon y cinco grandes editoriales acusados de fijar precios en la venta de ebooks.](https://www.theguardian.com/books/2021/jan/15/amazoncom-and-big-five-publishers-accused-of-ebook-price-fixing)

[Spotify registra una patente para utilizar grabaciones de nuestra voz y del ruido de fondo con el fin de conocer nuestro estado de ánimo y recomendarnos música.](https://www.genbeta.com/actualidad/spotify-esta-interesada-usar-grabaciones-nuestra-voz-ruido-fondo-para-decidir-que-musica-recomendar)

Tras la llegada de la [App Tracking Transparency](https://www.applesfera.com/seguridad/apple-anuncia-llegada-att-a-principios-primavera-facebook-lanza-sus-quejas), divesas entidades como [Mozilla](https://foundation.mozilla.org/en/blog/thank-you-apple-for-standing-strong-for-privacy/) han felicitado a Apple por la medida, mientras que personas como Enrique Dans también han publicado su opinión [sobre la privacidad y la necesidad continuar este camino para aumentar la transparencia y la información.](https://www.enriquedans.com/2021/01/sobre-la-privacidad-y-la-informacion.html)

Aprovechando el Día Internacional de la Privacidad de los Datos, [Tim Cook ha comentado en una entrevista que la privacidad y el cambio climático son los problemas más importantes del siglo.](https://www.fastcompany.com/90599049/tim-cook-interview-privacy-legislation-extremism-big-tech)

Al mismo tiempo, [Facebook preara un litigio contra Apple por su "injusto" enfoque sobre la privacidad y las aplicaciones por defecto en sus teléfonos.](https://www.macrumors.com/2021/01/28/facebook-preparing-antitrust-lawsuit-against-apple/)

Curiosamente esta semana se lanzaba un [comunicado interno en Facebook pidiéndole a sus empleados hacerlo mejor en el campo de la privacidad.](https://onezero.medium.com/the-big-shift-internal-facebook-memo-tells-employees-to-do-better-on-privacy-92cee35ba560)

Todo mientras la tecnológica se enfrenta a un nuevo caso en el que [WhatsApp puede ser multado con 50M € por no cumplir la normativa de protección de datos europea.](https://www.politico.eu/article/whatsapp-privacy-fine-data-protection-europe-50-million/)

Mientras tanto, algunos de sus competidores como [Signal, supera los 50 millones de descargas en Android, y Telegram pasa de los 500 millones de usuarios. Sí, la privacidad importa](https://www.genbeta.com/mensajeria-instantanea/signal-supera-50-millones-descargas-android-telegram-pasa-500-millones-usuarios-privacidad-importa)

[Google está investigando a otra investigadora en la ética de la IA](https://www.axios.com/scoop-google-is-investigating-the-actions-of-another-top-ai-ethicist-50030739-ea3d-4ea2-b452-c228b4fc9773.html). Según una fuente, esta investigadora habría estado usando scripts automatizados para revisar sus mensajes y encontrar ejemplos que mostraran un trato discriminatorio hacia Timnit Gebru, despedida semanas atrás, antes de que su cuenta fuera bloqueada.

[El plan de Google Chrome para reemplazar las cookies, está siendo investigado en UK.](https://www.bbc.com/news/technology-55219750) Al mismo tiempo, la Unión Europea [pone en duda sus prácticas en el mercado de la publicidad digital por ir en contra de la competencia.](https://www.euractiv.com/section/digital/news/googles-advertising-practices-targeted-by-eu-antitrust-probe/)

Hace un par de semanas comentábamos que Google se planteaba abandonar Australia motivado por una nueva ley que le obliga a pagar a los medios de comunicación por enlazar sus contenidos, [aquí tenemos un artículo que lo analiza más en profunidad](https://www.bbc.com/mundo/noticias-55766339)

Con la llegada del nuevo presidente Joe Biden, ¿qué pasará con las Big Tech? [Este artículo nos da una nueva visión, "Silicon Valley Takes the Battlespace"](https://prospect.org/power/silicon-valley-takes-battlespace-eric-schmidt-rebellion/).

[El Tribunal Superior del Reino Unido sostiene que los servicios de seguridad e inteligencia ya no pueden depender de "garantías generales" para interferir en la propiedad privada, incluidos los ordenadores.](https://edri.org/our-work/pi-uk-high-court-general-warrants-government-hacking/)

[Europa impuso 159 millones de euros en multas por vulnerar el RGPD en 2020, casi la mitad de todas las sanciones propuestas desde que está en vigor.](https://www.businessinsider.es/multas-europa-vulnerar-rgpd-dispararon-2020-793945)

[Italia ordena a Tik Tok bloquear perfiles de dudosa edad tras la muerte de una niña](https://www.lavanguardia.com/tecnologia/20210123/6193291/italia-tiktok-bloquear-perfiles-muerte-nina.html). Las cuentas de los usuarios de los que no se tenga información sobre su edad serán canceladas al menos hasta el 15 de febrero.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Amazon carga sobre las pymes españolas la tasa digital del 3% aprobada por el Gobierno](https://www.elconfidencial.com/empresas/2021-01-22/amazon-carga-3-impuesto-digital-vendedores_2917512/), la conocida popularmente como "tasa Google".

El Gobierno de España ha publicado su [Plan de  Digitalización de las Administraciones Públicas 2021 -2025](https://www.lamoncloa.gob.es/presidente/actividades/Documents/2021/270121-PlanDigitalizacionAdministracionesOptimizado.pdf), un plan que ha generado múltiples reacciones y dudas.

[El Ministerio de Trabajo propone sanciones automáticas a través de un algoritmo para evitar el fraude laboral.](https://www.lapoliticaonline.es/nota/86694-cerco-de-yolanda-diaz-a-los-empresarios-sanciones-automaticas-a-traves-de-un-algoritmo/)

[Caos en Madrid con la educación 'online'](https://www.elconfidencial.com/tecnologia/2021-01-16/educamadrid-david-calle-online-google-colegios-filomena_2909651/) ante la prohibición por parte de la Comunidad de utilizar plataformas no oficiales fuera de EducaMadrid. Según parece el aumento de tráfico en la plataforma ha generado problemas de servicio que están siendo solucionados con una nueva inversión en infraestructuras y mantenimiento.

[RadarCOVID solo ha registrado el 2% de todos los contagios de coronavirus en España durante los últimos 4 meses, cuando empezó a funcionar](https://www.businessinsider.es/radarcovid-fracaso-solo-4-meses-despues-lanzamiento-793255).

Estas semanas se ha comenzado a hablar del [euro digital, que levanta gran expectación, con record de consultas al BCE.](https://www.eleconomista.es/mercados-cotizaciones/noticias/10988974/01/21/Ciudadanos-y-empresas-piden-al-BCE-que-el-euro-digital-cuente-con-privacidad-en-los-pagos.html)

Para saber un poco más sobre [qué es el euro digital y por qué quiere introducirlo ahora el BCE, aquí tenemos más información.](https://www.eleconomista.es/economia/noticias/10838221/10/20/Que-es-el-euro-digital-y-por-que-quiere-introducirlo-ahora-el-BCE.html)

[Europa financió una 'máquina de la verdad' y prepara una base de datos con información biométrica de 400 millones de personas para proteger sus fronteras](https://www.businessinsider.es/tecnologias-vigilancia-europa-deben-preocuparte-ya-799627).

[La Comisión Europea presiona a las plataformas para desmonetizar la desinformación.](https://www.euractiv.com/section/digital/news/eu-commission-presses-platforms-to-de-monetise-disinformation/)

[La Comisión Europea confirma su multa de 7.8 millones de euros a Valve y a otras cinco productoras de videojuegos por bloquear geográficamente el acceso a sus contenidos.](https://ec.europa.eu/commission/presscorner/detail/en/ip_21_170)

Naciones Unidas revela que sus altos cargos tienen prohibido usar WhatsApp, [¿por qué?](https://elpais.com/tecnologia/2020/01/31/actualidad/1580429952_417173.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

Twitter lanza [Birdwatch, una aproximación basada en la comunidad para luchar contra la desinformación.](https://blog.twitter.com/en_us/topics/product/2021/introducing-birdwatch-a-community-based-approach-to-misinformation.html)

Nextcloud publica un estudio en el que dice que [sobre el 70% de las grandes empresas están migrando aplicaciones de vuelta desde los clouds "públicos" a versiones "on-premise" por sus dudas de seguridad y coste.](https://nextcloud.com/blog/over-70-of-enterprises-moving-applications-back-on-premises-among-security-and-cost-concerns/) El estudio habla de que el crecimiento de estos clouds "públicos" se está ralentizando y se tiende a un futuro híbrido entre públicos y privados.

[ProtonMail analiza la DMA que podría ayudar a crear un campo de juego más equitativo.](https://protonmail.com/blog/digital-markets-act-explained/)

[Mastodon, la red social que no censura porque funciona como un estado federado](https://www.vozpopuli.com/economia-y-finanzas/mastodon-red-social_0_1430257729.html).

Eticas Consulting, RepScan y IUVIA, [tres ideas de negocio que ponen transparencia y cordura en Internet.](https://www.emprendedores.es/ideas-de-negocio/ideas-transparencia-internet/)

["Mine" permite descubrir donde se encuentran nuestros datos personales y gestionar nuestra huella digital.](https://saymine.com/) Un proyecto muy interesante pero que me ha hecho reflexionar al ver quienes son [sus inversores](https://www.crunchbase.com/organization/saymine-technologies-mine/company_financials), entre los que se encuentra Gradient Ventures (Google).

[Wikipedia cumple 20 años, aquí tenemos su historia.](https://onezero.medium.com/an-oral-history-of-wikipedia-the-webs-encyclopedia-1672eea57d2)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGI Ledger busca proyectos para financiar herramientas "privacy-by-design" y aquellas que trabajen en el campo de los datos distribuidos](https://ledger-3rd-open-call.fundingbox.com/) con hasta 150K €. El plazo se encuentra abierto hasta el 22 de Febrero de 2021.

[NGIatlantic.eu, continua abierto en su tercer open call](https://ngiatlantic.eu/ngiatlanticeu-3rd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 26 de Febrero.

NGI también ha abierto el programa [TRUBLO](https://www.trublo.eu/apply), que busca finaciar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 19 de Marzo.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) mantiene su convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta.

Un lugar interesante a revisar de vez en cuando es [la web general de NGI que agrupa las múltiples acciones](https://www.ngi.eu/ngi-projects/) que la Unión Europea está lanzando en el campo de la soberanía tecnológica.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[The Coup We Are Not Talking About](https://www.nytimes.com/2021/01/29/opinion/sunday/facebook-surveillance-society-technology.html) por Shoshana Zuboff.

[What citizens of the world say on the future of the Internet](https://wetheinternet.org/wp-content/uploads/2020/12/WTI-final-results-report-v3e.pdf), un estudio de "We, the internet".

[Inteligencia artificial realmente inteligente](https://efectocolibri.com/gemma-galdon/) una entrevista con Gemma Galdón.

[“La privacidad es colectiva, como el medioambiente. Si no cuidas tus datos, otros sufren las consecuencias”](https://retina.elpais.com/retina/2020/11/27/talento/1606484799_921538.html), una entrevista con Carissa Véliz.

[Gigantes tecnológicos: por dónde viene la regulación](https://agendapublica.es/gigantes-tecnologicos-por-donde-viene-la-regulacion/) por Lucía Velasco.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
