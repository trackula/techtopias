---
title: "Las redes sociales contra Trump y las apps del COVID-19"
date: 2020-06-09T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-2.jpg"

# meta description
description: "Las redes sociales contra Trump y las apps del COVID-19"

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Las pasadas semanas la red social Twitter ha decidido [ocultar diversas publicaciones de el presidente de los Estados Unidos, Donald Trump por glorificar la violencia](https://www.elconfidencial.com/mundo/2020-05-29/twitter-dice-que-un-tuit-de-trump-glorifica-violencia_2616099/).

Otras redes como Facebook no han seguido su ejemplo y han permitido sus publicaciones a pesar de la [oposición que han mostrado parte de sus empleados](https://www.theverge.com/2020/5/29/21275044/facebook-trump-tweets-employee-reaction-criticism), llegando incluso a [realizar múltiples protestas formales ante el mismo Mark Zuckerberg](https://www.theverge.com/interface/2020/6/1/21276969/facebook-walkout-mark-zuckerberg-audio-trump-disgust-twitter).

En reacción a esta "censura", según Trump, el presidente ha firmado unha orden ejecutiva con la intención de terminar con la norma que exime a las redes sociales de responsabilidad sobre los contenidos que son publicados en ellas. [Maldita analiza algunas de la posibles repercusiones y cómo afectaría este cambio, en caso de prosperar, a las redes sociales y a otros servicios como TripAdvisor, Airbnb...](https://maldita.es/malditatecnologia/2020/05/30/posibles-repercusiones-orden-trump-twitter-facebook-redes-sociales-tripadvisor-airbnb/)

Android ha activado la API de notificaciones de exposición al COVID-19, [este es un buen enlace para entender un poco mejor lo que está pasando.](https://maldita.es/malditatecnologia/2020/06/06/notificaciones-exposicion-covid-19-que-son-google-apple/)

El colectivo XNET [denuncia que Apple y Google impiden una auditoría completa a su tecnología de rastreo de contactos y que no se sabe si "extraen los datos que se generan"](https://www.businessinsider.es/asociacion-denuncia-no-puede-auditar-app-rastreos-651851).

[Sanidad recela de la 'app' de rastreo de Economía al temer un alud de datos inútiles](https://www.elconfidencial.com/espana/2020-06-01/sanidad-app-rastreo-agenda-digital-simon-datos-economia_2617079/), teme que un rebrote colapse los servicios sanitarios con falsos positivos de contactos casuales.

[Francia estrena ‘app’ para rastrear el virus entre recelos por la privacidad y dudas de su eficacia](https://elpais.com/tecnologia/2020-06-01/francia-estrena-app-para-rastrear-el-virus-entre-recelos-por-la-privacidad-y-dudas-de-su-eficacia.html).

[Apple lanza una actualización de IOS que detecta si llevas puesta mascarilla para desbloquear el móvil](https://www.eldiario.es/tecnologia/iPhone-actualizacion-detecta-mascarilla-desbloquear_0_1029248253.html).

[El 'Gran Hermano' de China contra el covid amenaza con quedarse para siempre](https://www.elconfidencial.com/tecnologia/2020-05-29/vigilancia-ciudadan-china-coronavirus_2614984/) con la idea de ampliar su utilidad de cara al "crédito social", castigando conductas como el sedentarismo o la ingesta de alcohol.

[Marta Peirano comparte un análisis de Reuters Institute sobre los tipos, los orígenes y los "claims" de la desinformación.](https://mobile.twitter.com/minipetite/status/1265702003067490305)

El MIT Technology Review [analiza las 25 'apps' de rastreo de contactos más importantes del mundo](https://www.technologyreview.es/s/12241/asi-son-las-25-apps-de-rastreo-de-contactos-mas-importantes-del-mundo).

[Facebook empieza a etiquetar a los medios de comunicación controlados por los estados](https://about.fb.com/news/2020/06/labeling-state-controlled-media/) para luchar contra la desinformación y la influencia externa en las elecciones de EEUU.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Europa quiere poner en marcha [el proyecto Gaia-X, que busca la independencia europea en la nube](https://elpais.com/tecnologia/2020-06-06/el-proyecto-gaia-x-busca-la-independencia-europea-en-la-nube.html) impulsando una alternativa a los servicios cloud ofrecidos por los gigantes estadounidenses y asiáticos.

Idemia, un compañía francesa especializada en reconocimiento facial, fingerprinting y reconocimiento de iris, [verificará los datos de cerca de 800 millones de personas entre Estados Unidos, Europa y Australia](https://onezero.medium.com/idemia-will-operate-facial-recognition-for-nearly-800-million-people-69b72582202b) tras haber conseguido un nuevo gran contrato con la UE.

[Los tribunales franceses prohíben la vigilancia con drones en París para hacer cumplir las restricciones por el COVID-19.](https://www.medianama.com/2020/05/223-france-ban-drones-coronavirus-surveillance/)

[UK publica sus contratos con Amazon, Microsoft, Google, Faculty y Palantir para gestionar los datos del COVID de pacientes del NHS](https://www.opendemocracy.net/en/under-pressure-uk-government-releases-nhs-covid-data-deals-big-tech/).

[La Generalitat de Catalunya asume poner freno a Google en escuelas e institutos](https://www.elsaltodiario.com/google/la-generalitat-de-catalunya-asume-poner-freno-a-google-en-escuelas-e-institutos), podemos conocer más acerca de la propuesta [en este enlace](https://xnet-x.net/privacidad-datos-digitalizacion-democratica-educacion-sin-google/).

[China propone a la ONU reinventar internet para dar más control a los gobiernos.](https://www.elespanol.com/omicrono/20200330/china-propone-onu-reinventar-internet-dandole-gobiernos/478702320_0.html)

La pasada semana terminó la consulta europea sobre la estrategia europea para los datos y a la que ya no se pueden enviar sugerencias pero si es posible [consultar las preguntas realizadas y en breve serán publicados los resultados](https://ec.europa.eu/digital-single-market/en/news/online-consultation-european-strategy-data).

[La comisión europea lanza una nueva consulta pública sobre la futura normativa de Servicios Digitales de la Unión](https://ec.europa.eu/digital-single-market/en/news/commission-launches-consultation-seek-views-digital-services-act-package), anímate a participar.

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Tor Browser ha lanzado su release 9.5](https://blog.torproject.org/new-release-tor-browser-95), enfocada principalmente en hacer mas sencillos y entendibles los onion services.

La organización Fight For The Future ha [lanzado una campaña para pedir que se investigue a Amazon y a sus prácticas monopolísticas alrededor de su negocio y su "imperio de la vigilancia".](https://www.investigateamazon.com/)

Automattic, la empresa detrás de Wordpress, [invierte $5M en New Vector para continuar con el desarrollo de sus productos Riot y Modular.im](https://matrix.org/blog/2020/05/21/welcoming-automattic-to-matrix), ambos dentro del ecosistema de Matrix.

[ProtonVPN se ha convertido en la tercera app mas descargada en la App Store de Apple en Hong Kong](https://protonvpn.com/blog/hong-kong-security-law/), estos días en los que las protestas por la libertad y los derechos civiles han vuelto a la ciudad.

En estos momentos de confinamiento y trabajo en remoto, [Jitsi se ha vuelto claramente la principal alternativa open-source a Zoom.](https://thenextweb.com/apps/2020/05/21/a-look-at-how-jitsi-became-a-secure-open-source-alternative-to-zoom/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Grant for the web](https://www.grantfortheweb.org/) busca modelos de negocio que puedan hacer frente a los modelos típicos relacionados con la publicidad invasiva, el tráfico de datos o la IA, entre otros. El plazo de la convocatoria termina el 12 de Junio.

[eSSIF-Lab busca financiar proyectos para desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo termina el 29 de Junio.

[Reset Our Future Fund](https://www.reset.tech/open-calls/reset-our-future-fund/) busca apoyar proyectos que luchen contra el capitalismo de la vigilancia. La convocatoria termina el 1 de Julio.

Si tienes ideas que puedan ayudar a los gobiernos a empoderarse y construir un internet mejor, [NGI Policy-in-Practice fund](https://research.ngi.eu/policy-practice-fund/) ofrece hasta 25,000 € de financiación. La convocatoria termina el 3 de Julio.


### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, charlas y entrevistas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Our Data, our Cities — Building a Global Data Commons](https://20.re-publica.com/de/session/our-data-our-cities-building-global-data-commons) con Geraldine de Bastion y Francesca Bria que han analizado cómo algunas ciudades están probando nuevos métodos para devolver el control de los datos a sus ciudadanos y utilizarlos para el bien común.

En la facultad de derecho de la universidad de Chile han comenzando el ciclo ["Privacidad y Nueva Normalidad"](https://www.youtube.com/watch?reload=9&v=esCLGkZh000&feature=youtu.be), con una primera sesión con varias presentaciones y la participación de Manuela Battaglini, Johanna Caterina Faliero, Mónica Vargas y Claudia Negri.

Entrevista a la matemática Cathy O’Neil, ["A diferencia de China, en Occidente los gobiernos no nos dicen que nos vigilan".](https://retina.elpais.com/retina/2020/05/26/talento/1590502392_549399.html#?ref=rss&format=simple&link=guid)

Éticas Foundation ha presentado su trabajo, ["This App is not for you"](https://www.facebook.com/watch/live/?v=179760900120901&ref=watch_permalink) donde han analizado si el uso de las TIC aumenta o debilita los propósitos originales de la orientación de la policía comunitaria.

Tim Berners-Lee ha publicado un artículo de opinión en donde comenta que [el Covid-19 ha dejado claro que el acceso a internet debería ser un derecho fundamental](https://www.theguardian.com/commentisfree/2020/jun/04/covid-19-internet-universal-right-lockdown-online).

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
