---
title: "Contact tracing con QR, el terrorismo de depuradoras y las empresas con gobiernos propios."
date: 2021-02-16T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-20.jpg"
# meta description
description: "Contact tracing con QR, el terrorismo de depuradoras y las empresas con gobiernos propios."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

La vigilancia llega a los coches y a los seguros a través de [un pequeño dispositivo con cámara frontal que asegura saber dónde conduce cada cliente, cuanto tiempo, si da volantazos, etc](https://retina.elpais.com/retina/2021/01/29/tendencias/1611919608_607025.html).

[La empresa Neuralink de Elon Musk ha implantado un chip en el cerebro de un mono y ahora "puede jugar a videojuegos usando su mente"](https://www.businessinsider.es/mono-juega-videojuegos-gracias-chip-implantado-elon-musk-802633).

Signal ha superado los 40 millones de usuarios activos lo que está [abriendo una nueva batalla interna que busca que la plataforma prepare mecanismos y políticas para identificar y eliminar peligros como la violencia o la desinformación.](https://www.theverge.com/22249391/signal-app-abuse-messaging-employees-violence-misinformation)

[Un leak de datos ha permitido a The New York Times trazar la localización de miles de asaltantes del Capitolio.](https://www.nytimes.com/2021/02/05/opinion/capitol-attack-cellphone-data.html) Una acción que demuestra una vez más lo sencillo que es relacionar el "anonimato" digital con una persona real.

Al mismo tiempo, [Twitter ha anunciado que Trump está vetado de sus redes de forma permanente incluso aunque se vuelva a presentar a la presidencia.](https://cadenaser.com/ser/2021/02/10/internacional/1612975303_031848.html)

[Hackean el suministro de aguas de una ciudad de Florida para intentar envenenar a los vecinos: un recordatorio de hasta dónde llegan las amenazas digitales.](https://www.businessinsider.es/hackean-planta-aguas-envenenar-poblacion-807997)

Esta misma semana se ha sabido que [el estudio de videojuegos que creó Cyberpunk 2077 o The Witcher ha sido también comprometido](https://www.elconfidencial.com/tecnologia/2021-02-09/hackean-estudio-videojuegos-cyberpunk-2077_2942384/) y los piratas informáticos aseguran tener el código fuente de sus videojuegos así como de una versión inédita de Witcher 3.

[Millones de contraseñas de Gmail, Outlook y Hotmail han sido publicadas.](https://elandroidelibre.elespanol.com/2021/02/millones-de-contrasenas-gmail-filtradas-comprueba-si-te-has-visto-afectado.html) Este es un buen momento para cambiar de contraseña.

[Facebook trata de recordar a sus usuarios, los beneficios de la recopilación de datos ante el cambio de privacidad que prepara Apple.](https://edition.cnn.com/2021/02/01/tech/facebook-transparency-alert/index.html). Al respecto de estas novedades en las políticas de Apple, podemos profundizar un poco más en ["What We Learned From Apple’s New Privacy Labels"](https://www.nytimes.com/2021/01/27/technology/personaltech/apple-privacy-labels.html)

Según un antiguo director del US National Counterintelligence and Security Center, [China ha obtenido datos personales y ADN del 80% de los adultos estadounidenses.](https://www.infosecurity-magazine.com/news/china-steals-personal-data-of-80/)

La noticia del mes es que [Jeff Bezos deja (un poco) a un lado Amazon para centrarse en su empresa espacial, The Washington Post y en sus labores filantrópicas.](https://www.elespanol.com/invertia/disruptores-innovadores/innovadores/tecnologicas/20210203/paso-jeff-bezos-amazon-sigue-primer-nueva/556194663_0.html)

[Amazon planea introducir cámaras con inteligencia artificial en sus furgonetas de reparto mejorar la seguridad de sus conductores.](https://www.reuters.com/article/us-amazon-com-delivery/amazon-plans-ai-powered-cameras-in-delivery-vans-to-improve-driver-safety-idUSKBN2A4009)

[Supercookie.me demuestra un método para crear un identificador personal imborrable utilizando los favicons de las webs](https://www.microsiervos.com/archivo/seguridad/supercookie-me-identificador-personal-imborrable-icono-favicon.html), muchos navegadores se han apresurado a corregirlo.

[Una nueva IA asegura detectar las emociones de personas analizando variaciones en su ritmo cardíaco, obtenido al hacer rebotar ondas de radio en los sujetos.](https://www.defenseone.com/technology/2021/02/new-ai-can-detect-emotion-radio-waves/171863/)

[Google cierra acuerdos con diversos medios españoles para reabrir News y 'matar' el canon AEDE](https://www.elconfidencial.com/tecnologia/2021-01-26/google-news-canon-aede-cultura-propiedad-intelectual_2921776/) mientras [Microsoft ofrece a Bing para sustituirle en Australia.](https://elpais.com/tecnologia/2021-02-04/reemplazar-a-google-microsoft-pone-bing-a-disposicion-de-australia.html)

[Google bloquerá el acceso de Chromium y otros navegadores basados en él, a diversas APIs como la de geolocalización o la sincronización de cuentas.](https://www.xataka.com/aplicaciones/google-capara-chromium-eso-plantea-debate-necesidad-chromium-que-sea-open-source-verdad)

[La irrupción de Google en la escuela pública canaria alarma a expertos en privacidad de datos](https://www.eldiario.es/canariasahora/sociedad/irrupcion-google-escuela-publica-canaria-alarma-expertos-privacidad-datos_1_7198094.html). Como bien sabemos los lectores de esta newsletter, nada es gratis.

[Las empresas tecnológicas se han convertido en las mayores consumidoras de energía verde](https://www.ft.com/content/0c69d4a4-2626-418d-813c-7337b8d5110d), un dato que incita a la reflexión sobre el problema que supone el enorme consumo energético de sus centros de datos y el previsible aumento de consumo con el auge de la IA.

[Spotify España anuncia la subida de precio en sus planes familiares al mismo tiempo que apenas paga impuestos en el país tras abonar 6,4 millones a empresas del grupo.](https://cincodias.elpais.com/cincodias/2021/02/01/companias/1612210324_082314.html)

[21 estados de EE.UU. revisan las solicitudes de desempleo utilizando una tecnología de reconocimiento facial](https://onezero.medium.com/21-states-are-now-vetting-unemployment-claims-with-a-risky-facial-recognition-system-85c9ad882b60) sobre la que han surgido múltiples dudas sobre su correcto funcionamiento.

[Castillla-La Mancha lanza una app obligatoria para entrar en bares y restaurantes.](https://www.eldiario.es/castilla-la-mancha/coronavirus-castilla-la-mancha/funcionan-app-codigo-qr-entrar-obligatorios-bares-restaurantes-castilla-mancha_1_7211418.html) La idea es que los clientes lean un QR para registrar su presencia en el establecimiento y así facilitar la labor de búsqueda de contactos.

Una medida que pone de manifiesto la falta de lógica y coordinación por parte de las administraciones, que obligan a usar una nueva app a pesar de haber desarrollado la aplicación Radar COVID, pensada en la privacidad desde el primer momento, con el código público y que funciona no solo en bares y restaurantes sino en el metro o en cualquier otro lugar.

De hecho, habría sido una buena oportunidad para mejorar los datos de [Radar COVID, en la que actualmente sólo 8 de cada 100 códigos de positivos acaban siendo introducidos en la app](https://www.xatakamovil.com/aplicaciones/radar-covid-se-estrella-solo-8-cada-100-codigos-positivos-acaban-siendo-introducidos-app), muchos de ellos porque nunca llegan o porque los usuarios no tienen la app instalada.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El Gobierno busca un 'jefe de Datos' para explotar la información de los ciudadanos](https://www.vozpopuli.com/economia_y_finanzas/gobierno-jefe-datos-informacion-ciudadanos_0_1434756653.html) y que se encargará de diseñar las estrategias en gestión y compartición de datos entre empresas, ciudadanos y administraciones, así como el empleo masivo de datos para la toma de decisiones públicas o para uso empresarial.

España lanzará un [plan piloto para implantar la semana laboral de cuatro días en 200 empresas.](https://www.elconfidencial.com/espana/2021-02-03/semana-laboral-cuatro-dias-200empresas-sectores_2933264/)

Una propuesta de la candidata de En Comú Podem [pide crear un "Amazon público" para competir con los gigantes de las ventas online](https://www.vozpopuli.com/espana/podemos-amazon-publico_0_1434757523.html). Quizás es un buen momento para potenciar el uso de plataformas ya existentes como Correos Market u otras cooperativas a nivel local.

[El Ayuntamiento de Barcelona prohíbe el alquiler de habitaciones por menos de un mes,](https://www.libremercado.com/2021-02-04/golpe-colau-airbnb-barcelona-prohibe-alquiler-habitaciones-menos-de-un-mes-6705140/) una medida que busca luchar contra las habitaciones de uso turístico.

[Gobierno, patronal y sindicatos llegan a un consenso para asalariar a los 'riders'](https://www.20minutos.es/noticia/4578433/0/gobierno-patronal-sindicatos-consenso-laboralizar-riders/) a cambio de excluir de dicha ley a otros sectores de actividad.

[Un estudio afirma que desconocer el potencial del código abierto podría estar costando a la UE “cientos de miles de millones de euros al año”](https://www.genbeta.com/actualidad/estudio-afirma-que-desconocer-potencial-codigo-abierto-podria-estar-costando-a-ue-cientos-miles-millones-euros-al-ano).

[La aplicación para el control del coronavirus del gobierno de Polonia que permite hacer seguimiento con localización y reconocimiento facial, puede servir como peligrosa referencia para otros gobiernos.](https://www.politico.eu/article/poland-coronavirus-app-offers-playbook-for-other-governments/)

[La Europol se pone a la defensiva ante las dudas sobre su uso de big data de forma ilegal para investigar crimenes.](https://www.euractiv.com/section/digital/news/europol-on-defensive-as-concerns-raised-over-illegal-big-data-tactics/) El Supervisor Europeo de Protección de Datos (SEPD) ha criticado a Europol después de que una investigación descubriera pruebas de que las fuerzas de seguridad nacionales transmiten cada vez más "grandes conjuntos de datos" a Europol, en lugar de "datos específicos".

[La UE busca anular la victoria de Apple en su demanda de 13.000 M de euros, alegando que los jueces utilizaron razonamientos contradictorios.](https://www.bloomberg.com/news/articles/2021-02-01/eu-calls-15-8-billion-apple-tax-ruling-contradictory-in-appeal)


Se filtra un borrador sobre el plan de [Nevada para permitir a empresas tecnológicas, crear sus propios gobiernos locales separados de los actuales.](https://apnews.com/article/legislature-legislation-local-governments-nevada-economy-2fa79128a7bf41073c1e9102e8a0e5f0) Según el borrador, las empresas podrán cobrar impuestos, proveer servicios gubernamentales, y hasta tener sus propios juzgados.

[La ciudad de New York propone regular los algoritmos utilizados en la contratación de personal, para asegurar que no discriminan.](https://www.wired.com/story/new-york-city-proposes-regulating-algorithms-hiring/)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Las Fuerzas armadas alemanas se encuentran probando una aplicación de chat open source basada en Matrix.](https://joinup.ec.europa.eu/collection/open-source-observatory-osor/news/matrix-pilot-bwmessenger) Parece que la apuesta por plataformas libres es firme y el gobierno aleman también está probando [Wire](https://wire.com/en/) con la idea de implantar estas plataformas también en otros departamentos del gobierno.

[Telegram permite importar el historial de chat desde otras apps como WhatsApp, Line o KakaoTalk.](https://telegram.org/blog/move-history/es)

[ProtonMail, Threema, Tresorit y Tutanota entre otros, piden a la UE repensar las propuestas anti-cifrado publicadas por la misma el pasado mes de diciembre.](https://protonmail.com/blog/joint-statement-eu-encryption/)

[BigBrotherWatch ha presentado una propuesta para evitar que se cedan los datos de salud y de trazado de contactos a la policia](https://bigbrotherwatch.org.uk/wp-content/uploads/2021/02/Big-Brother-Watch-Briefing-on-The-Health-Protection-All-Tiers-and-Self-IsolationRegulations-2021-HC.pdf)

El pasado fin de semana tuvo lugar el [FOSDEM](https://fosdem.org/2021/), la conferencia europea mas grande sobre tecnologías libres con mas de 30 tracks simultáneos durante dos días completos, y en breves [estarán disponibles los videos en este enlace.](https://fosdem.org/2021/news/2021-02-11-videos-on-fosdem/). Este año el evento se realizó totalmente online [a través de Element (Matrix) y Jitsi](https://twitter.com/matrixdotorg/status/1358465480508534784), una muestra mas de que el software libre no se queda atrás.

Estas semanas también se ha presentado [Penpot](https://penpot.app/), realmente no tiene mucho que ver con privacidad pero parece un proyecto open source muy interesante [y que aspira a ser, dentro del diseño vectorial y de interfaces, una alternativa potente a Figma o Sketch.](https://www.genbeta.com/imagen-digital/penpot-google-docs-diseno-vectorial-interfaces-codigo-abierto)


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGI Ledger busca proyectos para financiar herramientas "privacy-by-design" y aquellas que trabajen en el campo de los datos distribuidos](https://ledger-3rd-open-call.fundingbox.com/) con hasta 150K €. El plazo se encuentra abierto hasta el 22 de Febrero de 2021.

[NGIatlantic.eu, continua abierto en su tercer open call](https://ngiatlantic.eu/ngiatlanticeu-3rd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 26 de Febrero.

NGI también ha abierto el programa [TRUBLO](https://www.trublo.eu/apply), que busca finaciar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 19 de Marzo.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) mantiene su convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta.

Un lugar interesante a revisar de vez en cuando es [la web general de NGI que agrupa las múltiples acciones](https://www.ngi.eu/ngi-projects/) que la Unión Europea está lanzando en el campo de la soberanía tecnológica.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Lecciones prácticas para una inteligencia artificial ética](https://www.elespanol.com/invertia/disruptores-innovadores/opinion/20210130/lecciones-practicas-inteligencia-artificial-etica/554574546_13.html) por Esther Paniagua.

[El momento de la transparencia algorítmica](https://www.newtral.es/transparencia-algoritmica-codigo-auditable/20210203/), por Marilín Gonzalo.

[Tech titans like Google and Facebook are built on a 'house of cards', says Oxford philosopher](https://www.cbc.ca/radio/spark/tech-titans-like-google-and-facebook-are-built-on-a-house-of-cards-says-oxford-philosopher-1.5893760) por Adam Killick.

[Post Apocalipsis Nau #49 Memeización de la política y echar a Google de las escuelas.](https://www.elsaltodiario.com/post-apocalipsis-nau/post-apocalipsis-nau-49-memeizacion-de-la-politica-y-echar-a-google-de-las-escuelas)

["Under the Cloud"](https://www.bbc.co.uk/sounds/play/m000nc1n) por Simon Hollis, acerca<> de la historia y la política alrededor de la nube.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
