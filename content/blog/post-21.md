---
title: "Los enlaces a noticias en Australia, la crisis de los microchips y los pasaportes de vacunación."
date: 2021-03-02T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-21.jpg"
# meta description
description: "Los enlaces a noticias en Australia, la crisis de los microchips y los pasaportes de vacunación."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Huawei anuncia en FOSDEM 21 el primer colaborador europeo para su sistema operativo open source OpenHarmony.](https://www.noticias2d.com/2021/02/12/huawei-anuncia-en-fosdem21-el-primer-colaborador-europeo-para-european-openharmony/)

La llegada de la pandemia y el auge del teletrabajo ha acabado por generar [una gran crisis en los semiconductores que ha llevado a empresas como Apple o Sony a lanzar sus productos por entregas](https://www.elconfidencial.com/tecnologia/2021-02-13/semiconductores-crisis-provedoores-industria_2942859/). En este contexto [la Unión Europea estudia un acuerdo para construir su primera planta de fabricación de semiconductores de última generación](https://twitter.com/isa_valgreen/status/1361422387036626945) en territorio comunitario, un movimiento muy hilado con los objetivos de aumentar su soberanía digital. Este es un debate que lleva tiempo encima de la mesa y [la arquitectura libre RISC-V puede ser la mejor baza de los centros de supercomputación europeos para independizarse de las CPU extranjeras.](https://www.xataka.com/investigacion/arquitectura-risc-v-mejor-baza-centros-supercomputacion-europeos-para-independizarse-cpu-estadounidenses)

[Mas de la mitad de los distritos de Londres utilizan tecnología China de vigilancia, conocida por su uso contra las minorías Uighur en China.](https://news.trust.org/item/20210218035835-cbgdz/)

[Kia Motors America ha recibido un ataque de ransomware solicitándole $20 millones como rescate.](https://www.bleepingcomputer.com/news/security/kia-motors-america-suffers-ransomware-attack-20-million-ransom/)

[El Departamento de Vehículos a Motor de California también ha recibido un ataque dejando expuestos más de 20 meses de datos.](https://www.engadget.com/california-dmv-potential-data-breach-ransomware-contractor-181156549.html)

[Amazon se hace con los derechos para distribuir La Liga Santander en Reino Unido,](https://www.lainformacion.com/empresas/tebas-firma-amazon-vende-derechos-laliga-reino-unido/2829068/) uno de los primeros acercamientos de las grandes tecnológicas al fútbol español.

[Científicos coreanos utilizan la red 5G para detectar tus emociones y predecir crímenes.](https://www.elconfidencial.com/tecnologia/novaceno/2021-02-17/5g-monitorear-emociones-prevenir-crimines_2955303/)

[Las dudas sobre cómo garantizan la privacidad se ciernen sobre la app de moda Clubhouse](https://www.elconfidencial.com/tecnologia/2021-02-27/italia-clubhouse-privacidad-datos-audios_2967116/), esta misma semana [Gemma Galdón lo comentaba en su cuenta de Twitter.](https://twitter.com/gemmagaldon/status/1364151367531651072)

[Jack Dorsey, el CEO de Twitter, quiere construir una app store de algoritmos para el contenido de las redes sociales.](https://www.theverge.com/2021/2/9/22275441/jack-dorsey-decentralized-app-store-algorithms)

[John Deere prometió a los granjeros que haría sus tractores fáciles de reparar pero no ha sido así.](https://www.vice.com/en/article/v7m8mx/john-deere-promised-farmers-it-would-make-tractors-easy-to-repair-it-lied)

[Una cuenta de TikTok muestra a un Tom Cruise digital indistinguible del real.](https://www.elconfidencial.com/tecnologia/novaceno/2021-02-26/tom-cruise-tiktok-deep-fake_2969284/) Un ejemplo más de que afrontar los deepfakes es uno de los grandes retos de los próximos años.

[Google continúa lo que muchos de sus empleados han calificado como "purga", al despedir a su principal especialista en IA ética 2 meses después de la destitución de su codirectora, dejando al equipo sin cabeza.](https://www.businessinsider.es/google-despide-principal-especialista-ia-etica-815253)

Australia se ha convertido en el centro de las noticias en el ámbito tecnológico de las últimas semanas ya que [el gobierno del país ha aprobado una ley que obliga a Google y Facebook a pagar a los medios por enlazar sus noticias.](https://www.publico.es/sociedad/google-facebook-australia-aprueba-ley-pionera-google-facebook-paguen-noticias.html) Como respuesta, [Facebook ha decidido bloquear todo el contenido de medios en ese país.](https://www.theguardian.com/technology/2021/feb/18/facebook-condemned-in-uk-and-us-for-attempt-to-bully-democracy)

Finalmente parece que las tecnológicas han cedido y [Facebook pagará 822 millones a medios de todo el mundo por enlazar sus noticias](https://www.elperiodico.com/es/internacional/20210224/facebook-pagara-822-millones-medios-11541928) al mismo tiempo que han [firmado un nuevo código de conducta para combatir las noticias falsas en las redes.](https://www.businessinsider.es/facebook-google-firman-nuevo-codigo-conducta-australia-816131)

La situación ha llegado a tal punto [que el mismo inventor de la web Tim Berners-Lee ha dicho que esta legislación podría convertir internet en un lugar "inviable".](https://www.independent.co.uk/news/australia-internet-law-tim-berners-lee-b1803988.html) Para profundizar un poco más en esta visión, [este artículo de Enrique Dans lo explica bastante bien.](https://www.enriquedans.com/2021/02/google-facebook-y-la-verguenza-australiana.html)

[IKEA trae a España su propio Glovo en el que "peones" montan un sofá por 13 euros y hacen cola por 10.](https://www.eldiario.es/economia/ikea-trae-espana-propio-glovo-peones-montan-quince-muebles-80-euros-cola-10_1_7234853.html)

[El Tribunal Supremo de UK considera empleados a los conductores de Uber.](https://www.bolsamania.com/noticias/empresas/derrota-clave-uber-supremo-reino-unido-sentencia-conductores-son-empleados--7818742.html)

La polémica también saltaba esta semana al publicar Renfe [una licitación para el desarrollo de un software de seguridad que pretende implantar en 25 estaciones y que incluía el análisis del origen étnico, sexo o vestimenta entre otros muchos criterios.](https://www.elconfidencial.com/tecnologia/2021-02-17/renfe-videovigilancia-pliego-condiciones-tecnicos_2953824/)

Nada mas saltar la noticia, y ante la fuerte repercusión que ha tenido, [Renfe ha decidido retirar la contratación temporalmente para revisarla, pero la intención de desarrollar un sistema de vigilancia similar sigue adelante.](https://www.eldiario.es/tecnologia/renfe-cancela-contratacion-sistema-detectar-edad-genero-etnia-tipo-ropa-animo-viajeros_1_7226541.html)

[La entrega de las administraciones públicas a Microsoft: 793 millones en contratos para licencias y servicios](https://www.elsaltodiario.com/tecnologia/administraciones-publicas-microsoft-793-millones-contratos-licencias-servicios), una inversión que podría redirigirse hacia soluciones open source que de verdad mejoren la autonomía estratégica del país.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[La ley rider saldrá adelante pero lo hará sin acuerdo](https://www.elconfidencial.com/economia/2021-02-10/calvino-diaz-chocan-ley-riders-regulacion-nacera-acuerdo_2943480/) ya que según parece, [el borrador final de la ley no incluirá las últimas propuestas de la patronal.](https://www.elespanol.com/invertia/economia/empleo/20210218/indignacion-patronal-ley-rider-trabajo-no-propuestas/559695313_0.html)

[El Congreso de los Diputados saca a concurso la contratación de su ciberseguridad](https://www.xataka.com/pro/congreso-diputados-saca-a-concurso-contratacion-su-ciberseguridad-2-5-millones-euros-cuatro-anos), un servicio de vigilancia digital que busca rastrear redes sociales, foros y otros lugares de la red como la deep web para localizar posibles amenazas cibernéticas contra el Congreso.

[El Gobierno busca reactivar la 'ley Beckham' en un intento por atraer talento e inversión en el campo de las startups y el emprendimiento.](https://www.lainformacion.com/economia-negocios-y-finanzas/hacienda-reactiva-ley-beckham-atraer-talento-inversiones-espana/2830681/) Al mismo tiempo, el gobierno también prepara la ley de startups [de la que ha hablado el Alto Comisionado para la España Nación Emprendedora, Francisco Polo.](https://sifted.eu/articles/spain-startup-law/)

[Más gigas y ciencia abierta para acabar con la “precaridad tecnológica” de la Universidad](https://elpais.com/educacion/2021-02-25/mas-gigas-y-ciencia-abierta-para-acabar-con-la-precaridad-tecnologica-de-la-universidad.html), una oportunidad para avanzar en la soberanía tecnológica si se invierte correctamente.

[La Unión Europea se preparar para la llegada del pasaporte de vacunación en verano](https://www.elconfidencial.com/mundo/europa/2021-02-25/creciente-apoyo-entre-los-lideres-europeos-hacia-el-pasaporte-de-vacunacion_2968087/), de hecho la UE [teme que las grandes tecnológicas se adelanten con sus propias soluciones si no se dan prisa](https://www.businessinsider.es/ue-teme-google-apple-adelanten-pasaporte-vacunal-819339).
Lo que parece una nueva carrera por las apps de contact tracing 2.0 y en la que [Apple y Google pueden estar ya trabajando aunque no lo reconozcan públicamente.](https://www.euractiv.com/section/coronavirus/news/apple-says-it-never-discussed-covid-19-certificate-app-with-who-eu/)

Para complementar el debate, ["El dilema del pasaporte covid"](https://www.lavanguardia.com/vida/20210222/6257367/pasaporte-covid-dudas-etica-economia-gobiernos.html) y las dudas éticas y prácticas de su implantación.

[Europa creará un Observatorio de Tecnologías Críticas para estudiar el impacto y las aplicaciones de coches autónomos, nanotecnología o la inteligencia artificial.](https://www.businessinsider.es/europa-creara-nuevo-observatorio-tecnologias-criticas-816193)

[La decisión de Francia de almacenar los datos de salud de sus ciudadanos bajo las plataformas de Microsoft ha generado bastante preocupación entre los defensores de la protección de datos.](https://www.euractiv.com/section/health-consumers/news/french-decision-to-have-microsoft-host-health-data-hub-still-attracts-criticism/)

[Italia propone la verificación de edad y de la identidad digital para acceder a redes sociales.](https://edri.org/our-work/italy-age-verification-digital-identities-social-media/)

[Maryland aprueba la primera legislación para tasar los beneficios en publicidad de las Big Tech.](https://www.nytimes.com/2021/02/12/technology/maryland-digital-ads-tax.html)

[La ciudad de Mineápolis prohíbe el uso de vigilancia con tecnologías de reconocimiento facial.](https://r3d.mx/2021/02/12/la-ciudad-de-mineapolis-prohibe-el-uso-de-vigilancia-con-tecnologia-de-reconocimiento-facial/)

La administración Biden ha retirado el proyecto de continuar el muro de Trump pero a cambio [ha lanzado una propuesta de "smart wall" en el que utilizarán todo tipo de tecnologías de vigilancia y reconocimiento facial.](https://thehill.com/policy/technology/540494-immigrant-groups-slam-biden-smart-wall-bill-trumps-wall-by-another-name) Muchos activistas se han lanzado a protestar en lo que consideran, "el muro de Trump pero con otro nombre".

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

Mozilla lanza su ["privacy not included" en versión San Valentín](https://foundation.mozilla.org/en/privacynotincluded/categories/dating-apps/) analizando desde el punto de vista de la privacidad, múltiples apps de citas.

[JustGetMyData](https://justgetmydata.com/), unha plataforma que ayuda a recuperar nuestros datos personales de múltiples servicios.

[WebSegura](https://websegura.pucelabits.org/) es un nuevo proyecto del grupo PucelaBits que analiza el nivel de cifrado de páginas web de administraciones públicas.

La Electronic Frontier Foundation ha documentado las tecnologías de vigilancia que utiliza la policia estadounidense en su [Atlas of surveillance](https://atlasofsurveillance.org/).

[Una nueva actualización de Firefox incluye protección total ante cookies](https://blog.mozilla.org/blog/2021/02/23/latest-firefox-release-includes-multiple-picture-in-picture-and-total-cookie-protection/) creando contenedores individuales para cada dominio web.

Desde el próximo 8 de Marzo hasta el día 19 tendrá lugar [la primera edición virtual del Mozilla Festival.](https://schedule.mozillafestival.org/plaza) Ya se puede consultar su programación.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

NGI también ha abierto el programa [TRUBLO](https://www.trublo.eu/apply), que busca finaciar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 19 de Marzo.

El Open Technology Fund vuelve a estar activo y lanza una nueva convocatoria de [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta comunitaria y resolver emergencias digitales con hasta 50.000 dólares.

[Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) mantiene su convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta.

Un lugar interesante a revisar de vez en cuando es [la web general de NGI que agrupa las múltiples acciones](https://www.ngi.eu/ngi-projects/) que la Unión Europea está lanzando en el campo de la soberanía tecnológica.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Nada que Esconder 07: Todos somos alguien con Lorena Sánchez](https://blog.iuvia.io/es/nadaqueesconder-07/), el último podcast de la temporada con la gente de IUVIA.

["El 5G es una gran trampa para espiarnos, nos están engañando a todos"](https://www.elconfidencial.com/tecnologia/2019-06-12/marta-peirano-5g-facebook-google-huawei-enemigo-conoce_2066566/), una entrevista a Marta Peirano.

[This is how we lost control of our faces](https://www.technologyreview.com/2021/02/05/1017388/ai-deep-learning-facial-recognition-data-history/) por Karen Hao en MIT Technology Review.

["La ‘Digital Services Act’: un esfuerzo necesario, ¿pero suficiente?"](https://www.youtube.com/watch?reload=9&app=desktop&v=m8GrbVOoaoE), una mesa redonda organizada por CIDOB.

[El 3%](https://mailchi.mp/bonillaware/la-tasa-google), una reflexión sobre la "Tasa Google" en La Bonilista.

Wikimedia España ha entrevistado a [Diego Naranjo (Director de Políticas Públicas en eDri) para hablar sobre la regulación del ecosistema digital.](https://www.youtube.com/watch?v=iFf0_2EnLbY&list=PLj1KaaNn92UIe8YApHQcV4vcYTg191Ci2&index=1)

[RedIRIS, historia de la privatización de la alternativa española a Silicon Valley](https://www.elsaltodiario.com/tecnologia/rediris-historia-privatizacion-plataforma-espanola-alternativa-silicon-valley) por Álvaro Lorite en El Salto.


### Una cookie extra <img src="/images/emojis/cookie.png" width="24px" style="margin: 0;"/>

Esta semana nos hemos enterado de una noticia que nos crea una profunda tristeza, en parte porque nos toca muy de cerca. El Ayuntamiento de Madrid ha decidido que [MediaLab Prado debe abandonar su espacio](https://www.madridesnoticia.es/2021/02/traslado-medialab-prado-matadero-madrid/) y será transladado a Matadero, donde según parece, verá reducido enormemente el espacio que ocupa y presumiblemente también su financiación.

La sensación es que se busca acabar con un proyecto que ha llevado a la ciudad de Madrid a ser un referente a nivel mundial, un proyecto que une a la gente alrededor de la colaboración, la curiosidad y el "aprender haciendo", un proyecto que creemos que debería ser reforzado y no destruido.

Como os comentábamos, esta noticia nos duele especialmente ya que **Trackula** es un proyecto que no habría surgido de no ser por este espacio por lo que desde aquí nos gustaría pediros a todos que [apoyéis este manifiesto sobre el futuro de MediaLab Prado.](https://wearethelab.org/)

**#WeAreTheLab #SaveTheLab**



---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
