---
title: "Google se carga las cookies, MediaLab Prado está amenazado y llega la Oficina de Ciencia y Tecnología."
date: 2021-03-16T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-22.jpg"
# meta description
description: "Google se carga las cookies, MediaLab Prado está amenazado y llega la Oficina de Ciencia y Tecnología."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[La Audiencia Provincial de Murcia absuelve a los gestores de Series Yonkis](https://www.eldiario.es/tecnologia/audiencia-provincial-murcia-absuelve-acusados-series-yonkis_1_7270338.html) y ratifica que el enlazado de contenidos protegidos con derechos de autor no era delito mientras la web estuvo activa.

[Apple abre la puerta a fabricar parte de sus Iphones en India para no depender de China.](https://www.elconfidencial.com/tecnologia/2021-03-14/india-apple-cambios-produccion-china-montaje-realidad_2986360)

[Clubhouse no solo graba tus conversaciones. La lista de polémicas de privacidad de la red social de moda es extensa.](https://www.genbeta.com/redes-sociales-y-comunidades/clubhouse-no-solo-graba-tus-conversaciones-lista-polemicas-privacidad-red-social-moda)

La automatización del empleo ha llegado para quedarse y [el reemplazo de empleados por máquinas se acelera por la pandemia.](https://www.elconfidencial.com/tecnologia/novaceno/2021-03-04/mcdonalds-mcauto-empleo-inteligencia-artificial_2978368/) Otro ejemplo es [la llegada de Amazon Fresh a Londres.](https://magnet.xataka.com/en-diez-minutos/primer-supermercado-cajeros-amazon-ha-llegado-a-europa-asi-arranca-fresh-londres)

[Múltiples ejecutivos de SolarWinds culpan a un becario por el leak de la password 'solarwinds123'.](https://edition.cnn.com/2021/02/26/politics/solarwinds123-password-intern/index.html)

[El SEPE ha sido atacado por un 'ransomware'. Miles de datos de empresas y ciudadanos están en juego.](https://www.businessinsider.es/ciberataque-sepe-datos-empresas-ciudadanos-peligro-826019) Maldita profundiza un poco mas sobre el tema [para conocer lo qué sabemos del hackeo al Servicio Público de Empleo Estatal y sus posibles consecuencias.](https://maldita.es/malditatecnologia/20210310/hackeo-servicio-publico-empleo-estatal-posibles-consecuencias-datos-cientos-miles-ciudadanos/)

[Un incendio en un datacenter de OVH deja a millones de páginas caídas.](https://www.xataka.com/pro/millones-paginas-caidas-incendio-ovh-gran-reto-montar-10-000-servidores-dos-semanas)

[Un hackeo vinculado a China afecta a decenas de miles de clientes estadounidenses de Microsoft.](https://www.bbc.com/mundo/noticias-56299627)

[Amazon, Slack o Lyft han sido objetivo de ataques a través de la utilización de paquetes de NPM maliciosos.](https://www.bleepingcomputer.com/news/security/malicious-npm-packages-target-amazon-slack-with-new-dependency-attacks/)

[El Golpe de Estado en Myanmar ha creado un movimiento de jóvenes internautas que evade los controles en la red.](https://www.france24.com/es/programas/revista-digital/20210306-myanmar-movimiento-jovenes-controles-internet)

[El Norwegian Consumer Council ha reportado a MyHeritage frente a la autoridad nacional de protección de datos por el uso ilegal de datos personales.](https://www.forbrukerradet.no/side/the-norwegian-consumer-council-reports-myheritage-for-unlawful-terms-and-conditions/)

[Un estudio asegura que 4 de cada 10 estudiantes universitarios son adictos a los teléfonos móviles.](https://www.theguardian.com/society/2021/mar/02/nearly-four-in-10-university-students-addicted-to-smartphones-study-finds)

[Hacer que Facebook o Google se responsabilicen más de lo que publiquen los usuarios, el mayor desafío de la nueva regulación europea.](https://www.businessinsider.es/digital-services-act-cual-sera-mayor-desafio-union-europea-772629)

[Google anuncia el final de las cookies en Chrome para 2022: ¿cómo afecta esto a mi privacidad en internet?](https://maldita.es/malditatecnologia/20210314/google-anuncia-final-cookies-chrome-2022-como-afecta-esto-privacidad-internet/) Una idea que suena interesante de primeras pero que en realidad puede servir únicamente para aumentar aún mas la posición de monopolio del gigante tecnológico.

Grupos como EFF han expresado su opinión y creen que [la llegada de Google FLoC es una idea terrible.](https://www.eff.org/deeplinks/2021/03/googles-floc-terrible-idea) Este hilo en Twitter [nos da un poco más de información sobre el tema.](https://twitter.com/ssice/status/1369014663598444547)

[Estados Unidos pide a Google datos detallados sobre las búsquedas en un caso antimonopolio.](https://www.bloomberg.com/news/articles/2021-03-01/u-s-asks-google-for-detailed-search-data-in-antitrust-case)

La futura directiva europea sobre seguridad informática planea permitir a los paises [imponer sanciones penales a los consejos de administración de la banca por brechas de ciberseguridad en sus compañías.](https://www.vozpopuli.com/economia_y_finanzas/ciberseguridad-banca-consejos.html)

[Arizona planea una legislación para reformar las app stores y los lobistas de Apple y Google ya trabajan para influir en ella.](https://www.protocol.com/policy/apple-google-lobbyists-arizona-bill)

Muchas veces escuchamos eso de "Si no pagas, tu eres el producto", pero hoy en día ya casi da igual lo que pagues que aún así hay casos en donde sigues siendo el producto. [Es el caso de las SmartTVs donde a pesar de gastar $1,500 en una televisión LG, la publicidad segmentada se abre paso en lugares aleatorios.](https://www.theverge.com/tldr/2021/3/10/22323790/lg-oled-tv-commercials-content-store)

[La Xunta de Galicia utilizará también una app con códigos QR en la hostelería.](https://www.economiadigital.es/galicia/politica/la-xunta-abre-la-puerta-a-ampliar-el-plazo-de-la-hosteleria-para-el-codigo-qr.html) Una nueva Comunidad Autónoma que decide promover su propia app sin garantías de privacidad e ignorar la app RadarCovid.

En la pasada edición hablábamos de los cambios anunciados en MediaLab Prado, unos cambios que han generado múltiples críticas de diversos sectores [entre los que se encuentran más de 200 académicos](https://www.elconfidencial.com/espana/madrid/2021-03-09/medialab-traslado-cultura-levy-madrid-unesco_2983131) así como [una veintena de arquitectos que denuncian los daños que el Ayuntamiento de Madrid provocará en el edificio del MediaLab.](https://www.eldiario.es/madrid/veintena-arquitectos-denuncian-danos-ayuntamiento-provocara-edificio-medialab_1_7290005.html) El propio Ministerio de Cultura [advierte a Almeida y Ayuso que la candidatura "Eje Prado" en la Unesco peligra si cierra Medialab.](https://cadenaser.com/ser/2021/03/14/cultura/1615705346_077462.html)

Si aún no has firmado para apoyar a MediaLab Prado, [aquí te dejamos nuevamente el link.](https://wearethelab.org/) **#WeAreTheLab** **#SaveTheLab**

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Tras años de esfuerzo, la organización "Ciencia en el Parlamento" ha conseguido que [se cree la Oficina de Ciencia y Tecnología del Congreso, con un presupuesto de 325.000 euros.](https://www.europapress.es/nacional/noticia-nace-oficina-ciencia-tecnologia-congreso-presupuesto-325000-euros-20210302172419.html) Desde aquí queremos enviar nuestras felicidaciones a "Ciencia en el Parlamento" por este pedazo logro 😁

[El Canon AEDE no recauda ni el 1% de lo esperado por la publicación de noticias de medios tras 7 años desde su aprobación.](https://www.elindependiente.com/economia/2021/03/02/la-tasa-google-no-recauda-ni-el-1-de-lo-esperado-por-la-publicacion-de-noticias-de-medios/) Esto ocurre mientras [Francia se coloca como referente en la aplicación de la directiva de derechos de autor contra los gigantes digitales.](https://elpais.com/sociedad/2021-02-28/francia-marca-el-paso-de-la-ue-en-la-negociacion-con-las-gigantes-digitales.html)


[Gobierno y agentes sociales pactan que las empresas den de alta a los 'riders' en tres meses](https://www.elconfidencial.com/empresas/2021-03-10/gobierno-agentes-sociales-llegan-acuerdo-ley-de-riders_2986588/), una legislación contra la que han protestado [más de 2.000 riders que exigen al Gobierno elegir ser asalariados o autónomos.](https://www.lainformacion.com/economia-negocios-y-finanzas/manifestaciones-riders-exigen-gobierno-ser-asalariados-autonomos/2831261/)

Añadimos también una interesante lectura sobre el tema, ["Yo te contrato, tú pones la moto: la realidad tras la futura 'ley rider' del Gobierno".](https://www.elconfidencial.com/tecnologia/2021-03-04/riders-repartidores-just-eat-glovo-deliveroo-uber-eats_2972516/)

[Bruselas anuncia que presentará este mes una propuesta legislativa con debate abierto sobre el certificado de vacunación para impulsar la movilidad.](https://www.eldiario.es/internacional/bruselas-acelera-certificado-vacunacion-impulsar-movilidad_1_7262752.html) Unos pasaportes inmunitarios que abren un nuevo desafío, esta vez para [coordinar a los 27, garantizar la privacidad de datos sanitarios y asegurar la confianza de los ciudadanos.](https://www.businessinsider.es/desafios-europa-desarrollo-pasaporte-vacunacion-824175)

Estas semanas han sido movidas para la SEDIA, en concreto ha habido [ceses en la Secretaría de Estado digital, entre los que se encuentra el de Lucía Velasco Jones, una de las impulsoras de Radar Covid](https://www.elconfidencial.com/tecnologia/2021-02-26/sedia-secretaria-estado-digitalizacion-carme-artigas-economia_2968155/) y que ha sido nombrada directora del Observatorio Nacional de las Telecomunicaciones y la Sociedad de la Información.

[El Gobierno también ha creado una nueva Dirección General de Digitalización e IA con Ángel Sánchez Aristi al frente.](https://www.europapress.es/economia/noticia-gobierno-crea-nueva-direccion-general-digitalizacion-ia-angel-sanchez-aristi-frente-20210309165555.html)

[La inteligencia artificial se abre paso en la justicia española.](https://elpais.com/tecnologia/2021-02-20/la-inteligencia-artificial-se-abre-paso-en-la-justicia-espanola.html)

[La fiscalía de Milán ordena a las empresas de reparto de comida que contraten a los riders y paguen 733 millones de euros en multas.](https://www.reuters.com/article/us-italy-prosecutors-riders-idUSKBN2AO2FG)

Los vínculos de Biden con Silicon Valley continúan apareciendo, [empleados de las Big Tech fueron una de las cinco mayores fuentes de dinero en la campaña de Biden con en torno a 15 millones de dólares.](https://www.wsj.com/articles/big-tech-employees-opened-wallets-for-biden-campaign-11613833201)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Adalab ha formado ya a más de 425 mujeres como programadoras y el 89% ha encontrado empleo.](https://www.businessinsider.es/tecnologia-no-ni-hombres-ni-genios-adalab-820795)

Bitwarden, el servicio de gestión de contraseñas, lanza [Bitwarden Send](https://www.muycomputer.com/2021/03/13/bitwarden-send/) nuevo servicio de envío archivos y texto cifrado.

EDRi continúa con su campaña [#ReclaimYourFace para ayudar a evitar el fin de la privacidad como la conocemos, ante la vigilancia con reconocimiento facial.](https://edri.org/our-work/reclaim-your-face-and-help-prevent-the-end-of-privacy-as-we-know-it/)

El Ayuntamiento de Barcelona busca [recoger las iniciativas ciudadanas en defensa de los derechos digitales, existentes en la ciudad.](https://ajuntament.barcelona.cat/digital/es/blog/participa-en-la-recogida-de-iniciativas-ciudadanas-en-defensa-de-los-derechos-digitales)

BigBrotherWatch lanza una nueva campaña [Stop Vaccine Passports](https://bigbrotherwatch.org.uk/campaigns/stopvaccinepassports/) con la idea de concienciar sobre los peligros de estos pasaportes y detener su implantación en UK.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El programa [TRUBLO](https://www.trublo.eu/apply) de NGI está a punto de cerrar y busca financiar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 19 de Marzo de 2021.

El [Digital freedom fund](https://digitalfreedomfund.org/grants/) lanza también sus "grants" con hasta 100.000€ para realizar litigaciones estratégicas en el campo de los derechos digitales en Europa. La convocatoria se encuentra abierta hasta el 31 de Marzo.

También se encuentra activo el programa [NGI Assure](https://www.assure.ngi.eu/open-calls/), con el fin de financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Abril de 2021.

La segunda open call de [NGI Pointer](https://ngi-pointer-open-call-2.fundingbox.com/) aún está abierta también y busca financiar con 200.000 € "equity-free" a personas y empresas que desarrollen protocolos y herramientas alineadas con los valores europeos. El plazo de presentación de solicitudes termina el 1 de Abril de 2021.

Después de un largo parón por parte de la administración Trump, [el Open Technology Fund vuelve con varias convocatorias abiertas](https://www.opentech.fund/funds/) entre las que se encuentran el Internet Freedom Fund y el Technology at Scale Fund.

El Open Technology Fund también mantiene la convocatoria de [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta comunitaria y resolver emergencias digitales con hasta 50.000 dólares.

Al igual que [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[¿Hay alternativas más justas que Spotify para escuchar música en ‘streaming’?](https://www.elsaltodiario.com/musica/alternativas-escuchar-streaming-spotify-mejores-condiciones) por Jose Durán Rodríguez.

[Do we underestimate the importance of privacy?](https://bridges-to-the-future.simplecast.com/episodes/do-we-underestimate-the-importance-of-privacy) un podcast con Carissa Véliz.

[Surveillance-based advertising: An industry broken by design and by default](https://edri.org/our-work/surveillance-based-advertising-an-industry-broken-by-design-and-by-default/), una publicación de EDRi. Cabe reseñar dentro del post [el enlace al estudio "Targeted Online", que EDRi ha realizado.](https://edri.org/wp-content/uploads/2021/03/Targeted-online-An-industry-broken-by-design-and-by-default.pdf)

[Inside ‘TALON,’ the Nationwide Network of AI-Enabled Surveillance Cameras](https://www.vice.com/en/article/bvx4bq/talon-flock-safety-cameras-police-license-plate-reader) por Joseph Cox.

Wikimedia ha entrevistado a Ariadna Matas (Asesora de Políticas en Europeana) para hablar sobre [derechos de autoría y acceso al conocimiento.](https://www.youtube.com/watch?v=KV15UCt27SU)

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
