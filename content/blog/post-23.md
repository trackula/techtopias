---
title: "La moratoria en el reconocimiento facial, los sindicatos en Amazon y las ETTs para los riders."
date: 2021-03-30T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-23.jpg"
# meta description
description: "La moratoria en el reconocimiento facial, los sindicatos en Amazon y las ETTs para los riders."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Un grupo de 70 académicos pide al Gobierno una moratoria en el uso del reconocimiento facial](https://www.elsaltodiario.com/inteligencia-artificial/70-voces-de-la-academia-piden-la-moratoria-de-sistemas-de-reconocimiento-facial). Una petición que coincide con la propuesta de EDRI y 61 organizaciones civiles para [solicitar a la UE regular la IA basándose en los derechos, en lugar de en los riesgos](https://edri.org/our-work/eu-should-regulate-ai-on-the-basis-of-rights-not-risks/) y pidiendo también la prohibición de la vigilancia biométrica masiva.

[Google Travel mostrará anuncios de las ofertas turísticas de manera gratuita, un duro golpe a Booking y Expedia.](https://www.20minutos.es/noticia/4614002/0/golpe-de-google-a-booking-y-expedia-los-hoteles-podran-mostrar-sus-anuncios-gratis-en-el-buscador/)

[Acer ha sido víctima de un ataque con ransomware y por el que le solicitan 50 millones de dólares, la mayor cifra conocida.](https://www.europapress.es/portaltic/ciberseguridad/noticia-solicitan-acer-50-millones-dolares-ataque-ransomware-mayor-cifra-conocida-20210322112438.html)

[Finlandia atribuye el ataque a los sistemas del parlamento finlandés, al grupo Chino APT31.](https://therecord.media/finland-pins-parliament-hack-on-chinese-hacking-group-apt31/)

[Tinder permitirá consultar los antecedentes de una cita antes del encuentro.](https://hipertextual.com/2021/03/tinder-permitira-consultar-los-antecedentes-de-una-cita-antes-del-encuentro) Una nueva funcionalidad de genera unas enormes dudas con la privacidad de sus usuarios. De hecho no es la primera vez que surgen dudas sobre cómo la compañía trata la privacidad de sus usuarios, [Mozilla ha creado una recopilación sobre cómo lo hacen las diversas apps de contactos y Tinder no sale muy bien parado.](https://foundation.mozilla.org/en/privacynotincluded/categories/dating-apps/)

[Barcelona colocará contenedores de basura que identifican a los vecinos para saber cuánto se recicla](https://www.xataka.com/ecologia-y-naturaleza/barcelona-contara-contenedores-basura-inteligentes-identificaran-a-usuarios-para-saber-cuanto-reciclan), una idea que ya se ha implementado en otras ciudades con la idea de premiar a quien recicle o castigar a quién no lo haga con una subida de sus tarifas de basura.

[Google Nest 2 trackeará también la actividad corporal de los usuarios en cama para generar informes sobre su sueño.](https://www.bbc.com/news/technology-56416578)

[Amazon también está ayudando a la radicalización de sus usuarios, empujando a sus lectores hacia temáticas relacionadas con teorías conspiranoicas sobre el coronavirus.](https://www.buzzfeednews.com/article/craigsilverman/amazon-covid-conspiracy-books)

Otro tema en auge es la [lucha sindicar que Amazon quiere sofocar](https://elpais.com/economia/2021-02-27/la-lucha-sindical-pone-a-amazon-contra-las-cuerdas.html) en Alabama, donde [la empresa ha hecho todo lo posible](https://mobile.twitter.com/Jjjjjjal3/status/1371951626492256266) por evitar que sus empleados se sindiquen, y que ha llevado a que hasta [el propio presidente de los Estados Unidos, Joe Biden, exprese públicamente su apoyo a los trabajadores.](https://www.elperiodico.com/es/internacional/20210328/amazon-historica-votacion-sindical-apoyo-biden-11615958)

[Este hilo incluye una serie de artículos muy interesantes sobre el tema](https://twitter.com/Shine_McShine/status/1375344702648635392), las condiciones de trabajo de sus empleados y lo dificil que es boicotear a la empresa debido a su extenso control.

[Amazon está pidiendo a sus conductores que firmen un formulario de "consentimiento biométrico" o perderán sus trabajos.](https://www.vice.com/en/article/dy8n3j/amazon-delivery-drivers-forced-to-sign-biometric-consent-form-or-lose-job)

[Lina Khan mantiene una lucha contra Amazon por su abuso de poder basándose en la dependencia que genera en miles de empresas de todo el mundo para poder sobrevivir.](https://www.elconfidencial.com/tecnologia/2021-03-22/lina-kahn-amazon-jeff-bezos-antitrust-ftc-joe-biden_2994072/) Su propuesta pasa por considerar este tipo de plataformas online como algo parecido a "bienes públicos" igual que el agua o la electricidad, y regularlos para eliminar la discriminación de precios.

[Wikipedia anuncia su versión de pago para cobrar a las empresas que deseen obtener los datos de su API de forma más rápida o con mejores opciones de formateo.](https://hipertextual.com/2021/03/wikipedia-de-pago)

Nuevamente salta una noticia que emula a 'Minority Report', esta vez es en el [estado de Florida donde un algoritmo predice qué personas volverán a cometer delitos con la idea de detenerlos antes de que lo hagan.](https://www.elconfidencial.com/mundo/2021-03-15/el-minority-report-de-florida-un-sheriff-persigue-delitos-todavia-no-cometidos_2993108/)

[La comunidad del software libre en pie de guerra contra Stallman tras su retorno a la FSF](https://www.genbeta.com/actualidad/comunidad-open-source-pie-guerra-stallman-su-retorno-a-fsf-hay-quien-quiere-renegar-licencia-gnu).

[Lenovo, forzada a pagar 20.000 euros en daños a un usuario por negarse reembolsar el costo de una licencia de Windows.](https://www.genbeta.com/actualidad/lenovo-forzada-a-pagar-20-000-euros-danos-a-usuario-negarse-reembolsar-costo-licencia-windows)

[El negocio de dar vida o color a fotos antiguas, ¿cómo afectan a la privacidad de nuestros antepasados?](https://elpais.com/tecnologia/2021-03-05/herramientas-para-dar-vida-o-color-a-fotos-antiguas-afectan-a-la-privacidad-de-nuestros-antepasados.html)

[Trump planea volver a las redes sociales en unos meses con su propia plataforma.](https://edition.cnn.com/2021/03/21/media/donald-trump-social-media-network/index.html)

[El Pentágono trabaja en desarrollar unas partículas invisibles que puedan ser inyectadas en el cuerpo humano, para leer o controlar el cerebro.](https://www.elconfidencial.com/tecnologia/novaceno/2021-03-24/pentagono-darpa-cerebro-control-nanoparticulas_3004028/)

[Apple ofrecerá aplicaciones aprobadas por el Gobierno Ruso preinstaladas en sus dispositivos.](https://www.macrumors.com/2021/03/16/apple-to-offer-government-approved-apps-russia/) Una noticia que genera pequeñas dudas con respecto a cómo de implicada está la compañía en la privacidad de sus usuarios y que se une a [la crítica de ProtonVPN, cuyas actualizaciones de la app han sido bloqueadas en la Apple Store](https://protonvpn.com/blog/apple-blocks-app-updates/) en un momento de gran necesidad en Myanmar, debido a las protestas por el golpe de estado.

[Un antiguo contratista del ejército estadounidense, ofrece localizar en tiempo real a coches específicos en casi cualquier país del mundo.](https://www.vice.com/en/article/k7adn9/car-location-data-telematics-us-military-ulysses-group)

Continúa el culebrón de Medialab, ahora [el Ministerio de Cultura pide frenar su mudanza](https://www.elconfidencial.com/espana/madrid/2021-03-14/medialab-cultura-ministerio-andrea-levy_2990871/) ya que podría afectar a la candidatura del eje Prado-Retiro para ser Patrimonio Mundial de la Unesco. El [Ayuntamiento ha comenzando ya dicha mudanza, y la comunidad cree que no es un traslado, es un desalojo.](https://www.elsaltodiario.com/medialab/medialab-prado-desaparece-traslado-desalojo) A pesar de ello, solo la mitad del equipo de Medialab ha sido trasladado, ya que [Madrid asume que tendrá que retrasar su plan por la candidatura de la Unesco.](https://www.elconfidencial.com/espana/madrid/2021-03-27/cultura-levy-ayuntamiento-madrid-medialab-retraso_3003823/)

[Reino Unido lleva dos años probando tecnología de vigilancia con la que rastrear la navegación web de cada persona del país.](https://www.genbeta.com/actualidad/reino-unido-lleva-dos-anos-probando-tecnologia-vigilancia-que-rastrear-navegacion-web-cada-persona-pais)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Tras el ciberataque, el SEPE perdió miles de solicitudes del paro y de ERTE,](https://www.vozpopuli.com/espana/ciberataque-sepe-solicitudes-paro-erte.html) mientras tanto el Gobierno [recurre a Eleven Paths para investigar el foco de dicho hackeo](https://www.lainformacion.com/economia-negocios-y-finanzas/gobierno-recurre-telefonica-investigar-foco-hackeo-sepe/2832258/), aunque los servicios de inteligencia españoles sospechan de que [Rusia ha estado detrás del ciberataque.](https://www.elconfidencial.com/espana/2021-03-21/rusia-hackeo-sepe-inteligencia-espana_3000291/)

[El Gobierno aprobará la Ley de Ciencia tras un pacto para revisar los contratos.](https://www.lainformacion.com/espana/ciencia-ley-gobierno-duque-investigadores-pedro-duque/2833491/)

[Amazón mantendrá su modelo de repartidores autónomos mientras se solucionan sus recursos judiciales.](https://www.lainformacion.com/empresas/amazon-pelea-judicial-inspeccion-riders-2022/2833361/).

A raíz de la "ley rider", surgen nuevas dudas sobre la calidad del trabajo de dichos trabajadores con la llegada de ETTs como [Jobandtalent, que controlará gran parte de la nueva economía en España.](https://www.eldiario.es/economia/ganadores-ley-rider-ett-controla-nueva-economia-espana_1_7324138.html)

Hace semanas hablábamos de los problemas existentes en EducaMadrid, y según parece [la Comunidad de Madrid ha cerrado un acuerdo con Microsoft para que todos sus centros educativos puedan usar Office 365](https://www.muycomputerpro.com/2021/03/09/comunidad-madrid-microsoft-office-365) en lugar de invertir recursos en la plataforma existente basada en tecnologías libres.

[Bruselas lanza su pasaporte de vacunación Covid basado en un QR gratuito y universal para evitar cuarentenas y PCR a los turistas.](https://www.elmundo.es/economia/macroeconomia/2021/03/17/6051f29f21efa07b528b465d.html) España urge la aprobación de dicho pasaporte y la [UE quiere que llegue en junio, pero los médicos avisan de los riesgos.](https://www.elconfidencial.com/mundo/europa/2021-03-26/certificado-digital-verde-pasaporte-derechos_3008044/) Algunos expertos dudan también de su utilidad y creen que el método tradicional de [un sello en una cartulina puede ser igual de útil y menos problemático.](https://www.businessinsider.es/cartulina-seria-suficiente-pasaporte-sanitario-820977)

La EU se prepara para regular el mundo digital, [Vestager (Comisaria CE): "Somos tan precisos porque se trata de asegurarnos de que un plan podrá ser llevado a la práctica".](https://www.eleconomista.es/economia/noticias/11114841/03/21/Vestager-Comisaria-CE-Somos-tan-precisos-porque-se-trata-de-asegurarnos-de-que-un-plan-podra-ser-llevado-a-la-practica-.html)

[La UE dará un máximo de una hora para que plataformas como Facebook o YouTube retiren contenidos terroristas](https://www.20minutos.es/noticia/4621386/0/la-ue-dara-un-maximo-de-una-hora-para-que-plataformas-como-facebook-o-youtube-retiren-contenidos-terroristas/), una medida criticada por la sociedad civil y por más de 60 organizacións que han escrito una [carta a los eurodiputados para frenar la censura automatizada en línea](https://www.liberties.eu/es/stories/reglamento-contenido-terrorista-carta-abierta-europarlamentarios/43410) ya que consideran que esta medida va mucho mas allá del control de contenido terrorista.

[Tim Wu, el "pade de la neutralidad de la red" se une a la administración Biden,](https://www.theverge.com/2021/3/5/22315224/tim-wu-net-neutrality-antitrust-big-tech-biden-administration-national-economic-council) una noticia que coincide en el tiempo con una publicación del Wall Street Journal donde se habla sobre [los vículos de Biden y la Casa Blanca con las Big Tech.](https://www.wsj.com/articles/biden-white-houses-ties-to-big-tech-are-detailed-in-new-disclosures-11616291989)

[Una sentencia del Tribunal Supremo de los Estados Unidos puede dar forma a cómo la Cuarta Enmienda protege a los ciudadanos en la era digital.](https://www.brennancenter.org/our-work/policy-solutions/fourth-amendment-digital-age)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

XNET ha publicado [el Plan de Digitalización Democrática de la Educación con el que el realizarán un primer piloto ](https://xnet-x.net/plan-digitalizacion-democratica-educacion-piloto-barcelona/) con Ayuntamiento y el Consorcio de Educación de Barcelona.

pCloud ha realizado un estudio sobre [las apps más invasivas compartiendo datos personales.](https://blog.pcloud.com/invasive-apps/)

["WhyPrivacyMatters"](https://whyprivacymatters.org/), una sencilla web que resume a la perfección los problemas que genera la pérdida de privacidad.

["No profit on pandemic"](https://noprofitonpandemic.eu/), una campaña para hacer que las vacunas y los tratamientos contra la COVID-19 sean un bien público y accesible de forma gratuita para todo el mundo, entre otras cosas, proponiendo liberar patentes.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El [Digital freedom fund](https://digitalfreedomfund.org/grants/) aún tiene disponibles sus "grants" con hasta 100.000€ para realizar litigaciones estratégicas en el campo de los derechos digitales en Europa. La convocatoria se encuentra abierta hasta el 31 de Marzo.

También se encuentra activo el programa [NGI Assure](https://www.assure.ngi.eu/open-calls/), con el fin de financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Abril de 2021.

La segunda open call de [NGI Pointer](https://ngi-pointer-open-call-2.fundingbox.com/) también está abierta y busca financiar con 200.000 € "equity-free" a personas y empresas que desarrollen protocolos y herramientas alineadas con los valores europeos. El plazo de presentación de solicitudes termina el 1 de Abril de 2021.

También recordar que [el Open Technology Fund ha vuelto con varias convocatorias abiertas](https://www.opentech.fund/funds/) entre las que se encuentran el Internet Freedom Fund, el Technology at Scale Fund y el Rapid Response Fund.

Al igual que [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

Francesca Bria, actual presidenta del Italian National Innovation Fund, ha hablado sobre [la soberanía digital y las smart cities](https://www.domusweb.it/en/sustainable-cities/2021/03/24/digital-sovereignty-and-smart-cities-what-does-the-future-hold.html) y también acerca de [cómo debe ser la reactivación europea.](https://www.lavanguardia.com/cultura/20210320/6535485/next-generation-francesca-bria-barcelona-datos-inteligencia-artificial.html)

Wikimedia continua su ciclo de entrevistas con Simona Levi (Co-fundadora de Xnet, co-directora del posgrado en tecnopolítica y derechos en la era digital de la UB y socia de WMES) para hablar sobre la [incidencia política para un conocimiento más libre](https://www.youtube.com/watch?v=KV15UCt27SU)

[Reconocimiento facial: los algoritmos que se quedan con tu cara](https://www.newtral.es/reconocimiento-facial-algoritmos-cara-biometricos/20210217/) por Marilín Gonzalo.

["Vamos hacia redes sociales más cerradas porque ya no aguantamos no tener razón"](https://www.elconfidencial.com/tecnologia/2021-03-20/redes-sociales-propaganda-politica-internet-digital_2995171/), una entrevista a Leticia Rodríguez Fernández.

Y para acabar con un poco de diversión, [un pequeño video que me he encontrado sobre cómo gestionamos las cookies en la web](https://twitter.com/5tevieM/status/1375116382770171906) 😇

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
