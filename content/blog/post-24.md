---
title: "Los problemas del pasaporte covid, el leak de Facebook y Google trackeando móviles."
date: 2021-04-13T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-24.jpg"
# meta description
description: "Los problemas del pasaporte covid, el leak de Facebook y Google trackeando móviles."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

La semana empezaba movida con [el anuncio de que los datos de 533 millones de usuarios de Facebook, unos 11 millones de usuarios españoles, habían sido robados y publicados.](https://www.businessinsider.es/filtran-datos-robados-533-millones-usuarios-facebook-841657) Unos datos que incluían información muy sensible como números de móvil o incluso ubicaciones.

Entre los afectados se encuentra el propio [Mark Zuckerberg, cuyo número de teléfono ha revelado que utiliza Signal.](https://www.indiatoday.in/technology/news/story/leaked-phone-number-of-mark-zuckerberg-reveals-he-is-on-signal-1787396-2021-04-05)

El Gobierno británico anuncia [una revolución militar 'high tech' con muchos menos soldados y más robots.](https://www.elconfidencial.com/tecnologia/2021-04-02/revolucion-militar-reino-unido-avance-tecnologico_3016336/)

Estas semanas se ha publicado la [lista de miembros del proyecto GAIA-X entre los que se encuentran empresas tan poco "privacy-friendly" como Palantir, Huawei o Alibaba entre otras.](https://www.data-infrastructure.eu/GAIAX/Redaktion/EN/Downloads/gaia-press-release-march-31-list-en.pdf)

[Un estudio publica que Android envía 20 veces más datos a Google que iOS a Apple.](https://www.muycomputer.com/2021/04/01/telemetria-de-android-ios-estudio/) Una noticia que salta al mismo tiempo que la asociación "noyb" ha [denunciado que el AAID (Android Advertising Identifier) permite a Google rastrear a los usuarios en sus teléfonos y no puede ser desactivado, incumpliendo así la legislación de protección de datos.](https://noyb.eu/en/buy-phone-get-tracker-unauthorized-tracking-code-illegally-installed-android-phones)

Por otro lado, [la EFF ha analizado FLoC (Federated Learning of Cohorts), la nueva herramienta de tracking que Google está probando en millones de navegadores](https://www.eff.org/deeplinks/2021/03/google-testing-its-controversial-new-ad-targeting-tech-millions-browsers-heres) y ha publicado la herramienta [Am I FLoCed?](https://amifloced.org/) para comprobar si nuestro navegador está incluido en este experimento.

[Google gana el juicio a Oracle después de 11 años y los tribunales confirman que copiar la API Java en Android fue un 'uso justo'](https://www.xatakandroid.com/mercado/google-gana-juicio-a-oracle-sentencia-confirma-que-copia-java-android-fue-uso-justo), aunque el tribunal no se ha pronunciado sobre si una API tiene copyright. [Aquí tenemos un hilo que profundiza un poco más en el tema.](https://twitter.com/emosqueira/status/1379106698493837316)

[Facebook bloquea también a Maduro por difundir información falsa sobre la COVID-19.](https://elpais.com/internacional/2021-03-27/facebook-bloquea-a-maduro-por-difundir-informacion-falsa-sobre-la-covid-19.html)

La polémica sobre el pasaporte COVID continúa, mientras [el Comité Europeo de Protección de Datos pide que se restrinja su uso para evitar abusos](https://elpais.com/sociedad/2021-04-02/el-comite-europeo-de-proteccion-de-datos-pide-que-se-restrinja-el-uso-del-futuro-pasaporte-de-vacunacion-covid-para-evitar-abusos.html), ya podemos ver cómo [se venden falsos pasaportes COVID británicos en la darkweb por 200 euros.](https://www.ticbeat.com/seguridad/ya-se-venden-falsos-pasaportes-covid-britanicos-en-la-darkweb-por-200-euros/)

Al respecto del tema, es importante destacar [este hilo de Carmela Troncoso, experta en privacidad y seguridad informática, en el que analiza en profundidad la implementación del pasaporte COVID](https://twitter.com/carmelatroncoso/status/1380439779951673347) y todos sus problemas.

[La llegada del primer sindicado de Amazon tendrá que esperar, los trabajadores que trataban de impulsarlo han perdido la batalla](https://www.bbc.com/mundo/noticias-internacional-56696522). Una lucha de meses, con muchas presiones como comentábamos en la anterior edición de Techtopias, en el que se han creado hasta [usuarios fake de Twitter apoyando a Amazon en su lucha por evitar que sus empleados se sindicaran.](https://www.theguardian.com/technology/2021/mar/30/amazon-twitter-defenders-fake-accounts)

[La carga inalámbrica irrumpe en el pulso entre Apple y la UE por un conector igual para todos los móviles.](https://elpais.com/tecnologia/2021-03-30/la-carga-inalambrica-irrumpe-en-el-pulso-entre-apple-y-la-ue-por-un-conector-igual-para-todos-los-moviles.html)

[Los equipos de seguridad de Google bloquean un ciberataque que era una operación antiterrorista.](https://hipertextual.com/2021/03/google-desmonto-operacion-hackers-antiterrorista)

[Empresas como Cabify o Blablacar quieren que la Unión Europea establezca un fondo de 100.000 millones de euros para impulsar el sector tecnológico](https://www.businessinsider.es/varias-startups-piden-ue-fondo-tecnologico-118-mil-millones-838705) y así tratar de evitar que las empresas europeas tengan que recurrir a EEUU, como por ejemplo ha tenido que hacer Spotify.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Las instituciones públicas españolas adjudicaron múltiples contratos sin concurso público para evitar ciberataques.](https://www.elconfidencial.com/tecnologia/2021-04-05/civio-contratos-emergencia-aapp-ciberataques-ciberseguridad_3011232/) Unos datos que han sido publicados por la organización [Civio, en su análisis de los contratos públicos realizados durante la pandemia.](https://civio.es/area/contratacion/)

[El Gobierno de España planea invertir 450 millones de euros para impulsar el sector de la ciberseguridad y crear una "Academia Hacker".](https://www.xataka.com/pro/academia-hacker-estatal-inversion-450-millones-euros-asi-quiere-gobierno-impulsar-ciberseguridad-espanola)

Continúan los problemas en el SEPE, que no ha consegido recuperar la normalidad por el ciberataque sufrido hace unas semanas, y [miles de personas siguen teniendo problemas para cobrar a tiempo sus prestaciones.](https://www.xataka.com/empresas-y-economia/miles-personas-cobrar-a-tiempo-ciberataque-que-le-falta-al-sepe-para-recuperar-normalidad)

[El Gobierno acuerda con las operadoras de telecomunicaciones, un Comité que elaborará un listado semanal de webs “espejo” de descargas para cerrar sin esperar a los jueces.](https://www.xataka.com/legislacion-y-derechos/listado-semanal-webs-espejo-a-bloqueadas-asi-acuerdo-gobierno-operadoras-para-ampliar-persecucion-descargas)

[VeriPol, el polígrafo ‘inteligente’ de la policía, puesto en cuestión por expertos en ética de los algoritmos.](https://elpais.com/tecnologia/2021-03-08/veripol-el-poligrafo-inteligente-de-la-policia-puesto-en-cuestion-por-expertos-en-etica-de-los-algoritmos.html) Maldita ha publicado un artículo en el que [detalla un poco mas cómo funciona Veripol.](https://maldita.es/malditatecnologia/20210309/veripol-detector-de-mentiras-policia-denuncias-falsas-robos-guardia-civil/)

[Una propuesta de subida del 400% del canon digital amenaza con encarecer la tecnología.](https://www.vozpopuli.com/economia_y_finanzas/canon-digital-subida-moviles.html)

En la última edición hablábamos de la futura Ley de Ciencia, que parece que se ha topado con las quejas de [investigadores y sindicatos que creen que no resuelve las necesidades de estabilización del personal investigador.](https://www.elsaltodiario.com/ciencia/investigadores-se-rebelan-contra-modificacion-ley-ciencia-tenure-track-sin-derechos-no-hay-investigacion)

[UK podría obligar a Facebook a añadir puertas traseras en sus servicios para el acceso policial.](https://www.theguardian.com/technology/2021/apr/01/uk-may-force-facebook-services-to-allow-backdoor-police-access)

[La enorme influencia de las grandes empresas tecnológicas provoca un rechazo a nivel estatal en EEUU.](https://apnews.com/article/new-york-data-privacy-coronavirus-pandemic-bills-laws-a8d5ad6c5089cbcbfb51db128a42b215)

[El Gobierno de Australia plantea forzar a los usuarios de redes sociales y apps de citas a indentificarse personalmente para poder utilizarlas.](https://www.clarin.com/tecnologia/australia-busca-redes-sociales-dejen-anonimas_0_703q0SSWW.html)

Una noticia de economía que afectará en gran medida a las Big Tech: [EE UU pedirá en el G-20 armonizar el impuesto de sociedades a nivel global al 28%.](https://cincodias.elpais.com/cincodias/2021/04/05/economia/1617625688_822481.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Signal lanza en su beta "Signal Payments"](https://gizmodo.com/signal-is-adding-a-new-privacy-focused-payments-feature-1846630444), una feature que permitirá realizar pagos con privacidad de forma sencilla a través de una criptomoneda.

La Electronic Frontier Foundation lanza [About Face](https://act.eff.org/action/about-face) en EEUU, que se une a [Reclaim Your Face](https://reclaimyourface.eu/) en Europa para tratar de detener el reconocimiento facial masivo. Al respecto de esto, Surfshark ha publicado [un artículo analizando cómo de extendido está el reconocimiento facial por el mundo.](https://surfshark.com/facial-recognition-map)

[ADNAUSEAM](https://adnauseam.io/) es una extensión diseñada para ofuscar los datos de navegación, que hace click en los anuncios de las diversas webs de forma aleatoria.

[GDPR Enforcement Tracker](https://www.enforcementtracker.com/) es una recopilación de las multas que ha impuesto la UE bajo la GDPR, relacionadas con protección de datos.

AccessNow lanza [una carta para pedir a Spotify que no manipule nuestras emociones,](https://www.accessnow.org/spotify-tech-emotion-manipulation/) tras la patente publicada hace unas semanas en la que se indicaba que la empresa pretende obtener datos personales de sus usuarios a partir del reconocimiento de voz.

[DuckDuckGo permite bloquear FLoC, el nuevo método de tracking de Google en Chrome.](https://spreadprivacy.com/block-floc-with-duckduckgo/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[eSSIF-Lab vuelve a lanzar convocatoria para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/open-calls/) con hasta 155K €. El plazo se encuentra abierto hasta el 30 de Junio de 2021.

También se encuentra activo el programa [NGI Assure](https://www.assure.ngi.eu/open-calls/), con el fin de financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Junio de 2021.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Junio de 2021.

También recordar que [el Open Technology Fund mantiene varias convocatorias abiertas](https://www.opentech.fund/funds/) entre las que se encuentran el Internet Freedom Fund, el Technology at Scale Fund y el Rapid Response Fund.

Al igual que [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

Un análisis interesante de Mozilla sobre [cómo funciona el SIM swapping](https://blog.mozilla.org/firefox/mozilla-explains-sim-swapping/).

[El reconocimiento facial necesita una cuarentena](https://www.elespanol.com/invertia/disruptores-innovadores/opinion/20210403/reconocimiento-facial-necesita-cuarentena/570572939_13.html) por Esther Paniagua.

[Why Democracy Needs Privacy](https://bostonreview.net/science-nature-politics/carissa-veliz-power-privacy) por Carissa Veliz.

[¿Retos clave en el uso de la inteligencia artificial para el bien común?](https://twitter.com/e_paniagua/status/1380170606667309058), un podcast sobre la gobernanza de la IA.

["Vigilancia informática"](https://www.publico.es/publico-tv/en-la-frontera/programa/947421/vigilancia-informatica-entrevista-a-gemma-galdon-en-la-frontera-7-de-abril-de-2021), una entrevista Gemma Galdón.

["Finn Myrstad on privacy case against Grindr, 'dark patterns'"](https://iapp.org/news/a/finn-myrstad-on-privacy-case-against-grindr-dark-patterns/), una discusión sobre estos patrones oscuros.

Y para acabar nuevamente con algo divertido, a ver si eres capaz de rechazar todas las cookies 🍪 en [Cookie Consent Speed.Run](https://cookieconsentspeed.run/)

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
