---
title: "La regulación de la IA, los ciberataques a la administración y la oferta de empleo a hackers."
date: 2021-04-27T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-25.jpg"
# meta description
description: "La regulación de la IA, los ciberataques a la administración y la oferta de empleo a hackers."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[El regulador de datos aleman trata de obligar a Facebook a dejar de utilizar los datos de WhatsApp.](https://www.genbeta.com/actualidad/autoridades-datos-alemanas-creen-que-whatsapp-facebook-pueden-compartir-datos-ue-quieren-bloquearlo)

[Facebook planea crear un Instagram para niños y más de 30 organizaciones instan a la compañía a abandonar sus planes.](https://elpais.com/tecnologia/2021-04-15/mas-de-30-organizaciones-instan-a-facebook-a-abandonar-sus-planes-de-un-instagram-para-ninos.html)

En la pasada edición comentábamos el enorme leak de datos que ha sufrido Facebook, [esta vez le ha tocado a Clubhouse al publicarse los datos de 1.3 millones de sus usuarios.](https://cybernews.com/security/clubhouse-data-leak-1-3-million-user-records-leaked-for-free-online/)

También se han publicado [los datos personales de más de 1 millón de clientes de Phone House tras no pagar el rescate del ciberataque sufrido.](https://www.genbeta.com/actualidad/difundidos-datos-personales-millon-clientes-phone-house-no-pagar-rescate-ciberataque-sufrido)

[La justicia australiana determina que Google engañó a sus usuarios recopilando datos sobre su ubicación a pesar de tener la opción desactivada](https://www.businessinsider.es/justicia-australiana-determina-google-engano-usuarios-849019). Una noticia que se une a la publicada esta semana en The Wall Street Journal donde se muestra que [la empresa usó datos de subastas previas, para dar más visibilidad a sus propios anuncios frente al resto.](https://dircomfidencial.com/marketing-digital/google-uso-datos-de-subastas-previas-para-dar-ventaja-a-su-sistema-sobre-competidores-a-costa-de-los-medios-20210413-0400/)

Parece que la llegada de FLoC no entusiasma a nadie más que a Google, [ningun otro navegador web se une a su propuesta.](https://www.theverge.com/2021/4/16/22387492/google-floc-ad-tech-privacy-browsers-brave-vivaldi-edge-mozilla-chrome-safari)

[Parler planea volver a la App Store de Apple gracias a los cambios que ha realizado en su sistema de moderación de contenido.](https://www.npr.org/2021/04/20/989115774/far-right-friendly-platform-parler-expected-to-return-to-app-store-next-week)

Como comentábamos, estas últimas semanas han sido bastante intensas en ciberataques, un ejemplo son [los datos que se encuentran a la venta en internet, de la división chilena de Eleven Paths y Telefónica](https://derechodelared.com/venta-datos-de-eleven-paths-y-telefonica/). También podemos ver el [ataque al Ayuntamiento de Castellón en el que se han filtrado datos de víctimas de maltrato, atestados policiales o contraseñas.](https://www.elconfidencial.com/tecnologia/2021-04-17/varon-suicidarse-lejia-hackeo-ayuntamiento-espanol_3031408/)

Y la última noticia es [la oleada de ciberataques que ha tumbado las webs del INE, Justicia, Economía y más ministerios](https://www.elconfidencial.com/tecnologia/2021-04-23/ciberataques-ccn-cni-justicia-ine-interior_3047336/), una advertencia más sobre la necesidad de invertir fuertemente en ciberseguridad.

[La policía de NY usó reconocimiento facial ilegal durante meses utilizando la tecnología de Clearview AI.](https://www.technologyreview.es/s/13220/la-policia-de-ny-uso-reconocimiento-facial-ilegal-durante-meses)

[Microsoft obtiene un contrato de 22.000 millones de dólares para proveer a la US Army con 120.000 dispositivos de realidad aumentada.](https://techcrunch.com/2021/03/31/microsoft-wins-contract-worth-up-to-22-billion-to-outfit-u-s-army-with-120000-ar-headsets/)

[México quiere vincular números telefónicos con datos biométricos y crear un registro con toda esa información.](https://www.xataka.com/moviles/quieres-usar-movil-dame-tu-huella-dactilar-asi-como-mexico-quiere-vincular-numeros-telefonicos-datos-biometricos)

[El FBI, con autorización judicial, elimina web shells de servidores de Microsoft Exchange de cientos de organizaciones estadounidenses.](https://www.cyberscoop.com/fbi-court-order-microsoft-exchange-server-web-shells/)

[La justicia de UK absuelve a 39 operadores de correos condenados por corrupción, robo y fraude.](https://www.theguardian.com/uk-news/2021/apr/23/court-clears-39-post-office-staff-convicted-due-to-corrupt-data) Uno de los mayores errores judiciales en la historia de Inglaterra y todo por culpa de un algoritmo de Fujitsu que no funcionaba correctamente.

Con las elecciones madrileñas a la vuelta de la esquina, [los partidos políticos se han lanzado a WhatsApp, Telegram y otras redes a cazar votos, muchos ignorando las leyes de protección de datos e incluso comprando bases de datos.](https://www.elconfidencial.com/tecnologia/2021-04-21/chapuza-partidos-whatsapp-cazar-votos-ayuso-madrid_3042739/)

[Un tribunal obliga a Uber a readmitir a cinco conductores británicos despedidos por un proceso automatizado](https://www.theguardian.com/technology/2021/apr/14/court-tells-uber-to-reinstate-five-uk-drivers-sacked-by-automated-process)

[Mas de 8.000 teletrabajadores se han instalado en los últimos meses en Canarias](https://elpais.com/tecnologia/2021-04-17/los-teletrabajadores-empiezan-a-colonizar-canarias.html), el futuro del trabajo también puede potenciar una nueva modalidad de turismo.

Por otro lado la robotización también puede suponer un gran cambio en nuestro sistema productivo y social, de hecho [el FMI alerta del riesgo de disturbios sociales por la robotización en plena pandemia](https://www.elconfidencial.com/economia/2021-04-23/fmi-alerta-riesgo-disturbios-sociales-robotizacion-coronavirus_3045156/).

[CNMC envía a Irlanda la primera gran reclamación contra TikTok en España](https://www.lainformacion.com/empresas/cnmc-envia-irlanda-primera-reclamacion-contra-tiktok-espana/2835557/).

Wired escribe sobre cómo [España pretente romper la caja negra de los algorítmos de las empresas de la economía colaborativa.](https://www.wired.co.uk/article/spain-gig-economy-algorithms)

[Se lanza una iniciativa para que el ciudadano pueda consultar todos sus antecedentes médicos desde el móvil](https://elpais.com/tecnologia/2021-04-13/una-iniciativa-para-consultar-todos-tus-antecedentes-medicos-desde-el-movil.html). Una propuesta que también genera dudas al centralizar tanta información sensible en un único punto y que también nos lleva a recordar la [propuesta que Tim Berners-Lee e Inrupt están probando en UK para que paciente sea dueño de sus datos.](https://www.bbc.com/news/technology-54871705)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El CNI publica una oferta de empleo en la que busca hackers para su departamento de ciberseguridad](https://www.20minutos.es/noticia/4655406/0/el-cni-publica-una-oferta-de-empleo-en-la-que-busca-hackers-para-su-departamento-de-ciberseguridad/), una pequeña buena noticia en el campo de la ciberseguridad que contrasta con [la academia hacker que había anunciado el gobierno hace unas semanas y que finalmente será un curso básico de ciberseguridad para animar a adolescentes a profundizar en esta disciplina.](https://www.xataka.com/pro/academia-hacker-que-anuncio-gobierno-curso-ciberseguridad-basico-para-adolescentes-busca-suscitar-su-interes-esta-disciplina)

[La UE admite que cada país ponga límites al pasaporte de vacunación](https://elpais.com/sociedad/2021-04-14/los-gobiernos-de-la-ue-acuerdan-restringir-el-uso-y-duracion-del-pasaporte-de-vacunacion.html) mientras que en Reino Unido [más de 60 empresas de hostelería se niegan a obligar a sus clientes a mostrar sus pasaportes COVID.](https://www.telegraph.co.uk/politics/2021/04/13/exclusive-wont-make-customers-show-covid-passports-hospitality/)

[España aprueba el "Sello de Reparabilidad" que llevarán los productos, para luchar contra la obsolescencia programada.](https://computerhoy.com/noticias/tecnologia/espana-aprueba-indice-reparabilidad-830169)

[La CNMV inicia la ronda de consultas para regular el Bitcoin y los criptoactivos.](https://www.lainformacion.com/mercados-y-bolsas/cnmv-regulacion-bitcoin-criptomonedas-criptoactivos/2834326/)

La UE se prepara para regular la IA y estas semanas [se ha filtrado el primer borrador de dicha regulación.](https://www.xataka.com/pro/filtrado-borrador-regulacion-europea-ia-primeras-impresiones-tres-expertos-materia) Según parece, el borrador pretende [prohibir la vigilancia biométrica masiva en ciertos casos y los sistemas de puntuación sociales basados en IA](https://cincodias.elpais.com/cincodias/2021/04/21/companias/1619008908_532683.html).

A pesar del anuncio, [grupos como EDRI creen que la propuesta de regulación no resuelve el problema y se queda muy corta ya que no impone una prohibición en la mayoría de los casos de vigilancia facial masiva](https://edri.org/our-work/new-ai-law-proposal-calls-out-harms-of-biometric-mass-surveillance-but-does-not-resolve-them/).

Seguimos obteniendo poco a poco más información sobre [la 'nube europea' Gaia-X, que no tendrá centros de datos ni cables, sino estándares para compartir información entre proveedores.](https://www.elespanol.com/invertia/disruptores-innovadores/innovadores/tecnologicas/20210404/europea-gaia-x-no-centros-estandares-compartir-informacion/569693598_0.html)

[China multa a Alibaba con 2.800 millones de dólares por prácticas monopolísticas,](https://www.cnbc.com/2021/04/09/china-fines-alibaba-in-anti-monopoly-probe.html) una multa que se convierte [en una advertencia para todos los gigantes tecnológicos de China.](https://www.bbc.com/mundo/noticias-56754282)

[Cabify y Glovo se han pasado a los envíos de paquetería y entran en el radar de la CNMC](https://www.lainformacion.com/economia-negocios-y-finanzas/cabify-glovo-entran-radar-competencia-negocio-envio-paquetes-domicilio/2835450/) mientras los operadores de paquetería [se preparan para demandar a Correos por el cobro en el uso de sus Citypaq.](https://cincodias.elpais.com/cincodias/2019/12/13/companias/1576259869_759171.html)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

Ante los continuos leaks de datos de las últimas semanas, es interesante recordar la existencia de plataformas como [have i been pwned](https://haveibeenpwned.com/) que permite comprobar si nuestros datos han sido comprometidos.

[Simplelogin](https://simplelogin.io/), permite crear un alias de email para proteger la privacidad de sus usuarios cada vez que se registran en un servicio. De esta forma utilizarán correos diferentes para cada uno.

[/e/ OS ofrece teléfonos móviles libres de Google con garantía](https://arstechnica.com/gadgets/2021/03/google-free-e-os-is-now-selling-pre-loaded-phones-in-the-us-starting-at-380/), utilizando LinageOS como sistema operativo y diversas apps alternativas. [En este enlace se puede acceder a la tienda online.](https://esolutions.shop/shop/)

DerechosDigitales ha publicado su estudio sobre ["Sistemas de identificación y protección social en Venezuela y Bolivia: Impactos de género y otras formas de discriminación"](https://www.derechosdigitales.org/wp-content/uploads/sistemas-de-Identificacion_ES.pdf).


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Pulsante matiene abierto su Fondo para Organizaciones de Empoderamiento Cívico](https://pulsante.org/fondo_organizaciones/) que podrán solicitar entre 80.000 y 120.000 dólares. La convocatoria termina el 30 de Abril de 2021.

[eSSIF-Lab vuelve a lanzar convocatoria para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/open-calls/) con hasta 155K €. El plazo se encuentra abierto hasta el 30 de Junio de 2021.

También se encuentra activo el programa [NGI Assure](https://www.assure.ngi.eu/open-calls/), con el fin de financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Junio de 2021.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Junio de 2021.

También recordar que [el Open Technology Fund mantiene varias convocatorias abiertas](https://www.opentech.fund/funds/) entre las que se encuentran el Internet Freedom Fund, el Technology at Scale Fund y el Rapid Response Fund.

Al igual que [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

["Por qué los populismos y las redes sociales hacen tan buena pareja"](https://twitter.com/CarmelaRios/status/1384552875859353603), un hilo de Carmela Ríos.

["No a FLoC: otra buena jugada de Brave"](https://www.enriquedans.com/2021/04/no-a-floc-otra-buena-jugada-de-brave.html), por Enrique Dans.

[“Los algoritmos son superdifusores de contenido falso, lo difícil es hacer viral su corrección”](https://elpais.com/tecnologia/2021-04-15/los-algoritmos-son-superdifusores-de-contenido-falso-lo-dificil-es-hacer-viral-su-correccion.html), una entrevista a Evan Henshaw-Plath.

[En Maldita Twitcheria han hablado sobre cómo funciona la vigilancia online](https://www.twitch.tv/videos/996578219?t=01h38m49s) con un montón de expertos invitados.

[Sobre el rol de Digital Ethics Officer y la ética de los datos en las empresas](https://mobile.twitter.com/manuelabat/status/1384815270057947137), un hilo de Manuela Battaglini.

[Coded Bias](https://www.netflix.com/es/title/81328723), un documental de Netflix sobre sesgos y errores en el reconocimiento facial.

["Data Is Power. Washington Needs to Craft New Rules for the Digital Age",](https://www.foreignaffairs.com/articles/united-states/2021-04-16/data-power-new-rules-digital-age) un artículo de Matthew J. Slaughter y David H. McCormick.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
