---
title: "El fin de las comunicaciones privadas, el hackeo de Glovo y la investigación a Apple."
date: 2021-05-11T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-26.jpg"
# meta description
description: "El fin de las comunicaciones privadas, el hackeo de Glovo y la investigación a Apple."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Twitter comienza a mostrar avisos a los usuarios con el objetivo de evitar respuestas tóxicas.](https://www.nbcnews.com/tech/social-media/twitter-begins-show-prompts-people-send-mean-replies-rcna839)

[Manhunt, el portal de citas gay, ha sido atacado y miles de cuentas y datos personales han sido robados.](https://techcrunch.com/2021/04/14/gay-dating-site-manhunt-hacked/)

En la lista de ataques informáticos, también se encuentra el caso de [Glovo, cuyos atacantes han vendido el acceso a cuentas de clientes y mensajeros.](https://www.forbes.com/sites/thomasbrewster/2021/05/04/exclusive-hackers-break-into-2-billion-delivery-startup-glovo/)

[Las nuevas apps para controlar el COVID en parques temáticos como Disneyland también almacenan datos de sus usuarios.](https://www.latimes.com/business/technology/story/2021-04-17/disneyland-theme-parks-apps-covid-distancing-virtual-lines)

[Varias tecnológicas como Google, Amazon o Microsoft están impulsando nuevos datacenters en España.](https://www.businessinsider.es/big-tech-invierten-centros-datos-espana-843717)

[Diversos eurodiputados han sido engañados mediante videollamadas con tecnologías "deepfake", imitando a líderes de la oposición rusa.](https://www.theguardian.com/world/2021/apr/22/european-mps-targeted-by-deepfake-video-calls-imitating-russian-opposition)

[Amazon abre su primer salón de belleza, esta vez en Londres.](https://blog.aboutamazon.co.uk/shopping-and-entertainment/introducing-amazon-salon)

Esta semana también se ha conocido que [el negocio de publicidad de Amazon no deja de crecer y está generando ya, sobre 7.000 millones de dólares cada trimestre, y creciendo.](https://www.cnbc.com/2021/04/29/amazons-ads-business-nears-7-billion-a-quarter-posts-77percent-growth.html)

[Facebook ha publicado sus planes sobre cómo monetizar WhatsApp](https://edition.cnn.com/2021/04/28/tech/facebook-whatsapp-earnings/), una noticia que coincide también con [la nueva política de privacidad de la app que finalmente no será necesario aceptar.](https://www.genbeta.com/mensajeria-instantanea/whatsapp-se-retracta-nueva-politica-privacidad-llega-dia-15-no-aceptamos-no-se-desactivara-nuestra-cuenta)

[Apple también apuesta por el negocio de la publicidad y los últimos cambios en temas de privacidad están diseñados para impulsar sus productos publicitarios.](https://www.wsj.com/articles/apples-privacy-changes-are-poised-to-boost-its-ad-products-11619485863?mod=djemalertNEWS)

Unos cambios ante los que [Google ha reaccionado y seguirá los pasos de Apple añadiendo etiquetas de privacidad en la Play Store.](https://hipertextual.com/2021/05/etiquetas-de-privacidad-google-play-store)

[Artistas y activistas piden a Spotify que descarte la implantación de su tecnología invasiva de reconocimiento de voz](https://www.reuters.com/article/us-tech-music-privacy/spotify-urged-to-rule-out-invasive-voice-recognition-tech-idUSKBN2CL1K9), tecnología por la que ha sido noticia semanas atrás.

El salseo entre las startups tecnológicas estas semanas ha estado alrededor de Basecamp, la empresa que entre otras herramientas ha lanzado el sistema de correo electrónico "Hey", y [donde un tercio de sus trabajadores han dimitido despues de que la empresa prohibiese hablar de política dentro del entorno laboral.](https://www.elconfidencial.com/tecnologia/2021-05-04/basecamp-hundimiento-silicon-valley-guerra-cultural_3063643/)

[Un error de Google permitía acceder a información privada de usuarios en las apps de rastreo de contactos.](https://elpais.com/tecnologia/2021-04-27/una-investigacion-revela-un-error-de-google-en-la-privacidad-de-las-aplicaciones-de-rastreo-de-contactos.html)

[Newtral obtiene, tras 6 meses de espera, la documentación de contratación de la app Radar Covid](https://www.newtral.es/radar-covid-transparencia-documentos/20210420/), así como también comparte la publicación en el BOE de [un contrato de 1.5M € para promocionar su uso.](https://www.newtral.es/novedades-boe-comunidades-convenios-covid/20210427/)

[Google mantiene las llaves sobre la privacidad en internet en Africa y Asia.](https://qz.com/2003273/googles-gaid-is-the-key-to-web-privacy-in-africa-and-asia/)

[El uso del reconocimiento facial por la policía gana terreno en el Reino Unido](https://elpais.com/tecnologia/2021-05-04/la-polemica-tecnologia-del-reconocimiento-facial-gana-terreno-en-reino-unido.html)

[Arranca el juicio de Epic Games contra Apple en su lucha por las comisiones de la App Store](https://www.xataka.com/aplicaciones/arranca-juicio-que-determinara-futuro-tienda-apple-epic-games-lucha-conseguir-ios-abierto), una noticia que se solapa con la presión de [la Comisión Europea que acusa a Apple de imponer comisiones abusivas a plataformas musicales como Spotify.](https://elpais.com/economia/2021-04-30/la-comision-europea-acusa-a-apple-de-imponer-comisiones-abusivas-a-plataformas-musicales-como-spotify.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Glovo se marcha de la CEOE por pactar la Ley Rider y forma una asociación con empresas sancionadas por usar falsos autónomos](https://www.eldiario.es/economia/glovo-marcha-ceoe-pactar-ley-rider-forma-asociacion-empresas-sancionadas-falsos-autonomos_1_7873510.html), todo mientras [UGT denuncia a Glovo, Amazon y Uber Eats por cesión ilegal de trabajadores](https://elpais.com/economia/2021-04-26/ugt-denuncia-a-glovo-amazon-y-uber-eats-por-cesion-ilegal-de-trabajadores.html), ahora contratados por ETT y empresas de logística.

La tecnología está profundizando en el campo laboral y poco a poco [los algoritmos llegan incluso a la negociación colectiva.](https://elpais.com/economia/2021-04-25/los-algoritmos-llegan-a-la-negociacion-colectiva.html)

[La nueva Ley Audiovisual que obligará a Netflix, HBO o Amazon a financiar cine europeo con el 5% de sus ingresos en España podría ver la luz en mayo o junio.](https://www.businessinsider.es/ley-obligara-netflix-financiar-cine-europeo-llegara-mayo-854469)

[Artigas anuncia que España presentará en junio la Carta de Derechos Digitales de los ciudadanos,](https://www.elespanol.com/wakeupspain/20210415/artigas-espana-presentara-carta-derechos-digitales-ciudadanos/573943047_0.html) una carta en la que Trackula ha participado a través de su consulta pública. Si tenéis curiosidad por ver nuestro punto de vista, [tenemos un post resumiéndolo.](http://techtopias.com/blog/post-17/)

[España ya trabaja para implantar el "visado covid" y pretende lanzarlo en mayo, pero hasta la UE recela.](https://www.elconfidencial.com/tecnologia/2021-04-30/batalla-tecnologica-covid-pasaporte-covid-seguro_3053111/) De hecho, esta misma semana el presidente [Pedro Sánchez ha presionado a los presidentes europeos para aprobarlo ya y salvar el turismo.](https://cadenaser.com/ser/2021/05/08/politica/1620472378_593835.html)

Mientras tanto, [Carmela Troncoso, conocida experta en seguridad y privacidad, ha publicado un nuevo hilo sobre el tema.](https://twitter.com/carmelatroncoso/status/1390676454573322249)

[Europa permitirá censurar contenidos en menos de una hora sin pasar por el juez con su nueva ley contra el terrorismo.](https://www.xataka.com/legislacion-y-derechos/europa-abre-puerta-a-eliminar-contenidos-hora-pasar-juez-su-nueva-ley-terrorismo)

[La regulación europea sobre IA sigue en el debate público](https://abcnews.go.com/International/wireStory/eu-proposes-rules-high-risk-artificial-intelligence-77212373), aunque diversos expertos creen que a pesar de ser [un avance positivo y necesario, es bastante abstracto](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/europa/20210501/europa-regula-inteligencia-artificial-positivo-necesario-abstracto/577443449_0.html) mientras que otros como [EDRI, creen que no resuelve el problema y que se debe ir más allá.](https://edri.org/our-work/new-ai-law-proposal-calls-out-harms-of-biometric-mass-surveillance-but-does-not-resolve-them/)

[EEUU apoya suspender las patentes de las vacunas anti-COVID](https://www.eldiario.es/internacional/eeuu-anuncia-apoyo-exencion-patentes-vacunas_1_7903561.html), una medida que ha hecho cambiar de postura a varios paises de la UE pero no a [Merkel que se opone al plan de Biden y enfría la posibilidad de que la UE apueste por suspender dichas patentes.](https://www.eldiario.es/internacional/merkel-opone-plan-biden-enfria-posibilidad-ue-apueste-suspender-patentes-vacunas-anti-covid_1_7904282.html)

Javier de la Cueva ha publicado un [hilo muy completo sobre el tema de la suspensión de patentes.](https://mobile.twitter.com/jdelacueva/status/1390056875736113152)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

La UE está preparando una legislación para perseguir la pornografía infantil que abriría la puerta a eliminar el cifrado de las comunicaciones y a la vigilancia masiva de los sistemas de mensajería. Patrick Breyer, miembro del Partido Pirata lo cuenta en ["El fin de la privacidad de la correspondencia digital"](https://www.patrick-breyer.de/en/posts/message-screening/?lang=en).

Un estudio de Global Witness [analiza si las personas realmente quieren personalizar los anuncios que ven en internet.](https://www.globalwitness.org/en/blog/do-people-really-want-personalised-ads-online/)

Wikimedia [pide al Parlamento europeo que rechace la votación final de TERREG, la nueva legislación antiterrorista.](https://www.wikimedia.es/2021/04/27/pedimos-al-parlamento-europeo-que-rechace-la-votacion-final-de-terreg/)

[Signal ha lanzado una campaña en Instagram para mostrar a sus usuarios lo que Facebook sabe de ellos.](https://signal.org/blog/the-instagram-ads-you-will-never-see/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[eSSIF-Lab vuelve a lanzar convocatoria para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/open-calls/) con hasta 155K €. El plazo se encuentra abierto hasta el 30 de Junio de 2021.

También se encuentra activo el programa [NGI Assure](https://www.assure.ngi.eu/open-calls/), con el fin de financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Junio de 2021.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Junio de 2021.

También recordar que [el Open Technology Fund mantiene varias convocatorias abiertas](https://www.opentech.fund/funds/) entre las que se encuentran el Internet Freedom Fund, el Technology at Scale Fund y el Rapid Response Fund.

Al igual que [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Mesa sobre la transposición de la nueva Directiva Europea sobre el Mercado Único Digital](https://www.youtube.com/watch?v=5Cyr3GC4H7A&t=32s) de la mano de Wikimedia España con Ariadna Matas (FESABID), Ignasi Labastida (Creative Commons España), Lorena Sánchez (Interferencias), Simona Levi (Xnet) y Virginia Díez (Wikimedia España).

["«Yo no tengo nada que esconder». La importancia de la privacidad"](https://mosaic.uoc.edu/2021/05/05/yo-no-tengo-nada-que-esconder-la-importancia-de-la-privacidad/), por Sofía Prósper en Mosaic.

["Un ex ‘youtuber’ de la ultraderecha explica cómo se propaga el odio"](https://www.nytimes.com/es/2021/04/21/espanol/youtube-ultraderecha-odio.html) por Cade Metz en The New York Times.

["For Big Tech whistleblowers, there’s no such thing as 'moving on'"](https://www.protocol.com/big-tech-whistleblowers) por Issie Lapowsky en Protocol.

["Facebook vs. Apple: la historia de cómo Mark Zuckerberg y Tim Cook se volvieron enemigos"](https://www.nytimes.com/es/2021/04/27/espanol/facebook-apple-zuckerberg-cook.html) por Mike Isaac y Jack Nicas en The New York Times.

[EMOSIDO ENGAÑADO: Sesgos cognitivos y desinformación](https://www.youtube.com/watch?v=CwdJkkMD8KQ) por Maldita Tecnología.

["Cuando el algoritmo que avisa a Asuntos Sociales cree que las familias negras son disfuncionales"](https://elpais.com/tecnologia/2021-05-09/cuando-el-algoritmo-que-avisa-a-asuntos-sociales-cree-que-las-familias-negras-son-disfuncionales.html) por Manuel G. Pascual en El País.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
