---
title: "El reglamento antiterrorista europeo, las fronteras con biometría y el cártel de las consultoras."
date: 2021-05-25T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-27.jpg"
# meta description
description: "El reglamento antiterrorista europeo, las fronteras con biometría y el cártel de las consultoras."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Chile ha decidido ser el primer país en regular los “neuroderechos” con la idea de evitar que manipulen nuestras mentes.](https://www.infobae.com/america/tecno/2021/05/08/que-son-los-neuroderechos-y-por-que-chile-quiere-ser-el-primer-pais-en-dar-un-paso-para-evitar-que-manipulen-tu-mente/)

[Descubiertas una serie de vulnerabilidades en el estándar WiFi que datan de 1997 y afectan a todo tipo de dispositivos vendidos en los últimos 24 años.](https://therecord.media/wifi-devices-going-back-to-1997-vulnerable-to-new-frag-attacks/)

[Intel invertirá 600 millones de dólares para expandir sus plantas en Israel.](https://www.reuters.com/world/middle-east/intel-invest-600-mln-expand-chip-mobileye-rd-israel-2021-05-02/)

Los ciberataques han sido portada en las noticias las últimas semanas, principalmente por [el secuesto de un oleoducto en Estados Unidos](https://cnnespanol.cnn.com/2021/05/11/ciberataque-oleoducto-estados-unidos-trax/), una llamada de atención para el futuro de la industria connectada. En el siguiente link podéis encontrar más información [sobre el grupo ruso DarkSide que ha estado detrás del ataque.](https://www.infobae.com/america/eeuu/2021/05/10/quienes-son-darkside-la-banda-de-cibercriminales-detras-del-ataque-a-uno-de-los-oleoductos-mas-importantes-de-eeuu/)

En España también hemos tenido lo nuestro con [el ciberataque que ha tumbado las webs del Tribunal de Cuentas, el Consejo de Seguridad Nuclear y varias instituciones](https://www.eldiario.es/tecnologia/ciberataque-proveedor-servicios-nube-tumbo-webs-instituciones-publicas_1_7920345.html) y también en Irlanda, donde [ha sido atacado el sistema nacional de salud, obligando a cerrar su sistema informático y a suspender las consultas.](https://www.abc.es/internacional/abci-ciberataque-obliga-sistema-salud-irlanda-cerrar-sistema-informatico-y-suspender-consultas-202105141055_noticia.html)

[Italia multa a Google por excluir a la app Enel de Android Auto.](https://www.reuters.com/technology/italys-antitrust-fines-google-102-million-euros-abuse-dominant-position-2021-05-13/)

[Amazon extiende su moratoria en la venta de software de reconocimiento facial a la policía](https://www.reuters.com/technology/exclusive-amazon-extends-moratorium-police-use-facial-recognition-software-2021-05-18/) al mismo tiempo que sus dispositivos [Amazon Ring se han convertido en la mayor red de vigilancia ciudadana de la historia de los Estados Unidos.](https://www.theguardian.com/commentisfree/2021/may/18/amazon-ring-largest-civilian-surveillance-network-us)

[Se descubre una vulnerabilidad que permite a las webs, la identificación única de los usuarios incluso utilizando Tor.](https://blog.desdelinux.net/descubrieron-una-vulnerabilidad-que-permite-el-seguimiento-de-usuario-incluso-si-usa-tor/)

[El CEO de Basecamp pide disculpas a su equipo en nuevo post](https://www.theverge.com/2021/5/4/22419799/basecamp-ceo-apologizes-staff-new-post) después del escándalo que llevó a que más de un tercio de sus trabajadores dejaran la compañía.

[Uber utiliza 50 empresas fantasma holandesas para evadir impuestos, según un informe](https://www.businessinsider.com/uber-tax-avoidance-50-dutch-shell-companies-5-billion-revenue-2021-5).

[A pesar de facturar 44.000 millones de euros en Europa, Amazon no ha pagado impuesto de sociedades.](https://www.theguardian.com/technology/2021/may/04/amazon-sales-income-europe-corporation-tax-luxembourg) Una noticia que coincide también con el fallo del Tribunal General de la Unión Europea [que declara nula la multa de la comisión europea que le pedía a Amazon 250 millones de euros.](https://www.theverge.com/2021/5/12/22431855/eu-amazon-back-tax-ruling-annulled)

[Twitch deja de funcionar durante varias horas en España por un fallo de las telecos que pueden bloquear webs sin necesidad de mandato judicial.](https://www.elconfidencial.com/tecnologia/2021-05-15/sistema-bloqueo-twitch-espana-error-fallo_3080936/)

[El regulador alemán prohibe a Facebook procesar los datos de los usuarios de WhatsApp al considerar ilegales los nuevos términos y condiciones.](https://www.reuters.com/business/legal/german-regulator-bans-facebook-processing-whatsapp-user-data-2021-05-11/)

[Las compañías tecnológicas quieren utilizar las ayudas COVID de las escuelas para introducir nuevas herramientas de vigilancia como reconocimiento facial y lectores de matrículas.](https://www.vice.com/en/article/pkbpz7/tech-companies-want-schools-to-use-covid-relief-money-on-surveillance-tools)

[La AEPD resuelve la denuncia de FACUA confirmando que la web de Alvise Pérez cometió 2 infracciones muy graves y 1 grave.](https://www.facua.org/es/noticia.php?Id=16758)

[El Tribunal Supremo cuestiona el Canon Digital y lo lleva a Luxemburgo.](https://www.vozpopuli.com/economia_y_finanzas/canon-digital-tribunal-supremo.html)

[Telegram se convierte en un altavoz de negacionistas de la COVID y ultraderechistas](https://www.lavanguardia.com/tecnologia/20210517/7450907/telegram-convierte-altavoz-negacionistas-covid-ultraderechistas.html), un interesante análisis sobre la plataforma de mensajería.

[Los gigantes tecnológicos continúan sus contactos con la CNMC para hablar sobre la directiva europea del copyright y otras normativas locales en materia audiovisual.](https://www.lainformacion.com/empresas/cani-fernandez-lidera-roadshow-cnmc-gigantes-tecnologicos-google-y-apple/2836928/)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[La CNMC multa con 6,3 millones a 22 consultoras por fraude en 172 concursos](https://www.lainformacion.com/empresas/la-cnmc-multa-con-6-3-millones-a-22-consultoras-y-13-directivos-del-cartel-de-bilbao/2838118/), una multa un tanto decepcionante tras varios años de investigación en donde pudimos ver [los correos que se intercambiaban las empresas para amañar ofertas.](https://www.eldiario.es/economia/correos-cartel-consultoras-amanar-ofertas-hay-hacerlas-si-fueramos-ganar_1_7927783.html)

El análisis de la geolocalización de la población parece que se está poniendo de moda en España, y este año podremos ver cómo se utiliza [para ‘vender’ Málaga en Sol, Atocha y Callao](https://www.elconfidencial.com/espana/2021-05-14/inteligencia-artificial-y-geolocalizacion-para-vender-malaga-en-sol-atocha-y-callao_3081431/) así como en el Ministerio de Transportes, que [monitorizará el turismo interior con otro rastreo de millones de móviles.](https://www.lainformacion.com/empresas/estudio-movilidad-movilia/2838802/)

España desarrollará un nuevo sistema de identificación personal para luchar contra [el drama de usabilidad de las apps públicas, que entra en la lista de deberes del Gobierno para los fondos europeos.](https://www.eldiario.es/tecnologia/drama-usabilidad-apps-publicas-entra-lista-deberes-gobierno-fondos-europeos_1_7949271.html)

La 'ley rider' sigue dando mucho de qué hablar ya que [no explica qué es lo que se debe saber del algoritmo que decide las rutas de reparto](https://www.elconfidencial.com/juridico/2021-05-15/informar-sobre-los-algoritmos-la-novedad-legal-de-la-ley-rider-plagada-de-lagunas_3081719/) y al mismo tiempo que diversos abogados y expertos [laboralistas creen que la norma "es insuficiente”](https://www.elconfidencial.com/juridico/2021-05-12/los-laboralistas-desinflan-la-ley-rider-es-una-normativa-insuficiente_3077628/).

Algunas empresas de reparto como [Glovo y Deliveroo anuncian que dejarán de repartir en ciudades pequeñas](https://www.elespanol.com/invertia/observatorios/digital/20210312/glovo-deliveroo-repartir-ciudades-espana-ley-rider/565193945_0.html) del país, mientras que otras como [Uber tiran de subcontratas como Parrondo, socio de Cabify, para montar sus flotas de riders.](https://www.lainformacion.com/empresas/uber-tira-socio-clave-cabify-parrondo-auro-montar-flotas-riders/2838687/)

Además, la vicepresidenta del gobierno, [Yolanda Díaz, avanza que ampliará la ley de los 'riders' a otros sectores de plataformas.](https://www.lainformacion.com/economia-negocios-y-finanzas/ley-riders-ampliar-otros-sectores-plataformas/2837965/)

[La CNMV vigilará de cerca a los criptoactivos, situándolos al mismo nivel que las acciones.](https://cincodias.elpais.com/cincodias/2021/05/06/mercados/1620323952_494727.html)

Barcelona prohibe la concesión de licencias a pisos turísticos durante un año mientras que [Airbnb rebate al ayuntamiento y asegura que ha creado 9.600 empleos en la ciudad.](https://cincodias.elpais.com/cincodias/2021/05/07/companias/1620366888_137340.html)

[Frontex anuncia la instalación de un sistema biométrico y con datos unificados para identificar a toda persona que entre en la UE.](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/europa/20210511/frontex-defendera-fronteras-exteriores-europa-digitalizado-unificados/580192450_0.html)

[Finalmente el Parlamento Europeo ha confirmado los nuevos poderes de censura online a través de la nueva regulación antiterrorista.](https://edri.org/our-work/european-parliament-confirms-new-online-censorship-powers/)

[El Certificado Verde Digital, el llamado "pasaporte covid", llegará el 1 de julio para facilitar los viajes en territorio comunitario.](https://www.xatakamovil.com/movil-y-sociedad/certificado-verde-digital-llega-1-julio-para-facilitar-viajes-asi-nuevo-pasaporte-covid)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[ProtonMail, el servicio de correo electrónico cifrado, confirma que ha superado los 50 millones de usuarios a nivel mundial.](https://techcrunch.com/2021/05/19/__trashed-13/?tpcc=ECTW2020&guccounter=1&guce_referrer=aHR0cHM6Ly90LmNvLw&guce_referrer_sig=AQAAAJhmWk-vIARPxEMWBXvvF7BOkB4Gp2is9XwshBv-W7ha4gK2igQ22aH1Lm1KPHlUDCEHMDGHzp3Ro6Ha8zQOfUIuNoNpe2FvYBJD_I2Kh6eHrjOs6RtcSM5G5PmBvDX4tLIrL_0TAKB8C4lhgH3UEeTfQAp4oR1iVVF9EK5nW6ee)

[Telegram anuncia sus futuros planes para monetizar y sus nuevas funcionalidades entre las que destacan las videollamadas al estilo Zoom o los pagos a través de la app.](https://www.eldiario.es/tecnologia/videollamadas-zoom-pagos-traves-app-planes-telegram-derrocar-whatsapp_1_7909588.html)

[Un grupo de activistas lanzan una campaña para tratar de salvar SCI-HUB, el también llamado "Pirate Bay de la ciencia"](https://www.vice.com/en/article/wx5nq9/activist-archivists-are-trying-to-save-the-pirate-bay-of-science). Para quien desee colaborar con la iniciativa, [en este link hay más información.](https://www.reddit.com/r/DataHoarder/comments/nc27fv/rescue_mission_for_scihub_and_open_science_we_are/)

[Una de las fundadoras de Element explica en un podcast cómo funciona su modelo de negocio y cómo ayuda a financiar el desarrollo de Matrix.](https://open.spotify.com/episode/7qYf5BLS0abUYF7I7IWAoK?si=u2IUC1-DR8-XKa-RVurUvg&nd=1)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGI Assure](https://www.assure.ngi.eu/open-calls/) se acerca a sus últimos días buscando financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Junio de 2021.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Junio de 2021.

[eSSIF-Lab también mantiene su convocatoria para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/open-calls/) con hasta 155K €. El plazo se encuentra abierto hasta el 30 de Junio de 2021.

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

También se mantiene abierto el [Information Controls Fellowship](https://www.opentech.fund/funds/icfp/) para investigar y examinar cómo los gobiernos de los paises restringen el acceso a internet, la censura, etc. con 5.000 dólares mensuales hasta 12 meses. La convocatoria termina el 30 de Junio de 2021.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[‘Make Algorithmic Audits As Ubiquitous As Seatbelts’— Why Tech Needs Outside Help To Serve Humanity](https://www.forbes.com/sites/ashoka/2021/04/26/make-algorithmic-audits-as-ubiquitous-as-seatbeltswhy-tech-needs-outside-help-to-serve-humanity/) una entrevista a Gemma Galdón en Forbes.

[Privacy activists are winning fights with tech giants. Why does victory feel hollow?](https://www.theguardian.com/commentisfree/2021/may/15/privacy-activists-fight-big-tech) por Evgeny Morozov en The Guardian.

[Proctoring: cuando la evaluación de exámenes es vigilancia](https://www.newtral.es/proctoring-que-es-evaluacion-examenes-vigilancia-unir/20210512/) por Marilín Gonzalo en Newtral.

[Nuestros derechos al compartir mediante mensajería](https://www.eldiario.es/murcia/murcia-y-aparte/derechos-compartir-mediante-mensajeria_132_7916963.html) por Gabriel Navarro en ElDiario.es.

[¿Por qué hace falta regular la inteligencia artificial?](https://maldita.es/malditatecnologia/20210517/regulaci%C3%B3n-inteligencia-artificial-union-europea/) en Maldita Tecnología.

[Censorship, Surveillance and Profits: A Hard Bargain for Apple in China](https://www.nytimes.com/2021/05/17/technology/apple-china-censorship-data.html) por Jack Nicas, Raymond Zhong y Daisuke Wakabayashi en The New York Times.

[El derecho a conocer los algoritmos que nos dirigen](https://cadenaser.com/programa/2021/05/15/a_vivir_que_son_dos_dias/1621067462_797194.html) con Gemma Galdón en Cadena Ser.


---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
