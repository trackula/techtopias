---
title: "Google en las aulas, el monedero digital y el impuesto de sociedades global."
date: 2021-06-08T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-28.jpg"
# meta description
description: "La alternativa a Google en las aulas, el monedero digital europeo y el impuesto de sociedades global."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[WhatsApp cambia de idea y finalmente no impedirá a sus usuarios llamar o enviar mensajes si no aceptan su nueva política de privacidad.](https://www.businessinsider.es/whatsapp-no-limitara-funciones-no-aceptas-nueva-politica-874237)

Una investigación acusa a Google de recopilar ilegalmente datos de localización de sus usuarios y [los empleados de la tecnológica admiten que la empresa ha hecho casi imposible que los usuarios mantengan su ubicación privada.](https://www.businessinsider.es/empleados-google-admiten-dificultad-ocultar-ubicacion-874111)

[Amazon compra Metro Goldwyn Mayer por 8.450 M](https://www.elconfidencial.com/tecnologia/2021-05-26/amazon-mgm-streaming-hollywood-netflix-hbo-disney_3097920/), un movimiento que permitirá a la compañía [darle un impulso a su negocio, ya masivo, de publicidad.](https://www.protocol.com/mgm-acquisition-amazon-ad-business)

[La mayor productora mundial de carne, JBS, suspende parte de su producción en Norteamérica por un ciberataque](https://www.efe.com/efe/america/economia/jbs-suspende-parte-de-su-produccion-en-norteamerica-por-un-ciberataque/20000011-4551355), un ataque que ocurre después de los recibidos el pasado mes de Mayo por uno de los mayores oleoductos de EEUU que dejó a medio país sin combustible.

[Múltiples activistas tratan de salvar a SCI-HUB, el "The Pirate Bay de la ciencia".](https://www.vice.com/en/article/wx5nq9/activist-archivists-are-trying-to-save-the-pirate-bay-of-science)

[Mientras juegas a Days Gone estás ofreciendo a Sony "datos ilimitados" sobre tu privacidad.](https://elchapuzasinformatico.com/2021/05/mientras-juegas-a-days-gone-estas-ofreciendo-a-sony-datos-ilimitados-de-tu-privacidad/)

En la anterior edición hablábamos de cómo la UE preparaba su plan de acción de economía circular, [el objetivo es acabar con la obsolescencia programada y reducir la basura electrónica.](https://ethic.es/2021/05/no-tire-su-movil-europa-quiere-acabar-con-la-basura-electronica/)

[Los hackers rusos de SolarWinds lanzan un nuevo ataque masivo de phising por email a múltiples agencias gubernamentales.](https://www.theguardian.com/technology/2021/may/28/russian-solarwinds-hackers-launch-assault-government-agencies)

Parece que la IA de seleccion de personal no funciona como debería, [simplemente añadiendo un fondo de biblioteca mejoran tus resultados.](https://twitter.com/HelenaMatute/status/1397474705347682308)

[TikTok se da permisos a si misma para recopilar datos biométricos de usuarios estadounidenses.](https://techcrunch.com/2021/06/03/tiktok-just-gave-itself-permission-to-collect-biometric-data-on-u-s-users-including-faceprints-and-voiceprints/)

[UCWeb, el navegador de Alibaba, obtiene datos de la búsqueda privada en millones de móviles Android y iPhone.](https://www.forbes.com/sites/thomasbrewster/2021/06/01/exclusive-alibabas-huge-browser-business-is-recording-millions-of-android-and-iphone-users-private-web-habits/)

[Múltiples influencers denuncian haber recibido propuestas de una agencia de medios relacionada con Rusia, para atacar a la vacuna de Pfizer.](https://www.theguardian.com/media/2021/may/25/influencers-say-russia-linked-pr-agency-asked-them-to-disparage-pfizer-vaccine)

[Maldita publica un estudio en donde confirma que los contenidos que se reenvían más de cinco veces en WhatsApp tienen hasta tres veces más probabilidades de ser un bulo.](https://www.businessinsider.es/contenidos-virales-whatsapp-suelen-ser-bulos-informe-876737) Todo esto ocurre la misma semana que [Maldita gana el European Press Prize 2021 por su chatbot de WhatsApp](https://maldita.es/nosotros/20210603/european-press-prize-innovacion-chatbot-maldita/). ¡Muchas felicidades por vuestro trabajo!

[Twitter prepara tres niveles para advertir a los usuarios sobre la desinformación que detecte.](https://www.elconfidencial.com/tecnologia/2021-05-31/twitter-prepara-tres-niveles-para-advertir-sobre-la-desinformacion-que-detecte_3108756/)

[Palantir está construyendo a cambio de £1, la plataforma para el COVID-19 que utilizarán los departamentos de salud estadounidense y británico.](https://twitter.com/StefSimanowitz/status/1282732079793483778) Todo apunta a que los datos serán el método de pago.

[Elon Musk (Tesla), Tim Cook (Apple) y Peter Thiel (Palantir) apuestan crear ciudades privadas y con sus propias normas](https://elpais.com/gente/2021-05-24/los-magnates-tecnologicos-quieren-construir-sus-propias-ciudades-con-sus-propias-leyes.html), una propuesta que básicamente supone recuperar el feudalismo en pleno siglo XXI.

[La asociación NOYB prepara una demanda contra 560 webs, 57 de ellas españolas, tras su éxito contra Facebook.](https://elpais.com/tecnologia/2021-05-31/los-activistas-que-doblegaron-a-facebook-preparan-una-demanda-masiva-contra-el-mal-uso-de-las-cookies.html)

Estas semanas se ha hablado bastante sobre la posibilidad de que el gobierno de España haya aprobado un "software de policía predictiva", [Maldita nos cuenta un poco más para desmentir los bulos sobre esta posibilidad de detener a alguien antes de que cometa un delito.](https://maldita.es/malditatecnologia/20210526/software-interior-policia-predictiva-detener-personas/)

[La UE se prepara para investigar por monopolio a Facebook marketplace.](https://www.reuters.com/technology/facebook-marketplace-faces-eu-antitrust-probe-source-2021-05-26/)

[El algoritmo de distribución de vacunas en California basado en áreas censales ha cambiado a códigos postales, dejando fuera de la priorización a 2 millones de personas vulnerables](https://twitter.com/gemmagaldon/status/1398025631301050370), cuenta Gemma Galdón.

[Un grupo de profesores de la Universidad de La Laguna impulsa una fundación para utilizar tecnología ética en la educación y proponer una alternativa a Google en las aulas.](https://www.eldiario.es/canariasahora/sociedad/alternativa-google-aulas-docentes-canarios-promueven-tecnologia-etica-educacion_1_7981842.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[España no ha transpuesto antes del 7 de junio la ley europea de Propiedad Intelectual](https://www.eldiario.es/economia/espana-no-transpondra-7-junio-ley-europea-propiedad-intelectual_1_7988373.html) por lo que se abre un plazo de gracia de seis meses antes de ser sancionada. [Una transposición de vital importancia que amenaza fuertemente a la libertad en internet como nos cuenta Lorena Sánchez en este elaborado hilo.](https://twitter.com/LastStrawberry/status/1401518551584870403)

[El Ministerio de Economía prepara la mayor reforma de la Bolsa para abrirla a la tecnología "blockchain".](https://cincodias.elpais.com/cincodias/2021/05/21/mercados/1621596233_934110.html)

[La Unión Europea abandona WhatsApp y se pasa a Signal para tener más seguridad.](https://www.elespanol.com/omicrono/software/20210526/union-europea-abandona-whatsapp-signal-tener-seguridad/584192025_0.html)

[La Unión Europea también prepara un "monedero digital" con el que almacenar en unas misma aplicación el DNI, carnet de conducir, pagos y contraseñas.](https://www.xataka.com/aplicaciones/union-europea-nos-prepara-identidad-digital-dni-carnet-conducir-pagos-contrasenas-aplicacion)

Lucía Velasco desvela algunas novedades sobre [el Board de GAIA-X (la gran iniciativa cloud europea) que parece que va a terminar siendo un consejo a la antigua y muy poco diverso.](https://twitter.com/jones_lucia/status/1400531933185122307)

[Venezuela lanza "Sistema Patria", la nueva herramienta de control social, obligatoria para recibir prestaciones públicas e incluso la vacuna de la COVID-19.](https://english.elpais.com/usa/2021-04-24/sistema-patria-a-new-digital-tool-for-social-control-in-venezuela.html)

[Lina Khan, la jurista que amenaza el reinado de las grandes tecnológicas, elegida para reforzar a la Comisión Federal de Comercio.](https://elpais.com/tecnologia/2021-05-30/la-jurista-que-amenaza-el-reinado-de-las-grandes-tecnologicas.html)

[La principal arquitecta de la GDPR pide su revisión para mejorar su eficiacia](https://www.politico.eu/article/eu-privacy-laws-chief-architect-calls-for-its-overhaul/), proponiendo centralizarla para los grandes asuntos como la lucha contra las grandes tecnológicas.

[Los principales países del mundo acuerdan un impuesto mínimo global del 15% a las empresas](https://www.eldiario.es/economia/principales-economias-mundo-acuerdan-tasa-digital-impuesto-minimo-global-15-empresas_1_8007206.html), un impuesto que finalmente [podía elevar en 50.000 millones la recaudación en la UE.](https://elpais.com/economia/2021-06-01/un-impuesto-minimo-del-15-a-las-multinacionales-elevaria-en-50000-millones-la-recaudacion-en-la-ue.html)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Blank es una wallet de Ethereum que te ayuda a aumentar tu privacidad](https://www.goblank.io/) ocultando tus datos financieros y mezclando las transferencias con el conjunto de fondos dentro de Blank, de forma que las transferencias hacia el exterior permanezcan anónimas.

Si estás buscando un móvil con Linux es una tarea compleja, en Ubuntupit han publicado una [lista con los 8 mejores teléfonos con Linux para mantener tu privacidad y seguridad.](https://www.ubuntupit.com/best-linux-secure-phones-for-privacy-and-security/)

[Dark Patterns Tip Line](https://darkpatternstipline.org/) es una web para analizar y aprender sobre los diversos "dark patterns" de diseño que se utilizan en el diseño de aplicaciones para tratar de modificar tu comportamiento.

[La EU ha lanzado una consulta pública sobre la "Ley de datos y modificación de las normas sobre la protección jurídica de las bases de datos"](https://ec.europa.eu/info/law/better-regulation/have-your-say/initiatives/13045-Data-Act-including-the-review-of-the-Directive-96-9-EC-on-the-legal-protection-of-databases-/public-consultation_es), una oportunidad interesante para influir en dicha legislación.

Privacy International ha publicado varios casos de estudio sobre [el uso y la recolección de datos biométricos bajo el pretesto de luchar contra el terrorismo.](https://privacyinternational.org/long-read/4528/biometrics-collection-under-pretext-counter-terrorism)

[Open Rights Group lanza la campaña #StopStalkerAds](https://www.stopstalkerads.org/) con la idea de luchar contra el tracking online.


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[eSSIF-Lab mantiene su convocatoria para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/open-calls/) con hasta 155K €. El plazo se encuentra abierto hasta el 30 de Junio de 2021.

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

También se mantiene abierto el [Information Controls Fellowship](https://www.opentech.fund/funds/icfp/) para investigar y examinar cómo los gobiernos de los paises restringen el acceso a internet, la censura, etc. con 5.000 dólares mensuales hasta 12 meses. La convocatoria termina el 30 de Junio de 2021.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[50 Cognitive Biases in the Modern World](https://www.visualcapitalist.com/50-cognitive-biases-in-the-modern-world/) por Marcus Lu en Visual Capitalist.

[In a push for privacy, tech giants are seen cracking down on competition](https://www.protocol.com/policy/facebook-privacy-competition) por Ben Brody en Protocol.

[How to Opt Out of Facial Recognition at the Airport](https://www.cntraveler.com/story/how-to-opt-out-of-facial-recognition-at-the-airport) por Jessica Puckett en Condé Nast Traveler.

[Mozilla Explica: YouTube me sugirió este video - ¿Por qué?](https://foundation.mozilla.org/es/blog/mozilla-explains-why-did-i-watch-that/), un articulo de Xavier Harding para entender cómo funcionan las recomendaciones en internet de forma sencilla.

Y hoy acabamos con algo diferente ya que no solemos publicar ofertas de trabajo pero esta nos parece especialmente interesante ya que se buscan [técnicos/as superiores para la Oficina de Ciencia y Tecnología del Congreso de los Diputados](https://empleo.fecyt.es/node/1248), una oportunidad para asesorar y ayudar a mejorar la situación de la ciencia en nuestro país.

### ¡Estamos de cumple! <img src="/images/emojis/party.png" width="24px" style="margin: 0;"/> <img src="/images/emojis/cookie.png" width="24px" style="margin: 0;"/>

¡En **Techtopias** cumplimos un año! Desde que empezamos con este pequeño y divertido proyecto en el poco tiempo libre que tenemos, el único objetivo ha sido el de compartir con todo el mundo, las noticias y contenidos que nosotros leíamos a diario sobre privacidad.

Es por todo esto que queremos agradeceros enormemente vuestro apoyo a las 236 personas que recibís nuestra newsletter cada quince días, esperamos seguir creciendo y llegando cada día a más y más gente porque creemos que vivir en una sociedad mejor informada es vivir en una sociedad más libre.

Aprovechando este aniversario, nos gustaría comentaros que durante los próximos tres meses queremos tomarnos un pequeño respiro, pero no nos vamos a ir completamente, únicamente reduciremos la frecuencia de la newsletter a una edición mensual, recargando pilas para volver con más fuerza después del verano :)

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
