---
title: "El gasto en smart cities, Github Copilot y el ataque al Ministerio de Trabajo."
date: 2021-07-06T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-29.jpg"
# meta description
description: "El gasto en smart cities, Github Copilot y el ataque al Ministerio de Trabajo."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

La publicación de GitHub Copilot, el asistente de permite autocompletar y generar código, no ha llegado sino con polémica por [el uso de código abierto para entrenar su IA y las dudas sobre qué licencia se debería aplicar al software que genera.](https://www.genbeta.com/desarrollo/uso-codigo-abierto-para-entrenar-ia-github-copilot-abre-polemica-que-licencia-aplicar-al-software-que-genera)

La llegada de las Smart Cities ha dejado proyectos totalmente inútiles como [el 'turismo inteligente' de Lepe, 200.000 euros de dinero público para llevar cuatro años invisible a los buscadores web](https://www.eldiario.es/andalucia/huelva/turismo-inteligente-lepe-200-000-euros-dinero-publico-llevar-cuatro-anos-invisible-buscadores-web_1_8047256.html), un problema que se extiende a lo largo y ancho del país.

Jaime Gómez-Obregón se ha decidido a [poner patas arriba las apps de las administraciones y mostrar el 'humo' de los políticos](https://www.businessinsider.es/hacker-charlataneria-tecnologica-politicos-886683) para tratar de provocar un cambio en la forma en la que dichas administraciones incorporan tecnología.

[Serbia despliega en Belgrado un sistema de reconocimiento facial chino creado por Huawei, a las puertas de la Unión Europea.](https://elpais.com/tecnologia/2021-06-18/el-reconocimiento-facial-chino-llega-a-las-puertas-de-la-union-europea.html)

[Google abandona su idea de cobrar a los buscadores que quisieran situarse en las opciones por defecto de Android ante la presión de la UE.](https://techcrunch.com/2021/06/08/google-ditches-pay-to-play-android-search-choice-auction-for-free-version-after-eu-pressure/)

[Google admite que "a veces" Assistant graba audio de smartphones y altavoces sin permiso, sin activar el comando 'Ok Google',](https://www.xataka.com.mx/privacidad/google-admite-que-a-veces-assistant-graba-audio-smartphones-altavoces-permiso-activar-comando-ok-google) una noticia que coincide en el tiempo con [una nueva investigación que la UE prepara sobre su monopolio publicitario.](https://www.infobae.com/america/agencias/2021/06/22/la-ue-vuelve-a-apuntar-a-google-en-una-investigacion-sobre-monopolio-publicitario/)

[Tesla es declarada culpable de limitar la velocidad de carga a miles de propietarios a través de una actualización de software.](https://electrek.co/2021/05/24/tesla-found-guilty-throttling-charging-speed-asked-pay-16000-thousands-owners/)

[Telefónica España abre la puerta a la semana laboral de cuatro días, pero con reducción de salario y establece una prueba piloto a partir de otoño.](https://cincodias.elpais.com/cincodias/2021/06/15/companias/1623750413_540410.html)

[Los altavoces Echo y las cámaras de seguridad Ring de Amazon pueden estar compartiendo tu conexión a internet con tus vecinos sin que lo sepas a través de Amazon Sidewalk.](https://www.washingtonpost.com/technology/2021/06/07/amazon-sidewalk-network/) La tecnológica ha estado en muchos temas de conversación durante las últimas semanas ya que [congresistas republicanos y demócratas han propuesto una ley antimonopolio para poder dividir a la compañía.](https://www.elconfidencial.com/empresas/2021-06-11/congresistas-republicanos-y-democratas-proponen-una-ley-para-poder-dividir-amazon_3128575/)

La empresa también ha sido noticia también [tras haber encargado 1.000 camiones a una startup de conducción autónoma en su camino hacia prescindir de los conductores.](https://www.businessinsider.es/amazon-ha-encargado-1000-camiones-sistemas-conduccion-autonoma-887051)

[Have I Been Pwned publica su listado de contraseñas con licencia libre ante la promesa del FBI de contribuir a dicho listado.](https://www.theregister.com/2021/06/01/in_brief_security/)

[Los datos de 700 millones de usuarios de LinkedIn han sido publicados en la red, lo que implica a la inmensa mayoría de los usuarios de la plataforma.](https://www.eleconomista.es/tecnologia/noticias/11299722/06/21/Hacker-pone-a-la-venta-los-datos-de-700-millones-de-usuarios-de-LinkedIn-.html)

[Electronic Arts ha recibido un ciberataque que ha permitido a los atacantes hacerse con el código de FIFA 21 y de herramientas como el motor Frostbite.](https://www.genbeta.com/actualidad/gran-hackeo-ea-costo-solo-10-dolares-asi-facil-se-usaron-unas-cookies-robadas-para-llegar-codigo-fifa-21)

[Flex, el tercer mayor fabricante del mundo, lanza uno de los peores pronósticos sobre la escasez de chips que augura que podría llegar a 2023.](https://www.eleconomista.es/economia/noticias/11257311/06/21/Flex-pesimista-con-la-escasez-de-chips-faltaran-semiconductores-hasta-mediados-de-2022-o-2023.html)

[Facebook está probando un sistema de inteligencia artificial para detener las peleas en sus grupos](https://edition.cnn.com/2021/06/16/tech/facebook-ai-conflict-moderation-groups/index.html) al mismo tiempo que comienza a avisar [cuando se ha sido expuesto a publicaciones extremistas.](https://hipertextual.com/2021/07/facebook-ahora-te-avisa-si-un-amigo-se-ha-vuelto-extremista)

[Encuentran muerto al magnate John McAfee en una prisión catalana tras aprobarse su extradición por supuestos delitos fiscales.](https://www.eldiario.es/catalunya/encuentran-john-mcafee-muerto-prision-catalana-despues-aprobara-extradicion_1_8069355.html)

[Un nuevo robot semi-autónomo será desplegado en la franja de Gaza](https://www.jpost.com/israel-news/new-semi-autonomous-robot-to-be-deployed-to-gaza-border-672278), la tecnología militar autónoma continúa su expansión.

[La caida de Fastly, una de las mayores CDNs del mundo](https://elpais.com/tecnologia/2021-06-09/un-invisible-cuello-de-botella.html), muestra la gran dependencia que tiene internet de unos pocos proveedores. Al respecto del tema, en Maldita Tecnología han analizado lo [qué es un CDN y por qué el fallo en una compañía como Fastly ha provocado una caída mundial de páginas web.](https://maldita.es/malditatecnologia/20210608/cdn-fastly-caida-mundial-paginas-web/)

[Madrid ha cerrado finalmente Medialab del Prado antes de julio sin esperar al dictamen de la Unesco y sin una hoja de ruta clara.](https://www.elconfidencial.com/espana/madrid/2021-06-08/levy-medialab-30-junio-salida-prado-unesco_3116795/)

El pasado mes debía haber entrado en vigor la transposición de la Directiva Europea sobre Derechos de Autor pero [el gobierno no ha llegado a tiempo, ¿qué sabemos de ella?](https://www.elsaltodiario.com/derechos-autoria/ley-directiva-europea-copyright-internet-trasposicion-gobierno) Para ampliar información, [diversos expertos han hablado sobre dicha directiva y cómo nos afecta.](https://maldita.es/malditatecnologia/20210702/algoritmos-copyright-twitch-directiva-derechos-autor/)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Hace unas semanas el Ministerio de Trabajo español recibió un nuevo ciberataque por el que le [exigían un rescate para liberar los sistemas atacados](https://www.elconfidencial.com/tecnologia/2021-06-10/ministerio-trabajo-ryuk-ransomware-rescate_3124375/). Semanas después, los [inspectores de Trabajo siguen sin correo electrónico, ordenador y usando papel y boli. "Estamos desesperados".](https://www.elconfidencial.com/tecnologia/2021-06-24/ciberataque-ryuk-ransomware-inspectores-trabajo-inspeccion_3147656/)

Y no solo el [Ministerio de Trabajo tiene problemas de seguridad, también otros 15 ministerios no tienen la certificación de ciberseguridad que exige el CNI a pesar del asalto al SEPE hace unos meses.](https://www.elespanol.com/invertia/economia/empleo/20210610/trabajo-ministerios-no-certificacion-ciberseguridad-cni-sepe/587692693_0.html)

[El Gobierno invertirá 244 millones en compra pública en ciberseguridad a través del Incibe (Instituto Nacional de Ciberseguridad)](https://cincodias.elpais.com/cincodias/2021/06/29/companias/1624959552_804284.html) mientras el propio instituto [lanza su canal de Telegram para informar de noticias y actualidad en cuanto a seguridad.](https://www.genbeta.com/actualidad/instituto-nacional-ciberseguridad-lanza-su-canal-telegram-para-informar-noticias-actualidad-a-seguridad)

[El Gobierno lanza una manifestación de interés para constituir un HUB español de GAIAX.](https://twitter.com/SEDIAgob/status/1407740277339656199)

Estas semanas también han sido movidas en la SEDIA, en donde [también ha salido uno de los "gurús" del plan digital](https://www.lainformacion.com/espana/gobierno-economia-calvino-moncloa-plan-digital/2841146/) mientras la Secretaria de Estado, Carme Artigas anuncia [el fichaje de un CDO (Chief Data Officer) y un observatorio de emprendimiento digital.](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/espana/20210617/bateria-anuncios-artigas-inminente-cdo-observatorio-emprendimiento/589691827_0.html)

Hablando de observatorios, [Lucía Velasco es la nueva directora del Observatorio Nacional de Tecnología y Sociedad: "La tecnología no sirve de nada si no está al servicio de la igualdad, la inclusión y la justicia social"](https://compromiso.atresmedia.com/levanta-la-cabeza/actualidad/lucia-velasco-tecnologia-sirve-nada-sino-esta-servicio-igualdad-inclusion-justicia-social_2021061560c896687d407d0001d765ad.html)

[Policía y Guardia Civil preparan ABIS, el sistema de reconocimiento facial por IA entrenado con 5,6 millones de imágenes.](https://bandaancha.eu/articulos/asi-abis-sistema-reconocimiento-facial-9930)

[France crea una agencia para luchar contra la desinformación exterior,](https://www.dawn.com/news/1627160/france-creates-agency-to-fight-foreign-disinformation) gran parte de ella proveniente de [Rusia, que según Facbook es actualmente el mayor productor de desinformación del mundo.](https://www.washingtonpost.com/technology/2021/05/26/facebook-disinformation-russia-report/).

[Biden revoca la prohibición de Trump sobre TikTok y WeChat pero eleva el escrutinio sobre las apps chinas.](https://cincodias.elpais.com/cincodias/2021/06/09/companias/1623261468_336693.html)

[El mayor condado de Washington prohíbe el uso gubernamental de software de reconocimiento facial.](https://abcnews.go.com/Technology/washingtons-largest-county-bans-government-facial-recognition-software/story?id=78038727)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

La startup española [Internxt obtiene $1M para ser "el Coinbase del almacenamiento descentralizado".](https://techcrunch.com/2021/06/17/internxt-gets-1m-to-be-the-coinbase-of-decentralized-storage)

[datarequests.org es una herramienta que permite generar peticiones con las que ejercer nuestros derechos sobre nuestros datos personales.](https://www.datarequests.org)

El nuevo buscador [Brave Search lanza su versión beta, ofreciendo a los usuarios el primer buscador totalmente independiente de las Big Tech.](https://brave.com/brave-search-beta/) Para probarlo, la plataforma está disponible en [search.brave.com](https://search.brave.com/)

[Discriminator es un documental interactivo sobre el reconocimiento facial y las bases de datos existentes.](https://www.discriminator.film/)

[Xnet ha conseguido que finalmente su "Plan de Digitalización Democrática de la Educación" tenga su primer piloto en Barcelona.](https://xnet-x.net/plan-digitalizacion-democratica-educacion-piloto-barcelona/)

[EDRI publica las cinco razones por las que se ha conseguido una victoria para los derechos humanos en el Certificado Digital COVID Europeo.](https://edri.org/our-work/five-reasons-to-claim-victory-on-the-eu-digital-covid-certificate/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGI Assure](https://www.assure.ngi.eu/open-calls/) abre una nueva open call para buscar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Agosto de 2021.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto de 2021.

NGI también abre [NGIatlantic.eu, en su cuarto open call](https://ngiatlantic.eu/ngiatlanticeu-4th-open-call) en busca de proyectos financiables con hasta 75k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 15 de Septiembre de 2021.

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[The Telegram Billionaire and His Dark Empire](https://www.spiegel.de/international/world/the-telegram-billionaire-and-his-dark-empire-a-f27cb79f-86ae-48de-bdbd-8df604d07cc8) por Christina Hebel, Max Hoppenstedt y Marcel Rosenbach en Der Spiegel.

[Everyone should decide how their digital data are used — not just tech companies](https://www.nature.com/articles/d41586-021-01812-3) por Jathan Sadowski, Salomé Viljoen y Meredith Whittaker en Nature.

[Sesgos cognitivos y desinformación en Twitch: cómo nuestro cerebro nos la intenta colar](https://maldita.es/malditateexplica/20210626/sesgos-cognitivos-desinformacion-Twitch/) en Maldita Tecnología.

[El efecto del ‘scroll’ infinito sobre cómo consumimos información](https://www.newtral.es/analisis-scroll-redes-informacion/20210629/) por Marcelino Madrigal en Newtral.

[‘Delete the Video or We’ll Block YouTube’: Inside Putin’s War on the Free Press](https://www.vice.com/en/article/pkbkjg/delete-the-video-or-well-block-youtube-inside-putins-war-on-the-free-press) por Anna Koslerova en Vice.

[Por una inteligencia artificial lenta](https://collateralbits.net/por-una-inteligencia-artificial-lenta/) por Iga Kozlowska en CollateralBits

[“Simpatizar con las tecnológicas es una forma perversa de síndrome de Estocolmo”](https://elpais.com/tecnologia/2021-06-14/simpatizar-con-las-tecnologicas-es-una-forma-perversa-de-sindrome-de-estocolmo.html) una entrevist aa Evgeny Morozov en El País.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
