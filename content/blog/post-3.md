---
title: "Palantir en UK y las moratorias al reconocimiento facial"
date: 2020-06-23T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-3.jpg"

# meta description
description: "Palantir en UK y las moratorias al reconocimiento facial"

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[La UE prepara una base de datos biométrica para controlar las fronteras Schengen](https://www.expansion.com/economia-digital/innovacion/2020/06/06/5edb6df6e5fdeade2a8b4578.html), al mismo tiempo que la [Comisión europea se ve presionada por sus controvertidas relaciones con Palantir](https://www.euractiv.com/section/digital/news/eu-commission-pressed-on-controversial-links-to-palantir/).

[El navegador Brave añadía automáticamente sus propios links de afiliados](https://www.engadget.com/brave-privacy-browser-redirect-crypto-searches-084953332.html) cuando los usuarios navegaban a ciertos servicios web relacionados con criptomonedas. [Su CEO se ha disculpado considerándolo un "error".](https://www.theverge.com/2020/6/8/21283769/brave-browser-affiliate-links-crypto-privacy-ceo-apology)

[“Yo fui un bot”: las confesiones de un agente dedicado al engaño en Twitter](https://elpais.com/tecnologia/2020-05-20/yo-fui-un-bot-las-confesiones-de-un-agente-dedicado-al-engano-en-twitter.html).

[IBM frena su negocio de reconocimiento facial por sus problemas relacionados con los sesgos y la desigualdad](https://techcrunch.com/2020/06/08/ibm-ends-all-facial-recognition-work-as-ceo-calls-out-bias-and-inequality/), aunque continuará con sus investigaciones alrededor de la Inteligencia Artificial.

[Empresas como Microsoft y Amazon se unen a IBM, motivadas por la presión social, para dejar de vender "temporalmente", sus tecnologías de reconocimiento facial a la policía](https://www.technologyreview.es/s/12332/la-presion-publica-obliga-amazon-paralizar-el-reconocimiento-facial) y algunas de ellas también han solicitado [que esta tecnología sea regulada](https://www.wired.com/story/microsoft-calls-for-federal-regulation-of-facial-recognition/).

Aún así, el uso de esta tecnología por parte de las fuerzas de seguridad, al que ahora algunas tecnológicas dicen querer renunciar, [es una ínfima parte de un mercado mucho mayor](https://elpais.com/tecnologia/2020-06-12/los-gigantes-tecnologicos-se-disputan-el-negocio-polemico-y-millonario-de-la-vigilancia-facial.html).

La polémica empresa [Clearview tiene una base de datos de miles de millones de fotografías de ciudadanos de todo el mundo](https://elpais.com/tecnologia/2020-06-16/la-empresa-que-almacena-fotos-tuyas-en-internet-para-identificarte.html) y que permite identificar a una persona a partir de una sola imagen.

La pasada semana hemos visto varios experimentos de nuevas formas de espionaje, [escuchando conversaciones a través de las vibraciones de una bombilla](https://www.xataka.com/seguridad/solo-analizar-vibraciones-bombilla-posible-escuchar-conversaciones-a-decenas-metros-distancia) así como [identificando los sentimientos de personas según el movimiento de su cuerpo al caminar.](https://www.wired.com/story/proxemo-robot-guesses-emotion-from-walking/)

[Cámaras termográficas e inteligencia artificial para proteger a la Liga Endesa de la covid](https://retina.elpais.com/retina/2020/06/17/innovacion/1592379373_513516.html).

Parece que en los últimos días se está volviendo a poner de moda la famosa app [FaceApp, un ejemplo de uso abusivo de datos](https://maldita.es/malditatecnologia/2020/06/15/faceapp-uso-abusivo-datos-aplicaciones-privacidad/) en aplicaciones gratuitas.

[Rusia levanta el bloqueo a Telegram tras llegar a un "acuerdo" con Pavel Durov del que poco o nada se sabe](https://www.genbeta.com/actualidad/rusia-levanta-bloqueo-a-telegram-llegar-a-acuerdo-pavel-durov-que-poco-nada-se-sabe).

[China sigue su camino para mantener el uso de las tecnologías utilizadas para el tracking del COVI-19](https://www.nytimes.com/2020/05/26/technology/china-coronavirus-surveillance.html) en el día a día de sus ciudadanos tras la pandemia.


### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Gran Bretaña ha dado acceso a datos médicos sensibles a la controvertida empresa Palantir a cambio de £1](https://www.cnbc.com/2020/06/08/palantir-nhs-covid-19-data.html). Así mismo Google ha ofrecido su soporte técnico de forma gratuita y otras empresas como Microsoft o la empresa de IA, Faculty, han cerrado tratos similares con el gobierno de UK.

[Alemania prohibe la de-identificación](https://www.enriquedans.com/2020/06/alemania-prohibe-la-de-identificacion.html), una técnica que permite añadir leves modificaciones en una fotografía, imposibles de percibir a simple vista, pero que evitan que las fotografías puedan ser utilizadas en procesos de reconocimiento facial.

[La UE trata de establecer un framework para permitir que las apps de contact tracing sean interoperables y funcionen más allá de las fronteras.](https://www.engadget.com/eu-commission-contact-tracing-interoperability-specifications-182445321.html)

[Hamburgo apuesta por el software de codigo abierto](https://www.muycomputer.com/2020/06/07/software-de-codigo-abierto-hamburgo/), siguiendo la estela de Munich, Stuttgart y de otras muchas instituciones alemanas.

[El PSOE lleva al Congreso una proposición no de ley para eliminar el dinero en efectivo de forma gradual](https://www.elconfidencial.com/espana/2020-06-13/congreso-elimnacion-gradual-pago-efectivo_2637763/), una medida polémica que eliminaría por completo la posibilidad de mantener la privacidad al realizar compras. Apenas unas horas después, el [BCE ha salido al paso, advirtiendo que no está permitida la eliminación del dinero en la eurozona](https://elpais.com/economia/2020-06-13/el-bce-advierte-de-que-no-esta-permitida-la-eliminacion-del-dinero-en-efectivo-en-la-eurozona.html) y que además sería una medida que perjudicaría principalmente a los más desfavorecidos.

[El Gobierno español prepara una Carta de Derechos Digitales que amplíe la LOPD con ayuda de expertos y de una consulta a la ciudadanía](https://www.genbeta.com/actualidad/gobierno-elaborara-carta-derechos-digitales-que-amplie-lopd-ayuda-expertos-consulta-a-ciudadania-para-ampliar).

[EE UU suspende las negociaciones de la tasa Google con España, Francia, Italia y Reino Unido](https://cincodias.elpais.com/cincodias/2020/06/17/economia/1592419043_664040.html).

[La Agencia Tributaria comienza a usar Zoom para registrarse en el sistema Cl@ve por videollamada](https://www.genbeta.com/actualidad/agencia-tributaria-comieza-a-usar-zoom-contribuyentes-permite-registrarse-sistema-cl-ve-videollamada), una acción polémica tras los últimos escándalos relacionados con hackeos y problemas de privacidad por parte de la tecnológica.

[Noruega detiene la recopilación de datos de su ‘app’ contra la covid-19](https://elpais.com/tecnologia/2020-06-15/noruega-detiene-la-recopilacion-de-datos-de-su-app-contra-la-covid-19.html).

[El tribunal constitucional francés derriba la mayor parte de la ley sobre los discursos de odio](https://www.politico.eu/article/french-constitutional-court-strikes-down-most-of-hate-speech-law/) que pretendía obligar a las plataformas digitales como Google o Facebook a eliminar el contenido de odio en menos de 24 horas y la propaganda terrorista en 1 hora.

[Boris Johnson se ve obligado a reducir el papel de Huawei en las redes 5G de Gran Bretaña](https://www.theguardian.com/technology/2020/may/22/boris-johnson-forced-to-reduce-huaweis-role-in-uks-5g-networks).

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Empathy.co, la startup asturiana que trabaja con Tim Berners-Lee para desarrollar un internet que no dependa de las tecnológicas](https://www.businessinsider.es/empathyco-factura-trimestre-50-todo-2019-648401), cree que el coronavirus impulsará la sensibilidad hacia el comercio electrónico.

[FIGHT FOR THE FUTURE lanza una campaña para pedir que se prohiban las tecnologías de reconocimiento facial](https://www.banfacialrecognition.com/). Piden que esta tecnología sea considerada al mismo nivel que las armas nucleares y biológicas.

[Nace ‘El Enemigo Anónimo’, la primera serie documental sobre ciberseguridad en España](https://www.elenemigoanonimo.com/nace-el-enemigo-anonimo-la-primera-serie-documental-sobre-ciberseguridad/) de la mano del periodísta C. Otto, que contará con 20 capítulos.

[Mozilla lanza una campaña para pedir a Amazon que deje de compartir con la policía, información obtenida con sus dispositivos "Ring"](https://foundation.mozilla.org/en/campaigns/tell-amazon-ring-stop-sharing-information-police-services/).

[La empresa detras de "Basecamp" lanza "Hey"](https://www.fastcompany.com/90516667/meet-the-big-tech-critic-behind-hey-basecamps-radical-new-email-platform), un nuevo servicio de email que, aseguran, ha sido diseñado para respetar la privacidad entre otras cosas y tratar de acabar con el dominio de las Big Tech. Lanzamiento que ha venido acompañado de [problemas con Apple que ha intentado obligar a Basecamp a utilizar su servicio de pagos in app](https://www.forbes.com/sites/robpegoraro/2020/06/17/apple-to-basecamps-hey-expect-to-pay-us-if-you-want-to-sell-privacy/#252629a04287), aprovechando su posición de control sobre la plataforma.

["Google got rich from your data. DuckDuckGo is fighting back"](https://www.wired.co.uk/article/duckduckgo-android-choice-screen-search), un artículo muy interesante sobre el navegador, su modelo de negocio y los cambios normativos que obligan a los fabricantes de teléfonos, a añadir una ventana para permitir al usuario seleccionar el buscador web a utilizar en lugar de incluir a Google por defecto.

[Signal añade una funcionalidad para ocultar automáticamente las caras de las personas en una fotografía.](https://signal.org/blog/blur-tools/)

[Ethical lanza una lista de recursos y alternativas éticas en tecnología](https://ethical.net/resources/), desde navegadores y apps hasta podcasts, conferencias o libros, desde un punto de vista amplio, más allá de la privacidad.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Luminate busca proyectos relacionados con el empoderamiento ciudadano, datos y derechos digitales, transparencia financiera y medios independientes](https://luminategroup.com/grants-and-investments) para apoyar y financiar, independientemente de si son non-profit o for-profit.

[El Open Technology Fund tiene varias convocatorias abiertas](https://www.opentech.fund/funds/) entre los que se encuentran el Internet Freedom Fund y el Core Infrastructure Fund cuyo deadline es el 1 de Julio.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a entender el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Julio.

Recuerda que si tienes algun proyecto para ayudar a gobiernos a empoderarse y construir un internet mejor, [NGI Policy-in-Practice fund](https://research.ngi.eu/policy-practice-fund/) ofrece hasta 25,000 € de financiación. La convocatoria termina el 3 de Julio.

[Si tienes un proyecto tecnológico para mejorar la privacidad y la confianza, NGI Zero Privacy & Trust Fund](https://nlnet.nl/PET/) ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto.


### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/> y entrevistas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Shoshana Zuboff, autora del libro "The Age of Surveillance Capitalism", ha estado debatiendo con Margrethe Vestager, vicepresidenta de la Comisión Europea para la era digital, sobre la futura Europa digital.](https://ida.dk/viden-og-netvaerk/videoer-fra-ida/shoshana-zuboff-meets-margrethe-vestager-a-conversation-about-a-future-digital-europe)

Enrique Dans dice que las tecnologías de reconocimiento facial están aquí para quedarse y [abre el debate sobre si es posible controlarlas.](https://www.enriquedans.com/2020/06/la-tecnologia-de-reconocimiento-facial-como-dilema-social.html).

[Cash, Kisses and Karaoke: Why the War on Covid must not become a War on Cash](https://alteredstatesof.money/cash-and-covid/), un interesante análisis en el debate de la prohibición del dinero en efectivo.

Una entrevista a Marta Peirano del mes de Enero pero que no deja de ser interesante en el día de hoy, hablando sobre [la economía de la atención: "Somos menos felices y menos productivos que nunca porque somos adictos"](https://www.bbc.com/mundo/noticias-51268343).

[Esta semana en Trackula comenzamos LaEscondite](https://laescondite.com/), un proyecto que busca unir a toda la comunidad de privacidad y soberanía tecnológica, y que esperamos sea mucho más. Por eso **[si estás impulsando iniciativas en el mundo de la privacidad, la soberanía tecnológica y la libertad en internet, entra en este link porque queremos conocerte!](https://laescondite.com/join_us/)**

## Bola extra
Si has llegado el final de esta newsletter, hoy nos gustaría pedirte tu apoyo [en esta campaña](https://saveinternetfreedom.tech/) en apoyo del **[Open Technology Fund](https://www.opentech.fund/)**, el que probablemente es uno de los fondos que más han aportado hasta la actualidad a la libertad por internet, la lucha contra la censura y la defensa de las comunicaciones seguras.

**Proyectos como Tor, WireGuard, Certbot, Tails, NoScript entre otros muchos han surgido gracias a su apoyo** pero de la noche a la mañana sus directores han sido despedidos y las perspectivas de futuro son malas.

Es por eso que nos gustaría pedirte que los apoyes **[en la campaña "Save internet freedom"](https://saveinternetfreedom.tech/)**, para pedirle al congreso de los Estados Unidos que defienda al Open Technology Fund.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
