---
title: "Un CDO para España, el spyware Pegasus y el nuevo DNI con datos faciales."
date: 2021-08-03T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-30.jpg"
# meta description
description: "Un CDO para España, el spyware Pegasus y el nuevo DNI con datos faciales."

# post type
type: "post"
---


### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Mercadona sancionada con 2,5 millones por el sistema de reconocimiento facial implantado en 48 de sus tiendas.](https://www.lavanguardia.com/local/valencia/20210722/7618442/mercadona-proteccion-datos-sancion-2-5-millones.html)

Los compañeros de Pucelabits continúan trabajando para sacar a la luz los sitios públicos que no protegen la privacidad ni la seguridad de sus usuarios, [desde la Casa Real al Tribunal Constitucional: el 97% de las webs institucionales no son seguras.](https://www.elespanol.com/omicrono/tecnologia/20210718/casa-real-tribunal-constitucional-institucionales-no-seguras/596941786_0.html)

La pasada semana, [una investigación de The Guardian](https://www.theguardian.com/world/2021/jul/18/revealed-leak-uncovers-global-abuse-of-cyber-surveillance-weapon-nso-group-pegasus) sacaba a la luz [uno de los mayores escándalos de espionaje de la historia, a través del uso de un spyware llamado Pegasus.](https://www.elconfidencial.com/tecnologia/2021-07-20/nso-paises-comran-software-espia-producto-pegasus_3191708/)

El escándalo revelaban que ciudadanos de todo el mundo, hasta 50.000 periodistas, políticos y activistas, [fueron objetivo del software de cibervigilancia.](https://www.elconfidencial.com/mundo/2021-07-18/software-espia-revelado-50-000-identidades_3190564/) Las reacciones no han tardado en llegar y múltiples activistas como [Edward Snowden han pedido prohibir la comercialización de este tipo de programas espía.](https://www.theguardian.com/news/2021/jul/19/edward-snowden-calls-spyware-trade-ban-pegasus-revelations)

[El cibercrimen organizado se convierte definitivamente en una industria, contando ya con inversores y un ecosistema de innovación.](https://www.businessinsider.es/criminales-informaticos-ya-buscan-inversores-como-startup-891153)

[Un laboratorio de tests prenatales cede los datos de millones de mujeres a una empresa vinculada al ejército chino](https://www.reuters.com/investigates/special-report/health-china-bgi-dna/), unos datos genéticos tremendamente valiosos. En [este hilo](https://mobile.twitter.com/bcarrebravo/status/1412772560324923395) se puede encontrar más información sobre el tema.

[EE.UU., la UE y la OTAN acusan a China de cometer delitos mediante ataques cibernéticos.](https://www.abc.es/internacional/abci-eeuu-acusa-china-cometer-actos-ciberneticos-maliciosos-y-comportarse-manera-irresponsable-202107191439_noticia.html) Una noticia que se une al miedo de [Alemania por la amenaza de interferencias extranjeras en sus elecciones.](https://elpais.com/internacional/2021-07-12/la-amenaza-de-interferencias-extranjeras-planea-sobre-las-elecciones-alemanas.html)

Gracias al trabajo del colectivo francés, La Quadrature du Net, [Luxemburgo impone una multa récord de 746 millones a Amazon por infringir la normativa de protección de datos de la UE.](https://www.infolibre.es/noticias/economia/2021/07/30/luxemburgo_multa_record_746_millones_amazon_por_infringir_las_normas_proteccion_datos_ue_123210_1011.html)

[Un sevillano trabaja en una herramienta para buscar en fuentes abiertas y obtener todo tipo de datos públicos de una persona en apenas un minuto.](https://www.elconfidencial.com/tecnologia/2021-07-12/jorge-coronado-osint-dantes-gates-telegram-bot_3167843/)

[Mark Zuckerberg quiere convertir a Facebook en el centro del metaverso](https://www.theverge.com/22588022/mark-zuckerberg-facebook-ceo-metaverse-interview), buscando la unión entre el mundo físico y el digital.

[Un fallo de seguridad en Madrid desvela datos de miles de residentes en la comunidad,](https://www.telemadrid.es/noticias/madrid/Madrid-Felipe-VI-Pedro-Sanchez-0-2357164311--20210707083059.html) una plataforma desarrollada por Indra por la que [Madrid pagó 225.000 euros.](https://www.eldiario.es/tecnologia/madrid-pago-225-000-euros-indra-sistema-filtro-datos-personales-error-novato_1_8117493.html)

Algo similar ha ocurrido en Cataluña donde [una brecha en la web de autocita de la vacuna, dejó a la vista datos personales de sus ciudadanos.](https://www.eldiario.es/tecnologia/brecha-web-autocita-vacuna-catalunya-dejo-vista-datos-personales-ciudadanos_1_8126424.html)

[Correos se pasa al negocio del cloud y prepara una plataforma para dar servicios en la nube a la administración pública.](https://www.lainformacion.com/economia-negocios-y-finanzas/correos-elige-competidor-indra-telefonica-servicios-nube/2841299/)

[La publicidad segmentada llega a las vallas publicitarias del fútbol](https://twitter.com/sergiojramos_/status/1411690047854858244), mientras se publica la noticia de que [el sector de los medios de comunicación temen una prohibición de la publicidad segmentada por parte de la UE.](https://www.euractiv.com/section/digital/news/media-sector-fears-eu-ban-on-targeted-advertising/)

[RisCanvi, el algoritmo que ayuda al juez en Cataluña a decidir si mereces la condicional.](https://elpais.com/tecnologia/2021-07-11/riscanvi-luces-y-sombras-del-algoritmo-que-ayuda-al-juez-en-cataluna-a-decidir-si-mereces-la-condicional.html)

Con la llegada de la Ley Rider, [Deliveroo anuncia su cierre en España,](https://www.elconfidencial.com/tecnologia/2021-07-30/deliveroo-se-plantea-su-cierre-en-espana-ante-la-imposibilidad-de-competir-con-uber-y-glovo_3209807/) y [Glovo contratará a 2.000 'riders' para adaptarse a la ley, pero seguirá contando con autónomos en su flota.](https://www.businessinsider.es/glovo-contratara-2000-riders-adaptarse-ley-rider-906459)

[Termina el mayor experimento con la semana laboral de 4 días en Islandia, aquí tenemos algunas de sus conclusiones.](https://www.eleconomista.es/actualidad/noticias/11308382/07/21/Este-es-el-resultado-del-mayor-experimento-con-la-semana-laboral-de-4-dias-acaba-de-terminar-en-Islandia.html)

[El Parlamento Europeo permite a las empresas tecnológicas buscar datos de abuso de menores en sus plataformas.](https://www.politico.eu/article/european-parliament-platforms-child-sexual-abuse-reporting-law/) Una noticia que trae bastante polémica ya que [abre la puerta al espionaje de las comunicaciones privadas como comenta EDRI.](https://edri.org/our-work/its-official-your-private-communications-can-and-will-be-spied-on/)
Aún así, la normativa tiene caracter temporal y aún queda mucho por hacer. [En Newtral explican muy bien y con mucho detalle lo que se ha aprobado.](https://twitter.com/marilink/status/1415215527564398593)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El nuevo DNI 4.0 incluirá identificación y verificación biométrica, añadiendo los datos faciales a la huella y la foto actuales.](https://www.genbeta.com/actualidad/expedicion-dni-4-0-incluira-identificacion-verificacion-biometrica-datos-faciales-se-suman-a-huella-foto)

[La SEDIA (Secretaría de Estado para la Digitalización e Inteligencia Artificial) anuncia la contratación de un CDO (Chief Data Officer)](https://twitter.com/SEDIAgob/status/1417530742666637320) para la nueva Oficina del Dato. Pero, [¿qué es y para qué sirve un CDO en la gestión de datos públicos de España?](https://www.newtral.es/chief-data-officer-espana-gestion-datos-publicos-pandemia/20210728/)

[El Gobierno publica finalmente la Carta de Derechos Digitales](https://twitter.com/e_paniagua/status/1415266358716248069) que realmente no tiene ningún valor legislativo y [propone a España como país piloto para la auditoría de algoritmos que planea la UE.](https://www.eldiario.es/tecnologia/gobierno-propone-espana-pais-piloto-auditoria-algoritmos-planea-ue_1_8153288.html)

[Más de la mitad de las respsuestas en la Manifestación de Interés para el hub nacional de Gaia-X son de pymes.](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/20210714/espanola-gaia-x-aportaciones-nacional-superan-grandes-corporaciones/596440795_0.html)

[El Gobierno aprueba el anteproyecto de la ley de startups](https://cincodias.elpais.com/cincodias/2021/07/14/pyme/1626215690_149828.html), una ley que el ecosistema considera ["poco ambiciosa" y “de impacto dudoso”.](https://www.xataka.com/pro/poco-realista-impacto-dudoso-errores-garrafales-que-piensa-ecosistema-startup-nueva-ley-startups)

Relacionado con la noticia anterior, también [se anuncia la creación de un nuevo fondo (Next Tech) de 4.000 millones para impulsar el desarrollo de 'unicornios' españoles.](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/espana/20210719/gobierno-movilizaran-millones-impulsar-desarrollo-unicornios-espanoles/597690619_0.html)

[Biden firma una orden ejecutiva contra el derecho a reparar, los ISP o la neutralidad de la red entre otros.](https://www.theverge.com/2021/7/9/22569869/biden-executive-order-right-to-repair-isps-net-neutrality)

[Un gran número de estados en Estados Unidos están utilizando reconocimiento facial como condición para recibir el desempleo y otros beneficios sociales.](https://edition.cnn.com/2021/07/23/tech/idme-unemployment-facial-recognition/index.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Typewise](https://www.typewise.app/) es un nuevo teclado con forma hexagonal y 100% privacy friendly.

[DuckDuckGo lanza un servicio de protección del correo electrónico que bloquea rastreadores y oculta la dirección.](https://www.genbeta.com/correo/duckduckgo-lanza-servicio-proteccion-correo-electronico-que-bloquear-rastreadores-ocultar-direccion)

Kaspersky ofrece una web desde donde poder descargar [descifradores de ransomware gratuitos](https://noransom.kaspersky.com/es/) por si has sido víctima de un ataque.

[El sistema nacional de salud alemán se pasa al servicio de comunicaciones cifrado Matrix.](https://matrix.org/blog/2021/07/21/germanys-national-healthcare-system-adopts-matrix)

El pasado mes de Junio tuvo lugar la RightsCon de forma online, [ya están disponibles los videos del evento.](https://www.youtube.com/playlist?list=PLprTandRM962kq7fcetWh8Uvp0bB0tM8C)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGIatlantic.eu mantiene su cuarto open call](https://ngiatlantic.eu/ngiatlanticeu-4th-open-call) en busca de proyectos financiables con hasta 75k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 15 de Septiembre de 2021.

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Reconocimiento biométrico en las fronteras europeas: sistemas informáticos controlarán la migración analizando caras y huellas dactilares](https://maldita.es/malditatecnologia/20210702/reconocimiento-biometrico-fronteras-europeas-schengen-migrantes/) via Maldita Tecnología.

[What you need to know about stalkerware](https://www.ted.com/talks/eva_galperin_what_you_need_to_know_about_stalkerware), un TED talk de Eva Galperin.

[We need more protection from government surveillance — not less](https://www.politico.eu/article/we-need-more-protection-government-surveillance-not-less/) vía Politico.

["Patrones oscuros": la técnica que hace que actúes contra tu voluntad en Internet](https://maldita.es/malditatecnologia/20210731/patrones-oscuros-tecnica-actuar-contra-voluntad-internet/) via Maldita Tecnología.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
