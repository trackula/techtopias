---
title: "El escaneo de contenidos de Apple, el impulso a la ciberseguridad y el problema de privacidad en Afganistan."
date: 2021-08-31T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-31.jpg"
# meta description
description: "El escaneo de contenidos de Apple, el impulso a la ciberseguridad y el problema de privacidad en Afganistan."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Ya estamos terminando el mes de Agosto y claramente la noticia de que [Apple revisará las fotos que se hagan desde sus dispositivos en busca de contenidos pederastas](https://elpais.com/tecnologia/2021-08-06/apple-revisara-todas-las-fotos-que-se-hagan-desde-sus-dispositivos-en-busca-de-contenidos-pedofilos.html) se ha convertido en el culebrón del verano. [En este artículo podemos profundizar un poco más en cómo funcionará.](https://techcrunch.com/2021/08/05/apple-icloud-photos-scanning/)

Así mismo, la [EFF también ha expresado su punto de vista según el cual, Apple convierte a los jóvenes en peones de la privacidad.](https://www.eff.org/es/deeplinks/2021/08/apples-plan-scan-photos-messages-turns-young-people-privacy-pawns)

[Tinder quiere que le des tu DNI para que puedas ligar por Internet](https://www.elmundo.es/tecnologia/2021/08/18/611bb46dfdddff455b8b4670.html), con la idea de dar más confianza y seguridad a sus usuarios, aunque de momento será opcional.

[Zoom acepta pagar más de 70 millones por violar la privacidad de sus usuarios.](https://www.lavanguardia.com/tecnologia/20210802/7639433/zoom-privacidad.html)

Las brechas de seguridad también han hecho que este mes sea bastante movido, algunos ejempos son [el agujero de seguridad en Zurich Seguros](https://www.elconfidencial.com/tecnologia/2021-08-13/zurich-seguros-robo-ciberataque-hackers_3230922/), el error de configuración que [ha dejado a la luz datos de personas sospechosas de terrorismo](https://www.oodaloop.com/briefs/2021/08/17/misconfigured-server-leaks-us-terror-watchlist/), la filtración de hasta [100 millones de clientes de T-Mobile](https://www.businessinsider.es/t-mobile-investiga-robo-datos-100-millones-usuarios-915695) o los miles de clientes afectados [en la principal base de datos de Microsoft Azure.](https://www.reuters.com/technology/exclusive-microsoft-warns-thousands-cloud-customers-exposed-databases-emails-2021-08-26/)

Noticias que coinciden con la publicación de un estudio que concluye que [la ciberseguridad industrial es un chollo para los delincuentes, donde la gran mayoría de las vulnerabilidades son de baja complejidad.](https://www.xataka.com/pro/ciberseguridad-industrial-chollo-para-delincuentes-90-vulnerabilidades-sector-2021-baja-complejidad)

Esta pasada semana [Biden se ha reunido con las Big Tech, definiendo la ciberseguridad como un "reto de seguridad nacional" y pidiéndoles su compromiso con el sector.](https://www.euractiv.com/section/cybersecurity/news/biden-calls-on-tech-giants-to-raise-the-bar-on-cybersecurity/). Es por ello que [Google y Microsoft se ha comprometido a invertir 25.000 millones de dólares.](https://www.lainformacion.com/mundo/google-microsoft-inyectaran-25000-millones-ciberseguridad-eeuu/2847465/)

[China ordena una revisión anual de seguridad para todos los operadores de infraestructuras críticas de información.](https://www.theregister.com/2021/08/18/china_critical_information_infrastructure_rules/) al mismo tiempo que sigue construyendo herramientas de espionaje, como ["Tetris", que ha sido encontrada en 58 webs ampliamente utilizadas y que servía para recopilar datos de posibles disidentes chinos.](https://therecord.media/chinese-espionage-tool-exploits-vulnerabilities-is-58-widely-used-websites/)

[La empresa rusa Xsolla ha despedido a 150 trabajadores basándose en los datos obtenidos de un software de monitorización que los catalogó como improductivos.](https://www.lavanguardia.com/tecnologia/20210808/7651651/empresa-tecnologica-xsolla-despedir-empleados-improductivos-segun-inteligencia-artificial.html)

[Google anuncia que añadirá nuevas medidas para proteger a los adolescentes y a su privacidad.](https://www.deccanherald.com/business/technology/google-will-add-privacy-steps-for-teenagers-1018331.html)

[Un grupo de investigadores australianos está tratando de investigar cómo funcionan los algoritmos de búsqueda de Google.](https://www.abc.net.au/news/science/2021-08-05/google-curating-covid-search-results-algorithm-project-finds/100343284)

[Las universidades en España no podrán usar reconocimiento facial para vigilar exámenes ‘online’ ya que el tratamiento de datos biométricos para autenticar a los alumnos no está justificado.](https://elpais.com/tecnologia/2021-08-15/las-universidades-no-podran-usar-reconocimiento-facial-para-vigilar-examenes-online.html)

[Un tercio de los ejecutivos que han participado en un estudio de KPMG, admite que su empresa "a veces utiliza métodos poco éticos de recogida de datos".](https://www.fastcompany.com/90666500/kpmg-privacy-study)

[Un estudio demuestra que la industria del petroleo y del gas continua promocionando el uso de combustibles fósiles y la desinformación sobre el cambio climático en Facebook a pesar de los intentos de la compañía de luchar contra ello.](https://www.climatica.lamarea.com/facebook-desinformacion-climatica/)

[Glovo rectifica su algoritmo tras las protestas de riders de Barcelona.](https://www.elsaltodiario.com/glovo/glovo-rectifica-su-algoritmo-tras-protestas-riders-barcelona)

La vuelta al poder de los Talibanes en Afganistán ha traido también [el drama digital para muchos ciudadanos, que se ven obligados a borrar su huella digital y el historial en internet para poder escapar.](https://elpais.com/tecnologia/2021-08-18/el-drama-digital-en-afganistan-borrarse-de-las-redes-y-limpiar-el-historial-online-para-escapar-de-los-talibanes.html) Además, también existe mucho miedo ante [las bases de datos biométricas e historiales digitales que pueden ser utilizadas para rastrearles.](https://www.reuters.com/article/afghanistan-tech-conflict/afghans-scramble-to-delete-digital-history-evade-biometrics-idUSL8N2PO1FH)

[El Servicio Nacional de Salud (NHS) de Gran Bretaña planeaba compartir los datos de salud de sus ciudadanos con empresas privadas, pero una campaña que animaba a la gente a hacer "opt-out" de sus datos, ha puesto en pausa el plan.](https://www.theguardian.com/society/2021/aug/22/nhs-data-grab-on-hold-as-millions-opt-out)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Sindicatos y Just Eat preparan el primer convenio colectivo de 'riders' de España.](https://www.elconfidencial.com/tecnologia/2021-08-03/convenio-colectivo-just-eat_3214440/)

[El Ministerio de Interior adquiere 15 analizadores forenses a la startup israelí "Cellebrite" para acceder a teléfonos móviles cifrados](https://www.elconfidencial.com/tecnologia/2021-08-24/policia-cellebrite-dispositivos-licitacion-efectividad_3246406/).

[El Gobierno prepara una ley para acabar con reseñas falsas en Internet y obligar a Google y otras empresas a explicar sus resultados.](https://www.genbeta.com/actualidad/asi-ley-que-gobierno-quiere-acabar-resenas-falsas-internet-obligar-a-google-otras-a-explicar-resultados)

[España y Alemania trabajan en una identidad digital común y han anunciado un acuerdo para poner en marcha un primer piloto con el que examinar su viabilidad.](https://elpais.com/tecnologia/2021-07-29/espana-y-alemania-trabajan-en-una-identidad-digital-comun-para-facilitar-los-tramites-online.html)

[El euro digital recibe la aprobación del Banco Central Europeo](https://www.xataka.com/empresas-y-economia/euro-digital-recibe-luz-verde-que-sabemos-proyecto-moneda-virtual-que-ha-aprobado-banco-central-europeo) y hasta 2023, el BCE seguirá investigando sobre su creación antes de empezar a hablar sobre su desarrollo e implantación.

[Rusia se desconecta con éxito del internet global en unas pruebas secretas.](https://www.elespanol.com/omicrono/tecnologia/20210730/rusia-desconecta-exito-internet-global-pruebas-secretas/600441093_0.html)

[Un proyecto de ley del Senado de EE.UU. obligaría legalmente a Apple a construir una puerta trasera en los iPhones.](https://9to5mac.com/2021/08/16/us-senate-bill-would-legally-require-apple-to-build-a-backdoor-into-iphones/)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Piped es un cliente de Youtube sin publicidad, sin tracking y privacy-friendly](https://piped.kavin.rocks/). Por supuesto el proyecto es software libre y está disponible [en este repositorio de código](https://github.com/TeamPiped/Piped)

[Element, la principal compañía detrás de Matrix, levanta una ronda de inversión de $30M.](https://element.io/blog/element-raises-30m-as-matrix-explodes/)

[El CEO de Proton dice que Pegasus es la prueba de que ninguna puerta trasera será utilizada únicamente por "los buenos".](https://www.euractiv.com/section/cybersecurity/news/pegasus-shows-no-backdoor-only-lets-the-good-guys-in-proton-ceo-says/)

[Cloudron es una plataforma para correr aplicaciones en tu propio servidor de forma sencilla.](https://www.cloudron.io/) La compañía lleva ya años trabajando para que [el Self-hosting sea sencillo.](https://blog.cloudron.io/selfhosting-is-easy/)

[Jaime Gómez-Obregón](https://twitter.com/JaimeObregon) es un ingeniero que trabaja en dar más transparencia al sector público. Ahora mismo se encuentra [haciendo lobby para que se liberen los datos mercantiles](https://www.eldiario.es/opinion/tribuna-abierta/ministra-libere-datos-mercantiles_129_8161672.html) en España y [analizando "la pandemia" de las plataformas municipales de comercio digital.](https://www.elespanol.com/opinion/tribunas/20210819/pandemia-plataformas-municipales-comercio-digital/605309466_12.html)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGIatlantic.eu mantiene su cuarto open call](https://ngiatlantic.eu/ngiatlanticeu-4th-open-call) en busca de proyectos financiables con hasta 75k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 15 de Septiembre de 2021.

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[DuckDuckGo’s surprisingly simple plan to make the internet more private](https://www.protocol.com/duckduckgo-ceo-interview), una entrevista a Gabriel Weinberg, CEO de DuckDuckGo.

[Qué hay después del fin de las cookies de terceros](https://www.newtral.es/fin-cookies-terceros/20210811/) por Marilín Gonzalo en Newtral.

[De-Identifying Medical Patient Data Doesn’t Protect Our Privacy](https://hai.stanford.edu/news/de-identifying-medical-patient-data-doesnt-protect-our-privacy) por Katharine Miller en Stanford University.

[Guía de Auditoría Algorítmica](https://www.eticasconsulting.com/wp-content/uploads/2021/01/Eticas-consulting.pdf) publicada por Eticas Consulting.

Y para acabar, un poco de humor: [El coche sin conductor de Tesla cruzará datos con Facebook para decidir a quién atropella](https://www.elmundotoday.com/2016/09/el-coche-sin-conductor-de-tesla-cruzara-datos-con-facebook-para-decidir-a-quien-atropella/)

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
