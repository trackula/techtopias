---
title: "The Facebook Files, Protonmail cede datos a la justicia y China prohibe las cryptos."
date: 2021-10-12T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-32.jpg"
# meta description
description: "The Facebook Files, Protonmail cede datos a la justicia y China prohibe las cryptos."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Tras un verano a medio gas, Techtopias recupera su frecuencia quincenal habitual, y lo hacemos con una de las grandes revelaciones del año, los ya famosos [The Facebook files](https://www.wsj.com/articles/the-facebook-files-11631713039), una serie de publicaciones de The Wall Street Journal, que ofrecen un visión de cómo Facebook e Instagram han ayudado a difundir bulos, generar depresión en adolescentes o incrementar la polarización y el enfrentamiento social.

Todo basándose en una serie de documentos internos que han sido filtrados y en donde se muestra cómo la multinacional tecnológica ha ignorado e incluso exacerbado su impacto negativo en la sociedad a sabiendas. [Aquí tenemos un rápido resumen del tema en un hilo de Twitter](https://twitter.com/carlesplb/status/1445084684585689106).

En España, el encargado de publicar dichas revelaciones ha sido el diario El Confidencial. A continuación enlazamos los artículos en castellano:

1. ['Facebook files': así permite la red social que los usuarios vip acosen y difundan bulos](https://www.elconfidencial.com/mercados/the-wall-street-journal/2021-09-15/facebook-files-elite-secreta-exenta-normas-red-social_3289248/)

2. [Depresión e ideas suicidas: Instagram es tóxico para las adolescentes y Facebook lo sabe](https://www.elconfidencial.com/mercados/the-wall-street-journal/2021-09-16/facebook-files-investigacion-interna-afirma-instagram-toxico-adolescentes_3290468/)

3. [Facebook descartó medidas para reducir la polarización para retener a los usuarios](https://www.elconfidencial.com/mercados/the-wall-street-journal/2021-09-18/facebook-intento-convertirse-lugar-sano-pero-sembro-ira_3290719/)

4. [Cárteles de droga y trata de personas: así ignora Facebook las denuncias de sus empleados](https://www.elconfidencial.com/mercados/the-wall-street-journal/2021-09-20/carteles-droga-trafico-personas-empleados-denunciaban-compania-no-actuaba_3291425/)

5. [Las confesiones del 'topo' de Facebook: “No odio a la compañía. Quiero salvarla”](https://www.elconfidencial.com/mercados/the-wall-street-journal/2021-10-05/informante-facebook-afirma-su-objetivo-es-mejorar-compania_3300989/)

Pero no solo de Facebook se ha hablado este mes que también ha venido bien cargadito:

[Intel planea invertir 95 mil millones de dólares en la fabricación de chips en Europa.](https://www.wsj.com/articles/intel-plans-investment-of-up-to-95-billion-in-european-chip-making-amid-u-s-expansion-11631027400)

[Un fallo de ciberseguridad en Quirón dejó al descubierto datos de 134.004 pacientes.](https://www.vozpopuli.com/economia_y_finanzas/quiron-ciberseguridad.html)

[Apple frena sus planes de lanzar su polémico sistema de escaneo de fotografías,](https://www.wired.com/story/apple-icloud-photo-scan-csam-pause-backlash/) una noticia que coincide con los nuevos planes de la compañía para [añadir funcionalidades a sus iPhones con la idea de ayudar a detectar depresión y deterioro cognitivo.](https://www.wsj.com/articles/apple-wants-iphones-to-help-detect-depression-cognitive-decline-sources-say-11632216601)

[Google también parece que se echa atrás en sus planes de ofrecer cuentas bancarias a sus usuarios.](https://www.wsj.com/articles/google-is-scrapping-its-plan-to-offer-bank-accounts-to-users-11633104001)

[Spotify lanza una nueva campaña para obtener más anunciantes.](https://www.wsj.com/articles/spotify-kicks-off-campaign-to-win-more-advertisers-11632736800)

[Se filtran unos documentos que muestran cómo Astro, el nuevo robot de Amazon, trackea todo lo que haces en tu casa.](https://www.vice.com/en/article/93ypp8/leaked-documents-amazon-astro-surveillance-robot-tracking)

El futuro de la publicidad segmentada también llegará al video, [una de las empresas de Bill Gates busca cambiar la publicidad dinámicamente dentro de imagenes de películas y series.](https://www.eldiario.es/tecnologia/ves-cerveza-refresco-proyecto-bill-gates-cambiar-productos-salen-series-peliculas_1_8275758.html)

Semanas atrás saltaba la [noticia](https://www.lavanguardia.com/videojuegos/20210830/7689667/china-prohibicion-jugar-online-3-horas-menores-restriccion.html) de que China pretendía imponer restricciones en el uso de videojuegos a niños, [unas restricciones que no protegen a los niños y sin embargo buscan proteger el interés del Estado.](https://hipertextual.com/2021/09/las-restricciones-en-los-videojuegos-impuestas-en-china-no-protegen-a-los-ninos-protegen-al-estado)

[Facebook habría proporcionado datos inexactos a los investigadores de desinformación.](https://www.theverge.com/2021/9/11/22668396/facebook-provided-inaccurate-data-misinformation-researchers)

Estas semanas Facebook también ha sido noticia por [la caída de su plataforma, que también ha incluido a WhatsApp e Instagram.](https://www.newtral.es/caida-de-facebook-instagram-whatsapp/20211004/)

[Google Assistant pronto podría escucharte sin utilizar el 'Ok Google'.](https://www.androidpolice.com/2021/09/01/assistant-could-soon-hear-you-without-the-obligatory-ok-google/)

La compañía de correo electrónico cifrado, ProtonMail, [ha entregado datos de usuarios a la policía,](https://www.eleconomista.es/tecnologia/noticias/11381173/09/21/El-correo-electronico-mas-seguro-pillado-entregando-datos-de-usuarios-a-la-policia.html) algo que ha desconcertado a sus usuarios ya que en sus políticas, indicaban que no almacenaban datos como logs o la IP.

[La empresa ha lanzado un comunicado para explicar lo sucedido](https://protonmail.com/blog/climate-activist-arrest/) y ha procedido a [eliminar el apartado de "sin logs" de su política de privacidad.](https://www.muyseguridad.net/2021/09/10/protonmail-elimina-sin-logs/) Maldita ha profundizado en el tema [ya que estas plataformas, a pesar de respetar tu privacidad, también tienen que cumplir con las leyes.](https://maldita.es/malditatecnologia/20210907/plataformas-privacidad-leyes-protonmail/)

[Los pasajeros del metro de Moscú podrán pagar el billete directamente utilizando su cara.](https://slate.com/technology/2021/09/facial-recognition-cameras-subway-russia.html)

En el campo laboral, [la plantilla de Telefónica rechaza la jornada de 4 días por la bajada salarial](https://www.lainformacion.com/empresas/plantilla-telefonica-jornada-cuatro-dias-golpe-salarial/2848780/) y en paralelo [la Eurocámara exige condiciones "justas" para los 'riders' y trabajadores de plataformas.](https://www.eldiario.es/economia/eurocamara-exige-condiciones-justas-riders-trabajadores-plataformas_1_8305335.html)

[Tesla comienza a activar su conducción autónoma a quien desee probarla, pero sólo para lo que ellos consideran "buenos conductores".](https://www.xataka.com/vehiculos/tesla-esta-activando-su-conduccion-autonoma-full-self-driving-a-quien-desee-probarla-solo-para-buenos-conductores)

Otra noticia que ha sacudido el mundo tecnológico es el anuncio de que [China ha declarado toda actividad relacionada con las criptodivisas como ilegal, lo que ha hecho desplomar la cotización del bitcoin.](https://www.elconfidencial.com/mercados/2021-09-24/china-banco-central-criptomonedas-ilegal_3295404/)

[Multa millonaria para WhatsApp: 225M por saltarse la ley de protección de datos de la UE.](https://www.elconfidencial.com/tecnologia/2021-09-02/gdpr-whatsapp-facebook-multa-millones-euros_3266338/)

[Indra y Westpole se llevan por 180 millones un contrato para garantizar la calidad de los sistemas informáticos de las fronteras inteligentes de la UE](https://www.elconfidencial.com/empresas/2021-09-13/indra-validacion-interior-justicia-ue_3288334/) mientras la Presidencia eslovena del Consejo de la UE [planea acelerar las negociaciones para la ampliación de la base de datos con datos sensibles de migrantes y solicitantes de asilo.](https://edri.org/our-work/eurodac-council-seeks-swift-agreement-on-expanded-migrant-biometric-database/)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Carme Artigas, la secretaria de Estado de Digitalización e IA, dice que pretenden cambiar el 'legacy' de la Administración Pública poco a poco.](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/espana/20210902/carme-artigas-vamos-cambiar-administracion-publica-no/608939561_0.html)

Las startups también han estado de actualidad este mes, primero por [el llamamiento de Artigas a la inversión en talento, emprendedores y empresas españolas: "Somos una gran fábrica de startups que luego malvendemos en el extranjero"](https://www.businessinsider.es/carme-artigas-somos-gran-fabrica-startups-malvendemos-931569) y posteriormente por la llegada del nuevo Fondo Next Tech de hasta 4.000 millones de euros para el cual [las gestoras de fondos ya tocan la puerta a Calviño.](https://www.lainformacion.com/emprendedores/gestoras-tocan-puerta-calvino-captar-dinero-megafondo-tech/2849876/)

[Alemania quiere obligar a que los fabricantes de móviles den siete años de actualizaciones de software en Europa](https://www.genbeta.com/actualidad/alemania-quiere-obligar-a-que-fabricantes-moviles-den-siete-anos-actualizaciones-software-europa) y en paralelo la [Unión Europea quiere un cargador universal que funcione en todos los móviles.](https://www.newtral.es/cargador-universal-union-europea/20210928/)

La soberanía tecnológica también coge carrerilla en la UE, en donde [Francia ha prohibido a sus ministros usar la nube de Office 365 por temor a que Microsoft comparta información sensible con el gobierno de los EE.UU](https://www.xataka.com/empresas-y-economia/francia-prohibe-a-sus-ministros-usar-nube-office-365-temor-a-que-microsoft-deba-compartir-informacion-sensible-gobierno-ee-uu) y la UE se prepara para lanzar [una ley europea de chips para no depender de Asia y EE UU en semiconductores.](https://cincodias.elpais.com/cincodias/2021/09/15/companias/1631709002_613061.html)

[El BCE continua con sus lentos pero continuos avances en el euro digital y pone en marcha la primera fase del plan para su creación.](https://www.finanzas.com/divisas/el-bce-pone-en-marcha-la-primera-fase-del-euro-digital.html)

[Google, Facebook y Microsoft lideran el gasto en acciones de lobby en la Unión Europea.](https://www.euractiv.com/section/politics/news/google-facebook-microsoft-top-eu-lobbying-spending/)

[China advierte de que continuará aumentando la presión regulatoria sobre el sector tecnológico.](https://www.eleconomista.es/empresas-finanzas/noticias/11373888/08/21/China-advierte-de-que-aumentara-la-presion-regulatoria-sobre-el-sector-tecnologico.html)

[EEUU se prepara para espiar a sus ciudadanos con inteligencia artificial de la mano de Hewlett Packard.](https://www.elconfidencial.com/tecnologia/novaceno/2021-09-05/eeuu-espiar-ciudadanos-inteligencia-artificial_3270594/)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Plausible](https://plausible.io/), una alternativa sencilla, open source y privacy-friendly a Google Analytics.

[Follow the changes to the terms of service](https://www.opentermsarchive.org/en), una herramienta para mantenerte al día de los cambios en los términos de servicios de múltiples plataformas.

["We are responsible for creating a culture in which we can remain private."](https://protonmail.com/blog/carissa-veliz-data-privacy/), una entrevista a Carissa Véliz en el blog de ProtonMail.

[Malloc](https://techcrunch.com/2021/09/27/y-combinator-malloc-spyware/), la startup chipriota apoyada por YCombinator que busca identificar potenciales spywares en tu teléfono.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

["The Facebook Files"](https://www.wsj.com/articles/the-facebook-files-a-podcast-series-11631744702) también está disponible en formato podcast.

["El problema no es sólo Facebook: es hora de trocear los monopolios en Internet"](https://www.publico.es/sociedad/internet-problema-no-facebook-hora-trocear-monopolios-internet.html) por Pablo Romero en Público.

["How Data Brokers Sell Access to the Backbone of the Internet"](https://www.vice.com/en/article/jg84yy/data-brokers-netflow-data-team-cymru) por Joseph Cox en Vice.

["This is the real story of the Afghan biometric databases abandoned to the Taliban"](https://www.technologyreview.com/2021/08/30/1033941/afghanistan-biometric-databases-us-military-40-data-points) por Eileen Guo y Hikmat Noori en MIT Technology Review.

[What Does Facebook Mean When It Says It Supports “InternetRegulations”?](https://themarkup.org/ask-the-markup/2021/09/16/what-does-facebook-mean-when-it-says-it-supports-internet-regulations) por Aaron Sankin en The Markup.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
