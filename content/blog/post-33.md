---
title: "La privacidad de los futbolistas, Google frenando leyes y los coletazos de Facebook Files."
date: 2021-10-27T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-33.jpg"
# meta description
description: "La privacidad de los futbolistas, Google frenando leyes y los coletazos de Facebook Files."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Los Facebook Files siguen dando sus últimos coletazos y estos días [se comentaba cómo diversos medios del mundo se disponían a publicar el resultado de una investigación coordinada sobre dicha plataforma.](https://mobile.twitter.com/CarmelaRios/status/1451861786496585730) Una investigación basada en los documentos internos filtrados por Frances Haugen y que [Carmela Ríos ha tratado de recopilar en este hilo de Twitter.](https://twitter.com/CarmelaRios/status/1452676325853040640)

Con este contexto, el artículo ["Los cinco colosos que sostienen la vida digital, cómo han llegado ahí y por qué se han vuelto peligrosos"](https://www.eldiario.es/tecnologia/cinco-colosos-sostienen-vida-digital-han-llegado-han-vuelto-peligrosos_1_8377235.html) aporta una buena reflexión sobre la situación de los monopolios tecnológicos actuales entre los que se encuentra el propio Facebook.

[Los colegios de UK han comenzado a utilizar reconocimiento facial para cobrar la comida de los niños.](https://www.theverge.com/2021/10/18/22732330/uk-schools-facial-recognition-lunch-payments-north-ayrshire) Una nueva prueba de cómo se está normalizando poco a poco el uso de estas tecnologías mientras organizaciónes como [Big Brother Watch ya se han puesto a trabajar para luchar contra ello.](https://twitter.com/BigBrotherWatch/status/1450407200686592001)

[Trump anuncia el lanzamiento de su propia empresa mediática y una red social llamada Verdad](https://elpais.com/internacional/2021-10-21/trump-anuncia-el-lanzamiento-de-su-propia-empresa-mediatica-y-una-red-social-llamada-verdad.html), una plataforma basada en un fork de la red social descentralizada Mastodon.

[Investigadores españoles descubren que a través de anuncios de Facebook es posible llegar a una persona en concreto](https://www.elconfidencial.com/tecnologia/ciencia/2021-10-24/facebook-identificar-manipular-anuncios_3311563/), únicamente conociendo sus intereses.

[Google ha ralentizado intencionadamente la carga de webs que no utilizaban AMP para mejorar los datos de sus comparativas.](https://mobile.twitter.com/tmcw/status/1451938637982142467)

[Apple estudia la posibilidad de utilizar los AirPods como un dispositivo de salud.](https://www.wsj.com/articles/apple-studying-potential-of-airpods-as-health-device-11634122800)

[Apple ha triplicado sus ingresos publicitarios en seis meses tras poner trabas a la competencia.](https://www.genbeta.com/actualidad/apple-ha-triplicado-sus-ingresos-publicitarios-seis-meses-poner-trabas-a-competencia)

[Mozilla implementará anuncios en la barra de direcciones de Firefox 92 disimulándolos como "sugerencias".](https://www.genbeta.com/navegadores/mozilla-implementara-anuncios-barra-direcciones-firefox-92-hara-disimulandolos-como-sugerencias)

[Twitter ha publicado un estudio sobre cómo su algoritmo amplifica contenidos políticos y ha revelado que dicho algoritmo beneficia a contenidos de derecha aunque no sabe explicar el motivo.](https://www.elperiodico.com/es/politica/20211021/twitter-revela-algoritmo-beneficia-derecha-12337090)

[Un grupo global de especialistas en ciberseguridad pide a tecnológicas y gobiernos que abandonen la idea de escanear el contenido en abierto en móviles, evitando así el cifrado.](https://elpais.com/tecnologia/2021-10-15/en-tu-movil-no-debe-entrar-nadie-un-grupo-global-de-expertos-pide-proteger-la-ultima-frontera-de-la-privacidad.html)

[Así es la vida de los jóvenes que desconectan de las redes sociales para conectar consigo mismos.](https://www.lavanguardia.com/vivo/lifestyle/20211023/7787378/jovenes-desconectados-redes-sociales.html)

[Éticas Foundation lanza un buscador para conocer qué algoritmos impactan en tu vida.](https://cincodias.elpais.com/cincodias/2021/10/07/companias/1633642034_800454.html) Se busca conocer más sobre los algoritmos que empresas y gobiernos utilizan para tomar decisiones automatizadas, y para ello Éticas ha lanzado el [Observatorio de Algoritmos con Impacto Social (OASI)](https://eticasfoundation.org/oasi/).

[850 futbolistas profesionales lanzan una denuncia para controlar los datos de rendimiento que generan y que muchas empresas utilizan para hacer negocio.](https://mobile.twitter.com/gemmagaldon/status/1452225077848776712)

[Facebook y Google están trabajando para dominar también la parte "física" de internet, desde datacenters hasta cables submarinos.](https://www.wired.co.uk/article/facebook-google-subsea-cables)

[DuckDuckGo y Ecosia piden a la UE que prohíba que Google sea el buscador predeterminado en navegadores y sistemas operativos.](https://www.genbeta.com/actualidad/duckduckgo-ecosia-piden-a-ue-que-prohiba-que-google-sea-buscador-predeterminado-navegadores-sistemas-operativos)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El Partido Popular presenta una proposición de ley para obligar a las tecnológicas a identificar a sus usuarios con el DNI y así acabar con el anonimato en internet.](https://www.elmundo.es/espana/2021/10/18/616c5ae3fdddfffa208b45c0.html)

[El 5G, la fibra y la ciberseguridad recibirán 1.550 millones de los Presupuestos Generales del Estado.](https://cincodias.elpais.com/cincodias/2021/10/13/companias/1634129163_157880.html)

[El Gobierno trabaja para aprobar la transposición de la directiva de derechos de autor el 2 de noviembre.](https://www.efe.com/efe/espana/cultura/el-gobierno-aprobara-la-directiva-de-derechos-autor-2-noviembre/10005-4659901)

[España no ha sido invitada a la cumbre de la ciberseguridad organizada por EEUU a la cual asistieron más de 30 paises.](https://www.elconfidencial.com/tecnologia/2021-10-16/espana-ciberseguridad-biden-reunion-ausencia_3307189/)

[Google reconoce en documentos internos el haber ralentizado satisfactoriamente normativas de privacidad europeas.](https://www.swissinfo.ch/spa/eeuu-google_google-presume-en-privado-de-ralentizar-la-normativa-de-privacidad-en-la-ue/47051196)

[La guardia pretoriana de Vestager se fuga a los bufetes de las 'Big Tech'.](https://www.elconfidencial.com/empresas/2021-10-17/guardia-pretoriana-vestager-fuga-bufetes-big-tech_3304998/)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

Los Verdes europeos lanzan la campaña ["A future without manipulation"](https://afuturewithoutmanipulation.eu/) con el objetivo de concienciar sobre la manipulación en internet.

Mozilla continúa ampliando su proyecto [Privacy not included](https://foundation.mozilla.org/es/privacynotincluded/) donde analiza herramientas, juguetes, apps y todo tipo de tecnologías desde la perspectiva de cómo esas tecnologías respetan la privacidad de sus usuarios.

Ya se ha lanzado también el [call for papers para el Mozfest 2022.](https://www.mozillafestival.org/en/proposals/)

El proyecto [IUVIA lanza un nuevo video y una web renovada,](https://iuvia.io/) anunciando que lanzará su campaña de Kickstarter el próximo mes.


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Nada que Esconder S02E01: ¿Qué relación hay entre Facebook y tu salud mental? con Mar Cabra](https://blog.iuvia.io/es/nadaqueesconder-s02e01/), el primer podcast de la nueva temporada de la gente de IUVIA.

["Como con el cambio climático, en el ámbito digital también nos estamos quedando sin tiempo"](https://www.epe.es/es/cultura/20211014/esther-paniagua-cambio-climatico-ambito-12238504), una entrevista a Esther Paniagua.

[Inside a European push to outlaw creepy ads](https://techcrunch.com/2021/10/21/inside-a-european-push-to-outlaw-creepy-ads/) por Natasha Lomas en Techcrunch.

[We need to talk about how Apple is normalising surveillance](https://www.wired.co.uk/article/apple-surveillance-technology) por Carissa Véliz en Wired.

[Adapt](https://adapt.internews.org/podcast), un nuevo podcast sobre privacidad de la mano de la Fundación Ciudadanía y Desarrollo.

[Creating a culture of data verification to fight disinformation in Wikipedia](https://diff.wikimedia.org/2021/09/29/creating-a-culture-of-data-verification-to-fight-disinformation-in-wikipedia/), un artículo de Virginia Diez.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
