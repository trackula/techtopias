---
title: "El metaverso, la ley de propiedad intelectual y el silencioso avance del reconocimiento facial."
date: 2021-11-10T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-34.jpg"
# meta description
description: "El metaverso, la ley de propiedad intelectual y el silencioso avance del reconocimiento facial."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Y de nuevo Facebook vuelve a ser noticia, esta vez por su conversión en [Meta, el nuevo nombre de la empresa matriz de Facebook.](https://www.elconfidencial.com/tecnologia/2021-10-28/cambio-nombre-facebook-compania_3315051/) Una noticia tildada de "lavado de cara" para intentar reducir la mala imagen de la tecnológica y que incluye también el anuncio del llamado [Metaverso con el que Zuckerberg anuncia su realidad paralela que también ha sido fuertemente criticada.](https://www.vice.com/en/article/qjb485/zuckerberg-facebook-new-name-meta-metaverse-presentation)

Las dudas sobre el proyecto se lanzan desde voces internas como [John Carmack, el CTO de Oculus](https://arstechnica.com/gaming/2021/10/john-carmack-sounds-a-skeptical-note-over-metas-metaverse-plans/) hasta otras externas como [el exCEO de Google que ha advertido de que el metaverso "no es necesariamente lo mejor para la sociedad".](https://www.businessinsider.es/exceo-google-preocupado-metaverso-facebook-957411)

[En este hilo](https://mobile.twitter.com/Hugo_saez/status/1454839710191394819) se recopilan diversas e interesantes noticias sobre el tema y también me gustaría acabar con [la reflexión que hace Sofía Prosper sobre los posibles problemas que puede traer la llegada del Metaverso.](https://twitter.com/sofipros/status/1454007477234814979)

[Apple también está trabajando en la carrera por la realidad aumentada con una nueva patente.](https://www.elconfidencial.com/tecnologia/novaceno/2021-10-29/apple-iphone-realidad-aumentada-metaverso_3315065/)

El reconocimiento facial está ganando poco a poco terreno, [más de 11 paises de la Unión Europea ya lo utilizan según un estudio](https://www.euractiv.com/section/data-protection/news/facial-recognition-technologies-already-used-in-11-eu-countries-and-counting-report-says/) y se está extendiendo por todo tipo de sectores como ya comentamos [con el pago del metro de Moscú](https://elpais.com/tecnologia/transformacion-digital/2021-10-23/el-pago-del-metro-por-reconocimiento-facial-lleva-la-sombra-del-gran-hermano-al-subsuelo-de-moscu.html) o el intento de [introducirla en los estadios de los equipos de fútbol franceses que de momento han sido frenados por el regulador.](https://www.biometricupdate.com/202110/ready-facial-recognition-market-among-french-soccer-clubs-restrained-by-regulator)

Mientras trando EDRI [cuenta cómo los Eurodiputados se están preparando para dar un cheque en blanco a la Europol para usar todo tipo de tecnologías biométricas.](https://edri.org/our-work/meps-poised-to-vote-blank-cheque-for-europol-using-ai-tools/)

[Facebook también ha anunciado que "abandona el uso del reconocimiento facial" pero según parece, esto no va a aplicar al Metaverso.](https://www.vox.com/recode/22761598/facebook-facial-recognition-meta)

[Empleados de Amazon en Nueva York dan los primeros pasos para constituir un sindicato.](https://www.elmundo.es/economia/empresas/2021/10/26/6177667921efa005468b457f.html)

Parece que Zoom se une al negocio de la publicidad ya que [la versión gratuita de Zoom comienza a mostrar anuncios a algunos usuarios.](https://es.gizmodo.com/la-version-gratuita-de-zoom-comienza-a-mostrar-anuncios-1847982382)

[Los Facebook Papers han desencadenado una investigación de la FTC para ver si Facebook violó un acuerdo de privacidad de 5.000 millones de dólares.](https://www.businessinsider.com/ftc-probing-facebook-over-leaked-whistleblower-files-report-2021-10)

[Cabify vuelve a las noticias ya que más de una veintena de conductores se querellan contra la empresa por sus condiciones laborales abusivas.](https://www.epe.es/es/empresas/20211031/conductores-querellan-cabify-condiciones-abusivas-12293047)

[ShotSpotter, la controvertida empresa que "escucha" lo que pasa en las calles de decenas de ciudades de EE.UU. en busca de sonidos de disparos de armas de fuego y alerta a la policía.](https://www.bbc.com/mundo/noticias-59113535)

[Frances Haugen, la mujer que ha desvelado los Facebook Papers, ha hablado sobre dicho escándalo ante el Parlamento Europeo: "La UE puede cambiar las reglas del juego".](https://www.epe.es/es/internacional/20211108/frances-haugen-garganta-profunda-facebook-12792424)

[La UE investiga la filtración de claves para falsificar pasaportes covid.](https://www.genbeta.com/actualidad/ue-investiga-filtracion-claves-para-falsificar-pasaportes-covid-bob-esponja-tiene-suyo-para-viajar-libremente)

[Estados Unidos está trabajando con Taiwan para asegurar la cadena de suministro de chips.](https://www.bloomberg.com/news/articles/2021-10-29/u-s-says-it-s-working-with-taiwan-to-secure-chip-supply-chain)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Estas semanas han estado muy movidas en el Ministerio de Cultura y Deporte con la transposición de la nueva [directiva de propiedad intelectual que obligará Google a comunicar cambios en su algoritmo a los medios.](https://elpais.com/cultura/2021-11-02/la-nueva-ley-de-propiedad-intelectual-facilita-la-vuelta-de-google-news-a-espana.html)

La llamada 'Ley Iceta' también ha sido noticia ya que [abre la puerta a la censura directa y algorítmica, entre otros para 'streamers' y retransmisiones en directo en la red.](https://www.publico.es/sociedad/ley-iceta-abre-puerta-censura-directa-algoritmica-streamers-retransmisiones-directo-red.html)

[Xnet ha realizado un más que recomendable análisis de algunos aspectos del #DecretazoCopyright desde el punto de vista de los derechos digitales.](https://xnet-x.net/es/decretazo-copyright-transpuesto-articulo17-censura-algoritmica/)

[El gobierno reduce un 26% la financiación del Instituto Nacional de Ciberseguridad pese al aumento de los ataques informáticos.](https://www.vozpopuli.com/economia_y_finanzas/ciberseguridad-presupuesto-aumento-ataques.html)

[España intentará construir el primer ordenador cuántico del sur de Europa.](https://www.eldiario.es/tecnologia/espana-intentara-construir-primer-ordenador-cuantico-sur-europa_1_8430862.html)

[Los gigantes de las redes sociales se enfrentarán a multas de 10 millones de dólares por violaciones de la privacidad según la propuesta de reforma del gobierno australiano.](https://theguardian.com/australia-news/2021/oct/25/social-media-giants-face-10m-fines-for-privacy-breaches-under-proposed-government-reform)

[El proyecto de ley de infraestructuras de 1,2 billones de dólares de Biden propone añadir balizas a ciclistas y peatones para facilitar su detección por parte de los coches autónomos.](https://www.forbes.com/sites/carltonreid/2021/11/06/bidens-12-trillion-infrastructure-bill-hastens-beacon-wearing-for-bicyclists-and-pedestrians-to-enable-detection-by-connected-cars/)

[Estados Unidos prohibe operar a China Telecom por motivos de seguridad nacional.](https://www.securityweek.com/us-bans-china-telecom-over-national-security-concerns)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Framework](https://frame.work/) es un portátil que busca facilitar que sea reparado y actualizado. [“Es demencial que productos tan caros y avanzados como un ordenador sean tan desechables”](https://elpais.com/tecnologia/2021-11-02/es-demencial-que-productos-tan-caros-y-avanzados-como-un-ordenador-sean-tan-desechables.html) decía su fundador a El País.

Un equipo de investigadores de la Universidad Estatal de Pensilvania ha creado [PrivaSeer](https://privaseer.ist.psu.edu/), un buscador de políticas de privacidad [que obtiene también datos sobre las industrias a las que pertenecen, qué regulaciones se tienen en cuenta o qué tecnologías de rastreo mencionan.](https://elpais.com/tecnologia/2021-11-03/textos-en-latin-y-longitudes-inasumibles-las-revelaciones-de-un-buscador-de-politicas-de-privacidad.html)

[Tim Berners-Lee ha presentado en el WebSummit su proyecto Inrupt](https://www.pcmag.com/news/tim-berners-lee-wants-to-put-online-privacy-on-a-solid-foundation) y sobre todo ha hablado de su propuesta de cómo debe cambiar internet para darle mayor control a los usuarios sobre sus datos personales, basándose en el [nuevo protocolo Solid.](https://solidproject.org/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[¿Por qué bailan los robots? Maldita Twitchería sobre lo que pueden hacer a día de hoy y lo que no](https://maldita.es/malditatecnologia/20210924/robots-preguntas-respuestas-maldita-twitch/) por Maldita Tecnología.

[Facebook told the White House to focus on the ‘facts’ about vaccine misinformation. Internal documents show it wasn’t sharing key data.](https://www.washingtonpost.com/technology/2021/10/28/facebook-covid-misinformation/) por Gerrit De Vynck, Cat Zakrzewski y Cristiano Lima en The Washington Post.

[Internet shutdowns are a political weapon. It’s time to disarm](https://techcrunch.com/2021/10/30/internet-shutdowns-are-a-political-weapon-its-time-to-disarm/) por Scott Carpenter en Techcrunch.

[The future of privacy](https://open.spotify.com/episode/253ZFodKdNATdCq3eTfOqB), una entrevista a Carissa Véliz.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
