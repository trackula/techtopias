---
title: "La ley Iceta, el procesador cuántico de IBM y Apple permitiendo reparar sus equipos."
date: 2021-11-23T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-35.jpg"
# meta description
description: "La ley Iceta, el procesador cuántico de IBM y Apple permitiendo reparar sus equipos."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Instagram comienza a pedir a sus usuarios un vídeo de su cara para verificar su identidad](https://www.elconfidencial.com/tecnologia/2021-11-16/instagram-verificar-usuarios-identidad-video_3325035/). Menos mal que decían que dejarían de utilizar reconocimiento facial...

[IBM presenta un procesador cuántico](https://elpais.com/tecnologia/2021-11-15/ibm-presenta-un-procesador-cuantico-cuya-potencia-ya-no-puede-ser-simulada-por-ordenadores-convencionales.html), una nueva tecnología que puede revolucionar el mundo con su capacidad de cálculo y que [amenaza con acabar con el cifrado tal y como lo conocemos, lo que podría afectar tambien a las criptomonedas.](https://www.elconfidencial.com/tecnologia/novaceno/2021-11-17/criptomonedas-perder-valor-ordenador-cuantico_3325197/)

[Un estudio demuestra que el Bluetooth puede revelar nuestra ubicación a terceros.](https://www.xatakamovil.com/seguridad/descubren-vulnerabilidad-conexion-bluetooth-que-puede-desvelar-nuestra-ubicacion-a-terceros)

Un grupo de padres de Estocolmo [desarrollan una alternativa open source a una desastrosa app escolar y son denunciados por la administración.](https://www.genbeta.com/desarrollo/padres-estocolmo-desarrollan-alternativa-open-source-a-desastrosa-app-escolar-ayuntamiento-les-manda-a-policia)

[Twitter decide volver atrás en el soporte de AMP y dejará de soportarlo](https://searchengineland.com/twitter-rolls-back-amp-support-no-longer-sends-users-to-amp-pages-376168).

La tecnológica también ha sido noticia estas semanas al [lanzar en EEUU su servicio de suscripción Blue, con acceso sin anuncios a más de 300 páginas.](https://www.cnbc.com/2021/11/09/twitter-blue-subscription-service-launches-for-us-users.html)

Estas semanas los ciberataques han vuelto a las andadas en nuestro entorno, por un lado [han paralizado la producción de cerveza de Damm](https://www.eldiario.es/tecnologia/ciberataque-paraliza-produccion-cerveza-damm_1_8481414.html), también [han bloqueado los servidores de Mediamarkt en Europa](https://www.epe.es/es/sociedad/20211109/ciberataque-bloquea-servidores-mediamarkt-europa-12804052) y se ha comprometido los datos de clientes de [Movistar por un fallo de seguridad.](https://www.elespanol.com/omicrono/software/20190808/movistar-sufre-fallo-seguridad-compromete-datos-clientes/419958727_0.html)

[Absueltos los responsables de 'elitedivx' tras 14 años y medio de calvario penal.](https://www.publico.es/sociedad/absueltos-responsables-elitedivx-14-anos-medio-calvario-penal.html)

[Apple acelera su esfuerzo en el coche autónomo y conectado](https://www.bloomberg.com/news/articles/2021-11-18/apple-accelerates-work-on-car-aims-for-fully-autonomous-vehicle) al mismo tiempo que la tecnológica anuncia que [permitirá que los usuarios reparen sus aparatos y venderá baterías y pantallas.](https://www.eldiario.es/tecnologia/apple-permitira-usuarios-reparen-aparatos-vendera-baterias-pantallas_1_8499284.html)

[Diario de un 'rider' nocturno en Madrid: mis dos meses trabajando para Glovo y UberEats.](https://www.elconfidencial.com/tecnologia/2021-11-05/rider-madrid-glovo-uber_3318569/).

Mientras tanto, la [Inspección de Trabajo multa a Glovo con más de 8,5 millones por no regularizar los contratos de sus repartidores en Sevilla.](https://elpais.com/economia/2021-11-19/la-inspeccion-de-trabajo-multa-a-glovo-con-mas-de-ocho-millones-y-medio-de-euros-por-no-regularizar-los-contratos-de-sus-repartidores-en-sevilla.html)

Y además de las plataformas ya existentes, [nuevas apps turcas y alemanas han aparecido y están llenando España de 'riders' a toda velocidad.](https://www.elconfidencial.com/tecnologia/2021-11-13/turcos-alemanes-espana-riders-diez-minutos_3322379/)

[Facebook anuncia que eliminará cientos de categorías sensibles que se podían utilizar para segmentar publicidad.](https://mobile.twitter.com/TTP_updates/status/1458197674394521604)

Un informe del gobierno de EEUU desvela que [grandes proveedores de servicios de Internet ISPs venden datos a terceros como la raza, orientación sexual, geolocalización, etc, incluso cuando prometen no hacerlo.](https://www.ftc.gov/news-events/press-releases/2021/10/ftc-staff-report-finds-many-internet-service-providers-collect)

[La Justicia europea confirma la multa de 2017 de 2.400 millones de euros a Google](https://www.elconfidencial.com/tecnologia/2021-11-10/la-justicia-europea-confirma-la-multa-de-2017-de-2-400-millones-de-euros-a-google_3321539/), una multa que reafirma el trabajo de la UE en la lucha por mejorar la competencia. [Margarethe Vestager también ha hablado sobre que las empresas deben ser expuestas a la competencia ya que también les ayuda a innovar](https://www.euractiv.com/section/competition/news/firms-must-be-exposed-to-competition-says-vestager-in-blow-to-eu-champions/) en un momento en el que el debate sobre crear "campeones europeos" para luchar contra los estadounidenses y chinos, está también sobre la mesa.

[La región de Sajonia va camino de convertirse en un polo de produción de chips de última generación.](https://www.euractiv.com/section/digital/news/silicon-saxony-to-play-key-role-in-europes-bid-for-producing-cutting-edge-chips/)

[Huawei y Alibaba patrocinan la primera cumbre sobre el cloud Gaia-X.](https://www.politico.eu/article/huawei-alibaba-sponsorship-overshadows-european-cloud-gaia-xs-summit/) Un patrocinio que ha generado bastante polémica ya que no deja de ser raro que estas plataformas patrocinen el cloud soberano europeo.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

La llegada de la famosa "Ley Iceta" ha generado un terremoto en el mundo de la propiedad intelectual e internet. Esta ley llega para transponer la directiva europea de propiedad intelectual y parece que [va un paso más allá y deja una puerta abierta a la censura automatizada en Internet.](https://www.microsiervos.com/archivo/internet/ley-iceta-sensura-automatizada-internet.html)

Múltiples expertos como [Carlos Almeida](https://blogs.publico.es/dominiopublico/41020/ley-iceta-contra-la-censura-hoy-ayer-y-siempre/) director del Bufetealmeida, [Julia Reda](https://mobile.twitter.com/PDLI_/status/1458831834725376002) eurodiputada del Partido Pirata o incluso [David Bonilla](https://mailchi.mp/bonillaware/ley-iceta) fundador de Manfred y la Bonilista, han escrito sobre la ley que, claramente no gusta. [En Newtral han realizado un artículo muy interesante y profundo sobre el tema.](https://www.newtral.es/ley-iceta-aciertos-y-problemas-de-la-nueva-ley-de-derechos-de-autor/20211110/)

[El gobierno aprueba el proyecto de la nueva Ley General de Telecomunicaciones, convirtiendo a aplicaciones como WhatsApp o Skype en operadoras.](https://www.genbeta.com/actualidad/whatsapp-skype-operadoras-espana-no-pagaran-ese-impuesto-asi-proyecto-ley-telecomunicaciones)

[El Ministerio de Trabajo refuerza su sistema de multas automáticas para atajar el fraude en temporalidad.](https://www.elperiodico.com/es/economia/20211116/trabajo-refuerza-sistema-multas-automaticas-12857496)

[El Ministerio de Consumo planea regulrar las 'cajas de recompensas', muy utilizadas en videojuegos, y que pueden incitar a la ludopatía.](https://compromiso.atresmedia.com/levanta-la-cabeza/actualidad/tienen-cajas-recompensas-videojuegos-dias-contados_202111166194c7c277bc800001a8432b.html)

[Portugal prepara una legislación para fomentar la vigilancia biométrica masiva.](https://www.genbeta.com/actualidad/portugal-quiere-aprobar-espionaje-masivo-video-biometria-esto-argumentan-activistas-derechos-digitales)

[RisCanvi, el algoritmo que decide sobre la libertad condicional en Cataluña.](https://www.epe.es/es/sociedad/20211117/algoritmo-decidir-libertad-condicional-cataluna-12862242)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Andreessen Horowitz lidera una ronda de inversión de 13 millones de dólares de Nym Technologies.](https://www.forbes.com/sites/ninabambysheva/2021/11/17/andreessen-horowitz-leads-investment-in-privacy-startup-integrating-with-bitcoin/)
La compañía está trabajando en [desarrollar una infraestructura para evitar la fuga de datos en base a proteger los metadatos de cada paquete que se envía por la red.](https://nymtech.net/)

[Matrix prepara videollamadas descentralizadas sobre múltiples nodos.](https://twitter.com/matrixdotorg/status/1462426052467302402)

[Skiff ofrece documentos compartidos privados, cifrados end-to-end y descentralizados.](https://www.skiff.org/) El servicio incluso permite [almacenar sus documentos en IPFS](https://www.fastcompany.com/90696585/skiff-ipfs-storage-private-document-editor), ayudando al ecosistema alrededor de esta tecnología a crecer poco a poco.

El programa de la UE [Next Generation Internet ha lanzado una consulta abierta](https://www.ngi.eu/news/2021/10/22/ngi-next-steps/) para obtener ideas sobre los siguientes pasos a dar en el programa.


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

Si trabajas en temas relacionados con portabilidad de datos y servicios, [NGI está a punto de cerrar su open call de DAPSI, su incubadora para proyectos de este ámbito](https://dapsi.ngi.eu/apply/) y que financia hasta 150.000 € a fondo perdido. La convocatoria termina hoy, 23 de Noviembre de 2021.

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[¿Qué es lo que realmente vemos al abrir nuestras RRSS? ¿Un reflejo de la realidad o el mundo que nos crea un algoritmo? ¿Contrastas todas las noticias y datos que aparecen en la web?](https://open.spotify.com/episode/3iUuIbinweIgqHWbNhfVJv), un podcast de URLs de la Tierra al Universo.

[1.300 millones de euros en multas: la batalla de la UE contra las big tech por la protección de datos](https://elordenmundial.com/1-300-millones-de-euros-en-multas-la-batalla-de-la-ue-contra-las-big-tech-por-la-proteccion-de-datos/) por Álvaro Merino en El Orden Mundial.

[Retos de la transición digital en Europa: El modelo europeo de inteligencia artificial.](https://www.youtube.com/watch?v=-hCmwapbiig) un debate entre Manuela Battaglini y Daniel Innerarity.

[La geopolítica de la inteligencia artificial](https://open.spotify.com/episode/33c5IjbNZbUayjXsG5NzTa) en el podcast Geopolitica Pop.

[Singapore’s tech-utopia dream is turning into a surveillance state nightmare](https://restofworld.org/2021/singapores-tech-utopia-dream-is-turning-into-a-surveillance-state-nightmare/) por Peter Guest en Rest of world.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
