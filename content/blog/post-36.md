---
title: "El nuevo CEO de Twitter, la ley audiovisual y la opinión del gobierno aleman sobre el reconocimiento facial."
date: 2021-12-07T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-36.jpg"
# meta description
description: "El nuevo CEO de Twitter, la ley audiovisual y la opinión del gobierno aleman sobre el reconocimiento facial."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Esta semana ha empezado movida en el mundo tech con la noticia de que [Jack Dorsey, el CEO de Twitter, deja su cargo](https://www.elmundo.es/economia/2021/11/29/61a4fb55fc6c8390118b459d.html) para centrarse en su otra empresa Square, un gigante de los medios de pago. [Con el cambio de CEO también ha llegado los primeros cambios en la cúpula de Twitter](https://www.infobae.com/america/tecno/2021/12/04/comenzaron-los-cambios-en-la-cupula-de-twitter-tras-la-llegada-del-nuevo-ceo/) y también las primeras dudas sobre si [el cambio de CEO traerá también un cambio en el modelo de negocio de la compañia.](https://stratechery.com/2021/twitter-has-a-new-ceo-what-about-a-new-business-model/)

Esther Paniagua ha entrevistado a [Ariadna Font, creadora del área de ética algorítmica de Twitter.](https://elpais.com/tecnologia/transformacion-digital/2021-11-22/ariadna-font-creadora-del-area-de-etica-algoritmica-de-twitter-las-criticas-no-nos-dan-miedo.html)

[Cuatro de cada diez adolescentes están en las redes sociales para no sentirse solos](https://compromiso.atresmedia.com/levanta-la-cabeza/actualidad/cuatro-cada-diez-adolescentes-estan-redes-sociales-sentirse-solos_202111166193d20277bc800001a77f32.html).

[Conductores de Tesla han estado hasta cinco horas sin poder arrancar su coche por una caida en los servidores de la empresa.](https://www.bbc.com/news/technology-59357306)

[GoDaddy, el gigante de los dominios de internet, publica una brecha de seguridad en la que se han visto expuestas 1.2 millones de cuentas.](https://www.engadget.com/godaddy-wordpress-security-issue-1-2-million-users-150142622.html)

[Niantic levanta $300M con una valoración de $9B para construir el "metarverso del mundo real".](https://techcrunch.com/2021/11/22/niantic-raises-300m-at-a-9b-valuation-to-build-the-real-world-metaverse)

[Facebook dice que el KGB bieloruso ha utilizado cuentas falsas para avivar la crisis fronteriza.](https://edition.cnn.com/2021/12/01/tech/facebook-belarus-poland/index.html)

[Amazon se enfrenta a protestas globales y huelgas en el Black Friday.](https://www.forbes.com/sites/edwardsegal/2021/11/25/amazon-prepares-for-next-crisis---a-global-strike-by-workers-on-black-friday/)

Jaime Gómez-Obregón y Mónica Redondo han analizado diversos marketplaces creados por ayuntamientos y administraciones y han llegado a la conclusión de que [el Amazon local no funciona.](https://hipertextual.com/2021/11/fiebre-marketplaces-comercio-local)

[Protección de Datos investiga a la Universidad Oberta de Catalunya por usar reconocimiento facial](https://www.vozpopuli.com/economia_y_finanzas/reconocimiento-facial-datos.html) ante la denuncia de un grupo de alumnos.

[El director de Instagram, Adam Mosseri, declarará por primera vez ante el Congreso.](https://www.cnbc.com/2021/11/24/instagram-chief-adam-mosseri-to-testify-before-congress.html)

[30 empresas demandan a Microsoft ante la UE por integrar Teams y OneDrive en Windows,](https://www.xataka.com/empresas-y-economia/eso-no-vale-30-empresas-demandan-a-microsoft-union-europea-integrar-teams-onedrive-windows-10-11) una medida que limita la capacidad de elección del usuario y crea barreras a otras empresas para competir.

[Los creadores de contenido se unen para crear una "SGAE de streamers"](https://www.eldiario.es/cultura/streamer-nuevo-trabajador-cultural-explotado-si-descansas-algoritmo-penaliza_1_8536500.html) que los defienda ante las condiciones de empresas como Youtube o Twitch.

[Se filtra un documento que muestra los datos que el FBI puede obtener de nosotros a través de WhatsApp, Telegram y otras plataformas de mensajería.](https://www.genbeta.com/actualidad/se-filtra-documento-que-muestra-todos-datos-que-fbi-obtiene-nosotros-a-traves-whatsapp-telegram-otros)

Una noticia que coincide del otro lado del Atlántico con la preocupación del [MI6 que debe adaptarse a las nuevas tecnologías como la computación cuántica o el reconocimiento facial que impide a sus espías pasar desapercibidos.](https://www.bbc.com/news/uk-59470026)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

El Gobierno de España prepara la nueva Ley Audiovisual que entre otras cosas proponía [obligar a que el 6% del contenido de las plataformas esté en catalán, gallego y euskera.](https://www.elespanol.com/invertia/empresas/tecnologia/20211130/gobierno-aprueba-ley-audiovisual-contenido-plataformas-catalan/631187497_0.html) Una propuesta que no ha sentado muy bien a las tecnológicas como Netflix, a las que el presidente [les prometió que les allanaría el camino para convertir a España en el "Hollywood de Europa"](https://www.elconfidencial.com/espana/2021-11-26/netflix-decepcionada-sanchez-cuota-catalan-pacto-erc-pge_3331248/).

Es por eso que [la tasa se retrasará hasta 2023 y probablemente no incluya la exigencia de producir el 6% de contenido en las otras lenguas cooficiales de España.](https://www.lainformacion.com/empresas/sanchez-perdona-netflix-tasa-rtve-negocia-erc/2854869/)

[La ley tampoco inquieta a plataformas como Filmin y Movistar+ ya que ellas ya cumplirían dicho requisito.](https://www.epe.es/es/politica/20211201/ley-audiovisual-inquieta-filmin-movistar-12925937)

Ya tenemos algunos datos extra sobre el ciberataque que recibió el SEPE hace 9 meses y que [provocó que sus técnicos trabajaran 19.000 horas extras en jornadas maratonianas y festivos.](https://www.businessinsider.es/vivio-ciberataque-sepe-dentro-19000-horas-extra-973861)

[El Gobierno Vasco elige a Microsoft para privatizar los datos de los colegios públicos y renuncia a la soberanía digital.](https://www.elsaltodiario.com/educacion/la-otra-cara-de-la-ley-de-educacion-el-gobierno-vasco-entrega-los-datos-de-los-colegios-publicos-a-microsoft)

[Bruselas quiere forzar a las 'big tech' a mostrar las tripas de los anuncios políticos](https://www.elconfidencial.com/tecnologia/2021-11-25/ue-bruselas-big-tech-anuncios-publicidad_3330702/) y forzarlas a que indiquen quién está pagando cada anuncio y el monto invertido.

[La nueva coalición de gobierno alemán apoya el prohibir el uso de reconocimiento facial en lugares públicos.](https://www.politico.eu/article/german-coalition-backs-ban-on-facial-recognition-in-public-places/)

[Margrethe Vestager dice que conseguir la independencia en el campo de los semiconductores no es factible,](https://www.cnbc.com/2021/11/29/eu-vestager-independent-semiconductor-production-isnt-doable.html) pero a pesar de ello buscarán duplicar la cuota de mercado europea para 2030 y situarla entorno al 20%.

[La UE planea mejorar las condiciones laborales de los riders y trabajadores de la llamada "economía colaborativa".](https://www.bloomberg.com/news/articles/2021-12-02/food-delivery-ride-apps-to-pay-billions-more-a-year-in-eu-plan)

China continua con su presión sobre las grandes empresas y [Tencent deberá obtener la aprobación de los reguladores antes de publicar nuevas apps y actualizaciones.](https://www.cnbc.com/2021/11/25/tencent-must-get-approval-from-regulators-before-publishing-new-apps.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

Una noticia que nos da bastante pena anunciar por lo cercano de la misma, pero [la crisis de suministros frustra el lanzamiento de IUVIA, una innovadora 'nube' gallega para escapar de Google.](https://www.publico.es/sociedad/crisis-suministros-frustra-lanzamiento-iuvia-innovadora-nube-gallega-escapar-google.html)

[Volla Phone](https://volla.online/en/index.html), un nuevo teléfono móvil basado en Android que buscar respetar la privacidad de los usuarios.

[Brave anuncia SugarCoat](https://www.genbeta.com/navegadores/sugarcoat-software-desarrollado-brave-que-altera-codigo-scripts-para-proteger-privacidad-romper-sitios-web) un software que altera el código de los scripts para proteger la privacidad de los usuarios sin 'romper' las webs.

[Mozilla anuncia el fin del soporte de Firefox Lockwise](https://www.muycomputer.com/2021/11/20/firefox-lockwise-adios/) aunque seguirá integrado dentro de Firefox.

FightForTheFuture lanza una campaña para [pedir a Spotify que detenga su plan de espiar las conversaciones de sus usuarios para manipularlos emocionalmente en su propio beneficio.](https://www.stopspotifysurveillance.org/)

Wikimedia España también lanza una campaña para [pedir al Congreso que no ratifique el Real Decreto-Ley 24/2021](https://www.wikimedia.es/2021/11/29/wikimedia-espana-pide-al-congreso-que-no-ratifique-el-real-decreto-ley-24-2021-para-que-la-transposicion-de-la-directiva-sobre-derechos-de-autoria-en-el-mercado-unico-digital-se-tramite-como-proyecto/) con el objetivo que la transposición de la Directiva sobre derechos de autoría en el Mercado Único Digital, se tramite como proyecto de Ley Ordinaria y se abra a debate público.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund tiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Nada que Esconder S02E02: Maldita desinformación con Naiara Bellio y Carlos H. Echevarría](https://blog.iuvia.io/es/nadaqueesconder-s02e02/), un nuevo podcast de IUVIA.

[China’s next generation of hackers won’t be criminals. That’s a problem.](https://techcrunch.com/2021/11/12/chinas-next-generation-of-hackers-wont-be-criminals-thats-a-problem/) por Dakota Cary en TechCrunch.

[Invisible Censorship. TikTok Told Moderators to Suppress Posts by “Ugly” People and the Poor to Attract New Users](https://theintercept.com/2020/03/16/tiktok-app-moderators-users-discrimination/) por Sam Biddle, Paulo Victor Ribeiro y Tatiana Dias en The Intercept.

[Nerea Luis: "Si los algoritmos tienen sesgos racistas es porque los arrastran de la sociedad"](https://www.epe.es/es/economia/20211122/nerea-luis-ciencias-computacion-maquinas-12887282) una entrevista a Nerea Luis en El Periódico de España.

[La mitad de los estudiantes de ESO no distingue las “fake news”](https://www.uc3m.es/ss/Satellite/UC3MInstitucional/es/Detalle/Comunicacion_C/1371320130990/1371215537949/La_mitad_de_los_estudiantes_de_ESO_no_distingue_las_%e2%80%9cfake_news%e2%80%9d), un estudio de la UC3M

[“La gente quiere recuperar la soberanía sobre su propia identidad digital”](https://www.agenciasinc.es/Entrevistas/La-gente-quiere-recuperar-la-soberania-sobre-su-propia-identidad-digital), una entrevista a Paloma Yaneza.

[Leyes de copyright y ley Iceta](https://www.youtube.com/watch?v=FdF47AaS3Tw), una charla con David Bravo.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
