---
title: "La Ley de Startups, Rusia nacionaliza la red social VK y la ley rider europea."
date: 2021-12-21T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-37.jpg"
# meta description
description: "La Ley de Startups, Rusia nacionaliza la red social VK y la ley rider europea."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Qualcomm está trabajando para [que los futuros móviles tengan siempre la cámara activa.](https://www.elespanol.com/elandroidelibre/noticias-y-novedades/20211202/snapdragon-gen-permitira-telefono-siempre-activa-camara/631687040_0.html)

[Instagram promovió en cuentas de adolescentes páginas que glorifican los trastornos alimenticios.](https://cnnespanol.cnn.com/2021/10/05/instagram-promovio-cuentas-adolescentes-paginas-trastornos-alimenticios-trax/)

[Glovo prueba en Madrid sus primeros robots de reparto.](https://www.elconfidencial.com/tecnologia/2021-12-14/glovo-robots-repartidores-madrid-robots-repartidores_3340466/)

La pasada semana una [vunerabilidad encontrada en la librería Log4J](https://thehackernews.com/2021/12/extremely-critical-log4j-vulnerability.html) ha dejado en riesgo a gran parte de internet. [En Newtral explican de forma muy sencilla lo que es Log4j y Log4Shell.](https://www.newtral.es/que-es-log4j-log4shell-vulnerabilidad-ataques/20211220/)

Una noticia que se une a otras como [el ataque al sistema de correo electrónico de IKEA](https://www.bleepingcomputer.com/news/security/ikea-email-systems-hit-by-ongoing-cyberattack/) o el hackeo de la cadena de supermercados [SPAR, que confirma un ataque con ransomware que le ha obligado a cerrar sus tiendas.](https://news.sky.com/story/supermarket-spar-forced-to-close-stores-due-to-cyber-attack-12488466)

[Una caida de servicio de Amazon Web Services afecta a plataformas como IMDb o Tinder.](https://www.theguardian.com/technology/2021/dec/07/amazon-web-services-outage-hits-sites-and-apps-such-as-imdb-and-tinder)

La compañía se encuentra en un momento complicado que comenzó con la noticia de que su programa de donaciones, [AmazonSmile, financia a grupos antivacunas en Estados Unidos](https://hipertextual.com/2019/03/amazonsmile-financia-grupos-antivacunas-estados-unidos) y que continúa tras la [muerte de al menos seis personas en uno de sus centros durante un tornado.](https://www.elsaltodiario.com/amazon/tormenta-criticas-amazon-tornado-illinois)

[Multa millonaria a Grindr por no permitir utilizar la aplicación si no se acepta la venta de los datos personales de sus usuarios.](https://www.eldiario.es/tecnologia/multa-millonaria-grindr-imponer-tomas-dejas-venta-datos-personales_1_8582538.html)

[Competencia en Reino Unido ordena a Facebook vender Giphy](https://www.europapress.es/economia/noticia-competencia-reino-unido-ordena-facebook-vender-giphy-20211130122022.html) al considerar que la compra de esta plataforma reduciría la competencia entre las redes sociales al eliminar a un gran competidor en el segmento de la publicidad gráfica.

[La red social apoyada por Trump anuncia haber levantado $1bn de inversores.](https://www.theguardian.com/us-news/2021/dec/04/trump-social-media-company-claims-to-raise-1bn-from-investors)

El metaverso sigue en "hype" y [Jefferies pronostica que será la mayor alteración de la vida jamás vista.](https://www.businessinsider.es/jefferies-preve-metaverso-sera-mayor-alteracion-vida-976837)

[El biólogo suizo Wilson Edwards no existe.](https://www.lavanguardia.com/tecnologia/20211204/7906714/biologo-suizo-wilson-edwards-no-existe-facebook-desinformacion-coronavirus-pmv.html) Dicha cuenta era conocida por difundir en Facebook información falsa sobre el origen del coronavirus, [una oscura industria la de la desinformación, que ha eclosionado en estos últimos años.](https://www.elperiodico.com/es/cuaderno/20211106/fake-news-industria-noticias-falsas-mentiras-12378051)

[Twitter ha suspendido cuentas de usuarios erróneamente después de que extremistas abusaran de su nueva política de imagenes privadas.](https://www.engadget.com/twitter-private-image-policy-abused-203117985.html)

[Israel digitaliza la ocupación palestina](https://elpais.com/internacional/2021-12-05/israel-digitaliza-la-ocupacion-palestina.html) con la proliferación de sistemas de reconocimiento facial y la intervención de teléfonos de activistas.

Un nuevo ejemplo de cómo se está utilizando la tecnología para aumentar la vigilancia de la población y [una nueva tendencia a la que también nos sumamos desde Europa.](https://www.theguardian.com/global-development/2021/dec/06/fortress-europe-the-millions-spent-on-military-grade-tech-to-deter-refugees) así como desde otras partes del mundo como [Corea del Sur, donde usarán el reconocimiento facial para controlar a los positivos por covid.](https://www.epe.es/es/internacional/20211214/corea-sur-reconocimiento-facial-positivos-covid-12977544)

La investigadora que Google despidió hace un año por exponer los riesgos de la Inteligencia Artificial, [Timnit Gebru, crea su propio centro de investigación sobre la IA](https://www.wired.com/story/ex-googler-timnit-gebru-starts-ai-research-center/) para plantear cuestiones sobre el uso responsable de dichas tecnologías.

[La UE propone un cargador común en todo el territorio europeo](https://elordenmundial.com/cargar-tu-iphone-con-un-cable-samsung-la-propuesta-europea-de-un-cargador-comun/) para acabar con 11.000 toneladas de desechos electrónicos y ahorrar a los consumidores 250 millones de euros al año.

La llegada del streaming y las nuevas inversiones del 5G están [reavivando el debate abierto por las telecos que piden que las grandes tecnológicas también contribuyan a pagar la infraestructura de internet.](https://www.elconfidencial.com/empresas/2021-12-10/5g-juego-calamar-disputa-trafico-big-tech-telecos_3338466/)

[Alemania aborta un plan de grupos antivacunas para matar al primer ministro de Sajonia](https://www.elperiodico.com/es/internacional/20211215/sholz-advierte-luchara-extremistas-minoritarios-12985184). Los implicados se coordinaban a través de Telegram por lo que el país [estudia regular a la aplicación ante la proliferación de llamadas a la violencia.](https://www.elperiodico.com/es/internacional/20211215/alemania-estudia-regular-telegram-proliferacion-12988434)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Luz verde a la ley de startups, una de las medidas con las que el Gobierno pretende "cambiar la estructura productiva del país" y que podrá entrar en vigor a mediados de 2022](https://www.businessinsider.es/ley-startups-aprobada-novedades-trae-cuando-estara-vigor-978531). Habrá que ver si finalmente se concreta de la forma que ha publicado inicialmente el gobierno, y que también indica que [dichas ayudas fiscales costarán unos 2.300 millones](https://www.lainformacion.com/espana/ayudas-fiscales-ley-starups-dejaran-agujero-2-300-millones/2855836/). Una cifra de la que gran parte de sector no cree realista ya que la mayoría de startups no tienen beneficios en sus primeros años de vida.

La Secretaria de Estado de Digitalización e Inteligencia Artificial [Carme Artigas, ha dado una interesante entrevista](https://www.eldiario.es/tecnologia/carme-artigas-startups-son-cooperativas-siglo-xxi_1_8582512.html) en donde cuenta su visión y lo que el gobierno está tratando de hacer con respecto a las startups, la tecnología y los derechos digitales.

También tenemos noticias en la transposición de la directiva europea del copyright que finalmente [se tramitará como proyecto de ley, abriéndose al debate público en parte gracias a la presión de grupos como Wikimedia o Xnet.](https://twitter.com/wikimedia_es/status/1466453513517641729) De hecho, la propia Xnet ha realizado un interesante [análisis de algunos aspectos de dicho decreto desde el punto de vista de los derechos digitales.](https://xnet-x.net/es/decretazo-copyright-transpuesto-articulo17-censura-algoritmica/)

[La Junta de Andalucía firma un acuerdo con Amazon para "trasladar los beneficios de la 'nube' de Amazon a la Administración andaluza y la ciudadanía"](https://www.diariosur.es/andalucia/junta-andalucia-amazon-colaboraran-digitalizacion-20211215170115-nt.html). En resumen, para empezar a desplegar el software de la administración pública en un proveedor fuera de su control.

[Bruselas prepara también su propia 'ley rider' con el objetivo de hacer aflorar a 4,1 M de falsos autónomos digitales.](https://www.elconfidencial.com/economia/2021-12-09/bruselas-espera-que-su-ley-rider-haga-aflorar-hasta-4-1-m-de-falsos-autonomos-digitales_3338140/) Una noticia no muy bien recibida por los mercados ya que [las grandes empresas de ‘delivery’ han perdido 9.000 millones en Bolsa ante dicho anuncio.](https://cincodias.elpais.com/cincodias/2021/12/06/companias/1638816473_978594.html)

Las nuevas directivas digitales también siguen adelante con el [nuevo borrador de la Digital Services Act (DSA), que descarta prohibir la publicidad segmentada](https://ipmark.com/nuevo-borrador-dsa-descarta-prohibir-publicidad-segmentada/)

Por otro lado, [la Digital Markets Act (DMA) también ha sido aprobada y ha recibido luz verde para comenzar las negociaciones institucionales.](https://www.xataka.com/empresas-y-economia/europa-no-quiere-que-google-amazon-facebook-apple-se-pasen-raya-aprueba-digital-markets-act-para-limitar-su-poder)
EDRI cree que es un paso en el buen camino, [hacia un mercado más justo e interoperable.](https://edri.org/our-work/eu-parliament-takes-first-step-towards-a-fair-and-interoperable-market/)

[La Comisión Europea se compromete a liberar todo el software que pueda beneficiar a la sociedad](https://www.muylinux.com/2021/12/09/comision-europea-liberar-software-codigo-abierto/).

[El Kremlin nacionaliza VK, el "Facebook ruso" creado por el fundador de Telegram, en una nueva ofensiva para controlar internet.](https://elpais.com/tecnologia/2021-12-03/el-kremlin-nacionaliza-el-facebook-ruso-en-una-nueva-ofensiva-para-controlar-internet.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

La Universitat de Barcelona junto con XNet lanza una nueva edición de su [Postgrado en Tecnopolítica y Derechos en la Era Digital](https://xnet-x.net/posgrado-tecnopolitica-simona-levi/).

[El gobierno de Suecia se pasa a Nextcloud](https://nextcloud.com/blog/swedish-government-nextcloud-premier-digital-collaboration-platform/) como plataforma principal de colaboración digital.

[Kickstarter moverá su plataforma de crowdfunding a blockchain.](https://www.bloomberg.com/news/articles/2021-12-08/kickstarter-blockchain-will-combine-crowdfunding-with-crypto?sref=w3vFo7gi)

[Autonomous weapons](https://autonomousweapons.org/) es una iniciativa que busca prohibir el uso de armas autónomas y que esta semana ha saltado nuevamente a la conversación ya que la [Convención sobre Ciertas Armas Convencionales no ha conseguido obtener un acuerdo internacional para prohibirlas.](https://blogs.publico.es/kaostica/2021/12/17/robots-asesinos/)
Para entender un poco más en profundidad hasta donde podrían llegar estas tecnologías, [Future of Life Institute ha publicado este video.](https://www.youtube.com/watch?v=9rDo1QxI260&feature=emb_title)


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Así se ha convertido Estonia en la nueva Finlandia ](https://www.abc.es/xlsemanal/a-fondo/educacion-estonia-mejor-sistema-educativo-informe-pisa.html) por Rachel Sylvester en ABC.

[Activostech: Algoritmos y democracia](https://www.youtube.com/watch?v=UWeFhq4KJoE) una mesa redonda sobre la IA y los derechos humanos.

[Multas millonarias a empresas tecnológicas por incumplir la protección de datos: ¿son realmente eficaces?](https://maldita.es/malditatecnologia/20211201/multas-millonarias-tecnologicas-proteccion-datos/) en Maldita Tecnología.

["En nuestras vidas conectadas no tenemos democracia"](https://theobjective.com/cultura/2021-12-15/esther-paniagua-error-404), una entrevista a Esther Paniagua.

[For truly ethical AI, its research must be independent from big tech](https://www.theguardian.com/commentisfree/2021/dec/06/google-silicon-valley-ai-timnit-gebru) por Timnit Gebru en The Guardian.

Y como se acercan las ansiadas cenas navideñas, os dejamos un par de videos sencillos con los que explicar a los "muggles" que no están en el mundillo, cómo funciona el capitalismo de la vigilancia:

[DESCUBRE quién empezó el negocio de los DATOS](https://www.youtube.com/watch?v=ONGmPTP5Li0)

[CÓMO MANIPULAR A UNA SOCIEDAD | Data Brokers y Microtargeting](https://www.youtube.com/watch?v=7PN-YmJraMc)

Nosotros nos tomaremos un cortísimo descanso por año nuevo pero volveremos en menos de un mes. **¡Felices fiestas a todos y todas!** 😇

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
