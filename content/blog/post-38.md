---
title: "Los NFTs y la web3, Kurz ficha por Palantir y el Private Relay de Apple."
date: 2022-01-18T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-38.jpg"
# meta description
description: "Los NFTs y la web3, Kurz ficha por Palantir y el Private Relay de Apple."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[El excanciller de Austria Sebastian Kurz ficha por Palantir](https://www.elperiodico.com/es/internacional/20211230/excanciller-austria-ficha-firma-espionaje-13044338), la firma de espionaje cuyo propietario es Peter Thiel, amigo de Trump y antiguo fundador de PayPal.

[Varapalo judicial a CEDRO y la prensa escrita, que de momento no podrá cobrar a Google por usar sus noticias en su servicio Discover.](https://www.publico.es/espana/varapalo-judicial-prensa-escrita-momento-no-podra-cobrar-euro-google-servicio-discover.html)

Las nuevas políticas de uso avisan de que Google comenzará a [revisar todo el contienido subido a Google Drive y a bloquear aquellos archivos que considere ilegales.](https://hipertextual.com/2022/01/google-revisara-todo-el-contenido-que-subas-a-drive-y-lo-restringira-si-lo-considera-inadecuado)

[El grupo NOYB asegura que Facebook sigue enviando datos de usuarios europeos a EE UU según documentos internos](https://elpais.com/tecnologia/2021-12-20/facebook-sigue-mandando-datos-de-usuarios-europeos-a-ee-uu-segun-documentos-internos.html), ignorando por completo las sentencias del Tribunal de Justicia de la Unión Europea.

[Un grupo de hackers se hace con el control de uno de los centros de datos de HP para minar criptomonedas](https://www.genbeta.com/actualidad/grupo-hackers-se-hace-100-000-dolares-al-utilizar-servidores-hp-para-minar-criptomonedas). Utilizaron la vulnerabilidad Java Log4j para obtener más de 100.000 dólares en criptomonedas.

[“Tener un sensor en la cabeza será de rigor en 10 años, igual que ahora todo el mundo tiene un teléfono inteligente”](https://elpais.com/ciencia/2022-01-05/tener-un-sensor-en-la-cabeza-sera-de-rigor-en-10-anos-igual-que-ahora-todo-el-mundo-tiene-un-telefono-inteligente.html), el inquietante mundo que nos espera.

Más de 80 organizaciones de [fact-checking de todo el mundo acusan a Youtube de propagar la desinformación](https://www.elperiodico.com/es/sociedad/20220112/verificadores-mundo-acusan-youtube-propagar-13084874) y exigen a la plataforma que actúe.

AlgorithmWatch he hecho un reportaje sobre la [app que Polonia está utilizando para controlar las cuarentenas COVID con geolocalización y selfies.](https://algorithmwatch.org/en/stay-at-home-selfie-app-poland/)

[Southern Co-op ha instalado cámaras de reconocimiento facial en más de 35 supermercados de UK.](https://bigbrotherwatch.org.uk/2021/12/co-op-doubles-orwellian-facial-recognition-cameras-in-supermarkets/)

[Tu impresora deja un rastro de puntos microscópicos en el papel que pueden llegar a delatarte.](https://www.xataka.com/seguridad/tu-impresora-deja-rastro-puntos-microscopicos-papel-que-permiten-identificarte)

[Las contraseñas maestras de múltiples usuarios de LastPass han sido comprometidas.](https://www.bleepingcomputer.com/news/security/lastpass-users-warned-their-master-passwords-are-compromised/)

Newtral habla sobre [el sindicato de creadores de contenido y qué buscan conseguir de las plataformas digitales](https://www.newtral.es/sindicato-red-creadores-contenido-youtubers-instagramers-tiktokers/20211220/).

[Dimite el fundador de Signal y uno de los creadores de WhatsApp se convierte en su nuevo CEO.](https://www.eldiario.es/tecnologia/creadores-whatsapp-ceo-interino-signal-dimision-fundador_1_8644209.html)

[Una startup quiere gamificar la justicia en EEUU con la idea de que los usuarios apuesten criptomonedas sobre el resultado de los juicios.](https://www.vice.com/en/article/v7d7x3/tech-startup-wants-to-gamify-the-us-court-system-using-crypto-tokens)

La llegada de las criptomonedas y los NFTs están generando un [nuevo "boom" inmobiliario virtual de cientos de millones para comprar propiedades en el metaverso.](https://www.elperiodico.com/es/activos/entender-mas/20220102/boom-inmobiliario-metaverso-12981350)

[La creación de una economía digital basada en criptomonedas será la clave del futuro del metaverso y no Facebook.](https://www.elconfidencial.com/tecnologia/2021-12-26/metaverso-facebook-criptomonedas-realidad-virtual_3348873/) Una economía que el mismo fundador de Twitter, [Jack Dorsey, ha criticado por como los fondos de capital riesgo la están tratando de controlar.](https://www.bloomberg.com/news/articles/2021-12-21/jack-dorsey-stirs-uproar-by-dismissing-web3-as-a-vc-plaything)

[Newtral nos deja un glosario sobre la web3 con los términos y los conceptos necesarios para entender la internet que viene.](https://www.newtral.es/glosario-web3-terminos-y-conceptos-para-entender-la-internet-que-viene/20211229/)

[EEUU contraataca y adelanta a China con nueva tecnología cuántica basada en cutrits en lugar de en cubits.](https://www.elconfidencial.com/tecnologia/novaceno/2021-12-22/eeuu-china-computacion-cuantica-cutrits-cubits_3348034/)

[Las operadoras piden prohibir la función de Private Relay de Apple, que impide el rastreo de nuestra navegación en Internet](https://www.20minutos.es/tecnologia/ciberseguridad/las-operadoras-piden-prohibir-la-funcion-de-privacidad-de-apple-que-impide-el-rastreo-de-nuestra-navegacion-en-internet-4938919/).
Enrique Dans profundiza en [las motivaciones de las telecos para atacar a Private Relay](https://www.enriquedans.com/2022/01/el-private-relay-de-apple-y-las-operadoras.html)

[El Supervisor Europeo de Protección de Datos dice que el uso de Google Analytics vulnera las leyes europeas.](https://www.vozpopuli.com/economia_y_finanzas/google-analytics-ue.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Just Eat suscribe el primer convenio de empresa del delivery en España y pagará a sus ‘riders’ un salario base de 15.200 euros al año, con 30 días de vacaciones y una jornada máxima de 9 horas.](https://www.businessinsider.es/just-eat-llega-acuerdo-riders-ganaran-15200-euros-ano-982317)

[Carme Artigas, la secretaria de Estado de Digitalización e Inteligencia Artificial, ha sido entrevistada sobre la ley de startups y el futuro de la tecnología.](https://www.eldiario.es/tecnologia/carme-artigas-startups-son-cooperativas-siglo-xxi_1_8582512.html)

La ley del 5G también vuelve a la palestra, por un lado [Competencia avisa al gobierno del riesgo de expulsar a Huawei con la ley del 5G](https://www.lainformacion.com/empresas/competencia-cnmc-avisa-gobierno-sanchez-riesgo-expulsar-huawei-ley-5g/2855832/) y por otro, [Calviño apura plazos con las telecos ya prácticamente decididas a sacar a Huawei.](https://www.lainformacion.com/empresas/calvino-gobierno-apura-plazos-ley-5g-telecos-decididas-sacar-huawei/2857632/)

[España creará una agencia para vigilar los peligros de la Inteligencia Artificial](https://www.elperiodico.com/es/ciencia/20211229/espana-creara-agencia-vigilar-peligros-13041283), mientras que otros países como [Reino Unido están trabajando para poner en marcha un Hub para el desarrollo de estándares para la IA.](https://mobile.twitter.com/jones_lucia/status/1481259754295111682)

[El Ministerio de Economía acelerará la regulación de criptoactivos en la nueva ley del mercado.](https://cincodias.elpais.com/cincodias/2021/12/28/mercados/1640708976_134122.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[iCloud Private Relay](https://support.apple.com/en-us/HT212614), una tecnología que funciona de forma similar a una VPN y que permite que ni las operadoras de telefonía ni Apple puedan conocer tu IP ni por donde navegas en internet.

Mozilla ha anunciado que pretendía entrar en el mundo de las criptomonedas pero finalmente la compañía [ha decidido echarse atrás y dejar de recibir donaciones con estas monedas tras recibir múltiples críticas.](https://www.theverge.com/2022/1/6/22870787/mozilla-pauses-crypto-donations-backlash-jwz)

DuckDuckGo ha lanzado en beta su [App Tracking Protection para Android](https://spreadprivacy.com/introducing-app-tracking-protection/) con la idea de proteger a sus usuarios de los trackers existentes en las apps móviles.

[White Rabbit](https://www.whiterabbit.one/) es una extensión que te ayuda a buscar películas en sitios de streaming no oficiales y te permite ver dicho contenido [pagando 2€ directamente a sus creadores](https://www.genbeta.com/multimedia/esta-extension-que-te-anima-a-ver-peliculas-sitios-streaming-no-oficiales-pagando-2-eur-a-creadores-esta-financiada-ue).

El próximo 5 y 6 de Febrero tendrá lugar nuevamente online el [FOSDEM 22](https://fosdem.org/2022/), el evento de tecnologías libres y abiertas mas importante de Europa.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Your attention didn’t collapse. It was stolen](https://www.theguardian.com/science/2022/jan/02/attention-span-focus-screens-apps-smartphones-social-media) por Johann Hari en The Guardian.

[France’s digital agenda](https://www.euractiv.com/section/digital/podcast/frances-digital-agenda/), un pequeño resumen de la agenda tecnológica ante la próxima presidencia francesa de la UE.

[Francesca Bria on Decentralisation, Sovereignty, and Web3](https://the-crypto-syllabus.com/francesca-bria-on-decentralisation/), una entrevista a Francesca Bria en The Crypto Syllabus.

[Will financialization of the music industry kill the Public Domain?](https://openfuture.eu/blog/will-financialization-of-the-music-industry-kill-the-public-domain/), por Paul Keller en Open_Future.

[Timnit Gebru: “No solo es Facebook, las redes sociales propagan el odio con impunidad”](https://elpais.com/tecnologia/2021-12-12/timnit-gebru-no-solo-es-facebook-las-redes-sociales-propagan-el-odio-con-impunidad.html) via El País.

["Hay algoritmos que recomiendan hombres para puestos directivos solo porque históricamente ellos los han ocupado"](https://www.elmundo.es/yodona/lifestyle/2021/12/30/61c9f31fe4d4d84f508b458d.html) una entrevista a Lucía Velasco en El Mundo.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
