---
title: "Microsoft compra Activision, la DSA avanza y la dificil compra de ARM por Nvidia."
date: 2022-02-01T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-39.jpg"
# meta description
description: "Microsoft compra Activision, la DSA avanza y la dificil compra de ARM por Nvidia."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Finalmente parece que el acuerdo entre [Nvidia y ARM no fructifica y Nvidia se prepara para abandonar su propuesta.](https://www.bloomberg.com/news/articles/2022-01-25/nvidia-is-said-to-quietly-prepare-to-abandon-takeover-of-arm) Los rumores indican que ARM intentará una salida a bolsa como alternativa.

[El gigante chino Tencent podrá escanear las caras de los niños para controlar sus horas de juego en videojuegos.](https://www.20minutos.es/tecnologia/fabricantes/tencent-utilizara-su-sistema-de-reconocimiento-facial-para-que-los-menores-no-jueguen-mas-de-14-horas-diarias-en-las-proximas-dos-semanas-cumpliendo-la-restriccion-china-de-uso-de-videojuegos-de-una-hora-al-dia-maximo-4943284/)

[Un ataque de denegación de servicio a Andora Telecom deja a los streamers del país sin internet a la vez.](https://es.gizmodo.com/los-streamers-de-andorra-se-quedan-sin-internet-a-la-ve-1848403292)

[Microsoft anuncia que ha conseguido parar el mayor ataque DDoS de la historia de internet a finales de 2021.](https://computerhoy.com/noticias/microsoft-consigue-parar-mayor-ataque-ddos-historia-internet-1002955)

[Se publica una nueva puerta trasera en Windows, macOS y Linux que no había sido detectada hasta el momento.](https://arstechnica.com/information-technology/2022/01/backdoor-for-windows-macos-and-linux-went-undetected-until-now/)

[Microsoft compra Activision en la mayor compra de la historia de los videojuegos](https://www.lainformacion.com/empresas/microsoft-abre-frente-con-el-regulador-tras-la-megacompra-de-activision/2857931/), una compra que le llevará a verse las caras con el regulador de competencia.

[Just Eat acusa de manera directa a Glovo de eludir la ley](https://www.merca2.es/2022/01/25/glovo-otro-competior-elude-ley-just-eat/), una queja que se une a las más de [300 causas abiertas que tiene Glovo en el mundo.](https://www.lavanguardia.com/economia/20220121/8001789/glovo-acumula-mas-300-causas-abiertas-mundo.html)

Just Eat también tiene lo suyo, en este caso la llegada de los convenios para los empleados de estas empresas ha sido una buena noticia, pero [la gran mayoría de ellos trabajan en subcontratas por lo que su situación sigue siendo complicada.](https://www.wired.com/story/gig-economy-outsourcing-uk/)

[Los cascos de realidad virtual aprenden más sobre nosotros que las pantallas tradicionales, los problemas de privacidad en el metaverso.](https://www.washingtonpost.com/technology/2022/01/13/privacy-vr-metaverse/)

[Google abandona FLoC e introduce Topics API para reemplazar a las cookies.](https://www.theverge.com/2022/1/25/22900567/google-floc-abandon-topics-api-cookies-tracking)

[Twitter incluye a España en su experimento para detectar tuits engañosos.](https://elpais.com/tecnologia/2022-01-18/twitter-incluye-a-espana-en-su-experimento-para-detectar-tuits-enganosos.html)

[Un algoritmo impreciso condiciona la libertad de los presos](https://www.lavanguardia.com/vida/20211206/7888727/algoritmo-sirve-denegar-permisos-presos-pese-fallos.html), una mirada profunda dentro de Riscanvi, el algoritmo que ayuda a decidir sobre a libertad de las personas en Cataluña.

[Grindr divide a las agencias de protección de datos](https://www.elconfidencial.com/juridico/2022-01-23/apps-citas-privacidad-grindr-agencias-proteccion-datos_3362667/), mientras Noruega multó a la web de citas, España ha archivado la reclamación al considerar que su uso no revela ninguna orientación sexual.

[Europol recibe la orden de borrar petabytes de datos que no estén claramente relacionados con investigaciones criminales.](https://www.theverge.com/2022/1/10/22877041/europol-delete-petabytes-crime-data-eu-privacy-law)

La posible guerra entre Rusia y Ucrania también se está librando en el mundo digital donde [Ucrania denuncia un ciberataque masivo contra sitios web gubernamentales](https://www.elconfidencial.com/mundo/europa/2022-01-14/ucrania-denuncia-un-ciberataque-masivo-contra-sitios-web-gubernamentales_3358172/) al mismo tiempo que un grupo de [activistas dicen haber hackeado el sistema de trenes de Bielorusia para detener los movimientos militares rusos.](https://arstechnica.com/information-technology/2022/01/hactivists-say-they-hacked-belarus-rail-system-to-stop-russian-military-buildup/)

[El regulador holandés dice que el plan de Apple para los pagos de terceros en las apps es insuficiente y multa a Apple con 5 millones de euros.](https://www.macrumors.com/2022/01/24/dutch-third-party-iap-plan-insufficient/)

[China obliga a los atletas de los JJOO de invierno a instalar una app que graba sus audios y los envía a servidores chinos.](https://www.xataka.com/privacidad/china-obliga-a-atletas-jjoo-invierno-a-instalar-app-que-graba-sus-audios-envia-a-servidores-chinos)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Los influencers no serán controlados en el fomento de criptos si lo hacen gratis](https://www.lainformacion.com/empresas/influencers-no-controlados-criptos-gratis/2857830/) en España.

Parece que la DSA sigue adelante y abre las negociaciones con los Estados miembros, [una ley que busca regular las plataformas digitales y hacer de la red un lugar seguro.](https://www.europarl.europa.eu/news/es/press-room/20220114IPR21017/una-ley-para-regular-las-plataformas-digitales-y-hacer-de-la-red-un-lugar-seguro) Una legislación que entre otras cosas [propone que los usuarios puedan bloquear la publicidad 'online' personalizada si así lo desean.](https://www.eleconomista.es/economia/noticias/11580276/01/22/Bruselas-permitira-el-bloqueo-de-la-publicidad-online-personalizada.html)

[Xnet mantiene actualizado este documento con su opinión sobre la DSA](https://xnet-x.net/es/posicion-xnet-dsa-package/), muy intereseante para seguir las actualizaciones.

[La Unión Europea prepara un DNS público propio con el que bloquear el tráfico "ilegal y malicioso"](https://www.xataka.com/privacidad/union-europea-prepara-dns-publico-propio-que-bloquear-trafico-ilegal-malicioso).

[La Comisión Europea lanza a través de su Open Source Programme Office una serie de recompensas para quién encuentre bugs (bug bounties) en varios proyectos de software libre.](https://ec.europa.eu/info/news/european-commissions-open-source-programme-office-starts-bug-bounties-2022-jan-19_en)

[El Banco Central de Rusia se alinea con China y propone prohibir operaciones con criptomonedas.](https://www.elconfidencial.com/tecnologia/2022-01-20/el-banco-central-de-rusia-propone-prohibir-operaciones-con-criptomonedas_3361745)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Unpaywall](https://unpaywall.org/) es una herramienta que agrega contenido de acceso abierto con millones de papers y artículos científicos.

[Helm](https://thehelm.com/) es una nube que permite al usuario almacenar en su casa sus datos a través de su correo electrónico, calendario, contactos o archivos entre otros muchos servicios.

Protonmail ha hecho un interesante resumen de [las 10 mayores victorias en privacidad del 2021.](https://protonmail.com/blog/privacy-wins-2021/)

[Un estudio sugiere que prohibir el uso de móviles en los colegios reduce el acoso escolar y mejora los resultados en matemáticas y ciencias](https://nadaesgratis.es/admin/prohibir-el-movil-en-los-colegios). Si de desea ver el estudio en profundidad [se puede encontrar en este enlace.](https://www.emerald.com/insight/content/doi/10.1108/AEA-05-2021-0112/full/html)

Fight for the Future lanza una campaña para pedir al [Servicio de Impuestos Internos de Estados Unidos que no obligue a sus usuarios a utilizar reconocimiento facial para acceder a sus impuestos.](https://www.dumpid.me/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Un estudio de Maldita Tecnología muestra que aunque sabemos que nuestros datos personales se usan sin control, no hacemos nada para evitarlo](https://maldita.es/malditatecnologia/20220128/datos-estudio-maldita-tecnologia/), en Maldita Tecnología.

[‘We are going to create the best environment for startups in Europe’](https://techcrunch.com/2022/01/26/spain-startup-law-interview-francisco-polo/), una entrevista con Francisco Polo (Alto Comisionado para la España Nación Emprendedora) en TechCrunch.

[La internet descentralizada: ilusión o quid de la web3](https://www.newtral.es/internet-descentralizada-web3/20220119/) por Marilín Gonzalo en Newtral.

[Big tech’s new frontiers](https://www.economist.com/podcasts/2022/01/19/big-techs-new-frontiers), un podcast con Kevin Scott (CTO de Microsoft) y Margrethe Vestager (Jefa de Competencia en la UE).

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
