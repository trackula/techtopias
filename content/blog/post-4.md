---
title: "Reconocimiento facial en Mercadona y el boicot a Facebook"
date: 2020-07-07T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-4.jpg"

# meta description
description: "Reconocimiento facial en Mercadona y el boicot a Facebook"

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Terminábamos la pasada semana con la noticia de que [Mercadona utilizará reconocimiento facial en 40 de sus supermercados para buscar e impedir la entrada en sus establecimientos a personas con órdenes de alejamiento.](https://www.levante-emv.com/economia/2020/07/01/mercadona-instala-reconocimiento-facial-detectar/2026462.html)

No han tardado las redes en llenarse de debates de [periodístas](https://twitter.com/javisalas/status/1278626273183649792) y [profesionales legales](https://twitter.com/josecanedo_/status/1278743277475414017) mientras también se desvelaba la [tecnología de vigilancia que utilizará Mercadona, proveniente de Israel.](https://www.elconfidencial.com/empresas/2020-07-02/mercadona-reconocimiento-facial-anyvision_2664608/)

Maldita también ha hecho un interesante resumen sobre [lo que sabemos acerca del reconocimiento facial que implementará la cadena de supermercados.](https://maldita.es/malditatecnologia/2020/07/03/reconocimiento-facial-mercadona-que-sabemos-que-no-cuentan/)

El reconocimiento facial tiene cada vez más personas que dudan de su utilidad y continuan surgiendo polémicas por su mal funcionamiento, llegando incluso a acusar a personas equivocadas de crímenes que no han cometido: ["Wrongfully Accused by an Algorithm"](https://www.nytimes.com/2020/06/24/technology/facial-recognition-arrest.html). Los sesgos son un [grave problema de estas tecnologías que favorece la discriminación de personas con piel oscura.](https://maldita.es/malditatecnologia/2020/06/15/reconocimiento-facial-problemas-discriminacion-personas-piel-oscura-negra/)

Múltiples empresas como [Verizon](https://www.cnbc.com/2020/06/25/verizon-pulling-advertising-from-facebook-and-instagram.html) o grandes gigantes como [Unilever](https://edition.cnn.com/2020/06/26/tech/facebook-twitter-stock-unilever/index.html) han detenido sus campañas publicitarias en Facebook y Twitter en protesta por la ausencia de medidas para luchar contra el discurso del odio en dichas plataformas.

Ante la repercusión que ha tenido, [Facebook se ha visto obligada a rectificar e implementar medidas para luchar contra estas publicaciones, motivada por la gran pérdida de anunciantes](https://elpais.com/tecnologia/2020-06-26/facebook-asegura-que-controlara-el-discurso-del-odio-tras-la-decision-de-unilever-de-retirar-la-publicidad.html) que le ha supuesto hasta un 7% de caida en sus acciones.

[Google eliminará por defecto los datos que guarda sobre sus usuarios a los 18 meses y ofrecerá una 'Verificación de privacidad' más proactiva](https://www.genbeta.com/seguridad/google-implementara-borrado-automatico-datos-personales-defecto-verificacion-privacidad-proactiva).

[Nuevos emails han sido publicados en los que se muestra cómo Microsoft ha tratado de vender su tecnología de reconocimiento facial a la DEA.](https://techcrunch.com/2020/06/17/microsoft-dea-facial-recognition)

[El FBI ha reconocido haber utilizado Instagram, Etsy y LinkedIn para identificar a un manifestante acusado de quemar un coche de policía.](https://www.lavanguardia.com/tecnologia/20200619/481838391100/fbi-basa-instagram-etsy-linkedin-identificar-autor-incendio.html)

Las leyes anti-monopolio cogen peso en los últimos meses, [Facebook ha perdido un nuevo caso, esta vez ante los tribunales alemanes, por haber violado leyes de competencia al abusar de su hegemonía en el mercado de las redes sociales en dicho país.](https://www.nytimes.com/2020/06/23/technology/facebook-antitrust-germany.html)

[Los reguladores de competencia en UK continuan revisando la relación entre Google y Apple por la que la empresa del famoso buscador paga "£1.2 billion" anualmente a Apple por mantener a su buscador configurado por defecto en sus dispositivos.](https://www.theverge.com/2020/7/1/21310591/apple-google-search-engine-safari-iphone-deal-billions-regulation-antitrust)

[Palantir, la controvertida empresa proveedora de servicios de vigilancia, analítica y big data, ha levantado mas de $500 millones](https://techcrunch.com/2020/07/02/sec-filing-indicates-big-data-provider-palantir-is-raising-961m-550m-of-it-already-secured/) ante una posible salida a bolsa.

[Múltiples empresas como TikTok, AliExpress o Viber leen el portapapeles de tu móvil cada 2 o 3 segundos.](https://twitter.com/Jorge_Morell/status/1277191528620883973)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Poco a poco salen nuevas noticias sobre el proyecto Gaia-X, [Alemania y Francia ultiman una plataforma europea de proveedores 'cloud' para eludir a Amazon, Microsoft y Google.](https://cincodias.elpais.com/cincodias/2020/06/04/companias/1591273229_337258.html)

[La app española para detección de contagios de coronavirus desarrolla por Indra ya ha sido publicada](https://www.xataka.com/aplicaciones/probamos-radar-covid-asi-funciona-aplicacion-rastreo-contactos-que-usaremos-espana) y la prueba piloto ya está en funcionamiento en la isla de La Gomera, [donde se simularán contagios para testear su capacidad de respuesta y su utilidad real.](https://www.eldiario.es/tecnologia/espana-simulara-contagios-gomera-contactos_1_6059585.html)

[La tecnología de reconocimiento facial desarrollada por Clearview AI podría ser ilegal en Europa.](https://www.cnbc.com/2020/06/11/clearview-ai-facial-recognition-europe.html) Recordemos que la empresa ha estado envuelta en diversas [polémicas por vender millones de fotos de redes sociales a empresas y agencias de seguridad.](https://www.elmundo.es/tecnologia/2020/01/21/5e26c3e4fdddffda088b45f6.html)

[El gobierno de UK ha adquirido el 20% de una empresa de satélites para crear su propia alternativa al sistema de geoposicionamiento Galileo que impulsa la Unión Europea.](https://www.theguardian.com/science/2020/jun/26/satellite-experts-oneweb-investment-uk-galileo-brexit)

[La Comisión Europea está trabajando por conseguir la interoperabilidad entre las aplicaciones de rastreo de los diversos paises.](https://euractiv.es/section/digital/news/la-ce-busca-interoperabilidad-entre-las-aplicaciones-de-rastreo/)

El Ministerio de Asuntos Económicos y Transformación Digital de España [ha lanzado una consulta pública para que cualquier ciudadano pueda contribuir a la elaboración de una carta de derechos digitales.](https://www.mineco.gob.es/portal/site/mineco/menuitem.32ac44f94b634f76faf2b910026041a0/?vgnextoid=8d86787c93413710VgnVCM1000001d04140aRCRD)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

La Electronic Frontier Foundation [lanza una nueva campaña para tratar de detener la EARN IT Act que dará al gobierno de los Estados Unidos la posibilidad de romper el cifrado y la libertad de expresión online.](https://act.eff.org/action/stop-the-earn-it-bill-before-it-breaks-encryption)

Hoy nos queremos hacer eco de un proyecto español, [MaadiX, que busca facilitar que despliegues todas las aplicaciones que necesitas para tu día a día en tu propio servidor.](https://maadix.net/)

[ProtonMail ha publicado un pequeño análisis de la situación de censura en Hong Kong y en concreto sobre el artículo 43 de la Ley de Seguridad Nacional](https://protonmail.com/blog/china-censorship-hong-kong/) que China ha aprobado en las pasadas semanas y que prácticamente elimina la autonomía de dicho territorio.

El proyecto [Tor lanza una petición de apoyo al Open Technology Fund](https://blog.torproject.org/save-open-technology-fund), un fondo gracias al cual el proyecto ha sido posible durante todos estos años.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Si tienes un proyecto tecnológico para mejorar la privacidad y la confianza, NGI Zero Privacy & Trust Fund](https://nlnet.nl/PET/) ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto.

Si trabajas en temas relacionados con portabilidad de datos y servicios, [NGI ha publicado la web de DAPSI, su incubadora para proyectos de este ámbito](https://dapsi.ngi.eu/) y que financia hasta 150.000 € a fondo perdido. La convocatoria aún no está abierta, pero estaremos atentos para cuando esté disponible :).

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/> y entrevistas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

Enrique Dans habla de la nueva red social de moda, [TikTok y de su peligrosidad](https://www.enriquedans.com/2020/06/tiktok-es-muy-popular-si-pero-muy-peligrosa.html).

Continua el ciclo [Privacidad y nueva normalidad](https://www.youtube.com/watch?reload=9&v=KS-ZHr9oJRA&feature=youtu.be) de la Facultad de Derecho de la Universidad de Chile.

Parece que el reconocimiento facial se está volviendo mainstream, incluso John Oliver habla de ello y de por qué puede ser peligroso: [Facial Recognition: Last Week Tonight with John Oliver (HBO)](https://www.youtube.com/watch?reload=9&time_continue=11&v=jZjmlJPJgug&feature=emb_title)

El Orden Mundial publica varios artículos muy interesantes sobre el problema geopolítico de Europa ante su [dependencia tecnológica de EEUU y China](https://elordenmundial.com/dependencia-tecnologica-union-europea/) y también nos hablan de cómo el [big data y nuestros datos le han dado más poder a la economía y a la política.](https://elordenmundial.com/big-data-poder-datos-economia-politica/).

Manuela Battaglini ha publicado un artículo en el que habla de cómo [Fitbit comercializa nuestros datos y hace peligrosas predicciones.](https://www.transparentinternet.com/es/tecnologia-y-sociedad/fitbit/)

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
