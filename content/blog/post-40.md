---
title: "El podcast negacionista de Spotify, la alternativa de Xnet a Google y el amago de Meta con irse de la UE."
date: 2022-02-15T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-40.jpg"
# meta description
description: "El podcast negacionista de Spotify, la alternativa de Xnet a Google y el amago de Meta con irse de la UE."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Este mes ha comenzado movido por la noticia de que Spotify ha permitido y promocionado un podcast que difunde teorías conspirativas sobre el COVID-19. [En señal de protesta varios músicos encabezados por Neil Young han abandonado la plataforma.](https://www.infobae.com/america/eeuu/2022/02/02/sangria-de-musicos-en-spotify-crosby-stills-y-nash-tambien-abandonan-la-plataforma-por-difundir-desinformacion/)

[El CEO de la tecnológica defiende el trato para promocionar a Joe Rogan, el autor de dicho podcast](https://www.theverge.com/2022/2/3/22915456/spotify-ceo-joe-rogan-daniel-ek-town-hall-speech-platform-podcast), indicando que la plataforma no tiene responsabilidad editorial y que únicamente son "una plataforma". [Una visión que también ha defendido frente a sus empleados.](https://www.latimes.com/entertainment-arts/story/2022-02-02/la-ent-spotify-rogan-platform)

Finalmente parece que [Spotify no eliminará ningún contenido y únicamente se añadirán avisos antes de reproducir dichos contenidos negacionistas.](https://computerhoy.com/noticias/entretenimiento/spotify-medidas-desinformacion-1004005)

[Un ciberataque contra empreas alemanas, belgas y holandesas deja afectadas a decenas de terminales de almacenamiento y transporte de petróleo por todo el mundo.](https://www.bbc.com/news/technology-60250956)

[Google anuncia 'Topics API' como alternativa a las cookies, reconociendo que FLoC ha sido problemático.](https://www.zdnet.com/article/google-reveals-topics-cookie-replacement-acknowledges-floc-was-problematic/)

[Sngular y Eticas se alían para ayudar a mejorar algoritmos desplegados o en fase de planificación y hacerlos más "responsables" socialmente.](https://www.businessinsider.es/sngular-eticas-alian-lograr-algoritmos-responsables-1005673)

[Varias cadenas de supermercados en Gran Bretaña están probando reconocimiento facial para adivinar la edad de los compradores de alcohol.](https://www.bbc.com/news/technology-60215258)

Peter Thiel, el antiguo fundador de Paypal, [abandona Facebook para apoyar a los candidatos del partido republicano defensores de Trump en las elecciones legislativas.](https://elpais.com/economia/2022-02-08/peter-thiel-abandona-facebook-para-apoyar-al-trumpismo-en-las-elecciones-legislativas.html)

[Twitter lanza el botón ‘No me gusta’ en todo el mundo.](https://hipertextual.com/2022/02/twitter-expande-alcance-boton-no-me-gusta)

[Las "big tech" de EE UU siguen resilientes pese a la inflación, el aumento de costes y otros desafíos.](https://cincodias.elpais.com/cincodias/2022/02/04/companias/1643974526_808566.html)

[El F.B.I. alerta de posibles ciberataques procedentes de Rusia.](https://www.publico.es/internacional/ciberataques-guerra-rusia.html)

El otro gran salseo de estas semanas ha sido [la amenaza de Facebook e Instagram con irse de la UE si no les dejan transferir datos de sus ciudadanos a USA.](https://www.epe.es/es/tecnologia/20220207/facebook-e-instagram-amenazan-irse-13203323)
La respuesta de los primeros ministros de Alemania y Francia ha sido ["We’re Fine Without Facebook".](https://www.bloomberg.com/news/articles/2022-02-07/we-re-fine-without-facebook-german-and-french-ministers-say)

Por supuesto, la tecnológica se ha echado atrás indicando que [Meta no va a abandonar Europa,](https://about.fb.com/news/2022/02/meta-is-absolutely-not-threatening-to-leave-europe/) al mismo tiempo que admite que [los cambios de privacidad que Apple ha hecho en iOS, le impactarán con una reducción de beneficios de $10 billions.](https://www.cnbc.com/2022/02/02/facebook-says-apple-ios-privacy-change-will-cost-10-billion-this-year.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El Ayuntamiento de Barcelona crea un "software" para las escuelas alternativo a Google y a Microsoft](https://elpais.com/espana/catalunya/2022-02-09/barcelona-crea-un-software-para-las-escuelas-alternativo-a-google-y-a-microsoft.html) basado en software libre y gracias el trabajo realizado por Xnet.

[Indra recupera el monopolio de los recuentos electorales, esta vez en Castilla y León,](https://www.lainformacion.com/empresas/castilla-leon-resucita-monopolio-indra-recuentos-electorales/2858942) un recuento adjundicado sin concurso público y por emergencia por 5 veces lo que costó el anterior en la misma comunidad.

[La CNMC plantea crear un canal de denuncia contra youtubers, tiktokers y streamers](https://www.eldiario.es/tecnologia/cnmc-plantea-crear-canal-denuncia-youtubers-tiktokers-streamers_1_8703460.html) para analizar si deben ser considerados "prestadores de servicios del mercado audiovisual" y cumplir las mismas reglas que las televisiones tradicionales.

[Bruselas presenta un plan de 12.000 M para producir chips en la UE](https://www.elconfidencial.com/economia/2022-02-08/bruselas-plan-producir-chips_3371701/), buscando cuadruplicar la actual producción para 2030.

[El regulador británico advierte a la banca sobre el uso de la IA en las solicitudes de préstamos](https://thenyledger.com/markets/uk-regulators-warn-banks-on-use-of-ai-in-loan-applications/), obligándoles a demostrar que su uso no discriminará a minorias que ya tienen dificultades para conseguir préstamos.

[El Servicio de Impuestos Internos (IRS) de Estados Unidos está buscando alternativas a la verificación facial con ID.me](https://www.cbsnews.com/news/irs-id-me-tax-return-alternatives/) para que los ciudadanos puedan acceder a sus impuestos.

[Estados Unidos prueba perros patrulla robóticos en la frontera mexicana.](https://www.theguardian.com/us-news/2022/feb/04/us-tests-of-robotic-patrol-dogs-on-mexican-border-prompt-outcry)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Mozilla ha decidido dejar de soportar su navegador web para realidad virtual llamado Firefox Reality](https://techcrunch.com/2022/02/03/mozilla-is-shutting-down-its-vr-web-browser-firefox-reality/). Pero parece que la VR continará teniendo su propio navegador ya que en su lugar la gallega [Igalia anuncia el navegador Wolvic](https://www.igalia.com/2022/02/03/Introducing-Wolvic.html) que llega para continuar el camino de su predecesor en el campo de la VR.

[Firefox lanza una feature anti-tracking que junta las ventajas de su VPN con los Multi-Account Containers.](https://www.theverge.com/2022/2/2/22914078/mozilla-vpn-multi-account-containers-add-on)

Qubes OS, el conocido sistema operativo promocionado por expertos de la privacidad y la seguridad como Edward Snowden, [lanza una nueva y esperada versión.](https://www.qubes-os.org/news/2022/02/04/qubes-4-1-0/)

Signal lanza por fin la posibilidad de [cambiar tu número de teléfono sin tener que crear una nueva cuenta](https://signal.org/blog/change-number/) y manteniendo todos los chats y grupos.

Mozilla lanza [una encuesta a los profesionales de la IA](https://foundation.mozilla.org/en/blog/survey-call-to-ai-practitioners-making-ai-transparent-together/) para hacer una inteligencia artificial más transparente.

[Element ha escrito un post](https://element.io/blog/fosdem-2022s-communication-infrastructure-was-provided-by-element-matrix-services/) sobre cómo han gestionado la infraestructura de comunicaciones para FOSDEM 2022, la conferencia más grande de Europa sobre tecnologías libres. [En el blog de Matrix también han publicado un análisis un poco más detallado.](https://matrix.org/blog/2022/02/07/hosting-fosdem-2022-on-matrix)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Don't Break Up Social Media, Bifurcate It](https://www.project-syndicate.org/commentary/social-media-separate-network-infrastructure-from-editorial-role-by-luigi-zingales-2021-08) por Luigi Zingales en Project Syndicate.

[Sony compra Bungie: más allá que una adquisición defensiva](https://www.newtral.es/sony-compra-bungie-playstation-ips/20220205/) por Emilio Doménech en Newtral.

[La Justicia impide la apertura del código fuente de la aplicación que concede el bono social](https://civio.es/novedades/2022/02/10/la-justicia-impide-la-apertura-del-codigo-fuente-de-la-aplicacion-que-concede-el-bono-social/), una lucha de Civio.

[Big Tech Needs to Stop Trying to Make Their Metaverse Happen](https://www.wired.co.uk/article/metaverse-big-tech-land-grab-hype) por Gian M. Volpicelli en Vice.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
