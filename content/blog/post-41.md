---
title: "La ciberguerra de Rusia, la Oficina del Dato y la llegada de la Data Act."
date: 2022-03-01T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-41.jpg"
# meta description
description: "La ciberguerra de Rusia, la Oficina del Dato y la llegada de la Data Act."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

La noticia que llena estas semanas nuestros telediarios es la invasión de Ucrania por parte de Rusia. Es por eso que hoy traemos algunas de las diversas noticias que nos está dando desde un punto de vista tecnológico, [la primera guerra 'híbrida' de la historia](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/europa/20220226/primera-hibrida-historia-ciberataques-activismo-defenderse-virtual/652934946_0.html).

Por supuesto los ciberataques están siendo muy relevantes, de hecho [EE UU ofrece hasta 10 millones de dólares por la identificación de cualquier ciberataque ruso contra infraestructuras críticas.](https://elpais.com/tecnologia/2022-02-25/ee-uu-ofrece-hasta-10-millones-de-dolares-por-la-identificacion-de-cualquier-ciberataque-ruso-contra-infraestructuras-criticas.html)

Incluso el grupo [Anonymous también ha entrado en guerra contra Rusia](https://mobile.twitter.com/YourAnonTeam/status/1497678548764696585) atacando todo tipo de webs e infraestructuras rusas. Mientras tanto [Putin ha decidido bloquear Twitter para censurar las imágenes de su invasión](https://hipertextual.com/2022/02/rusia-bloquea-twitter-conflicto-ucrania) y también ha atacado todo tipo de webs y plataformas ucranianas mientras [activistas y archivistas intentan salvarlas.](https://www.vice.com/en/article/4awbnd/ukrainian-websites-are-going-dark-archivists-are-trying-to-save-them)

En nuestro país, [el gobierno ha pedido a los funcionarios que cambien contraseñas y apaguen equipos no esenciales](https://www.eldiario.es/tecnologia/espana-pide-funcionarios-cambien-contrasenas-apaguen-equipos-no-esenciales-crisis-ucrania_1_8784085.html).

Las sanciones también han llegado al plano tecnológico y [EE.UU. está apuntando contra la tecnología rusa, desde los móviles hasta los aviones](https://www.lavanguardia.com/internacional/20220224/8079629/sanciones-rusia-ucrania-invasion-tecnologia-moviles-aviones.html) y a estas sanciones se ha unido el mayor fabricante de chips del mundo, [TSMC en Taiwan, que dice que cumplirá con el control en sus exportaciones a Rusia.](https://www.reuters.com/technology/taiwans-tsmc-says-comply-with-export-control-rules-russia-2022-02-25/)

También existe la posibilidad de que [las élites rusas traten de usar criptomonedas para evitar las sanciones](https://www.elsaltodiario.com/bitcoin/criptomonedas-podrian-utilizadas-elites-rusas-para-evitar-swift-sanciones) pero aún está por ver hasta que punto podrán esquivarlas.

Pero además de los ciberataques, existe otro [punto débil de Europa ante Rusia, y son los miles de kilómetros de cables submarinos](https://www.eldiario.es/tecnologia/punto-debil-europa-rusia-lejos-ucrania-miles-kilometros-cables-submarinos_1_8766575.html) que discurren por el Atlántico y que Putin ya ha tanteado anteriormente.

En otras noticias, hemos visto como [se han publicado los nombres de 92.000 donantes del "Freedom Convoy" de Canadá.](https://www.vice.com/en/article/k7wpax/freedom-convoy-givesendgo-donors-leaked)

[Arranca 'Truth Social', la red social de Donald Trump tras su veto en Twitter y Facebook.](https://www.epe.es/es/internacional/20220221/arranca-truth-social-red-social-13268581)

[Twitter añade etiquetas para las cuentas de bots que los desarrolladores podrán añadir](https://cincodias.elpais.com/cincodias/2022/02/17/lifestyle/1645094882_141519.html), una acción que se une al botón de "downvote" que ya hemos comentado anteriormente. [Aquí podemos ver cómo influye en nuestro timeline el hecho de utilizar dicho botón.](https://www.washingtonpost.com/technology/2022/02/04/twitter-downvote/)

Una investigación revela que [la provincia argentina de Salta aprobó en 2018 el desarrollo de un algoritmo de Microsoft para determinar qué adolescentes de bajos ingresos podrían quedarse embarazadas.](https://www.businessinsider.es/algoritmo-microsoft-sirvio-predecir-embarazos-adolescentes-1015941) Para ello utilizaron todo tipo de datos como edad, etnia, discapacidad, país de origen y si su casa tenía o no agua caliente...

[Texas demanda a Facebook por comercializar datos biométricos de usuarios sin su conocimiento](https://elpais.com/economia/2022-02-14/texas-demanda-a-facebook-por-comercializar-datos-biometricos-de-usuarios-sin-su-conocimiento.html) la misma semana que la empresa acuerda que [pagará 90 millones a varios usuarios por violar su privacidad](https://www.lainformacion.com/empresas/meta-facebook-pagara-90-millones-varios-usuarios-violar-privacidad/2859973/) al rastrearles incluso cuando se habían desconectado de sus cuentas.

[Un estudio indica que TikTok comparte más datos que cualquier otra aplicación social](https://www.cnbc.com/2022/02/08/tiktok-shares-your-data-more-than-any-other-social-media-app-study.html) y se desconoce a donde va dicha información.

Parece que el phishing también llega al mundo de los NFTs donde [han desaparecido 1,7 millones de dólares en obras digitales.](https://www.eldiario.es/tecnologia/robo-museo-nft-desaparecen-1-7-millones-dolares-obras-digitales-phishing_1_8766150.html)

[Los 'deepfake' son ya tan perfectos que los científicos exigen su prohibición](https://www.elconfidencial.com/tecnologia/novaceno/2022-02-17/cientificos-deepfake-peligrosos-piden-parar-desarrollo_3377002).

[La empresa Clearview AI dice que podrá identificar con reconocimiento facial a casi todos los humanos en el próximo año.](https://www.elconfidencial.com/tecnologia/novaceno/2022-02-20/compania-podra-identificar-habitantes-del-planeta_3378114/)

[Una publicación de informes de la CIA ha revelado que es muy probable que la organización esté recopilando más datos sobre los ciudadanos estadounidenses de lo que se sabía.](https://thehill.com/policy/technology/593948-late-night-reports-suggest-cia-collecting-more-data-on-americans)

En el apartado de ataques, esta semana [British Airways ha sufrido una caída de su sistema a nivel global](https://www.elperiodico.com/es/economia/20220225/british-airways-caida-sistema-global-13293184) mientras que la tecnológica [Nvidia también confirma que está investigando un incidente de ciberseguridad.](https://techcrunch.com/2022/02/25/nvidia-cyber-incident)

[Bosh está trabajando en cámaras de reconocimiento facial que revolucionarán la videovigilancia.](https://theintercept.com/2022/02/11/surveillance-video-ai-bosch-azena/)

[UK planea introducir controles de edad en páginas de pornografía que podría conducir a la introducción de un sistema de identificación digital para acceder a Internet.](https://www.theguardian.com/media/2022/feb/13/plans-for-age-checks-on-porn-sites-a-privacy-minefield-campaigners-warn)

[Vodafone, BBVA, EDP y Mercadona, las empresas con mayores multas por incumplir las normas de protección de datos.](https://civio.es/el-boe-nuestro-de-cada-dia/2022/02/22/vodafone-bbva-edp-y-mercadona-las-empresas-con-mayores-multas-por-incumplir-las-normas-de-proteccion-de-datos/)

[Amazon España compartirá con Hacienda información de los vendedores que operan en su tienda.](https://cincodias.elpais.com/cincodias/2022/02/13/companias/1644750177_564929.html)

[El Supervisor Europeo de Protección de Datos propone el veto al software espía Pegasus en la Unión Europea, pero quizás llega tarde.](https://hipertextual.com/2022/02/la-tardia-recomendacion-del-veto-al-software-espia-pegasus-a-la-union-europea)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

Parece que [la CNMV empieza a vigilar la publicidad de criptomonedas](https://www.elespanol.com/invertia/mercados/criptomonedas/20220217/cnmv-empieza-vigilar-publicidad-criptomonedas-ejercera-control/650685191_0.html) mientras que Ana Botín ha defendido que a la banca tradicional debe permitírsele entrar en el mundo de las criptodivisas. [El Banco de España le ha respondido que competir en criptomonedas podria crear grandes riesgos para el sistema financiero.](https://www.eldiario.es/economia/banco-espana-responde-botin-competir-criptomonedas-expone-riesgos-reputacionales-patrimoniales_1_8765897.html)

La Secretaría de Estado de Digitalización e Inteligencia Artificial [lanza la Oficina del Dato para impulsar la llamada "Economía del Dato".](https://datos.gob.es/es/noticia/la-oficina-del-dato-el-reto-de-impulsar-la-economia-del-dato)

[El gobierno ya ha elegido quién va a dirigir la AEPD y España se arriesga a una sanción de la UE por su politización.](https://www.elconfidencial.com/juridico/2022-02-21/cuenta-atras-aepd-espana-riesgo-sancion-europa-politizacion_3378229/) Una noticia que llega cuando la agencia [ha rechazado elaborar un informe sobre si las cookies de Google Analytics suponen transferencias ilegales de datos a EEUU](https://www.businessinsider.es/aepd-todavia-no-pronuncia-transferencias-eeuu-1014821), porque según indica, ya está tramitando varias denuncias al respecto.

[6.000 millones en satélites, el plan de la UE para impulsar la conexión a Internet](https://www.elperiodico.com/es/economia/20220216/6-000-millones-satelites-plan-13246495) y competir con Starlink (Elon Musk) o Kuiper (Amazon).

Parece que ya tenemos [Ley de Datos europea que busca acabar con el control de la información en muy pocas manos.](https://www.xataka.com/legislacion-y-derechos/tenemos-ley-datos-europea-su-objetivo-acabar-exclusividad-informacion) Damian Boeselager, eurodiputado de Volt, [ha publicado algunos trozos y sus primeras impresiones sobre la misma.](https://mobile.twitter.com/d_boeselager/status/1489575577912754183)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

En momentos de conflicto, TOR y las VPNs se vuelven especialmente relevantes por lo que [la comunidad de TOR ha publicado unas guías de cómo utilizar su plataforma en Rusia y Ucrania](https://twitter.com/torproject/status/1497332772200804352) para saltarse los bloqueos de webs en dichos paises.

IPFS, el protocolo que permite almacenar datos en un sistema de ficheros distribuido, [tiene publicado un interesante directorio con los proyectos realizados sobre dicha tecnología.](https://ecosystem.ipfs.io/)

["El impacto de las pantallas en la vida familiar"](https://empantallados.com/estudio-4edicion/), un estudio de Empantallados y GAD3 tras el confinamiento y cómo las pantallas han influido en las familias y los adolescentes.

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGIatlantic.eu abre su quinto open call](https://ngiatlantic.eu/ngiatlanticeu-5th-open-call) en busca de proyectos financiables con hasta 50k € y enfocados en dos puntos. La interconexión de la UE y los EEUU, que permita mantener la privacidad y las garantías sobre la información, y por otro lado proyectos que busquen aumentar la confianza y la resistencia de Internet. El plazo de presentación termina el 31 de Marzo de 2022.

[NGI Assure](https://www.assure.ngi.eu/open-calls/) abre también su octava open call para financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Abril de 2022.

Se encuentra también abierto el programa de NGI, [TRUBLO](https://www.trublo.eu/apply), y que busca financiar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 30 de Marzo de 2022.

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares. Un fondo que en estas semanas de guerra en Ucrania se vuelve realmente relevante.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[«60 segundos después de entrar me acosaron»: la misoginia del mundo real se replica en el metaverso](https://smoda.elpais.com/feminismo/metaverso-acoso-violencia-sexual-mujeres/) por Carmen López en El País.

[La amenaza de una nueva forma de dominación para África](https://elpais.com/planeta-futuro/2022-02-21/la-amenaza-de-una-nueva-forma-de-dominacion-para-africa.html) por Carlos Bajo en El País.

[Bosco, Civio, el bono social, y la importancia de saber cómo funcionan los algoritmos que deciden sobre nuestras vidas](https://www.microsiervos.com/archivo/ordenadores/bosco-civio-el-bono-social-y-la-importancia-de-saber-como-funcionan-los-algoritmos-que-deciden-sobre-nuestras-vidas.html) un artículo de Wicho en Microsiervos.

[Digital Brief: Data Act official, the (real) cost of semiconductors](https://www.euractiv.com/section/digital/news/digital-brief-data-act-official-the-real-cost-of-semiconductors/) por Luca Bertuzzi y Molly Killeen en Euractiv.

[Meet The Secretive Surveillance Wizards Helping The FBI And ICE Wiretap Facebook And Google Users](https://www.forbes.com/sites/thomasbrewster/2022/02/23/meet-the-secretive-surveillance-wizards-helping-the-fbi-and-ice-wiretap-facebook-and-google-users/) por Thomas Brewster en Forbes.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
