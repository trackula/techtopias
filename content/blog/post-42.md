---
title: "El hackeo de los contadores de la luz, los ciberataques en auge y el plan de Rusia con internet."
date: 2022-03-15T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-42.jpg"
# meta description
description: "El hackeo de los contadores de la luz, los ciberataques en auge y el plan de Rusia con internet."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

La guerra en Ucrania sigue siendo el tema estrella de estas semanas y también en el mundo de la soberanía tecnológica ya que [la UE ha prohibido la difusión de los medios estatales rusos RT y Sputnik a los operadores de comunicaciones](https://www.dw.com/es/la-ue-proh%C3%ADbe-difusi%C3%B3n-de-los-medios-estatales-rusos-rt-y-sputnik/a-60976107). Una prohibición que se ha extendido a otras plataformas como [YouTube, TikTok y Facebook que también han bloqueado dichos canales rusos](https://www.eldiario.es/tecnologia/youtube-tiktok-facebook-bloquean-canales-rusos-rt-sputnik-peticion-bruselas_1_8791581.html) mientras que la propia [Facebook ha ido más allá y permite difundir contenidos incitando a la violencia contra los invasores rusos.](https://www.reuters.com/world/europe/exclusive-facebook-instagram-temporarily-allow-calls-violence-against-russians-2022-03-10/)

También es interesante ver el perfil que ha adoptado [Telegram y su fundador Pavel Durov, el multimillonario que huyó de Rusia y que ahora tiene a su 'app' en el ojo del huracán.](https://www.businessinsider.es/telegram-ojo-huracan-debido-desinformacion-guerra-1026351)

Mientras tanto sobre el terreno [Elon Musk ha cumplido su promesa enviando el equipamiento necesario para que Starlink funcione en Ucrania.](https://www.lavanguardia.com/tecnologia/20220301/8090707/elon-musk-activa-internet-ucrania-starlink-pmv.html)

De satélites también va esta noticia en la que [analistas de la NSA prueban el saboteo del internet por satélite ucraniano durante la invasión](https://www.reuters.com/world/europe/exclusive-us-spy-agency-probes-sabotage-satellite-internet-during-russian-2022-03-11/) mientras el gobierno de Ucrania [pide voluntarios a la comunidad hacker del país para defender infraestructuras críticas.](https://www.reuters.com/world/exclusive-ukraine-calls-hacker-underground-defend-against-russia-2022-02-24/)

Todo esto sucede mientras en Rusia [la policia revisa teléfonos móviles en busca de contenido contra el régimen](https://twitter.com/KevinRothrock/status/1500458582902460420) y se prohibe difundir información en contra de la guerra. De hecho [Rusia ha bloqueado Twitter](https://www.theverge.com/2022/2/26/22952006/russia-block-twitter-ukraine-invasion-censorship-putin) y la propia red social ha lanzado [su propio Onion Service de Tor](https://www.vice.com/en/article/v7dqxd/twitter-tor-onion-service-dark-web-version) para facilitar su acceso en el país.

[Samsung ha sido víctima de un gran ciberataque en el que se que han robado datos confidenciales de la empresa.](https://www.epe.es/es/economia/20220307/samsung-victima-gran-ciberataque-robo-datos-13335538)

Semanas atrás hablamos del ciberataque a Nvidia, [ahora tenemos nuevo capítulo en donde los atacantes amenazan con publicar más de 1TB de datos de la compañía.](https://arstechnica.com/information-technology/2022/03/cybercriminals-who-breached-nvidia-issue-one-of-the-most-unusual-demands-ever/)

[Tinder ofrece la comprobación de antecedentes penales, lo que genera grandes dudas desde el punto de vista de la privacidad.](https://www.theguardian.com/technology/2022/mar/11/tinder-criminal-background-checks-problems)

[Proveedores españoles de ‘cloud’ crean la asociación Apecdata para hacer frente a los gigantes de EE UU.](https://cincodias.elpais.com/cincodias/2022/02/17/companias/1645123217_826607.html)

Y en plena subida de la luz llega la noticia de que [una empresa española libera un dispositivo para demostrar cómo se pueden 'hackear' los contadores y dejar sin luz a barrios enteros.](https://www.businessinsider.es/investigadores-espanoles-liberan-herramienta-capaz-dejar-luz-barrios-enteros-1026301). La empresa Tarlogic, se ofreció a Iberdrola y Naturgy para arreglar estos agujeros pero ellas defendieron que sus redes de contadores son seguras e ignoraron sus avisos durante años.

[El 80% de los ecommerce españoles incumple la normativa vigente de comercio electrónico](https://www.lawandtrends.com/noticias/tic/el-80-de-los-ecommerce-espanoles-incumple-la-normativa-vigente-de-comercio-electronico-1.html)

[Si has escuchado rumores sobre que Signal ha sido hackeado, parece que no son ciertos, la empresa lo desmiente.](https://twitter.com/signalapp/status/1498437474611343367)

[Take-Two, creador de NBA 2K, se enfrenta a una demanda colectiva por la venta de 'Loot Boxes' a menores en sus juegos.](https://www.bloombergquint.com/onweb/take-two-faces-lawsuit-over-controversial-loot-boxes-in-nba-2k)

[Orange y Másmóvil pactan una fusión en España de 19.600 millones de euros](https://www.lainformacion.com/empresas/fusion-orange-masmovil-espana/2861694/) mientras Telefónica vuelve a sacar un tema ya recurrente, [su deseo de que las tecnológicas paguen por usar sus redes.](https://www.elconfidencial.com/tecnologia/2022-02-28/telefonica-discurso-inaugural-pallete_3383176/)

[Protección de Datos multa a CaixaBank con 2 millones por cobrar a cambio de no recoger datos personales.](https://www.eldiario.es/tecnologia/proteccion-datos-multa-caixabank-2-millones-cobrar-no-recoger-datos-personales_1_8803209.html)

[Amazon deberá pagar multa de dos millones por exigir certificado de ausencia de penales a sus repartidores,](https://www.elespanol.com/invertia/empresas/20220301/amazon-debera-millones-certificado-ausencia-penales-repartidores/653934810_0.html) una práctica que también siguen otras empresas como Uber Eats.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Escrivá espera un alud de ingresos por los 'riders' tras recaudar 4,6 millones por las cotizaciones que las empresas dejaron de pagar al haber tenido a sus empleados como falsos autónomos.](https://www.lainformacion.com/economia-negocios-y-finanzas/escriva-ingresos-riders-falsos-autonomos-plataformas/2861456/)

[Hacienda prepara su 'tasa Amazon' para gravar el reparto a domicilio por la ocupación del espacio público en las entregas a domicilio de paquetería.](https://www.elmundo.es/economia/macroeconomia/2022/03/05/622219e8e4d4d864748b45a9.html)

[Meta quiere instalar un centro de datos en Talavera de la Reina con una inversión de 1.000 millones de euros](https://www.eldiario.es/castilla-la-mancha/meta-quiere-instalar-centro-datos-talavera-reina-inversion-1-000-millones-euros_1_8818955.html) que ha sido calificada por el presidente de Castilla-La Mancha como "Una de las noticias más importantes que habrá en el año”.

[La SEPI se prepara la tomar el control de Indra de la mano de Marc Murtra.](https://www.lainformacion.com/empresas/murtra-prepara-toma-poder-blindar-sepi-control-indra/2860522/)

[Bélgica aprueba la semana laboral de 4 días pero sin rebajar las 38 horas de trabajo semanal que actualmente se encuentran vigentes.](https://www.euronews.com/next/2022/02/15/belgium-approves-four-day-week-and-gives-employees-the-right-to-ignore-their-bosses)

[Parece que Rusia está preparando su desconexion de internet](https://twitter.com/nexta_tv/status/1500553480548892679), un cambio para el que el Kremlin lleva meses preparándose con la idea de crear [RuNet, el internet de Putin que permite a Rusia estar conectada aunque todo el mundo le bloquee.](https://www.elespanol.com/omicrono/tecnologia/20220308/runet-internet-putin-permite-rusia-conectada-bloquee/655434471_0.html)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

La gente de Interferencias organiza en Granada sus ya míticas [Jornadas de Anonimato, Seguridad y Privacidad, las JASYP 22](https://jasyp.interferencias.tech/).

[Vuelve la ‘Guerra de los Navegadores’: Chrome pierde por primera vez terreno gracias a Safari, Brave o Edge.](https://hipertextual.com/2022/03/guerra-navegadores-nuevos)

"Pensar el Diseño y la Tecnología" organiza un nuevo curso de ["Pensar con las máquinas. Introducción a la ética de la tecnología"](https://pensardyt.com/maquinas).

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGIatlantic.eu mantiene abierto su quinto open call](https://ngiatlantic.eu/ngiatlanticeu-5th-open-call) en busca de proyectos financiables con hasta 50k € y enfocados en dos puntos. La interconexión de la UE y los EEUU, que permita mantener la privacidad y las garantías sobre la información, y por otro lado proyectos que busquen aumentar la confianza y la resistencia de Internet. El plazo de presentación termina el 31 de Marzo de 2022.

[NGI Assure](https://www.assure.ngi.eu/open-calls/) también mantiene su octava open call para financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Abril de 2022.

Se encuentra también abierto el programa de NGI, [TRUBLO](https://www.trublo.eu/apply), y que busca financiar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 30 de Marzo de 2022.

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares. Un fondo que en estas semanas de guerra en Ucrania se vuelve realmente relevante.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Bosco, el bono social y por qué auditar los sistemas informáticos que deciden en nuestras vidas 🤖](https://civio.es/novedades/2022/02/24/encuentro-online-transparencia-algoritmica-codigo-fuente-civio-bosco/), un encuentro online de CIVIO.

[Why choice should be the default in the DMA](https://www.euractiv.com/section/digital/opinion/why-choice-should-be-the-default-in-the-dma/) por Andy Yen en Euractiv.

["La privacidad de los datos es una ilusión"](https://cincodias.elpais.com/cincodias/2022/02/25/fortunas/1645819526_363266.html), una entrevista a Andreas Weigend en El País.


---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
