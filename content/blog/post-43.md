---
title: "La llegada de la regulación cripto, Dialer y Messages robando nuestros datos y el nuevo Privacy Shield."
date: 2022-03-29T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-43.jpg"
# meta description
description: "La llegada de la regulación cripto, Dialer y Messages robando nuestros datos y el nuevo Privacy Shield."

# post type
type: "featured"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Las apps Messages y Dialer envia de forma silenciosa información a Google sobre las llamadas y mensajes enviados desde el teléfono.](https://www.theregister.com/2022/03/21/google_messages_gdpr/).

["50€ en marihuana": tus secretos dejan de serlo cuando se los cuentas a las apps de control de gastos.](https://www.eldiario.es/tecnologia/50-marihuana-secretos-dejan-serlo-cuentas-apps-control-gastos_1_8848399.html)

[Microsoft prueba a introducir anuncios en el explorador de archivos de Windows 11.](https://www.europapress.es/portaltic/software/noticia-microsoft-prueba-introducir-anuncios-explorador-archivos-windows-11-20220315115252.html)

[Twitch planea abrir la mano para añadir contenido de temática "adulta" y está preguntando a los streamers si lo ven bien.](https://www.hobbyconsolas.com/noticias/twitch-tematica-adulta-plataforma-ya-pregunta-ven-bien-1032617)

[Un ciberataque contra Israel deja bloquedas las webs gubernamentales durante horas.](https://www.thenationalnews.com/mena/2022/03/14/several-israeli-government-websites-down-reports/)

[Crean un sistema de neuroimplantes que permite comunicarse a un paciente con ELA.](https://www.elconfidencial.com/tecnologia/ciencia/2022-03-22/crean-dispositivo-permite-hablar-persona-paralisis-ela_3396080/)

[Facebook y YouTube aseguran haber borrado un deepfake de Zelensky](https://edition.cnn.com/2022/03/16/tech/deepfake-zelensky-facebook-meta/index.html) en el que el presidente pedía a sus ciudadanos que entregasen las armas a Rusia.

[Rusia se está quedando sin espacio de almacenamiento tras el éxodo de los principales proveedores de cloud](https://www.xataka.com/empresas-y-economia/rusia-tiene-enorme-problema-tecnologico-se-esta-quedando-espacio-huida-centros-datos), y hablando de centros de datos, [Ucrania se prepara para qué hacer con sus datos si Kiev cae en manos rusas.](https://www.elconfidencial.com/tecnologia/2022-03-17/ucrania-servidores-datos-rusia-secretos_3392852/)

En las últimas semanas se han sumado a la guerra [varios drones con inteligencia artificial que deciden por si mismos cuál es el objetivo.](https://www.businessinsider.es/guerra-ucrania-inteligencia-artificial-llega-drones-rusos-1030387)

[Ucrania ha decidido utilizar Clearview AI como sistema de reconocimiento facial para identificar a espías y a los muertos.](https://www.cope.es/actualidad/internacional/noticias/clearview-polemica-estrategia-reconocimiento-facial-que-usa-ucrania-para-identificar-rusos-muertos-20220326_1993018)

[Hablan los soldados del ejército informático de Ucrania, la llamada IT Army, que organiza ciberataques a Rusia de forma pública.](https://www.elconfidencial.com/tecnologia/2022-03-16/it-army-ucrania-ejercito-informatico-ciberataques-rusia_3391491/)

[Alemania alerta del "riesgo considerable de ataque informático" a través de los programas de Kaspersky](https://elpais.com/tecnologia/2022-03-15/alemania-alerta-del-riesgo-considerable-de-ataque-informatico-a-traves-de-los-programas-de-kaspersky.html) mientras la tecnológica [se defiende en una declaración pública](https://www.kaspersky.es/blog/declaracion-de-kaspersky-sobre-la-advertencia-del-bsi/26979/)

La BBC ha hecho un interesante análisis sobre [cómo el Kremlin utiliza sus cuentas para manipular Twitter.](https://www.bbc.com/news/technology-60790821)

[Facebook lanza en España sus gafas Ray-Ban para ir grabando todo y nos pide que las usemos con moderación por "privacidad",](https://www.genbeta.com/actualidad/facebook-lanza-espana-sus-gafas-ray-ban-para-ir-grabando-todo-nos-pide-que-usemos-moderacion-privacidad) las agencias de protección de datos desconfían.

[La justicia de Brasil ordena la prohibición de Telegram por no haber actuado contra la desinformación de Bolsonaro.](https://www.elperiodico.com/es/tecnologia/20220319/brasil-prohibicion-telegram-desinformacion-bolsonaro-elecciones-13398487)

[El Defensor del Pueblo en España recibe quejas de ciudadanos que han invertido en 'criptos' y lo han perdido todo.](https://www.europapress.es/economia/finanzas-00340/noticia-defensor-pueblo-recibe-quejas-ciudadanos-invertido-criptos-perdido-todo-20220320133350.html)

[La opacidad del Gobierno impide ver si los algoritmos que usa perjudican a los más vulnerables](https://www.publico.es/ciencias/opacidad-gobierno-impide-ver-algoritmos-perjudican-vulnerables.html), ejemplos como el algoritmo para el acceso al bono social eléctrico o el que se encarga de evaluar los riesgos de reincidencia en violencia de género son muy claros. Debemos poder ver su código.

[Jaime Gómez-Obregón ha publicado los datos del "Registro Oficial de Licitadores y Empresas Clasificadas del Estado" y Hacienda ha inhabilitado su buscador de licitadores al enterarse.](https://www.eldiario.es/tecnologia/hacienda-inhabilita-buscador-licitadores-liberar-hacker-base-datos_1_8851519.html)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Entra en vigor una nueva regulación para las criptomonedas en España](https://www.xataka.com/legislacion-y-derechos/entra-vigor-nueva-regulacion-para-criptomonedas-espana-como-afecta-que-obligaciones-se-anaden) al mismo tiempo que en Europa [la Unión Europea trabaja en su regulación a través de la Markets in Crypto Assets.](https://cincodias.elpais.com/cincodias/2022/03/24/legal/1648105613_320699.html)

Esto ocurre mientras [Rusia utiliza criptomonedas para eludir las sanciones, y el BCE prepara un sistema para que los refugiados puedan cambiar sus ahorros a euros.](https://www.businessinsider.es/empresas-particulares-rusia-usan-criptomonedas-eludir-sanciones-1032319)

[La CRUE (Conferencia de Rectores de las Universidades Españolas) firma un acuerdo con Amazon para impulsar la formación en AWS en las universidades españolas](https://www.europapress.es/sociedad/educacion-00468/noticia-crue-aws-impulsaran-formacion-capacidades-digitales-modernizacion-universidades-espanolas-20220322115308.html), una nueva pezuña de una big tech dentro del sistema educativo español.

Esta semana se ha publicado el Anteproyecto de Ley de protección de informantes y alertadores y [XNET también ha publicado su opinión y denuncia gravísimos ataques a la libertad de información y a la gobernanza democrática que deben ser corregidos.](https://xnet-x.net/es/xnet-enmendar-anteproyecto-ley-proteccion-informantes-ministerio-justicia/)

[La AEPD denuncia que su personal "apenas ha sufrido variaciones" a pesar de que reciben casi un 500% más de reclamaciones que hace 14 años.](https://www.businessinsider.es/proteccion-datos-denuncia-plantilla-no-ha-crecido-14-anos-1031663)

[Europa y EEUU han llegado a un acuerdo para transferir datos entre ambos continentes.](https://www.genbeta.com/actualidad/tus-datos-podran-transferirse-a-estados-unidos-libremente-acuerdo-europa-joe-biden-que-queria-mark-zuckerberg) Un acuerdo muy esperado por las empresas tecnológicas y que [tras sus dos primeras versiones, no es seguro que a la tercera vaya la vencida.](https://www.businessinsider.es/tumbaron-acuerdos-previos-transferir-datos-eeuu-1034253)
El abogado y activista Max Schrems ha publicado también su opinión sobre el anuncio en ["Privacy Shield 2.0"?](https://noyb.eu/en/privacy-shield-20-first-reaction-max-schrems)

[Intel apuesta por Europa para realizar una inversión de 80.000 millones de euros para desarrollo y fabricación de semiconductores](https://www.elespanol.com/invertia/disruptores-innovadores/politica-digital/europa/20220315/intel-apuesta-europa-inversion-desarrollo-fabricacion-semiconductores/657434561_0.html), una inversión en la que el claro ganador es [Alemania, a pesar de que la inversión se dividirá entre 6 paises.](https://www.reuters.com/technology/germany-wins-big-intel-spreads-chip-investment-across-six-eu-countries-2022-03-15/)

La DMA también tiene su hueco en esta newsletter ya que el pasado jueves el Parlamento y el Consejo Europeo [han llegado a un acuerdo sobre las nuevas reglas para limitar el poder de las grandes plataformas online.](https://www.europarl.europa.eu/news/en/press-room/20220315IPR25504/deal-on-digital-markets-act-ensuring-fair-competition-and-more-choice-for-users)

La idea es que [un mensaje desde Telegram se pueda recibir en WhatsApp](https://www.genbeta.com/actualidad/enviar-mensaje-telegram-que-otra-persona-reciba-whatsapp-asi-quiere-ue-quitar-poder-a-apple-facebook) aunque empresas como Apple [creen que la regulación "limitaría las protecciones de privacidad y seguridad" que esperan sus usuarios.](https://www.faq-mac.com/2022/03/el-inminente-proyecto-de-ley-de-descargas-alternativas-de-la-ue-limitaria-las-protecciones-de-privacidad-y-seguridad-que-esperan-los-usuarios-de-iphone-segun-apple/)

[Privacy international también ha publicado su opinión sobre el texto de la DMA. ](https://mobile.twitter.com/privacyint/status/1507348888465907717)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/>, campañas y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

El Centro de Información sobre Empresas y Derechos Humanos ha publicado [una guía para inversores en el campo de la vigilancia](https://www.business-humanrights.org/es/de-nosotros/informes/navegando-por-el-ecosistema-de-las-tecnolog%C3%ADas-de-vigilancia-una-gu%C3%ADa-de-debida-diligencia-en-derechos-humanos-para-inversionistas/) y sobre los problemas que pueden generar estas tecnologías sobre los derechos humanos.

Mozilla continua buscando cómo monetizar, esta vez [creando una suscripción de pago](https://www.theregister.com/2022/03/24/mozillas_mdn_subscription/) para utilizar su plataforma de documentación web, MDN, con una serie de funcionalidades extra. [Aquí se puede obtener la suscripción](https://developer.mozilla.org/plus)

Matrix también ha publicado [su opinión sobre la DMA con la idea de que exista interoperabilidad pero sin sacrificar la privacidad.](https://matrix.org/blog/2022/03/25/interoperability-without-sacrificing-privacy-matrix-and-the-dma)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[NGIatlantic.eu mantiene abierto su quinto open call](https://ngiatlantic.eu/ngiatlanticeu-5th-open-call) en busca de proyectos financiables con hasta 50k € y enfocados en dos puntos. La interconexión de la UE y los EEUU, que permita mantener la privacidad y las garantías sobre la información, y por otro lado proyectos que busquen aumentar la confianza y la resistencia de Internet. El plazo de presentación termina el 31 de Marzo de 2022.

[NGI Assure](https://www.assure.ngi.eu/open-calls/) también mantiene su octava open call para financiar proyectos alrededor de tecnologías distribuidas, blockchain y tecnologías relacionadas con hasta 200.000€ a fondo perdido. La convocatoria termina el 1 de Abril de 2022.

Se encuentra también abierto el programa de NGI, [TRUBLO](https://www.trublo.eu/apply), y que busca financiar con hasta 175.000 € a proyectos enfocados en crear modelos de confianza y reputación en blockchains, así como en pruebas de validez y ubicación. El plazo de presentación termina el 30 de Marzo de 2022.

El Open Technology Fund mantiene varias convocatorias y entre las que se encuentra el [Technology at Scale Fund](https://www.opentech.fund/funds/technology-scale/) para tecnologías que permitan evitar la censura de noticias objetivas y permitan a los periodistas comunicarse con sus fuentes de forma segura.

El [Internet Freedom Fund](https://www.opentech.fund/funds/internet-freedom-fund/) busca apoyar proyectos que defiendan los derechos humanos, la libertad en internet y las sociedades abiertas con entre 10.000 y 900.000 dólares. La convocatoria se encuentra abierta de forma continua pero las candidaturas son revisadas el día 1 de los meses de Enero, Marzo, Mayo, Julio, Septiembre y Noviembre.

Por último el OTF matiene el [Rapid Response Fund](https://www.opentech.fund/funds/rapid-response-fund/) para facilitar la respuesta rápida a emergencias digitales en lugares en donde la libertad de expresión ha sido fuertemente reprimida con hasta 5.000 dólares. Un fondo que en estas semanas de guerra en Ucrania se vuelve realmente relevante.

Una convocatoria similar a la que ofrece [Derechos Digitales](https://www.derechosdigitales.org/sobre-el-fondo-de-respuesta-rapida/) cuya convocatoria de financiación de respuesta rápida para activistas digitales en América Latina con hasta 8.000 dólares por propuesta, también sigue activa.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas, y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

[Todo será contenido. Producción compulsiva y consumo acelerado](https://retinatendencias.com/opinion/todo-sera-contenido-produccion-compulsiva-y-consumo-acelerado/) por Antonio Ortiz en Retina.

[Los límites del uso de la tecnología: reconocimiento facial en tiempos de guerra](https://www.epe.es/es/opinion/20220318/limites-tecnologia-reconocimiento-facial-tiempos-guerra-13390749) por Lucía Velasco en el Periódico de España.

[«Lo importante no es lo que pasa en el metaverso, sino al otro lado»](https://ethic.es/2022/02/lo-importante-no-es-lo-que-pasa-en-el-metaverso-sino-al-otro-lado/) por Lucía Velasco en Ethic.

[“El metaverso puede convertir internet en un espacio totalitario mayor de lo que ya es hoy”](https://www.elsaltodiario.com/internet/esther-paniagua-metaverso-puede-convertir-internet-espacio-totalitario-mayor-que-hoy), una entrevista a Esther Paniagua en El Salto.

[Abuse on the blockchain](https://www.youtube.com/watch?v=hXBZ-BXfCSY) por Molly White en Stanford University.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias y proponernos nuevos contenidos o lugares donde informarnos.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
