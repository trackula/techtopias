---
title: "La prohibición de TikTok, el fin del Privacy Shield y la ofensiva de la UE"
date: 2020-07-21T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-5.jpg"

# meta description
description: "La prohibición de TikTok, el fin del Privacy Shield y la ofensiva de la UE"

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Telegram y Facebook suspenden su colaboración con las autoridades de Hong Kong por la nueva ley que concede más poder a China.](https://www.genbeta.com/actualidad/telegram-facebook-suspenden-su-colaboracion-autoridades-hong-kong-nueva-ley-que-concede-poder-a-china)

[Estados Unidos está analizando si prohibir TikTok y las redes sociales chinas según afirma Mike Pompeo.](https://www.cnbc.com/2020/07/07/us-looking-at-banning-tiktok-and-chinese-social-media-apps-pompeo.html) Aprovechando este movimiento y que la red social se ha vuelto un tema tan candente, [la campaña de Trump está pagando anuncios en otras redes sociales como Instagram para pedir a sus seguidores que les apoyen en la prohibición de TikTok.](https://www.bloomberg.com/news/articles/2020-07-18/trump-campaign-urges-supporters-to-back-tiktok-ban-in-online-ads)

[El banco estadounidense Wells Fargo obliga a sus empleados a borrarse TikTok.](https://www.theverge.com/2020/7/11/21320935/wells-fargo-bans-tiktok-devices-amazon-pompeo) Y no solo la banca sino también grandes tecnológicas como [Amazon también han enviado mensajes a sus empleados pidiéndoles borrar TikTok de sus móviles. Una noticia que Amazon se ha apresurado a desmentir alegando que envió dicho mensaje por error.](https://elpais.com/tecnologia/2020-07-10/amazon-asegura-que-envio-por-error-el-mensaje-en-el-que-pedia-a-sus-empleados-que-borraran-tiktok-de-sus-moviles.html)

[Facebook se está planteando prohibir los anuncios políticos días antes de las elecciones en Estados Unidos.](https://edition.cnn.com/2020/07/10/tech/facebook-political-ads-ban/index.html)

[Palantir, una de las mayores empresas de espionaje del mundo, inicia los trámites para salir a bolsa en Wall Street.](https://www.lainformacion.com/mercados-y-bolsas/palantir-inicia-los-tramites-para-cotizar-en-wall-street/2809755/)

[Deutsche Bank firma una alianza estratégica con Google Cloud, enfocada en acelerar su transición a la nube.](https://www.abc.es/economia/abci-deutsche-bank-pone-manos-google-202007071611_noticia.html)

[Apple pagará unos 25 dólares a los usuarios de iPhone afectados por la obsolescencia programada en EEUU.](https://www.europapress.es/portaltic/sector/noticia-apple-pagara-25-dolares-usuarios-iphone-afectados-obsolescencia-programada-eeuu-20200713183734.html)

En la última newsletter hablamos del reconocimiento facial de Mercadona, hoy sabemos que [la Agencia Española de Protección de Datos ha abierto una investigación de oficio contra Mercadona por las cámaras de reconocimiento facial que está instalando en sus supermercados.](https://www.businessinsider.es/aepd-investiga-mercadona-camaras-reconocimiento-facial-672287)

[El superespía del Mossad que inventó el sistema de reconocimiento facial de Mercadona: así funciona.](https://www.elespanol.com/reportajes/20200715/superespia-mossad-invento-sistema-reconocimiento-mercadona-funciona/505200693_0.html)

[LinkedIn es demandada por leer el portapapeles sin permiso.](https://www.bloomberg.com/news/articles/2020-07-10/linkedin-sued-for-spying-on-users-with-apps-for-apple-devices)

[El móvil del presidente del Parlament, Roger Torrent, fue objetivo de un programa de espionaje que solo se vende a gobiernos.](https://www.publico.es/politica/movil-del-presidente-del-parlament.html)

[El Comité Nacional Demócrata pide a sus candidatos y a los trabajadores de la campaña que no usen teléfonos ZTE ni Huawei.](https://money.cnn.com/2018/08/03/technology/democratic-national-committee-zte-huawei/index.html)

[India contra China: el campo de batalla se amplía a Internet y la India bloquea 59 aplicaciones chinas.](https://elpais.com/tecnologia/2020-07-10/india-contra-china-el-campo-de-batalla-se-amplia-a-internet.html)

[Una empresa propiedad del asesor de Boris Johnson, Dominic Cummings, pagó £260,000 a "Faculty", la firma de inteligencia artificial que trabajó en la campaña a favor del Brexit.](https://www.theguardian.com/politics/2020/jul/12/revealed-dominic-cummings-firm-paid-vote-leaves-ai-firm-260000) La empresa acumula mas de 30 contratos con el gobierno central adjudicados desde 2016.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[El Tribunal de Jusiticia de la Unión Europea ha invalidado el “Privacy Shield” entre la UE y Estados Unidos.](https://twitter.com/CFBarbudo/status/1283687830879899648) Una noticia de gran calado que refuerza lo que desde hace tiempo se empezaba a intuir, [EEUU tendrá que cambiar su legislación si quiere que sus empresas compitan en Europa](https://www.elespanol.com/invertia/empresas/tecnologia/20200716/justicia-europea-privacy-shield-transferencia-europa-eeuu/505699757_0.html).

[El debate de los 'paraísos fiscales intra-UE' estalla en la cuenta atrás a la gran cumbre,](https://www.elconfidencial.com/economia/2020-07-15/debate-paraisos-fiscales-intra-ue-estalla-cumbre_2681968/) una noticia que afecta especialmente a las grandes tecnológicas estadounidenses entre las que se encuentra Apple, que esta misma semana ha recibido el apoyo de [la jjusticia europea, que anula la 'multa' de 13.000 M que Bruselas le había impuesto por la excesiva ayuda fiscal que le había proporcionado Irlanda.](https://www.elconfidencial.com/empresas/2020-07-15/la-justicia-europea-anula-la-multa-de-13-000m-de-bruselas-a-apple_2682756/)

[Para conocer más sobre el tema, "Descifrando la Guerra" ha publicado un hilo muy interesante sobre esta ofensiva de la UE para acabar con las estrategias que utilizan la grandes multinacionales para reducir su carga fiscal.](https://twitter.com/descifraguerra/status/1283136337735045120)

[La Comisión Europea comienza a investigar el sector del Internet de las Cosas y pone el foco en Alexa, Siri y Google Assistant.](https://www.xataka.com/legislacion-y-derechos/comision-europea-comienza-a-investigar-sector-internet-cosas-pone-foco-alexa-siri-google-assistant)

[EE. UU. intenta prohibir que la policía use reconocimiento facial.](https://www.technologyreview.es/s/12398/ee-uu-intenta-prohibir-que-la-policia-use-reconocimiento-facial) Parece que la presión de grupos activistas y los grandes segos que estos algoritmos tienen, están empezando a dar sus frutos.

[El Reino Unido se une a EE.UU. en su veto a la tecnología 5G de la empresa china Huawei.](https://www.rtve.es/noticias/20200715/5g-guerra-comercial-datos-reino-unido-eeuu-china-huawei/2028300.shtml)

[El rival de Slack, Element (anteriormente Riot), obtiene un gran contrato con el sector público alemán para utilizar su tecnología en la administración y en el sector educativo.](https://sifted.eu/articles/element-germany-deal/)

[En la reforma de los derechos de autor, Alemania quiere evitar el bloqueo excesivo de contenido legítimo y no descarta los filtros de contenido a pesar de haber prometido que no habría dichos filtros.](http://copyrightblog.kluweriplaw.com/2020/07/09/in-copyright-reform-germany-wants-to-avoid-over-blocking-not-rule-out-upload-filters-part-1/)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

El proyecto Dfinity busca [rehacer internet para que la gente vuelva a ser dueña de sus datos.](https://www.technologyreview.es/s/12410/rehacer-internet-para-que-la-gente-vuelva-ser-duena-de-sus-datos)

[PrivacyInternational lanza una campaña para tratar de impedir la compra de Fitbit por parte de Google.](https://privacyinternational.org/campaigns/googlefitbit-merger-not-our-watch)

[DuckDuckGo está comenzando a inundar España con vallas publicitarias.](https://twitter.com/search?q=duckduckgo%20valla&src=typed_query)

[Privacy As A Service: The DuckDuckGo Application.](https://hackernoon.com/privacy-as-a-service-the-duckduckgo-application-up2w3uyt)

[Riot, la aplicación de mensajería instantánea sobre Matrix, es ahora Element.](https://element.io/blog/welcome-to-element/)

[Mozilla lanza un servicio de VPN con Mullvad como partner.](https://blog.mozilla.org/blog/2020/07/15/mozilla-puts-its-trusted-stamp-on-vpn/)

[Mozilla laza una campaña para luchar contra las empresas que obtienen beneficio del odio. #StopHateForProfit](https://foundation.mozilla.org/en/blog/join-mozilla-telling-major-companies-stophateforprofit/)

En internet y en las redes sociales solemos vivir en una burbuja de nuestras opiniones y nuestra forma de pensar. [Esta web nos permite ver las recomendaciones de Youtube tal y cómo las vería una persona con otra forma de pensar](https://www.their.tube/).

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Si tienes un proyecto tecnológico para mejorar la privacidad y la confianza, NGI Zero Privacy & Trust Fund](https://nlnet.nl/PET/) ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto.

[Si tu proyecto trabaja en el ámbito de las búsquedas para balancear el poder entre los proveedores de búsqueda y los usuarios, puedes optar a NGI Zero Discovery Fund](https://nlnet.nl/discovery/) que ofrece ofrece entre 5 y 50k € para desarrollar tu proyecto. La convocatoria termina el 1 de Agosto.

Si quieres más información sobre [las open calls abiertas por la iniciativa NGI (Next Generation Internet), aquí puedes ver en qué estado está cada una.](https://ngi.eu/opencalls/)

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

En IUVIA conocen bien la censura y la opresión en la India, por lo que han publicado un post muy interesante sobre ["La guerra india de Internet"](https://blog.iuvia.io/es/la-guerra-india-de-internet/), también [disponible en inglés](https://blog.iuvia.io/the-indian-internet-wars/).

Lucía Velasco ha publicado un [hilo sobre la historia de TikTok y cómo la tecnología no es neutral y por supuesto, influye en política.](https://twitter.com/jones_lucia/status/1282352318441295872)

En Maldita Tecnología han hecho un análisis sobre el login social y [el precio que pagamos por iniciar sesión con Facebook o Google en las aplicaciones.](https://maldita.es/malditatecnologia/2020/07/08/precio-iniciar-sesion-facebook-google-en-las-aplicaciones/)

[What's wrong with WhatsApp](https://www.theguardian.com/technology/2020/jul/02/whatsapp-groups-conspiracy-theories-disinformation-democracy), analizando los problemas de los grupos cerrados de Whatsapp como las fake news o las conspiraciones que ayudan a impulsar.

[Leer las condiciones de tus ‘apps’ te puede llevar más tiempo que el Quijote.](https://elpais.com/tecnologia/2020-06-23/leer-las-condiciones-de-tus-apps-te-puede-llevar-mas-tiempo-que-el-quijote.html)

[Pandemia y derechos humanos: ¿Cómo hacer una implementación tecnológica responsable?](https://web.karisma.org.co/conversacion-pandemia-y-derechos-humanos-como-hacer-una-implementacion-tecnologica-responsable/) con Zulma Cucunubé y Gemma Galdón.

[¿Soberanía tecnológica? Democracia, datos y gobernanza en la era digital](https://lab.cccb.org/es/soberania-tecnologica-democracia-datos-y-gobernanza-en-la-era-digital/) por Gemma Galdón.

[Cómo surgieron y evolucionaron los principales problemas legales y éticos en Machine Learning](https://www.transparentinternet.com/es/tecnologia-y-sociedad/como-surgieron-y-evolucionaron-los-principales-problemas-legales-y-eticos-en-machine-learning/), por Manuela Battaglini.

---

Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
