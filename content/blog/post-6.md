---
title: "Reconocimiento facial en el Ministerio de Interior, implantes inteligentes y el coto a las Big Tech."
date: 2020-08-04T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-6.png"

# meta description
description: "Reconocimiento facial en el Ministerio de Interior, implantes inteligentes y el coto a las Big Tech."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Irlanda dona el código de su popular app de rastreo de contactos de coronavirus a la Fundación Linux](https://www.genbeta.com/actualidad/irlanda-dona-codigo-su-popular-app-rastreo-contactos-coronavirus-a-fundacion-linux)

[Uber ha ayudado silenciosamente a los Gobiernos con el rastreo de contactos de contagiados del coronavirus durante meses.](https://www.businessinsider.es/uber-ha-rastreado-posibles-casos-coronavirus-clientes-681683)

Llega a España [el primer seguro que dice "personalizar el precio de la póliza según la forma de conducir"](https://www.economiadigital.es/tecnologia-y-tendencias/tecnologia-para-evaluar-la-conduccion-y-ajustar-el-precio-del-seguro_20038116_102.html) y obliga a sus clientes a instalar un aparato que controla y evalúa su conducción en todo momento.

[Elon Musk asegura que su chip de Neuralink te permitirá reproducir música directamente al cerebro](https://www.businessinsider.es/elon-musk-asegura-neuralink-transmitira-musica-directamente-cerebro-681679), unas tecnologías que comienzan a probarse en humanos reales como el caso de [Suecia donde 5.000 personas utilizan ya un implante NFC para abrir puertas o viajar en tren](https://retina.elpais.com/retina/2020/07/22/innovacion/1595432782_713134.html) y que generan muchas dudas por ser tremendamente invasivas.

El fabricante de relojes inteligentes y proveedor de tecnología GPS [Garmin, confirma el ciberataque que ha dejado tirados a sus clientes 4 días.](https://www.elconfidencial.com/tecnologia/2020-07-28/garmin-ciberataque-ransomware_2696847/)

El reconocimiento facial sigue en auge en España, ahora es [Bankia quien ultima un servicio de transferencias y pagos mediante reconocimiento facial](https://www.vozpopuli.com/economia-y-finanzas/bankia-pagos-reconocimiento-facial_0_1376263129.html) mientras que [CaixaBank estrena en A Coruña sus cajeros con reconocimiento facial.](https://www.elcorreogallego.es/galicia/cajeros-de-reconocimiento-facial-pioneros-en-a-coruna-instalados-por-caixabank-HE4053229)

Un grupo de hackers españoles [ha encontrado en un servidor poco seguro, los planos de las casas generados por las aspiradoras 'Conga' de la marca Cecotec.](https://www.businessinsider.es/aviso-grupo-hackers-espanoles-cecotec-aspiradoras-conga-685303)

[Varias Big Tech como Google, Amazon o Qualcomm financian un Think Tank para tratar de impulsar una legislación más suave y flexible para dichas tecnológicas.](https://www.nytimes.com/2020/07/24/technology/global-antitrust-institute-google-amazon-qualcomm.html)

[Piratas informáticos han hackeado durante varios años, diversos portales web para publicar noticias falsas contra la OTAN.](https://arstechnica.com/information-technology/2020/07/hackers-broke-into-real-news-sites-to-plant-fake-stories/)

[Google se alía con Telefónica para abrir un centro de datos en España](https://cincodias.elpais.com/cincodias/2020/06/10/companias/1591817089_211160.html) y motivado por ello, la tecnológica [conectará por primera vez uno de sus cables submarinos en nuestro país.](https://elpais.com/tecnologia/2020-07-28/grace-hopper-el-primer-cable-submarino-que-google-anclara-en-espana.html)

Las grandes tecnológicas están [en su nivel más alto de adquisiciones en un lustro](https://www.elconfidencial.com/mercados/2020-07-28/rodillo-gigantes-adquisiciones-lustro_2697432/) al mismo tiempo que los [presidentes de Apple, Amazon, Google y Facebook comparecen juntos ante el comité antimonopolio del Congreso de EEUU.](https://elpais.com/tecnologia/2020-07-28/el-dia-en-que-las-grandes-tecnologicas-dejaron-de-ser-intocables.html)

[Las apps de rastreo de contactos piden que tengas activado el GPS en el móvil para que funcione,](https://www.businessinsider.es/app-rastreo-contactos-necesita-gps-movil-681439) entre ellas la app española, por lo que varios países europeos quieren saber para qué necesita Google que se active el GPS.

Y mientras tanto España confirma la eficacia de su app [Radar COVID, que dobla su eficacia frente a los rastreadores físicos en su prueba en La Gomera.](https://www.elconfidencial.com/tecnologia/2020-08-03/radar-covid-app-prueba-piloto-gomera_2703732/)


### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Adif instala cámaras en las estaciones de Atocha y Sants para tomar la temperatura a los pasajeros y comprobar que llevan la mascarilla correctamente colocada,](https://www.larazon.es/economia/20200730/tti4e63hsrc2fmo3akesyr4chy.html) por lo que parece que incluirán alguna tecnología de reconocimiento facial para poder medir dichos parámetros.

La pasada semana también se ha publicado en el BOE que el [Ministerio del Interior usará el reconocimiento facial en fiestas populares o "festivales como Viña Rock" para detectar personas con causas pendientes](https://www.eldiario.es/tecnologia/guardia-civil-quiere-reconocimiento-facial-controlar-fiestas-populares-festivales-vina-rock_1_6120151.html), sembrando nuevamente dudas sobre la proporcionalidad en su uso.

[El Ministerio del Interior adjudica “sin publicidad y sin concurrencia” por motivos de seguridad su sistema de espionaje de comunicaciones por 15 millones al año.](https://elpais.com/espana/2020-07-16/interior-gasta-15-millones-al-ano-en-su-sistema-de-espionaje-de-comunicaciones.html)

La secretaria de Estado de Digitalización e Inteligencia Artificial, [Carme Artigas dice que no es posible digitalizar España sin modernizar a la Administración Pública,](https://innovadores.larazon.es/es/carme-artigas-no-podemos-digitalizar-espana-sin-modernizar-aapp/) y expone una serie de propuestas entre las que figura el utilizar proveedores de cloud externos o eliminar trabas e incorporar tecnologías como la automatización robótica de procesos o la inteligencia artificial.

[Francia limita las licencias para equipos 5G de Huawei con el objetivo de prohibirlas de facto para 2028.](https://www.reuters.com/article/us-france-huawei-5g-security-exclusive-idUSKCN24N26R)

[La Comisión Europea, en colaboración con el European Investment Fund (EIF), lanza el primer fondo de inversión de la EU para el desarrollo de Inteligencia Artificial y tecnologías blockchain.](https://ec.europa.eu/digital-single-market/en/blogposts/forging-new-frontiers-finance-digital-innovations)

[La AEPD recoge las directrices europeas: el acceso a una web no debe estar condicionado al consentimiento de las cookies.](https://elderecho.com/aepd-recoge-las-directrices-europeas-acceso-una-web-no-estar-condicionado-al-consentimiento-las-cookies)

[El Centro Común de Investigación de la UE indica en un estudio que la pandemia del coronavirus ha revelado los riesgos de la dependencia tecnológica de la UE.](https://www.eldiario.es/tecnologia/la-pandemia-revela-los-riesgos-de-la-dependencia-tecnologica-de-la-ue_1_6134322.html)

[Noruega elimina los datos de su applicación centralizada de contact tracing por sus dudas sobre privacidad.](https://www.theweek.co.uk/107270/norway-deletes-contract-tracing-data-privacy-concerns)

[La UE sanciona por primera vez a la inteligencia rusa y a empresas chinas por sus ciberataques contra la Unión.](https://www.lavanguardia.com/internacional/20200802/482631191884/ue-union-europea-europa-rusia-china-espionaje-ciberataques-sancion.html)

[Nueva Zelanda es el primer país del mundo que crea un estatuto sobre el uso de algoritmos en instituciones públicas.](https://twitter.com/Jorge_Morell/status/1289524404096045056)


### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Data Detox Kit](https://www.datadetoxkit.org/en/home/) nos invita a desintoxicarnos de nuestra vida llena de datos a través de una serie de dinámicas a realizar diariamente.

ProtonMail: [Cómo Apple utiliza prácticas anti-competitivas a través de su App Store para extorsionar a los developers y apoyar a régimenes autoritarios.](https://protonmail.com/blog/apple-app-store-antitrust/)

[¿Qué ha pasado con Firefox Send, el servicio de Mozilla para compartir archivos cifrados?](https://blog.mozilla.org/community-es/que-paso-con-firefox-send/)

[DuckDuckGo y sus "bangs" suben como la espuma, cada vez más usuarios buscan alternativas al todopoderoso buscador de Google.](https://www.xataka.com/servicios/duckduckgo-sus-bangs-suben-como-espuma-cada-vez-usuarios-buscan-alternativas-al-todopoderoso-buscador-google)

[El colectivo XNET pide al gobierno de Cataluña que reconsidere su menoscabo del plan de digitalización democrática de los centros educativos.](https://xnet-x.net/digitalizacion-democratica-centros-educativos-cataluna/)

[Brave y Guardian lanzan un nuevo servicio de firewall y VPN para iOS.](https://www.techradar.com/news/brave-and-guardian-launch-new-system-wide-firewall-and-vpn-for-ios-and-ipados)

[Diversas entidades exigen explicaciones sobre la implementación de sistemas de reconocimiento facial en América Latina.](https://www.derechosdigitales.org/14207/la-sociedad-exige-explicaciones-sobre-la-implementacion-de-sistemas-de-reconocimiento-facial-en-america-latina/)


### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.


### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

Gemma Galdón, Romina Garrido y María Paz Hermosilla han participado en la charla, ["Incorporando la ética de datos en la trazabilidad del COVID-19"](https://www.youtube.com/watch?v=dZ0knRVpzzo) en el GobLab de la Universidad Adolfo Ibáñez.

["Now is the time to build a better internet, the coronavirus pandemic has shown we have a long way to go"](https://www.independent.co.uk/independentpremium/voices/coronavirus-internet-online-hate-learning-surveillance-a9616451.html), una lectura muy interesante de Mitchell Baker, la CEO de Mozilla.

[Richard Stallman: “Los móviles espían y transmiten nuestras conversaciones, incluso apagados”.](https://retina.elpais.com/retina/2019/02/22/tendencias/1550819915_405396.html)

La pasada semana ha tenido lugar la [RightsCon](https://www.rightscon.org/program/) y por supuesto [algunas de las charlas mas interesantes están disponibles en vídeo.](https://www.youtube.com/playlist?reload=9&list=PLprTandRM961s376IH8TmvbThcxjfPNwL)

[Una ética universal para la inteligencia artificial,](https://innovadores.larazon.es/es/una-etica-universal-para-la-inteligencia-artificial/) por Esther Paniagua

[“El modelo de aprovecharse de los datos para hacerse rico se está agotando”](https://retina.elpais.com/retina/2020/07/01/tendencias/1593628901_482346.html), una entrevista con Inma Martínez.

Empresas, comunidades y estados se lanzan a por la "Soberanía digital", pero ¿tódos queremos decir lo mismo?: ['Digital Sovereignty' huh?](https://www.youtube.com/watch?reload=9&v=ZzCqacxXYCE&list=PL7sG5SCUNyeYx8wnfMOUpsh7rM_g0w_cu&index=11) por Jaya Klara Brekke y Dan Hassan.

### Una cosilla más...

Los amigos de IUVIA [lanzan #NadaQueEsconder, un podcast sobre privacidad y soberanía tecnológica.](https://blog.iuvia.io/es/nada-que-esconder-01-reconocimiento-facial/) El primer primer capítulo trata sobre reconocimiento facial y en él analizan las noticias más interesantes de las últimas semanas, la situación actual del reconomiento facial y también incluyen una sección técnica sobre esta tecnología.

Podcast muy recomendable que podeis escuchar en [iVoox](https://www.ivoox.com/01-reconocimiento-facial-ojo-todo-audios-mp3_rf_54566058_1.html), [Spotify](https://open.spotify.com/episode/1FFBTQ4EoJpfj1oom9ymdx) o utilizando el [feed RSS](https://podcast.blog.iuvia.io/feed.rss.xml).

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
