---
title: "El reconocimiento facial en AENA, la venta de TikTok y la app española del COVID."
date: 2020-08-18T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-7.jpg"

# meta description
description: "El reconocimiento facial en AENA, la venta de TikTok y la app española del COVID."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[AENA instalará puertas con reconocimiento facial para embarcar en todos los aeropuertos españoles con la excusa de detectar a quién no lleva mascarilla y medir su temperatura.](https://www.elconfidencialdigital.com/articulo/vivir/aena-instalara-detectores-mascarillas-todos-aeropuertos/20200812181334154649.html) Un programa piloto que [lleva meses funcionando en el aeropuerto de Barajas](https://cincodias.elpais.com/cincodias/2019/11/21/companias/1574354350_675284.html) y que pretende expandirse por todo el territorio nacional aprovechando la pandemia.

Todo esto pasa al mismo tiempo que [logran hackear un modelo de reconocimiento facial similar al que se usa en los aeropuertos para identificar a personas que tienen prohibido volar.](https://www.businessinsider.es/hackean-modelo-reconocimiento-facial-similar-aeropuertos-463041) La excusa de la seguridad no acaba de ser muy válida.

Hace unos días Naiara Bellio escribía en AlgorithWatch sobre [la estación de autobuses mas grande de España, la Estación del Sur de Madrid, en la que hace ya 4 años que se ha desplegado el reconocimiento facial y prácticamente nadie se ha enterado.](https://algorithmwatch.org/en/story/spain-mendez-alvaro-face-recognition/)

[Una vulnerabilidad en Alexa habría posibilitado que los hackers accedieran a grabaciones de voz y datos personales de los usuarios.](https://www.businessinsider.es/como-evitar-link-pirateando-cuentas-alexa-696775)

[Google alcanza un acuerdo con ADT de 450 millones de dólares para lanzarse de lleno a la seguridad en el hogar inteligente](https://www.businessinsider.es/google-invierte-450-millones-dolares-adt-689437) con dispositivos de protección contra incendios y servicios de monitoreo de alarmas.

[TikTok ha estado guardando saltándose una protección de privacidad en Android para recopilar las direcciones MAC de los dispositivos donde era instalada.](https://www.businessinsider.es/tiktok-rastreo-datos-usuarios-tactica-prohibida-google-695747)

[Investigadores de la Universidad de Chicago han creado una herramienta que retoca ciertos pixeles para evitar el reconocimiento facial](https://www.nytimes.com/2020/08/03/technology/fawkes-tool-protects-photos-from-facial-recognition.html), de forma que a la vista del ojo humano no se distinga la diferencia.

[Un juzgado de California ha ordenado a Uber y Lyft que deben tratar a sus conductores como empleados](https://www.ft.com/content/8adf4de7-90e9-4304-aa6e-71d652946e80), una noticia que rápidamente ha sido contestada por [Uber que amenaza con irse de California si le obligan a contratar a sus conductores.](https://www.elconfidencial.com/empresas/2020-08-12/uber-dice-que-ira-california-obliga-contratar-conductores_2712536/)

[Google Tag Manager publica su primera versión "Server-Side" para avanzar con el tracking más allá del propio navegador web.](https://analiticadigital.es/tag-manager-server-side-privacidad-y-optimizacion-web/) Será dificil impedir el tracking cuando este se realice en el lado del servidor.

[Epic Games demanda a Apple y Google por retirar 'Fortnite' de sus tiendas](https://www.eleconomista.es/tecnologia/noticias/10721399/08/20/Epic-Games-demanda-a-Apple-y-Google-por-retirar-Fortnite-de-sus-tiendas-.html) y abre de nuevo el debate sobre el monopolio y control que tienen Google y Apple sobre el mercado móvil debido a la hegemonía de sus app stores. [Un debate que inició el servicio de Basecamp, HEY, hace apenas un par de meses.](https://www.forbes.com/sites/robpegoraro/2020/06/17/apple-to-basecamps-hey-expect-to-pay-us-if-you-want-to-sell-privacy/#5477bd6c4287)

[Madrid desarrollará un sistema de inteligencia artificial que pueda hablar con el ciudadano sobre el coronavirus a través de 'Alexa'.](https://www.vozpopuli.com/economia-y-finanzas/madrid-robot-alexa_0_1379862250.html)

[Un contratista del gobierno de los EE.UU. añadió software para rastrear teléfonos en más de 500 aplicaciones móviles.](https://www.wsj.com/articles/u-s-government-contractor-embedded-software-in-apps-to-track-phones-11596808801)

[La NSA advierte a sus militares y personal de inteligencia que los datos de localización de los móviles podrían suponer una amenaza para la seguridad nacional.](https://www.wsj.com/articles/nsa-warns-cellphone-location-data-could-pose-national-security-threat-11596563156)

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Radar COVID: Luces y sombras de la app de rastreo de España.](https://www.newtral.es/radar-covid-app-rastreo-espana/20200810/) Un análisis muy interesante y completo.

ElPais también se ha lanzado a responder a las preguntas más frecuentes sobre dicha app, [¿Cómo funciona? ¿Respeta la privacidad? ¿Podré usarla fuera de España?](https://elpais.com/tecnologia/2020-08-04/como-funciona-respeta-la-privacidad-podre-usarla-fuera-de-espana-resolvemos-las-dudas-sobre-la-app-de-rastreo.html)

[Trump veta hacer transacciones con ByteDance (TikTok) y Tencent (WeChat) a partir de septiembre,](https://www.genbeta.com/actualidad/trump-veta-hacer-transacciones-bytedance-tiktok-tencent-wechat-a-partir-septiembre-esto-que-supone) lo que ha sido el comienzo de una lucha comercial que ha llevado a [Microsoft a postularse para comprar TikTok después de una llamada con el presidente Trump.](https://elpais.com/economia/2020-08-03/microsoft-confirma-sus-planes-de-comprar-tiktok-despues-de-una-llamada-con-el-presidente-trump.html)

Según parece, [Trump pone condiciones a dicha compra: debe ser antes del 15 de septiembre y parte de los beneficios deben ir al Tesoro de EE UU,](https://elpais.com/economia/2020-08-04/trump-pone-condiciones-a-la-compra-de-tiktok-antes-del-15-de-septiembre-y-parte-de-los-beneficios-deben-ir-al-tesoro-de-ee-uu.html) mientras la red social [Twitter entra también en la puja por el negocio de TikTok en Estados Unidos.](https://www.businessinsider.es/twitter-entra-puja-negocio-tiktok-estados-unidos-693651)

[La Comisión Europea inicia una investigación contra Google por la compra de Fitbit y su uso de los datos de salud](https://www.xataka.com/legislacion-y-derechos/comision-europea-inicia-investigacion-google-compra-fitbit-su-uso-datos-salud). Se puede encontrar [más información en la nota de prensa oficial de la Comisión Europea](https://ec.europa.eu/commission/presscorner/detail/en/ip_20_1446)

[Europa sanciona a hackers rusos, chinos y norcoreanos por primera vez.](https://www.technologyreview.es/s/12477/europa-sanciona-hackers-rusos-chinos-y-norcoreanos-por-primera-vez)

[China ha comenzado a bloquear todo el tráfico HTTPS cifrado que utilice TLS 1.3 o ESNI](https://www.zdnet.com/article/china-is-now-blocking-all-encrypted-https-traffic-using-tls-1-3-and-esni/).

[El comité ético de la policia británica determina que la IA que estaban utilizando para predecir delitos no aporta nada, falla el 80% de las veces y no garantiza la no-discriminación. Después de 10M £ invertidos, se descarta.](https://www.wired.com/story/a-british-ai-tool-to-predict-violent-crime-is-too-flawed-to-use/)

[El Ministerio del Interior británico deja de utilizar un algoritmo para gestionar las solicitudes de visado tras constatar que tomaba decisiones racistas depués de múltiples denuncias.](https://www.bbc.com/news/technology-53650758)

Más noticias sobre algorítmos que crean más problemas de los que solucionan, esta vez en [Gales donde la policía ha perdido un caso histórico de reconocimiento facial en la que el tribunal dictaminó que violaba la privacidad y no cumplia las leyes de igualdad.](https://www.theguardian.com/technology/2020/aug/11/south-wales-police-lose-landmark-facial-recognition-case)

En Escocia también se han visto obligados a cancelar un algoritmo por sus desastrosos resultados, [se trata de una IA para priorizar la entrada de alumnos en las universidades y que discrimina a aquellos estudiantes que proceden de zonas pobres.](https://www.theguardian.com/uk-news/2020/aug/10/nicola-sturgeon-promises-urgent-review-of-124000-downgraded-exam-results-scotland)

[Estados Unidos lanza el programa «Clean Network» con el objetivo de aislar a las telecos chinas del Internet global.](https://www.nytimes.com/reuters/2020/08/05/business/05reuters-usa-china-apps-pompeo.html)

[Jeff Merkley y Bernie Sanders proponen una nueva legislación que obligue a las empresas a obtener consentimiento por escrito de los usuarios para obtener sus datos biométricos.](https://www.vox.com/recode/2020/8/4/21354053/bernie-sanders-jeff-merkley-national-biometric-information-privacy-act)

[El gobierno de UK ha pagado a la firma de IA, Faculty, que trabajó en la campaña a favor del Brexit, para analizar los tweets de los ciudadanos británicos durante la pandemia.](https://www.theguardian.com/world/2020/aug/10/government-paid-vote-leave-ai-firm-to-analyse-uk-citizens-tweets)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Mozilla ha despedido a 250 personas, el 25% de su plantilla, y planea un nuevo foco en obtener rentabilidad para ser sostenibles.](https://www.theverge.com/2020/8/11/21363424/mozilla-layoffs-quarter-staff-250-people-new-revenue-focus) La propia Mozilla ha publicado una nota de prensa explicándolo: [Changing World, Changing Mozilla](https://blog.mozilla.org/blog/2020/08/11/changing-world-changing-mozilla/)

El proyecto [Firefox Voice permite navegar por la web utilizando la voz.](https://voice.mozilla.org/firefox-voice/) Se basa en el proyecto [Common Voice](https://commonvoice.mozilla.org/en) y en las librerías [Mozilla TTS](https://github.com/mozilla/TTS) y [DeepSpeech](https://github.com/mozilla/STT).

[Cómo Librem 5 resuelve la advertencia de la NSA sobre los datos de localización de los teléfonos móviles.](https://puri.sm/posts/how-librem-5-solves-nsas-warning-about-cellphone-location-data/)

[Have I Been Pwned, la plataforma que te indica si tus passwords han sido hackeadas, se vuelve open source.](https://www.theverge.com/2020/8/7/21359191/password-breach-have-i-been-pwned-open-source-troy-hunt)

[DuckDuckGo cree que la cuota de mercado de Google Search para móviles disminuirá alrededor de un 20% cuando se introduzcan los menús de selección de preferencias de búsqueda](https://spreadprivacy.com/search-preference-menu-research/), en lugar de la configuración actual que lleva a que todos los móviles con Android incluyan a Google como buscador por defecto.

[En Buenos Aires también luchan contra el reconocimiento facial a través del proyecto, "ConMiCaraNo".](https://conmicarano.adc.org.ar/)

[La Free Software Foundation lanza una petición para mantener la libertad en las aulas y la educación.](https://www.fsf.org/blogs/community/sign-this-petition-for-freedom-in-the-classroom)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

Entidades como Mozilla o la Open Society Foundation se unen a la Ford Foundation para [invertir en proyectos de investigación que mejoren las infraestructuras digitales](https://fordfoundation.forms.fm/2020-digital-infrastructure-research-rfp/forms/8103). La convocatoria termina el 5 de Septiembre.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.

### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

["Predecir el futuro: herramientas predictivas y sociedades segmentadas"](https://www.youtube.com/watch?v=b7ytXGfKAUg&feature=youtu.be), una charla sobre las tendencias de la aplicación de la Inteligencia Artificial en la prevención del delito y la administración de la justicia.

[Marta Peirano: “La imagen que se transmite de internet no es real, la han creado las tecnológicas”.](https://www.lavanguardia.com/magazine/personalidades/20200808/27157/marta-peirano-utilizamos-masivamente-tecnologias-fabricamos-comunidades-imaginarias-unidas-gustos-eliminando-asi-relaciones-proximidad-son-mas-importantes.html)

["La era del reinado del jefe digital"](https://retina.elpais.com/retina/2020/07/16/tendencias/1594858796_446067.html), una revisión sobre el futuro del trabajo, la vigilancia y los derechos tecnológicos en el campo laboral.

[TikTok es solo el principio: por qué internet se tambalea por una 'app' de vídeos musicales.](https://www.elconfidencial.com/tecnologia/2020-08-09/tiktok-internet-eeuu-veto-compra-microsoft-trump_2707839/)

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
