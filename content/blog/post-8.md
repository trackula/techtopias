---
title: "Humanos vs algoritmos, Google a los seguros y a enseñar ética en la IA."
date: 2020-09-01T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-8.jpg"

# meta description
description: "Humanos vs algoritmos, Google a los seguros y a enseñar ética en la IA."

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

[Google acaba de realizar un movimiento silencioso llevando a Verily, su división de investigación en las ciencias de la vida, a entrar en el negocio de los seguros](https://www.businessinsider.es/alphabet-adentra-negocio-seguros-traves-verily-705643). Un movimiento controvertido tras la compra de Fitbit el pasado año y que la Unión Europea investiga por atentar contra la competencia.

Google prepara una nueva tecnología llamada [WebBundle, que convierte las webs en cajas negras en las que no se podría bloquear la publicidad ni los trackers, según Brave](https://www.genbeta.com/web/webbundle-tecnologia-google-que-convierte-webs-cajas-negras-que-no-se-podria-bloquear-publicidad-brave) destruyendo por completo la web abierta.

[El gigante tecnológico chino Tencent defiende el uso de los 'deepfakes'](https://www.technologyreview.es/s/12468/el-gigante-tecnologico-chino-tencent-defiende-el-uso-de-los-deepfakes) que podría tener aplicaciones en la industria del cine, el comercio electrónico e incluso la asistencia médica.

[Un datacenter construido por Huawei expone archivos secretos del gobierno de Papúa Nueva Guinea.](https://www.afr.com/companies/telecommunications/huawei-data-centre-built-to-spy-on-png-20200810-p55k7w)

Como comentamos en la última edición de Techtopias, [miles de estudiantes británicos protestan por un algoritmo sesgado que ha reducido sus resultados académicos, principalmente a alumnos de barrios pobres.](https://www.genbeta.com/actualidad/fuck-the-algorithm-miles-estudiantes-britanicos-protestan-sistema-que-ha-reducido-sus-resultados-academicos).

En este [hilo de twitter podemos seguir cómo se han desarrollado las protestas donde los estudiantes portaban pancartas con frases como "Fuck the algorithm" o "Your algorithm doesn't know me".](https://twitter.com/HUCKmagazine/status/1294968757262196736).

[Tras días de protestas en las calles, los estudiantes han conseguido que el gobierno de Boris Johnson eche atrás su algoritmo de selectividad](https://www.elconfidencial.com/mundo/europa/2020-08-18/los-estudiantes-humillan-a-boris-y-derrotan-en-las-calles-a-su-algoritmo-de-selectividad_2717188/) pero parece que el problema no está solucionado ya que [muchos podrían no acceder a la universidad este año por el caos generado que hace que sea muy dificil encontrar universidades con plazas.](https://es.euronews.com/2020/08/25/miles-de-estudiantes-britanicos-no-podran-acceder-a-una-carrera-universitaria-este-ano)

La batalla por la compra de TikTok continua, [esta vez es el gigante Oracle quien entra en la carrera por adquirir las operaciones de la compañía en USA](https://www.ft.com/content/272cfc69-b268-45ac-88d6-d55821f27e78?accessToken=zwAAAXRA7VYQkc8nLPxpsmhFrNOI1tVYIfJ-eA.MEQCIGoxCmytuNhjukyB1tj2cQOc8zuUZ64MKpUz14mMOWwXAiA6m0TqeUtwoK-kE-Xil_fEbX07LjzW1fTuomRcMwkmvg&sharetype=gift) al mismo tiempo que [Walmart también comiezan conversaciones con Microsoft para tratar de asociarse en dicha compra.](https://www.telegraph.co.uk/technology/2020/08/27/walmart-enters-talks-buy-tiktok-microsoft/)

[Facebook elimina cientos de grupos de QAnon, un movimiento conspiracionista que trata de ayudar a Donald Trump a continuar en la Casa Blanca.](https://www.elconfidencial.com/tecnologia/2020-08-20/facebook-elimina-grupos-qanon-conspiracion-trump_2720275/)

[La RAE, Freepik o eDreams, entre las decenas de entidades denunciadas por permitir las transferencias de datos de sus usuarios a EEUU después de que una sentencia del TJUE invalidara el Privacy Shield.](https://www.businessinsider.es/5-empresas-espanolas-denunciadas-enviar-datos-usuarios-eeuu-493151)

[Mozilla firma extender el trato que mantiene con Google por el cual la tecnológica mantiene a su buscador como el por defecto en Firefox.](https://www.forbes.com/sites/barrycollins/2020/08/13/mozilla-extends-critical-firefox-search-deal-with-google/#19d3a84d6ea2) Una noticia que ha generado cierta polémica ya que las pasadas semanas la propia Mozilla redujo un 25% su personal en un movimiento por buscar la sostenibilidad económica.

[La Comisión Federal del Comercio investiga a Twitter por utilizar los números de teléfono que sus usuarios introducían para activar la autenticación en 2 pasos, con fines publicitarios.](https://www.nytimes.com/2020/08/03/technology/ftc-twitter-privacy-violations.html)

[Apple y Google han retirado 'Fortnite' de sus tiendas por utilizar un método de pago propio y así eludir las comisiones que ambos gigantes imponen en sus app stores.](https://www.xataka.com/aplicaciones/apple-retira-fortnite-app-store-usar-su-propio-metodo-pago-epic-games-ha-llevado-caso-a-tribunales) Epic ha llevado el caso a los tribunales mientras que [Microsoft ha salido a apoyarles diciendo que el bloqueo de Apple amenaza a los desarrolladores.](https://cincodias.elpais.com/cincodias/2020/08/24/companias/1598263977_368344.html?ssm=TW_CM_CD)

[Amazon lanza Halo, una pulsera que te escucha y te dice si estás enfadado o estás gordo](https://www.elmundo.es/tecnologia/gadgets/2020/08/27/5f47df76fc6c83fb7f8b45a9.html) y que como comenta Antonio Ortiz, ["tiene dos micrófonos que escuchan nuestra voz e interpretan nuestro estado emocional a lo largo del día. Lo hará de forma intermitente, no todo el rato" "analiza la intensidad, el ritmo y el tempo y clasifica como aburrido, feliz, cariñoso, preocupado"](https://twitter.com/antonello/status/1299003660563820549). Un nuevo paso para tratar de detectar el estado emocional de las personas, y un movimiento similar al que ha realizado ["Elon Musk, mostrando su tecnología Neuralink en cerdos con implantes de monitorización cerebral."](https://techcrunch.com/2020/08/28/elon-musk-demonstrates-neuralinks-tech-live-using-pigs-with-surgically-implanted-brain-monitoring-devices/)

[Google se lanza a ofrecer ayuda a otros con el objetivo de resolver los problemas de ética en la IA.](https://www.wired.com/story/google-help-others-tricky-ethics-ai/) El pasado mes de febrero la propia [Google decidía dejar de marcar como hombres o mujeres, las imágenes de personas para evitar los sesgos](https://www.businessinsider.com/google-cloud-vision-api-wont-tag-images-by-gender-2020-2?IR=T), lo que no habla muy bien de la forma en la que resuelven dichos problemas.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Benidorm compró datos de tarjetas de crédito a Mastercard para analizar a los turistas.](https://www.vozpopuli.com/economia-y-finanzas/benidorm-mastercard-tarjetas-credito_0_1382261834.html) También han hecho seguimiento de los contadores de agua y se ha certificado como "Destino Turístico Inteligente" con el objetivo de crear un completo perfil de los gustos de sus visitantes.

[Reino Unido valora legalizar la conducción sin conductor la próxima primavera](https://www.elconfidencial.com/tecnologia/2020-08-19/reino-unido-legaliza-conduccion-automatica-2021_2719075/) permitiendo viajar hasta los 115 km/h.

[Los coches deberán tener certificado de ciberseguridad para venderse a partir de 2022](https://www.abc.es/motor/reportajes/abci-coches-deberan-tener-certificado-ciberseguridad-para-venderse-partir-2022-202008120103_noticia.html).

[Turquía aprueba una ley que facilitará el control gubernamental de las redes sociales](https://elpais.com/internacional/2020-07-29/turquia-aprueba-una-ley-que-facilitara-el-control-gubernamental-de-las-redes-sociales.html), obligando a las tecnológicas a radicar sus servidores en el país.

[#ClearviewAI bajo investigación de la AGPD de Hamburgo por posible vulneración del RGPD.](https://twitter.com/Jorge_Morell/status/1297569910185439233)

[Habilitan en Santiago de Chile el primer semáforo operado con inteligencia artificial que da luz verde peatonal cuando detecta aglomeraciones.](https://www.latercera.com/nacional/noticia/habilitan-en-santiago-el-primer-semaforo-operado-con-inteligencia-artificial-que-da-luz-verde-peatonal-cuando-detecta-aglomeraciones/PABSZXMIEBARJLC4R5GOQRTPZQ/) Los semáforos con reconocimiento facial ya no solo se pueden encontrar en China.

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Nextcloud anuncia que el cifrado de extremo está listo para producción.](https://www.muylinux.com/2020/08/19/nextcloud-cifrado-de-extremo/)

[Alpha, la rama de investigación de Telefónica centrada en la salud, anuncia que ha pasado su primera auditoría de ética.](https://www.alpha.company/announcing-our-first-external-ethics-audit/)

El grupo Interferencias organiza la [sala de Derechos Digitales, Privacidad en Internet y Seguridad Informática](https://interferencias.tech/eslibre2020/) dentro del congreso esLibre.

[La Free Software Foundation Europe cree que la Unión Europea debería reconsiderar el concepto de "propiedad intelectual".](https://fsfe.org/news/2020/news-20200820-01.en.html)

[Alexagate, el dispositivo que permite bloquear a Alexa utilizando ultrasonidos.](https://alexagate.com/)

[Signal lanza las "Message requests" para permitir a sus usuarios controlar quién les trata de enviar contenido.](https://signal.org/blog/message-requests/)

[La Electronic Frontier Foundation ha realizado un FAQ sobre la "Exposure Notification API" que han lanzado Apple y Google para luchar contra el COVID-19.](https://www.eff.org/deeplinks/2020/04/apple-and-googles-covid-19-exposure-notification-api-questions-and-answers)

[El grupo BIG BROTHER WATCH lanza la campaña "Stop Facial Recognition" con el objetivo de seguir los pasos de otros lugares del mundo y prohibir el reconocimiento facial en UK.](https://bigbrotherwatch.org.uk/campaigns/stop-facial-recognition/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

Entidades como Mozilla o la Open Society Foundation se unen a la Ford Foundation para [invertir en proyectos de investigación que mejoren las infraestructuras digitales](https://fordfoundation.forms.fm/2020-digital-infrastructure-research-rfp/forms/8103). La convocatoria termina el 5 de Septiembre.

[Derechos Digitales](https://derechosdigitales.tumblr.com/post/187151070656/qu%C3%A9-son-los-derechos-digitales-una-gu%C3%ADa-al) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.


### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

elDiario.es publica una interesante entrevista con [Shoshana Suboff: "La forma de socavar el dividendo que genera la vigilancia tecnológica es haciéndola ilegal"](https://www.eldiario.es/tecnologia/shoshana-suboff-forma-socavar-dividendo-genera-vigilancia-tecnologica-haciendola-ilegal_128_6137458.html).

El País lanza una serie de videos cortos y reportajes sobre el reconocimiento facial:

[Capítulo I. Los orígenes. Cómo el reconocimiento facial empezó en una tableta de los 60](https://retina.elpais.com/retina/2020/08/06/tendencias/1596718301_894713.html)

[Capítulo II. La técnica. ¿Cómo pueden identificarme los sistemas de reconocimiento facial?](https://retina.elpais.com/retina/2020/08/11/tendencias/1597131119_814894.html)

[Facebook sabe lo que hacías hace tres años, ¿por qué insiste en recordártelo?](https://elpais.com/tecnologia/2020-08-16/facebook-sabe-lo-que-hacias-hace-tres-anos-por-que-insiste-en-recordartelo.html), en El País.

[El software para monitorizar a estudiantes durante sus exámenes, ayuda a mantener las desigualdades y viola su privacidad.](https://www.technologyreview.com/2020/08/07/1006132/software-algorithms-proctoring-online-tests-ai-ethics/)

Enrique Dans publica ["Gestionando la conversación: la mano dura funciona"](https://www.enriquedans.com/2020/08/gestionando-la-conversacion-la-mano-dura-funciona.html), un pequeño análisis sobre cómo Reddit ha mejorado sus números luchando contra los contenidos de odio.

[Alternative Business Models for the Web: A MozFest Virtual Panel](https://www.pscp.tv/w/1ypKdwXZYepxW?t=1), un debate de Mozilla.

¿Cómo se extiende las teorías conspiranoicas en Facebook? Un análisis muy interesante [‘Corona? 5G? or both?’: the dynamics of COVID-19/5G conspiracy theories on Facebook](https://wt.social/post/th4w9kz5326641661794) y que también tiene [su correspondiente presentación en video](https://www.youtube.com/watch?v=Eu-BMi4TiQs&feature=youtu.be&t=1575).

Carlos Barbudo, profesor de Filosofía Política y del Derecho especializado en privacidad y economía de los datos, [ha hablado en Radio 3 sobre cómo la privacidad está dejando de ser un derecho para convertirse cada día más en un bien de lujo.](https://www.rtve.es/alacarta/audios/y-ahora-que/ahora-privacidad-colectiva-biotopia-pablo-wilson-19-08-20/5648597/). Minutos 11:30 - 33:40.


---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
