---
title: "Radar COVID en Github, Portland prohibe el reconocimiento facial y España trabaja en hacerlo masivo."
date: 2020-09-15T00:00:00+02:00
draft: false

# post thumb
image: "images/post/post-9.jpg"

# meta description
description: "Radar COVID en Github, Portland prohibe el reconocimiento facial y España trabaja en hacerlo masivo"

# post type
type: "post"
---

### Noticias distópicas <img src="/images/emojis/newspaper.png" width="20px" style="margin: 0;"/>

Seis empresas tecnológicas asesoradas y supervisadas por el Ministerio del Interior [trabajan en un proyecto para proveer de gafas con realidad aumentada que permitan a la policía identificar a personas con reconocimiento facial en multitudes y espacios como conciertos, estadios de fútbol, etc](https://www.lavanguardia.com/vida/20200906/483329209528/camaras-videovigilancia-interior.html).

[La AEPD recuerda que el reconocimiento facial está prohibido en la vigilancia privada.](https://www.eldiario.es/politica/aepd-recuerda-reconocimiento-prohibido-vigilancia_1_6012437.html)

[El reconocimiento facial diseñado para detectar personas con mascarillas falla entre un 5 y un 50% de las veces según un estudio.](https://www.cnet.com/health/facial-recognition-designed-to-detect-around-face-masks-is-failing-study-finds/)

[El CNI investiga el jaqueo de los móviles de varios ministros y altos cargos del Gobierno de España.](https://www.elconfidencial.com/espana/2020-08-30/investigan-el-jaqueo-de-moviles-de-ministros-y-altos-cargos_2729308/)

[Microsoft tiene dos nuevas herramientas para intentar frenar los 'deepfakes', un problema creciente y que preocupa enormemente por  las elecciones de EE.UU.](https://www.genbeta.com/actualidad/microsoft-tiene-dos-nuevas-herramientas-para-intentar-frenar-deepfakes-elecciones-ee-uu)

[Trabajadores de Amazon Logistics cuelgan sus smartphones en árboles cercanos a las centrales para obtener más pedidos a repartir y así tener más trabajo.](https://www.bloomberg.com/news/articles/2020-09-01/amazon-drivers-are-hanging-smartphones-in-trees-to-get-more-work)

Ante noticias como esta, me ha parecido interesante recordar artículos como: ["Cuando tu jefe es un algoritmo".](https://www.informacion.es/economia/2020/09/12/jefe-algoritmo-9582034.html)

[La ingeniera española Carmela Troncoso, que lideró la investigación europea en el desarrollo del protocolo de privacidad que hay tras las apps de rastreo de contactos, aparece en la lista Fortune de personas más relevantes menores de 40 años.](https://www.eldiario.es/tecnologia/ingeniera-espanola-apps-rastreo-contactos-lista-fortune-personas-relevantes-menores-40-anos_1_6195397.html)

[La Comisión Europea no mantuvo registros de la reunión que tuvo lugar en Davos en el mes de Enero entre von der Leyen y el CEO de Palantir.](https://www.euractiv.com/section/data-protection/news/commission-kept-no-records-on-davos-meeting-between-von-der-leyen-and-palantir-ceo/)

[La CEO de Mozilla, Mitchell Baker, insta a la Comisión Europea a aprovechar la oportunidad "única en una generación" para construir un internet mejor.](https://blog.mozilla.org/blog/2020/09/07/mozilla-ceo-mitchell-baker-urges-european-commission-to-seize-once-in-a-generation-opportunity/)

[Facebook restringirá reenvíos en Messenger para evitar bulos en las elecciones de EE UU.](https://elpais.com/tecnologia/2020-09-03/facebook-limitara-los-reenvios-en-messenger.html)

[Hackers rusos atacan el sistema informático de un hospital de Barcelona y piden un rescate.](https://www.larazon.es/cataluna/20200903/l4u3g7ncr5gklnnxfbgm7bpqda.html)

[¿Número desconocido? Google ahora te dirá qué empresa te llama y por qué.](https://www.elmundo.es/tecnologia/trucos/2020/09/09/5f589a0a21efa0b0188b4577.html)

[El ex-jefe de la NSA, Keith Alexander, se une a la junta directiva de Amazon,](https://www.theverge.com/2020/9/9/21429635/amazon-keith-alexander-board-of-directors-nsa-cyber-command) un movimiento que ha suscitado múltiples sospechas y rumores.

Alexander era la cara visible de la NSA cuando Edward Snowden reveló las prácticas de vigilancia masiva que realizaba la agencia por lo que [el propio Snowden no ha tardado en criticar la contratación por parte de Amazon.](https://www.genbeta.com/actualidad/edward-snowden-critica-a-amazon-contratar-al-ex-director-nsa)

[Los tribunales de EE.UU. dictaminan que el sistema de vigilancia masiva utilizado por la NSA durante años y denunciado por Edward Snowden es ilegal y puede ser también inconstitucional.](https://www.reuters.com/article/us-usa-nsa-spying/u-s-court-mass-surveillance-program-exposed-by-snowden-was-illegal-idUSKBN25T3CK)

[Este artículo de The Guardian amplia información e incluye también un enlace a las revelaciones que hizo Snowden hace ya 7 años.](https://www.theguardian.com/us-news/2020/sep/03/edward-snowden-nsa-surveillance-guardian-court-rules)

[Apple y Google liberan a los países de crear una 'app' propia como es por ejemplo Radar Covid, y ofrecerán su propia solución.](https://www.technologyreview.es/s/12595/apple-y-google-liberan-los-paises-de-crear-una-app-como-radar-covid)

[Se filtran datos sensibles de más de 100.000 argentinos con la app del coronavirus](https://www.infotechnology.com/online/Escandalo-internacional-se-filtraron-datos-sensibles-de-100.000-argentinos-con-la-app-del-coronavirus-20200807-0006.html) y que se encontraban expuestos sin contraseña ni otro método de autenticación.

### Políticas "techies"  <img src="/images/emojis/light.png" width="20px" style="margin: 0;"/>

[Portland se une a otras muchos territorios y adopta la prohibición de reconocimiento facial más estricta hasta la fecha.](https://laverdadnoticias.com/mundo/Portland-adopta-la-prohibicion-de-reconocimiento-facial-mas-estricta-hasta-la-fecha-20200910-0080.html)

[El ayuntamiento de Portland ha sacado finalmente esta medida adelante](https://edition.cnn.com/2020/09/09/tech/portland-facial-recognition-ban/index.html) a pesar de los esfuerzos de empresas como [Amazon, que había invertido más de $24,000 en labores de presión para tratar de eliminar o al menos mitigar dicha prohibición.](https://www.vice.com/en_us/article/g5p9z3/amazon-spent-dollar24000-to-kill-portlands-facial-recognition-ban)

[Facebook y Google amenazan con una nueva guerra contra los medios en Australia](https://elpais.com/tecnologia/2020-09-01/facebook-y-google-amenazan-con-una-nueva-guerra-contra-los-medios-en-australia.html). Si las medidas siguen adelante, [Facebook amenaza con bloquear las noticias de medios locales australianos en sus plataformas.](https://www.theguardian.com/media/2020/sep/01/facebook-instagram-threatens-block-australians-sharing-news-landmark-accc-media-law)

[Los gigantes de Internet podrían ser multados con hasta 12 millones de dólares por infringir la ley austriaca sobre discursos de odio.](https://www.euractiv.com/section/digital/news/internet-giants-could-be-fined-up-to-12-million-under-austrian-hate-speech-law/)

[Irlanda envía una orden preliminar a Facebook para que deje de enviar los datos de usuarios europeos a Estados Unidos](https://www.xataka.com/privacidad/irlanda-envia-orden-preliminar-a-facebook-deje-enviar-datos-usuarios-europeos-a-estados-unidos-wsj) tras la anulación del Privacy Shield el pasado mes de Julio.

[Amazon obtiene permiso de la FAA para hacer sus primeras entregas con drones.](https://derechodelared.com/amazon-obtiene-permiso-de-la-faa-para-hacer-entregas-con-drones/)

[El Gobierno condiciona el despliegue en España del 5G chino a que Pekín ofrezca garantías de seguridad y privacidad.](https://www.eldiario.es/tecnologia/gobierno-condiciona-despliegue-espana-5g-chino-pekin-ofrezca-garantias-seguridad-privacidad_1_6198847.html) Pese a las trabas, España no ha bloqueado el empleo de tecnología de Huawei y de hecho algunos de sus productos 5G han recibido la certificación de seguridad del CNI.

[El Gobierno libera el código de Radar COVID para mostrar cómo funciona, como pedían académicos y expertos](https://www.eldiario.es/tecnologia/gobierno-libera-codigo-radar-covid-mostrar-funciona-solicitarlo-academicos-expertos_1_6207478.html). El código se encuentra [en el siguiente repositorio de Github](https://github.com/radarcovid) pero se desconoce si se ha hecho como mera formalidad o si realmente se van a aceptar las contribuciones que la comunidad está enviado y se continuará con un desarrollo más colaborativo ya que según parece, [la versión publicada ya difiere de la versión que se encuentra disponible para Android.](https://www.xatakandroid.com/analisis/radar-covid-analisis-a-fondo-su-codigo-como-funciona-que-esta-bien-que-esta-mal-que-falta)

### Proyectos <img src="/images/emojis/tube.png" width="20px" style="margin: 0;"/> y startups  <img src="/images/emojis/technologist.png" width="20px" style="margin: 0;"/>

[Netflix publica "El dilema de las redes" ("The Social Dilemma" en inglés)](https://www.netflix.com/es/title/81254224), un documental producido por expertos y trabajadores de la industria tecnológica que analiza cómo las redes sociales y la tecnología se diseña para manipular las mentes humanas y puede acabar con nuestra salud, nuestras relaciones sociales e incluso la democracia.

[Eticas lanza el "Civic Tech Hub" en Barcelona,](https://twitter.com/EticasFdn/status/1301889260358901760) un espacio de co-trabajo para iniciativas dedicadas a la tecnología ética.

[Forget Google Maps. It’s time to try a privacy-friendly alternative](https://www.wired.co.uk/article/google-maps-alternatives-privacy).

[Invisible entre la multitud: la chaqueta que te oculta de los algoritmos de vigilancia.](https://www.elconfidencial.com/tecnologia/ciencia/2020-09-12/chaqueta-invisble-algoritmos-video-vigilancia-manifestaciones_2720499/)

[Mozilla lanza una guía rápida para protegerse en la era del activismo.](https://blog.mozilla.org/firefox/activism-digital-security/)

### Funding <img src="/images/emojis/money.png" width="20px" style="margin: 0;"/> y convocatorias <img src="/images/emojis/trophy.png" width="20px" style="margin: 0;"/>

Vuelve nuevamente el programa [NGIatlantic.eu, esta vez con su segundo open call](https://ngiatlantic.eu/ngiatlanticeu-2nd-open-call) en busca de proyectos financiables con hasta 150k € y enfocados en la interconexión de la UE y los EEUU, que permitan mantener la privacidad y las garantías sobre la información. El plazo de presentación termina el 30 de Septiembre.

[Derechos Digitales](https://derechosdigitales.tumblr.com/post/187151070656/qu%C3%A9-son-los-derechos-digitales-una-gu%C3%ADa-al) y [Pulsante](https://pulsante.org/en/rapid-response-fund/) mantienen sendas convocatorias de financiacón de respuesta rápida para situaciones de emergencia en temas de derechos digitales y para aprovechar ventanas de oportunidad que lancen discusiones publicas desde movimientos ciudadanos. Ambas están enfocadas principalmente en América Latina.

[Reset busca proyectos de individuos y organizaciones para resetear internet](https://www.reset.tech/open-calls/) a través de su "Reset Our Future Fund" así como otras pequeñas becas para ayudarles a luchar contra el avance del capitalismo de la vigilancia. Todas las convocatorias terminan el 1 de Noviembre.

[eSSIF-Lab continúa buscando proyectos para financiar el desarrollo, integración y adopción de Self-Sovereign Identities (SSI)](https://essif-lab.eu/) con hasta 155K €. El plazo se encuentra abierto hasta el 4 de Enero de 2021.



### Podcasts <img src="/images/emojis/micro.png" width="20px" style="margin: 0;"/>, entrevistas y lecturas <img src="/images/emojis/antenna.png" width="20px" style="margin: 0;"/>

IUVIA lanza una nueva edición de su podcast [#NadaQueEsconder: Privacidad y virus, destripando Radar COVID](https://blog.iuvia.io/es/nadaqueesconder-02-radarcovid/) en la que analizan la aplicación Radar COVID con una visión técnica pero entendible para todos los públicos.

[David Cabo, fundador de Civio, analiza la publicación de la app Radar COVID en Twitter.](https://twitter.com/dcabo/status/1304418827279773697)

["Europe Feels the Squeeze as Tech Competition Heats Up Between U.S. and China"](https://www.nytimes.com/2020/09/11/world/europe/eu-us-china-technology.html), quizás el papel de Europa como regulador global puede no ser suficiente.

["Lo que ves en tus redes sociales no está ahí por casualidad: por qué es noticia que TikTok cuente cómo ordena los vídeos"](https://maldita.es/malditatecnologia/2020/06/19/redes-sociales-algoritmos-recomendaciones-tiktok-ordena-videos/), un artículo de Maldita sobre el filtro burbuja.

Éticas Foundation publica su segundo análisis ["Quien defiende tus datos"](https://www.youtube.com/watch?v=4Nl-wvKEvUc) sobre las prácticas de privacidad en España. Puedes leer [el estudio completo en este link.](https://eticasfoundation.org/ya-esta-aqui-la-2a-edicion-de-quien-defiende-tus-datos/)

El País continúa con su análisis sobre el reconocimiento facial: [Capítulo III. De la seguridad al control. La invasión silenciosa del reconocimiento facial](https://retina.elpais.com/retina/2020/08/11/tendencias/1597139929_517193.html)

[Reaprendiendo el valor de la privacidad](https://www.eldiario.es/tecnologia/reaprendiendo-privacidad_129_6137661.html) por Carissa Véliz.

[¡Sonría, le están vigilando!](https://ethic.es/2020/09/vigilancia-de-datos-sonria-le-estan-vigilando/) por Florence Rodhain

---


Muchas gracias por seguirnos, esperamos que os haya resultado útil e interesante y por supuesto, nos encantaría tener todo el feedback que nos podáis dar para mejorar Techtopias.

**hola (at) techtopias.com**

<img src="/images/pictures/message.png" width="100px" style="margin: 0;"/>
